export var config = {

  ipAddress: "IPARTNER-MSITE-KRG",

  //prod old  
  // baseURL: 'https://origin21-ipartner.icicilombard.com/mobileagentapi',

  //prod new
  baseURL: 'https://ipartner.icicilombard.com/mobileagentapi',
  homeURL: 'https://ipartner.icicilombard.com/WebPages/SwapSession.aspx?token=',
  loginurl: 'https://ipartner.icicilombard.com/WebPages/Login.aspx',
  hospifundBaseURL:'https://app9.icicilombard.com',
  paymentURL:'https://ipartner.icicilombard.com/mobileagentapi/ipartnermsite/webpage/#/payment',
  razorpayURL: 'https://ipartner.icicilombard.com',
  hospiFundPlanCode: '16079',
  hospiFundProductCode: '6001'

  // uat
  // baseURL: 'http://cldiliptrapp03.cloudapp.net:9006/MobileAPIUAT',
  // homeURL: 'https://cldiliptrapp02.cloudapp.net/WebPages/SwapSession.aspx?token=',
  // loginurl: 'https://cldiliptrapp02.cloudapp.net/WebPages/Login.aspx',
  // hospifundBaseURL: 'https://ilesb.southindia.cloudapp.azure.com',
  // paymentURL:'http://cldiliptrapp03.cloudapp.net:9006/ipartnermsite/webpage/#/payment',
  // hospiFundPlanCode: '62873',
  // hospiFundProductCode: '50028',
  // razorpayURL: 'http://cldiliptrapp03.cloudapp.net:9006',

 // sanity
  // baseURL: 'http://cldiliptrapp03.cloudapp.net:9006/MobileAPI',
  // homeURL: 'https://cldiliptrapp01.cloudapp.net/WebPages/SwapSession.aspx?token=',
  // loginurl: 'https://cldiliptrapp01.cloudapp.net/WebPages/Login.aspx',
  // hospifundBaseURL: 'https://cldilbizapp02.cloudapp.net:9001',
  // paymentURL: 'http://cldiliptrapp03.cloudapp.net:9006/ipartnermsite/webpagesanity/#/payment',
  //razorpayURL: 'http://cldiliptrapp03.cloudapp.net:9006',
  // hospiFundPlanCode: '10528',
  // hospiFundProductCode: '1008'

  // mobiURL: 'http://cldiliptrapp03.cloudapp.net:9006/MobileAPI',
  // mobiURL1: 'http://cldiliptrapp03.cloudapp.net:9006/MobileAPIUAT',  //json
  // healthMasterURL: 'https://ipartner.icicilombard.com/mobile',
  // travelPrem: 'http://cldiliptrapp03.cloudapp.net:9006/mobileservicesPreUAT/Quote.svc',
  // propRelation: 'http://cldiliptrapp03.cloudapp.net:9006/IPartnerUAT',
  // searchCust: 'http://cldiliptrapp03.cloudapp.net:9006/MobileAgentNew',
  // finalUAT: 'http://cldiliptrapp03.cloudapp.net:9006/IPartnerUAT'  //xml

  //aws sanity
  // baseURL: 'https://cldilmobilapp01.insurancearticlez.com/MobileAPI',
  // homeURL: 'https://cldiliptrapp01.cloudapp.net/WebPages/SwapSession.aspx?Healthtoken=',
  // loginurl: 'https://cldiliptrapp01.cloudapp.net/WebPages/Login.aspx',
  // hospifundBaseURL: 'https://cldilbizapp02.cloudapp.net:9001',
  // paymentURL: 'https://cldilmobilapp01.insurancearticlez.com/ipartnermsite/webpagesanity/#/payment',
  // hospiFundPlanCode: '10528',
  // hospiFundProductCode: '1008',
  // razorpayURL: 'https://cldilmobilapp01.insurancearticlez.com',
  
}