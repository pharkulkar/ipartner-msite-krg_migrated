import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';


export declare type Permission = 'denied' | 'granted' | 'default';

@Injectable({
  providedIn: 'root'
})
export class NotificationService implements OnInit{

  permission: Permission;

  constructor() { 
    this.permission = this.isSupported() ? 'default' : 'denied';
    console.log(this.permission);
  }

  ngOnInit(){
    // this.requestPermission();
  }


  isSupported() {
    return 'Notification' in window;
  }

  requestPermission(){
    let self = this;
    if ('Notification' in window) {
      Notification.requestPermission((status) => {
        console.log("status",status);
        return self.permission = status;
      });
    }
  }

  create(title: any, option: any) {
    let self = this;
    return new Observable((obs) => {
      if (!('Notification' in window)) {
        console.log('Notification not available');
        obs.complete();
      }
      if (self.permission !== 'granted') {
        console.log('Your hasnt granted permission to send push notification');
        obs.complete();
      }
      let _notify = new Notification(title, option);

      _notify.onshow = function (e) {
        return obs.next({
          notification: _notify,
          event: e
        });
      }

      _notify.onclick = function (e) {
        return obs.next({
          notification: _notify,
          event: e
        });
      };

      _notify.onerror = function (e) {
        return obs.error({
          notification: _notify,
          event: e
        });
      };

      _notify.onclose = function () {
        return obs.complete();
      };
      //   }
      // }
    })
  }

  generateNotification(source: Array<any>): void {
    let self = this;
    source.forEach((item) => {
      console.log(item);
      let options = {
        body: item.alertContent,
        icon: "../resource/images/bell-icon.png"
      };
      let notify = self.create(item.title, options).subscribe();
    })
  }

}
