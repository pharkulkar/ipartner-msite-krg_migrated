import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable, from, of } from 'rxjs';
import { switchMap, finalize } from 'rxjs/operators';
import { ToastController } from '@ionic/angular';

const STORAGE_REQ_KEY = 'storedreq';

interface StoredRequest {
  url: string,
  type: string,
  data: any,
  time: number,
  id: string
}

@Injectable({
  providedIn: 'root'
})
export class OfflineManagerService {

  storedObj:any;

  constructor(public storage: Storage, public toastController: ToastController) { }

  checkForEvents(): Observable<any> {
    return from(this.storage.get(STORAGE_REQ_KEY)).pipe(
      switchMap(storedOperations => {
        this.storedObj = JSON.parse(storedOperations);
        console.log(this.storedObj);
        return this.storedObj;
        // if (this.storedObj && this.storedObj.length > 0) {
        //   return this.sendRequests(this.storedObj).pipe(
        //     finalize(() => {
        //       let toast = this.toastController.create({
        //         message: `Local data succesfully synced to API!`,
        //         duration: 3000,
        //         position: 'bottom'
        //       });
        //       toast.then(toast => toast.present());
 
        //       this.storage.remove(STORAGE_REQ_KEY);
        //     })
        //   );
        // } else {
        //   console.log('no local events to sync');
        //   return of(false);
        // }
      })
    )
  }

  sendRequests(operations: StoredRequest[]) {
    let obs = [];
    console.log('OPERATIONS', operations);
 
    // for (let op of operations) {
    //   console.log('Make one request: ', op);
    //   let oneObs = this.http.request(op.type, op.url, op.data);
    //   obs.push(oneObs);
    // }
 
    // // Send out all local events and return once they are finished
    // return forkJoin(obs);
  }
}
