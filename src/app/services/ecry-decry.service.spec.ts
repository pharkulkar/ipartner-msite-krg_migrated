import { TestBed } from '@angular/core/testing';

import { EcryDecryService } from './ecry-decry.service';

describe('EcryDecryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EcryDecryService = TestBed.get(EcryDecryService);
    expect(service).toBeTruthy();
  });
});
