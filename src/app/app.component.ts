import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CommonService } from './services/common.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent  implements OnInit{

 
  constructor(
    private platform: Platform, public cs: CommonService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public activatedRoute: ActivatedRoute
  ) {
    this.initializeApp();
    // this.router.events.subscribe((ev) => {
    //   // console.log("Route", ev);
    //   if (ev instanceof NavigationEnd) { 
    //     console.log("Route", ev.url);
    //   }
    // });
  }

  ngOnInit(){
    console.log(this.cs.showLoader);
  }

  getRoute(){
    console.log("FUnction", this.router);
  }

  initializeApp() {
    this.platform.ready().then(() => {
    //   this.statusBar.styleDefault();
    //   this.splashScreen.hide();
    });
  }
}
