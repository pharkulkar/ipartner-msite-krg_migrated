import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatnumbersPipe } from '../pipes/formatnumbers.pipe';

@NgModule({
  declarations: [FormatnumbersPipe],
  imports: [
    CommonModule
  ],
  exports: [
    FormatnumbersPipe
  ]
})
export class ApplicationPipesModule { }
