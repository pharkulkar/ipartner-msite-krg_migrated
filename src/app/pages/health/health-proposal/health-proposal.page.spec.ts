import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthProposalPage } from './health-proposal.page';

describe('HealthProposalPage', () => {
  let component: HealthProposalPage;
  let fixture: ComponentFixture<HealthProposalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthProposalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthProposalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
