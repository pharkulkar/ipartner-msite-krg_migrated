import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalHealthPremiumPage } from './cal-health-premium.page';

describe('CalHealthPremiumPage', () => {
  let component: CalHealthPremiumPage;
  let fixture: ComponentFixture<CalHealthPremiumPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalHealthPremiumPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalHealthPremiumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
