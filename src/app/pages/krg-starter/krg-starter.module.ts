import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { KrgStarterPage } from './krg-starter.page';

const routes: Routes = [
  {
    path: '',
    component: KrgStarterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [KrgStarterPage]
})
export class KrgStarterPageModule {}
