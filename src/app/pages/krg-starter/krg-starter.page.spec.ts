import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KrgStarterPage } from './krg-starter.page';

describe('KrgStarterPage', () => {
  let component: KrgStarterPage;
  let fixture: ComponentFixture<KrgStarterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KrgStarterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KrgStarterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
