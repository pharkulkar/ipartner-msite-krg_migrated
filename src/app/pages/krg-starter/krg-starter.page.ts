import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { config } from 'src/app/config.properties';
import { IfStmt } from '@angular/compiler';

@Component({
  selector: 'app-krg-starter',
  templateUrl: './krg-starter.page.html',
  styleUrls: ['./krg-starter.page.scss'],
})
export class KrgStarterPage implements OnInit {
  disabledFeatureData: any;
  agentAuthToken: any;
  showHome: boolean;
  deal: any; decodeddeal: any
  authToken: any;
  agentData: any;
  plan: any; vendorData: any
  loading: any;
  user_name: any;
  mobile_number: any;
  email_address: any;
  whatsAppData:any;iswhatsAppDetails:boolean=true;
  constructor(
    public cs: CommonService,
    public alertCtrl: AlertController,
    public router: Router,
    public toastController: ToastController,
    public loadingCtrl: LoadingController,
    public activeRoute: ActivatedRoute
  ) {
    this.agentAuthToken = JSON.parse(localStorage.getItem('authToken'));
    this.plan=localStorage.getItem('custplan');
    this.decodeddeal=localStorage.getItem('decodeddeal');
  }

  ngOnInit() {
    this.getDisabledFeaturesList().then(() => {
      this.getCustomerDetails();
    });
  }

  getDisabledFeaturesList(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.getWithParams1('/api/Agent/GetDisableFeatures', this.agentAuthToken).then((res: any) => {
        this.disabledFeatureData = res.DisableResponseList;
        let isKeralacess = this.disabledFeatureData.find((x: any) => x.Key == 'isKeralaCessEnabled').Value;
        localStorage.setItem('isKeralaCessEnabled', isKeralacess);
        resolve();
      }).catch((err) => {
        console.log("Error", err);
        this.messageAlert('Sorry we could not process your request. We will reach out to you soon.');
        this.cs.showSwapLoader = false;
      });
    });
  }

  getCustomerDetails(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.showSwapLoader = true;
      this.cs.swapAppMaster('/api/agent/AgentAppMasterData', this.agentAuthToken).then((res: any) => {
        console.log("Auth", res);
        this.showHome = true;
        localStorage.setItem('userData', JSON.stringify(res));
        this.agentData = res;
        this.cs.showSwapLoader = false;
        resolve();
      }).catch((err) => {
        console.log("Error", err);
        this.messageAlert('Sorry we could not process your request. We will reach out to you soon.');
        this.cs.showSwapLoader = false;
      });
    });
  }
  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'ICICI Lombard',
      message: 'Currently this product is not available, Kindly contact your partner',
      buttons: [{
        text: 'Okay',

      }]
    });
    await alert.present();
  }
  async messageAlert(msg) {
    const alert = await this.alertCtrl.create({
      header: 'ICICI Lombard',
      message: msg,
      buttons: [{
        text: 'Okay',

      }]
    });
    await alert.present();
  }
  validateEmailAddress(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  SubmitUserDetails() {
    if (this.user_name == '' || this.user_name == null || this.user_name == undefined) {
      this.messageAlert('Please Enter Full Name');
    }
    else if ((this.mobile_number == '' || this.mobile_number == null || this.mobile_number == undefined)) {
      this.messageAlert('Please Enter Mobile Number');
    }
    else if (this.mobile_number.toString().length != 10) {
      this.messageAlert('Please Enter Valid Mobile Number');
    }
    else if ((this.email_address == '' || this.email_address == null || this.email_address == undefined)) {
      this.messageAlert('Please Enter Email Address');
    }
    else if (!this.validateEmailAddress(this.email_address)) {
      this.messageAlert('Please Enter Valid Email Address');
    }
    else {
      this.cs.showLoader=true;
      let request_body =
      {
        "CustName": this.user_name,
        "MobileNo": this.mobile_number,
        "Email": this.email_address,
        "Product": "Motor",
        "SubProduct": "",
        "Module": config.ipAddress,
        "Deal": localStorage.getItem('deal'),
      }
      localStorage.setItem('krg_data',JSON.stringify(request_body));
      this.cs.postWithParams('/api/utility/SubmitVedorData', request_body).then((res: any) => {
        this.vendorData = res;
        this.cs.showLoader=false;
        // if (this.vendorData.StatusCode == 0) 
        // {
        //   this.messageAlert('Something went wrong');
        // }
        // else {
        //   if(this.agentData !=undefined && this.agentData !=null && this.agentData !='')
        //   {
        //     this.callWhatsappService();
        //     this.redirectToModule();
        //   }
        //   else{
        //     this.getCustomerDetails().then(()=>{
        //       this.redirectToModule();
        //     });
        //   }
        // }
        this.getCustomerDetails().then(()=>{
          this.redirectToModule();
        });

      });
    }
  }

  numberOnly(event:any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    console.log(charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  alphaOnly(event: any): boolean {
    const key = event.keyCode;
    return ((key >= 65 && key <= 90) || key == 8 || key == 32);
  }


  callWhatsappService() {
    let request_body =
    {
      "MobileNo": this.mobile_number,
      "Mode": "in"
    }
    this.cs.postWithParams('/api/Whatsapp/Whatsapp_OPTINOUT', request_body).then((res: any) => {
      this.whatsAppData = res;
      if (this.whatsAppData.StatusCode == 0) {
        // this.messageAlert('Something went wrong');
      }
      else {
      }
    });
  }
  redirectToModule() {
    if ((this.agentData.MappedProduct.Health.isHealthBoosterMapped || this.agentData.MappedProduct.Health.isHealthCHIMapped || this.agentData.MappedProduct.Health.HealthCHISubProducts.Visible_iHealth || this.agentData.MappedProduct.Health.isHealthPPAPMapped || this.agentData.MappedProduct.Health.isHospifundMapped) && ((this.plan == 'HEALTHCHI') || (this.plan == 'HEALTHCHI-IHEALTH') || (this.plan == 'HEALTHBOOSTER') || (this.plan == 'HEALTHPPAP') || (this.plan == 'HEALTHHOSPIFUND'))) {
      console.log("Health Show");
      if (this.plan == 'HEALTHCHI' && this.agentData.MappedProduct.Health.isHealthCHIMapped && (this.decodeddeal == decodeURIComponent(this.agentData.MappedProduct.Health.HealthCHIDealId))) {
        this.router.navigateByUrl('health');
      }
      else if (this.plan == 'HEALTHCHI-IHEALTH' && this.agentData.MappedProduct.Health.isHealthCHIMapped && this.agentData.MappedProduct.Health.HealthCHISubProducts.Visible_iHealth && (this.decodeddeal == decodeURIComponent(this.agentData.MappedProduct.Health.HealthCHIDealId))) {
        this.router.navigateByUrl('health');
      }
      else if (this.plan == 'HEALTHBOOSTER' && this.agentData.MappedProduct.Health.isHealthBoosterMapped && (this.decodeddeal == decodeURIComponent(this.agentData.MappedProduct.Health.HealthBoosterDealId))) {
        this.router.navigateByUrl('health');
      }
      else if (this.plan == 'HEALTHPPAP' && this.agentData.MappedProduct.Health.isHealthPPAPMapped && (this.decodeddeal == decodeURIComponent(this.agentData.MappedProduct.Health.HealthPPAPDealId))) {
        this.router.navigateByUrl('health');
      }
      else if (this.plan == 'HEALTHHOSPIFUND' && this.agentData.MappedProduct.Health.isHospifundMapped) {
        this.router.navigateByUrl('health');
      }
      else {
        this.presentAlert();
      }
    }
    else if (this.plan == 'TRAVELINT' && this.agentData.MappedProduct.Travel.isTRAVELINTERNATIONALMapped) {
      this.router.navigateByUrl('travel-quote');
    }
    else {
      this.messageAlert('Sorry we could not process your request. We will reach out to you soon.');
    }
  }

  
}




