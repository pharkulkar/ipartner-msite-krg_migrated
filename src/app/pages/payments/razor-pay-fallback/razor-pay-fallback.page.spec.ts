import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RazorPayFallbackPage } from './razor-pay-fallback.page';

describe('RazorPayFallbackPage', () => {
  let component: RazorPayFallbackPage;
  let fixture: ComponentFixture<RazorPayFallbackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RazorPayFallbackPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RazorPayFallbackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
