import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RazorPopUpPage } from './razor-pop-up.page';

const routes: Routes = [
  {
    path: '',
    component: RazorPopUpPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RazorPopUpPage]
})
export class RazorPopUpPageModule {}
