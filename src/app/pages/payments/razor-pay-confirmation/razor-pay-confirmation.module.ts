import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RazorPayConfirmationPage } from './razor-pay-confirmation.page';

const routes: Routes = [
  {
    path: '',
    component: RazorPayConfirmationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RazorPayConfirmationPage]
})
export class RazorPayConfirmationPageModule {}
