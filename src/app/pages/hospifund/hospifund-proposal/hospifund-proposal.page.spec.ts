import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HospifundProposalPage } from './hospifund-proposal.page';

describe('HospifundProposalPage', () => {
  let component: HospifundProposalPage;
  let fixture: ComponentFixture<HospifundProposalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HospifundProposalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HospifundProposalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
