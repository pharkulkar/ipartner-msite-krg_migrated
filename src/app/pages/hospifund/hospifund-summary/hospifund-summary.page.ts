import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
@Component({
  selector: 'app-hospifund-summary',
  templateUrl: './hospifund-summary.page.html',
  styleUrls: ['./hospifund-summary.page.scss'],
})
export class HospifundSummaryPage implements OnInit {
  isHosifund: boolean = true; isPlanDetails: boolean = true; isPEDDetails: boolean = false;
  isCoverDetails: boolean = true; isMedicalDetails: boolean = true; summaryBox: boolean = false;
  selectedPED: boolean; medDetails: any; validInsuredAge: boolean; isInsuredDetails: boolean = true;
  isApplicantDetails: boolean = true; insuredDate: any;
  agentReportDecl:boolean=false;flowType:any;
  covers: any; quoteData: any; proposalData: any; tenure: any; proposal_request_data: any;
  cust_identification_no: any; sp_code: any;
  sp_name: any; lg_code: any; branch_code: any; partnerDetails: any;
  addDetails: any;
  constructor(private routes: Router,
    public alertController: AlertController,
    public cs: CommonService
  ) {
    this.flowType = localStorage.getItem('flowType');
   }

  ngOnInit() {
    this.flowType = localStorage.getItem('flowType');
    localStorage.setItem('isRenewal', 'false');
    this.covers = JSON.parse(localStorage.getItem('coverDetails'));
    this.quoteData = JSON.parse(localStorage.getItem('quoteData'));
    this.proposalData = JSON.parse(localStorage.getItem('proposalData'));
    this.tenure = localStorage.getItem('Tenure');
    this.proposal_request_data = JSON.parse(localStorage.getItem('Proposal_Request'));
    this.addDetails = JSON.parse(localStorage.getItem('CorrrespondentStateCity'));
    this.insuredDate = moment(this.proposal_request_data.FieldsGrid[2].FieldValue).format('DD-MM-YYYY');
    if (this.proposal_request_data.insuredDataDetails[0].PreIllness != '') { this.selectedPED = true; } else { this.selectedPED = false; }
    if (this.proposal_request_data.insuredDataDetails[0].PreIllness != '') {
      this.medDetails = this.proposal_request_data.insuredDataDetails[0].PreIllness.split(',');
    }
    let InsuredAge = moment().diff(this.proposal_request_data.FieldsGrid[2].FieldValue, 'years');
    if (InsuredAge >= 57) { this.validInsuredAge = true; } else { this.validInsuredAge = false; }
    var imgUrl = './assets/images/minus.png';
    document.getElementById("planDetails").style.background = "url(" + imgUrl + ")";
    document.getElementById("planDetails").style.backgroundRepeat = 'no-repeat';
    document.getElementById("insuredDetails").style.background = "url(" + imgUrl + ")";
    document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
    // document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
    document.getElementById("coverDetails").style.background = "url(" + imgUrl + ")";
    document.getElementById("coverDetails").style.backgroundRepeat = 'no-repeat';
    document.getElementById("medicalDetails").style.background = "url(" + imgUrl + ")";
    document.getElementById("medicalDetails").style.backgroundRepeat = 'no-repeat';
  }

  async presentAlert(message: any) {
    const alert = await this.alertController.create({
      header: 'ICICI Lombard',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }
  showHosifund() {
    this.isHosifund = !this.isHosifund;
  }
  showPlanDetails() {
    this.isPlanDetails = !this.isPlanDetails;
    if (this.isPlanDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("planDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("planDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("planDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("planDetails").style.backgroundRepeat = 'no-repeat';
    }
  }
  showPEDDetails() {
    this.isPEDDetails = !this.isPEDDetails;
    if (this.isPEDDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("pedDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("pedDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("pedDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("pedDetails").style.backgroundRepeat = 'no-repeat';
    }
  }
  showCoverDetails() {
    this.isCoverDetails = !this.isCoverDetails;
    if (this.isCoverDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("coverDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("coverDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("coverDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("coverDetails").style.backgroundRepeat = 'no-repeat';
    }
  }
  showInsuredDetails() {
    this.isInsuredDetails = !this.isInsuredDetails;
    if (this.isInsuredDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("insuredDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("insuredDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
    }
  }
  // showApplicantDetails() {
  //   this.isApplicantDetails = !this.isApplicantDetails;
  //   if (this.isApplicantDetails) {
  //     var imgUrl = './assets/images/minus.png';
  //     document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
  //   } else {
  //     var imgUrl = './assets/images/plus.png';
  //     document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
  //   }
  // }
  showMedicalDetails() {
    this.isMedicalDetails = !this.isMedicalDetails;
    if (this.isMedicalDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("medicalDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("medicalDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("medicalDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("medicalDetails").style.backgroundRepeat = 'no-repeat';
    }
  }

  getDeclarationApproval(event: any) {
    this.summaryBox = event.target.checked;
  }
  getDeclarationAgentApproval(event: any)
  {
    this.agentReportDecl = event.target.checked;
  }
  submitSummary() {
    if (this.summaryBox==false) {
      this.presentAlert('Please accept the declarations.');
    }
    else if(this.agentReportDecl==false)
    {
      this.presentAlert('Please accept the declarations.');
    }
    else {
      localStorage.setItem('quoteResponse',JSON.stringify( this.quoteData));
      this.proposal_request_data['ProductType']='Hospifund';

      let tempCust=JSON.parse(localStorage.getItem('customerRequest')) ;

      tempCust.EmailAddress= tempCust.emailAddress;
      tempCust.MobileNumber=tempCust. telephoneNumber;
      tempCust.Name=tempCust.lastName;
      localStorage.setItem('customerRequest',JSON.stringify(tempCust));
      let policyID=localStorage.getItem('PolicyId');
      this.proposalData['TotalPremium']=Math.round(this.quoteData.totalPremium);
      this.proposalData['PF_CustomerID']=tempCust.pF_CUSTID;
      this.proposalData['CustomerID']=this.proposalData.customerId;
      this.proposalData['PolicyID']=policyID;
      this.proposalData['ProposalNo']=  this.proposalData.proposalNumber;
      
      let tempArray=[];
      tempArray=this.proposalData;
      let tempObj={SavePolicy:[]}
      tempObj.SavePolicy[0]=tempArray;
      localStorage.setItem('proposalRequest',JSON.stringify( this.proposal_request_data));
      localStorage.setItem('proposalResponse',JSON.stringify(tempObj));
      if (this.flowType == 'A') {
        let proposal_data = JSON.parse(localStorage.getItem('proposalResponse'));
        if(this.cust_identification_no==null ||this.cust_identification_no==undefined || this.cust_identification_no=='')
        {
          this.presentAlert('Enter customer identification number');
        }
        else if(this.sp_code==null ||this.sp_code==undefined || this.sp_code=='')
        {
          this.presentAlert('Enter sp code');
        }
        else if(this.sp_name==null ||this.sp_name==undefined || this.sp_name=='')
        {
          this.presentAlert('Enter sp name');
        }
        else if(this.lg_code==null ||this.lg_code==undefined || this.lg_code=='')
        {
          this.presentAlert('Enter lg code');
        }
        else if(this.branch_code==null ||this.branch_code==undefined || this.branch_code=='')
        {
          this.presentAlert('Enter branch code');
        }
        else
        {
          this.cs.showLoader = true;
          let request_body =
          {
            "POLICY_ID": proposal_data.SavePolicy[0].PolicyID,
            "EMPLOYEE_REFERENCE_NO": '',
            "SP_CODE": this.sp_code,
            "SP_NAME": this.sp_name,
            "LG_CODE": this.lg_code,
            "BRANCH_CODE": this.branch_code,
            "CUST_IDENTIFICATION_NO": this.cust_identification_no
  
          }
          this.cs.postWithParams('/api/Common/Save_Partners_Details', request_body).then((res: any) => {
            this.partnerDetails = res;
            this.cs.showLoader = false;
            this.routes.navigateByUrl('payment');
          });
        }
      }
      else{
        this.routes.navigateByUrl('payment');
      }
     // this.routes.navigateByUrl('payment');
    }

  }

  home(ev: any) {
    console.log("Home", ev);
    this.cs.goToHome();
  }

  showReport() {
    document.getElementById("myNav4").style.height = "100%";
  }

  closeReport()
  {
    document.getElementById("myNav4").style.height = "0%";
  }

  showDeclarartion()
  {
    document.getElementById("myNav5").style.height = "100%";
  }

  closeDeclaration()
  {
    document.getElementById("myNav5").style.height = "0%";
  }

  showTerms()
  {
    document.getElementById("myNav6").style.height = "100%";
  }

  closeTerms()
  {
    document.getElementById("myNav6").style.height = "0%";
  }

}
