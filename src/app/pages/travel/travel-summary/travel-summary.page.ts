import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { config } from 'src/app/config.properties';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-travel-summary',
  templateUrl: './travel-summary.page.html',
  styleUrls: ['./travel-summary.page.scss'],
})
export class TravelSummaryPage implements OnInit {
  request: any; response: any; createCustReq:any; planDetails:any; travelProp:any; subPlanName: any; travelPropReq:any;
  isJourneyDetails = false; isInsuredDetails = false;  isApplicantDetails = false; isInstaPay = false;
  instaPaydata:any;flowType: any; cust_identification_no: any; sp_code: any;
  sp_name: any; lg_code: any; branch_code: any; partnerDetails: any;
  constructor(public router: Router, public cs: CommonService,public alertController: AlertController) { 
    this.flowType = localStorage.getItem('flowType');
  }

  ngOnInit() {
    this.flowType = localStorage.getItem('flowType');
    localStorage.removeItem('newPID');
    this.getData();
  }

  getData():Promise<any>{
    return new Promise((resolve:any) => {
      localStorage.setItem('isRenewal','false');
      this.request = JSON.parse(localStorage.getItem('quoteRequest'));
      this.response = JSON.parse(localStorage.getItem('quoteResponse'));
      this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
      this.travelProp = JSON.parse(localStorage.getItem('proposalResponse'));
      this.travelPropReq = JSON.parse(localStorage.getItem('proposalRequest'));
      this.createCustReq = JSON.parse(localStorage.getItem('customerRequest'));
      if(this.request.IsVisitingUSCanadaYes == "Yes"){
        this.subPlanName = "USA or CANADA";
      }else if(this.request.IsVisitingSCHENGENYes == "Yes"){
        this.subPlanName = "SCHENGEN";
      }else if(this.request.IsVisitingOtherCountries == "Yes"){
        this.subPlanName = "OTHER";
      }
      console.log(this.travelProp);
      console.log(this.request);
      console.log(this.response);
      console.log(this.planDetails);
      console.log(this.travelPropReq);
      console.log("Map Data",this.createCustReq);
      if(this.travelProp.SavePolicy[0].InstaDetails == null || this.travelProp.SavePolicy[0].InstaDetails == ''|| this.travelProp.SavePolicy[0].InstaDetails == undefined){
        this.isInstaPay = false;
      }else{
        console.log(this.travelProp.SavePolicy[0].InstaDetails.isInstaAvailable)
        this.instaPaydata = this.travelProp.SavePolicy[0].InstaDetails;
        if(this.travelProp.SavePolicy[0].InstaDetails.isInstaAvailable){
          this.isInstaPay = true;
        }else{
          this.isInstaPay = false;
        }
      }
      resolve();
    });
  }

  showJourneyDetails(ev:any){
    console.log(ev);
    this.isJourneyDetails = !this.isJourneyDetails;
    if (this.isJourneyDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("journey").style.background = "url(" + imgUrl + ")";
      document.getElementById("journey").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("journey").style.background = "url(" + imgUrl + ")";
      document.getElementById("journey").style.backgroundRepeat = 'no-repeat';
    }
  }

  showInsuredDetails(ev:any){
    this.isInsuredDetails = !this.isInsuredDetails;
    if (this.isInsuredDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("insured").style.background = "url(" + imgUrl + ")";
      document.getElementById("insured").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("insured").style.background = "url(" + imgUrl + ")";
      document.getElementById("insured").style.backgroundRepeat = 'no-repeat';
    }
  }

  showApplicantDetails(ev:any){
    this.isApplicantDetails = !this.isApplicantDetails;
    if (this.isApplicantDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("applicant").style.background = "url(" + imgUrl + ")";
      document.getElementById("applicant").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("applicant").style.background = "url(" + imgUrl + ")";
      document.getElementById("applicant").style.backgroundRepeat = 'no-repeat';
    }
  }

  payNow(){
    console.log("Prop Resp", this.travelProp);
    if(localStorage.getItem('isPendingPage')){
      localStorage.removeItem('isPendingPage');
      localStorage.removeItem('pendingNavigation');
    } 
    if (this.flowType == 'A') {
      let proposal_data = JSON.parse(localStorage.getItem('proposalResponse'));
      if(this.cust_identification_no==null ||this.cust_identification_no==undefined || this.cust_identification_no=='')
      {
        this.presentAlert('Enter customer identification number');
      }
      else if(this.sp_code==null ||this.sp_code==undefined || this.sp_code=='')
      {
        this.presentAlert('Enter sp code');
      }
      else if(this.sp_name==null ||this.sp_name==undefined || this.sp_name=='')
      {
        this.presentAlert('Enter sp name');
      }
      else if(this.lg_code==null ||this.lg_code==undefined || this.lg_code=='')
      {
        this.presentAlert('Enter lg code');
      }
      else if(this.branch_code==null ||this.branch_code==undefined || this.branch_code=='')
      {
        this.presentAlert('Enter branch code');
      }
      else
      {
        let request_body =
        {
          "POLICY_ID": proposal_data.SavePolicy[0].PolicyID,
          "EMPLOYEE_REFERENCE_NO": '',
          "SP_CODE": this.sp_code,
          "SP_NAME": this.sp_name,
          "LG_CODE": this.lg_code,
          "BRANCH_CODE": this.branch_code,
          "CUST_IDENTIFICATION_NO": this.cust_identification_no

        }
        this.cs.postWithParams('/api/Common/Save_Partners_Details', request_body).then((res: any) => {
          this.partnerDetails = res;
          this.cs.showLoader = false;
          this.router.navigateByUrl('payment');
        });
      }
    }
    else{
      this.router.navigateByUrl('payment');
    }
  }

  instaPay(ev:any){
    console.log("Insta Pay", this.instaPaydata);
    document.getElementById("myNav2").style.height = "100%";    
  }

  instaPayment(ev:any){
    if(this.response.PremiumPayable < this.instaPaydata.Balance){
      let body = {
        "PolicyID": this.travelProp.SavePolicy[0].PolicyID,
        "IPAddress": config.ipAddress,
        "UserRole": "AGENT",
        "IsIAS": ""
    }
      let str = JSON.stringify(body); 
      console.log("STR", str);
      this.cs.showLoader = true;
      this.cs.postWithParams('/api/Payment/TravelInstaPayment', str).then((res:any) => {
        console.log(res);
        if(res.Message == undefined || res.Message == '' || res.Message == null){
          console.log("Correct", res);
        }else{
          console.log("Wrong", res);
        }
        this.cs.showLoader = false;
      }).catch((err) => {
        console.log("Error", err);
        this.cs.showLoader = false;
      })
    }else{
      console.log("Insufficient Balance");
    }
  }

  close(ev:any){
    document.getElementById("myNav2").style.height = "0%";
  }

  home(ev:any){
    console.log("Home",ev);
    this.cs.goToHome();
  }

  async presentAlert(message: any) {
    const alert = await this.alertController.create({
      header: 'ICICI Lombard',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

}
