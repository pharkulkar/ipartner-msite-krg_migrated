import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelSummaryPage } from './travel-summary.page';

describe('TravelSummaryPage', () => {
  let component: TravelSummaryPage;
  let fixture: ComponentFixture<TravelSummaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TravelSummaryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelSummaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
