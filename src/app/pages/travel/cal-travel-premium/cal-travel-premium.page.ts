import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { ToastController } from '@ionic/angular';
import { config } from 'src/app/config.properties';

@Component({
  selector: 'app-cal-travel-premium',
  templateUrl: './cal-travel-premium.page.html',
  styleUrls: ['./cal-travel-premium.page.scss'],
})
export class CalTravelPremiumPage implements OnInit {
  customPopoverOptions: any = {}
  minDate: any;
  dob: any;
  leaveDate: any;
  showAddOn= false;
  returnDate: any;
  noOfDays: any;
  noOfTraveller = 1;
  subProduct: any;
  travelType: any;
  tripType: any;
  planId: any;
  planType: any;
  isMultiTrip = false;
  isFamilyCover = false;
  travelForm: FormGroup;
  traveller1 = true;
  traveller2 = false;
  traveller3 = false;
  traveller4 = false;
  traveller5 = false;
  traveller6 = false;
  plans = [];
  IsAnnualMultiTrip: any;
  isVisitingUSCanada: any;
  isVisitingSCHENGEN: any;
  isVisitingOtherCountries: any;
  travelQuate: any;
  isImmigrant = false;

  constructor(public cs: CommonService, public xml2JsonService: XmlToJsonService, public toastCtrl: ToastController) { }

  ngOnInit() {
    this.createTravelForm();
    let date = new Date();
    this.minDate = moment(date).format('YYYY-MM-DD');
    console.log(this.minDate);
  }

  createTravelForm() {
    this.travelForm = new FormGroup({
      productName: new FormControl('internationalTravel'),
      subProduct: new FormControl(), tripType: new FormControl(), travelLoc: new FormControl(),
      isImmigrantVisa: new FormControl(), leavingDate: new FormControl(), returningDate: new FormControl(),
      noOfTripDays: new FormControl(), coverageType: new FormControl(), coverNeeded: new FormControl(),
      noOfTraveller: new FormControl(), traveller1DOB: new FormControl(), traveller2DOB: new FormControl(),
      traveller3DOB: new FormControl(), traveller4DOB: new FormControl(), traveller5DOB: new FormControl(),
      traveller6DOB: new FormControl(), dateDiff: new FormControl(), plan: new FormControl(),
      tripDuration: new FormControl(), policyStartDate: new FormControl(), state: new FormControl()
    });
  }

  getCover(ev:any){
    console.log(ev.target.innerText);
    if(ev.target.innerText == 'Individual Plan'){
      this.isFamilyCover = false;
    }else{
      this.isFamilyCover = true;
    }
}

  changeSubProduct(ev: any) {
    console.log("Event", ev.target.value);
    this.subProduct = ev.target.value;
    console.log("Sub Product", this.subProduct);
    if (this.subProduct == "Individual") {
      this.travelType = 1;
      this.travelForm.patchValue({ 'noOfTraveller': 1 });
    } else if (this.subProduct == "Family") {
      this.isMultiTrip = false;
      this.noOfTraveller = 2;
      this.travelType = 2;
    } else if(this.subProduct == 'Senior Citizen'){
      this.isMultiTrip = false;
      this.noOfTraveller = 1;
      this.travelType = 3;
    }else{
      console.log("Invalid SubProduct...");
    }
  }

  seeImmigrant(ev: any) {
    console.log(ev);
    if (ev.target.textContent == 'Yes') {
      this.isImmigrant = true;
      this.travelForm.patchValue({ 'isImmigrantVisa': 'YES' });
    } else {
      this.isImmigrant = false;
      this.travelForm.patchValue({ 'isImmigrantVisa': 'NO' });
    }
  }

  changeTrip(ev: any) {
    console.log("Event",ev);
    this.tripType = ev.target.outerText;
    if (ev.target.outerText == "Round Trip") {
      this.isMultiTrip = false;
      this.IsAnnualMultiTrip = 'No'
      this.travelForm.patchValue({ 'tripType': false });
      this.travelForm.patchValue({ 'policyStartDate': "" });
    } else {
      this.isMultiTrip = true;
      this.IsAnnualMultiTrip = 'Yes';
      this.isVisitingUSCanada = 'Yes';
      this.isVisitingSCHENGEN = 'No';
      this.isVisitingOtherCountries = 'No';
      this.travelForm.patchValue({ 'tripType': true });
      this.travelForm.patchValue({ 'leavingDate': "" });
    }
  }

  getTravelLoc(ev: any) {
    console.log("Location", ev.target.checked, ev.target.id);
    if (this.tripType == "Round Trip" && ev.target.checked == true && ev.target.id == 'USAorCANADA') {
      this.isVisitingUSCanada = 'Yes'; this.isVisitingSCHENGEN = 'No'; this.isVisitingOtherCountries = 'No';
    } else if (this.tripType == "Round Trip" && ev.target.checked == true && ev.target.id == 'SCHENGEN') {
      this.isVisitingUSCanada = 'No'; this.isVisitingSCHENGEN = 'Yes'; this.isVisitingOtherCountries = 'No';
    } else if (this.tripType == "Round Trip" && ev.target.checked == true && ev.target.id == 'OTHER') {
      this.isVisitingUSCanada = 'No'; this.isVisitingSCHENGEN = 'No'; this.isVisitingOtherCountries = 'Yes';
    }
  }

  getReturnDate(ev: any) {
    this.leaveDate = moment(this.travelForm.value.leavingDate).format('DD-MMMM-YYYY');
    this.returnDate = moment(ev.target.value).format('DD-MMMM-YYYY');
    let difference = new Date(this.returnDate).getTime() - new Date(this.leaveDate).getTime();
    this.noOfDays = (difference / (24 * 3600 * 1000)) + 1;
    console.log(this.noOfDays);
    this.travelForm.patchValue({ 'dateDiff': this.noOfDays });
    this.getTravelPlan();
  }

  getPolStartDate(ev: any) {
    console.log(ev.detail.value);
    let polStartDate = moment(ev.detail.value).format('DD-MMMM-YYYY');
    this.travelForm.patchValue({ 'policyStartDate': polStartDate });
    this.getTravelPlan();
  }

  toggleAddOn(ev:any){
    console.log(ev);
    this.showAddOn = !this.showAddOn;
  }

  getTravelPlan() {
    console.log(this.travelForm.value);
    let body = {
      "GeoCode": "WORLDWIDE",
      "isMultiTrip": this.travelForm.value.tripType,
      "LeavingIndiaDate": moment(this.travelForm.value.leavingDate).format('DD-MM-YYYY'),
      "PolicyStartDate": moment(this.travelForm.value.policyStartDate).format('DD-MM-YYYY'),
      "TravelType": this.travelType,
      "NoOfTravellers": this.travelForm.value.noOfTraveller,
      "CoverNeededFloater": false,
      "IndividualPlan": true,
      "FamilyPlan": false
    }
    let str = JSON.stringify(body);
    console.log(str);
    this.cs.postWithParams('/api/Travel/GetInternationalTravelPlan', str).then((res: any) => {
      console.log("Response", res);
      if (res.StatuMessage == "Success") {
        console.log("Response", res.lstTravelPlans.length);
        if (res.lstTravelPlans.length) {
          this.plans = [];
          res.lstTravelPlans.forEach((plan: any) => {
            this.plans.push(plan);
          });
        } else {
          this.presentToast();
        }
      }
    });
  }

  async presentToast() {
    const toast = await this.toastCtrl.create({
      message: 'No plan available for ' + this.noOfTraveller + ' members',
      duration: 2000
    });
    toast.present();
  }

  noOfTravellerChange(ev: any) {
    console.log(ev.target.value);
    this.noOfTraveller = ev.target.value;
    if (ev.target.value == 1) {
      this.travelForm.patchValue({ 'noOfTraveller': '1' });
      this.traveller2 = false; this.traveller3 = false; this.traveller4 = false; this.traveller5 = false; this.traveller6 = false;
    } else if (ev.target.value == 2) {
      this.travelForm.patchValue({ 'noOfTraveller': '2' });
      this.traveller2 = true; this.traveller3 = false; this.traveller4 = false; this.traveller5 = false; this.traveller6 = false;
    } else if (ev.target.value == 3) {
      this.travelForm.patchValue({ 'noOfTraveller': '3' });
      this.traveller2 = true; this.traveller3 = true; this.traveller4 = false; this.traveller5 = false; this.traveller6 = false;
    } else if (ev.target.value == 4) {
      this.travelForm.patchValue({ 'noOfTraveller': '4' });
      this.traveller2 = true; this.traveller3 = true; this.traveller4 = true; this.traveller5 = false; this.traveller6 = false;
    } else if (ev.target.value == 5) {
      this.travelForm.patchValue({ 'noOfTraveller': '5' });
      this.traveller2 = true; this.traveller3 = true; this.traveller4 = true; this.traveller5 = true; this.traveller6 = false;
    } else {
      this.travelForm.patchValue({ 'noOfTraveller': '6' });
      this.traveller2 = true; this.traveller3 = true; this.traveller4 = true; this.traveller5 = true; this.traveller6 = true;
    }
    if(this.subProduct == 'Individual'){
      this.getTravelPlan();
    }
    
  }

  traveller1DOB(ev: any) {
    let traveller1DOB = moment(ev.target.value).format('DD-MMM-YYYY');
    this.travelForm.patchValue({ 'traveller1DOB': traveller1DOB });
  }

  doChange(ev: any) {
    console.log("Event", ev.target.checked, ev.target.id);
  }

  getPlanDetail(ev: any) {
    console.log(ev.target.value);
    let plan = ev.target.value;
    let selectedPlan = this.plans.find(x => x.PlanName == plan);
    console.log(selectedPlan);
    this.planType = selectedPlan.PlanType;
    this.planId = selectedPlan.PlanCode;
  }

  calculateQuote(form: any) {
    console.log(form);

    let xmlRequest = `<Request>
    <UserID>NTkyNDYwMF9FWUZkKzE5RnlndFhJUHJNY0gxaVhJZ1Jxam5qSktXRW95c1dRYmZLQmhNPQ==</UserID>
    <UserType>AGENT</UserType>
    <ipaddress>`+config.ipAddress+`</ipaddress>
    <IsResIndia>Yes</IsResIndia>
    <PolicyStartDate></PolicyStartDate>
    <PolicyEndDate></PolicyEndDate>
    <IsPlanUpgrade>No</IsPlanUpgrade>
    <email></email>
    <telno></telno>
    <NoOfTravelers>`+ form.value.noOfTraveller + `</NoOfTravelers>
    <IsImmigrantVisa>`+ form.value.isImmigrantVisa + `</IsImmigrantVisa>
    <IsAnnualMultiTrip>`+ this.IsAnnualMultiTrip + `</IsAnnualMultiTrip>
    <MaximumDurationPerTrip>0</MaximumDurationPerTrip>
    <coverageType>INDIVIDUAL</coverageType>
    <planType>`+ this.planType + `</planType>
    <IsVisitingUSCanadaYes>`+ this.isVisitingUSCanada + `</IsVisitingUSCanadaYes>
    <IsVisitingSCHENGENYes>`+ this.isVisitingSCHENGEN + `</IsVisitingSCHENGENYes>
    <IsVisitingOtherCountries>`+ this.isVisitingOtherCountries + `</IsVisitingOtherCountries>
    <LeavingIndia>`+ moment(form.value.leavingDate).format('DD-MMM-YYYY') + `</LeavingIndia>
    <ReturnIndia>`+ moment(form.value.returningDate).format('DD-MMM-YYYY') + `</ReturnIndia>
    <TripDuration>`+ this.noOfDays + `</TripDuration>
    <IsIndividualPlan>True</IsIndividualPlan>
    <IsFamilyPlan>False</IsFamilyPlan>
    <IsCoverNeededIndividual>False</IsCoverNeededIndividual>
    <IsCoverNeededFloater>False</IsCoverNeededFloater>
    <Travellers>
        <Member>
            <DateOfBirth>`+ form.value.traveller1DOB + `</DateOfBirth>
            <PlanID>`+ this.planId + `</PlanID>
        </Member>
    </Travellers>
    <planIds>`+ this.planId + `,</planIds>
    <IsProfessionalSportCover>false</IsProfessionalSportCover>
    <IsAdventureSportCover>false</IsAdventureSportCover>
    <GSTStateCode>55</GSTStateCode>
    <GSTStateName><![CDATA[MAHARASHTRA]]></GSTStateName>
</Request>`
    // let xmlRequest =  `<Request><UserID>NTkyNDYwMF9FWUZkKzE5RnlndFhJUHJNY0gxaVhJZ1Jxam5qSktXRW95c1dRYmZLQmhNPQ==</UserID><UserType>AGENT</UserType><ipaddress>MOBILE-ANDROID</ipaddress><IsResIndia>Yes</IsResIndia><PolicyStartDate /><PolicyEndDate /><IsPlanUpgrade>No</IsPlanUpgrade><email></email><telno></telno><NoOfTravelers>1</NoOfTravelers><IsImmigrantVisa>NO</IsImmigrantVisa><IsAnnualMultiTrip>No</IsAnnualMultiTrip><MaximumDurationPerTrip>0</MaximumDurationPerTrip><coverageType>INDIVIDUAL</coverageType><planType>SINGLE</planType><IsVisitingUSCanadaYes>Yes</IsVisitingUSCanadaYes><IsVisitingSCHENGENYes>No</IsVisitingSCHENGENYes><IsVisitingOtherCountries>No</IsVisitingOtherCountries><LeavingIndia>07-May-2019</LeavingIndia><ReturnIndia>09-May-2019</ReturnIndia><TripDuration>3</TripDuration><IsIndividualPlan>True</IsIndividualPlan><IsFamilyPlan>False</IsFamilyPlan><IsCoverNeededIndividual>False</IsCoverNeededIndividual><IsCoverNeededFloater>False</IsCoverNeededFloater><Travellers><Member><DateOfBirth>07-May-1993</DateOfBirth><PlanID>60888</PlanID></Member></Travellers><planIds>60888,</planIds><IsProfessionalSportCover>false</IsProfessionalSportCover><IsAdventureSportCover>false</IsAdventureSportCover><GSTStateCode>55</GSTStateCode><GSTStateName><![CDATA[MAHARASHTRA]]></GSTStateName></Request>`
    console.log(xmlRequest);
    this.cs.postTravel('/ITPFCalculatePremium/MzU1NTI3', xmlRequest).then((res: any) => {
      console.log(res);
    }).catch((err: any) => {
      this.travelQuate = this.xml2JsonService.xmlToJson(err.error.text);
      console.log(this.travelQuate);
    })
  }


}
