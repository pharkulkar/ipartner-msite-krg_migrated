import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelQuotePage } from './travel-quote.page';

describe('TravelQuotePage', () => {
  let component: TravelQuotePage;
  let fixture: ComponentFixture<TravelQuotePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TravelQuotePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelQuotePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
