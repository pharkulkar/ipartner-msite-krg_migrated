import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { config } from '../config.properties';
import { Router } from '@angular/router';
var CommonService = /** @class */ (function () {
    function CommonService(http, router) {
        this.http = http;
        this.router = router;
        this.showLoader = false;
        this.showSwapLoader = false;
        this.validArray = [];
        this.isBackFormTravelPremium = false;
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'text/xml',
                'Accept': 'text/xml',
                'Response-Type': 'text' //<- b/c Angular understands text
                ,
                'No-Auth': 'True'
            })
        };
        console.log("BASEURL", config.baseURL);
        this.authToken = JSON.parse(localStorage.getItem('authToken'));
    }
    CommonService.prototype.postWithParams = function (endPoint, body) {
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", this.authToken);
        headers = headers.append('Content-Type', 'application/json');
        return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
    };
    CommonService.prototype.swapAppMaster = function (endPoint, token) {
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", token);
        headers = headers.append('Content-Type', 'application/json');
        return this.http.post(config.baseURL + endPoint, '', { headers: headers }).toPromise();
    };
    CommonService.prototype.getWithParams = function (endPoint) {
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", this.authToken);
        headers = headers.append('Content-Type', 'application/json');
        return this.http.get(config.baseURL + endPoint, { headers: headers }).toPromise();
    };
    CommonService.prototype.getWithParams1 = function (endPoint, token) {
        console.log(token);
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", token);
        headers = headers.append('Content-Type', 'application/json');
        return this.http.get(config.baseURL + endPoint, { headers: headers }).toPromise();
    };
    CommonService.prototype.getAuthToken = function (endPoint, body) {
        var headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
    };
    CommonService.prototype.getAgentAuthToken = function (endPoint, agentID) {
        return this.http.get(config.baseURL + endPoint, agentID).toPromise();
    };
    CommonService.prototype.postTravel = function (endPoint, body) {
        var httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'text/xml',
                'Accept': 'text/xml',
                'Response-Type': 'text' //<- b/c Angular understands text
            })
        };
        return this.http.post(config.baseURL + endPoint, body, httpOptions).toPromise();
    };
    CommonService.prototype.postPropRel = function (endPoint) {
        var httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'text/xml',
                'Accept': 'text/xml',
                'Response-Type': 'text' //<- b/c Angular understands text
            })
        };
        return this.http.post(config.baseURL + endPoint, httpOptions).toPromise();
    };
    CommonService.prototype.postPropCal = function (endPoint, body) {
        var httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'text/xml',
                'Accept': 'text/xml',
                'Response-Type': 'text' //<- b/c Angular understands text
            })
        };
        return this.http.post(config.baseURL + endPoint, body, httpOptions).toPromise();
    };
    // getPinCodeData(endPoint: any) {
    //   return this.http.get(config.baseURL + endPoint).toPromise();
    // }
    // getForAilmentList(endPoint: any) {
    //   return this.http.get(config.baseURL + endPoint).toPromise();
    // }
    CommonService.prototype.postCustSearch = function (endPoint, body) {
        var headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
    };
    CommonService.prototype.getHomeAuth = function (endPoint, data) {
        var userCredentials = data.userName + ":" + data.passWord;
        console.log(data);
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", "Basic " + btoa(userCredentials));
        // headers = headers.append("Authorization", this.authToken);
        headers = headers.append('Content-Type', 'application/json');
        return this.http.post(config.baseURL + endPoint, '', { headers: headers }).toPromise();
    };
    // getBank(endPoint: any) {
    //   let headers = new HttpHeaders({
    //     'Content-Type': 'application/json'
    //   });
    //   return this.http.post(config.baseURL + endPoint, { headers: headers }).toPromise();
    // }
    // swapAuth(endPoint: any, token: any) {
    //   console.log(token);
    //   let headers = new HttpHeaders();
    //   headers = headers.append("Authorization", token);
    //   headers = headers.append('Content-Type', 'application/json');
    //   return this.http.post(config.baseURL + endPoint, {}, { headers: headers }).toPromise();
    // }
    CommonService.prototype.getDisbaledFeatures = function (endPoint, token) {
        console.log(token);
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", token);
        headers = headers.append('Content-Type', 'application/json');
        return this.http.get(config.baseURL + endPoint, { headers: headers }).toPromise();
    };
    CommonService.prototype.getUserData = function (endPoint) {
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", this.authToken);
        headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
        return this.http.post(config.baseURL + endPoint, {}, { headers: headers }).toPromise();
    };
    CommonService.prototype.pdfDownload = function (type, policID) {
        return config.baseURL + '/api/file/DownloadPDF?type=' + type + '&value=' + policID;
    };
    CommonService.prototype.save = function (file, fileName) {
        console.log(file);
        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(file, fileName);
        }
        else {
            console.log('creation');
            var downloadLink = document.createElement("a");
            downloadLink.style.display = "none";
            document.body.appendChild(downloadLink);
            downloadLink.setAttribute("href", file);
            downloadLink.setAttribute("download", fileName);
            downloadLink.click();
            document.body.removeChild(downloadLink);
            console.log('clicked');
        }
    };
    CommonService.prototype.saveBlob = function (file, fileName) {
        var fileData = window.URL.createObjectURL(file);
        var a = document.createElement("a");
        a.href = fileData;
        a.download = fileName;
        a.target = '_blank';
        document.body.appendChild(a);
        a.click();
    };
    CommonService.prototype.goToHome = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.showSwapLoader = true;
            var agentId = JSON.parse(localStorage.getItem('userData'));
            //  console.log(str);
            _this.getAgentAuthToken('/api/common/GetIpartToken', agentId.AgentID).then(function (res) {
                //   console.log("",res);
                // this.agentAuthToken = res;
                // let token = parseInt(this.agentAuthToken);
                var webLink = config.homeURL + res;
                window.location.href = webLink;
                _this.showSwapLoader = false;
                resolve();
            }).catch(function (err) {
                console.log("Error", err);
                _this.showSwapLoader = false;
                resolve();
            });
        });
    };
    //--------------------Renewal APIs------------------------//
    // getDatathroughRNFetch(endPoint: any, body: any) {
    //   let headers = new HttpHeaders();
    //   headers = headers.append("Authorization", this.authToken);
    //   headers = headers.append('Content-Type', 'application/json');
    //   return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
    // }
    // PPAPPolicyRenewalRN(endPoint: any, body: any) {
    //   let headers = new HttpHeaders();
    //   headers = headers.append("Authorization", this.authToken);
    //   headers = headers.append('Content-Type', 'application/json');
    //   return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
    // }
    CommonService.prototype.isDataNullorUndefined = function (data) {
        var _this = this;
        this.validArray = [];
        Object.keys(data).forEach(function (key) {
            if (data[key]) {
                _this.validArray.push('Valid');
            }
            else {
                _this.validArray.push('Invalid');
            }
        });
    };
    // SendMailRnNotification(endPoint: any, body: any) {
    //   let headers = new HttpHeaders();
    //   headers = headers.append("Authorization", this.authToken);
    //   headers = headers.append('Content-Type', 'application/json');
    //   return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
    // }
    // RnNotification(endPoint: any, body: any) {
    //   let headers = new HttpHeaders();
    //   headers = headers.append("Authorization", this.authToken);
    //   headers = headers.append('Content-Type', 'application/json');
    //   return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
    // }
    //------------------- Pay-In-slips -------------------
    CommonService.prototype.getMyPayInSlip = function (endPoint) {
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", this.authToken);
        headers = headers.append('Content-Type', 'application/json');
        return this.http.post(config.baseURL + endPoint, '', { headers: headers }).toPromise();
    };
    CommonService.prototype.getChequeStatus = function (endPoint) {
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", this.authToken);
        headers = headers.append('Content-Type', 'application/json');
        return this.http.get(config.baseURL + endPoint, { headers: headers }).toPromise();
    };
    CommonService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient, Router])
    ], CommonService);
    return CommonService;
}());
export { CommonService };
//# sourceMappingURL=common.service.js.map