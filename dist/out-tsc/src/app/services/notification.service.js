import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
var NotificationService = /** @class */ (function () {
    function NotificationService() {
        this.permission = this.isSupported() ? 'default' : 'denied';
        console.log(this.permission);
    }
    NotificationService.prototype.ngOnInit = function () {
        // this.requestPermission();
    };
    NotificationService.prototype.isSupported = function () {
        return 'Notification' in window;
    };
    NotificationService.prototype.requestPermission = function () {
        var self = this;
        if ('Notification' in window) {
            Notification.requestPermission(function (status) {
                console.log("status", status);
                return self.permission = status;
            });
        }
    };
    NotificationService.prototype.create = function (title, option) {
        var self = this;
        return new Observable(function (obs) {
            if (!('Notification' in window)) {
                console.log('Notification not available');
                obs.complete();
            }
            if (self.permission !== 'granted') {
                console.log('Your hasnt granted permission to send push notification');
                obs.complete();
            }
            var _notify = new Notification(title, option);
            _notify.onshow = function (e) {
                return obs.next({
                    notification: _notify,
                    event: e
                });
            };
            _notify.onclick = function (e) {
                return obs.next({
                    notification: _notify,
                    event: e
                });
            };
            _notify.onerror = function (e) {
                return obs.error({
                    notification: _notify,
                    event: e
                });
            };
            _notify.onclose = function () {
                return obs.complete();
            };
            //   }
            // }
        });
    };
    NotificationService.prototype.generateNotification = function (source) {
        var self = this;
        source.forEach(function (item) {
            console.log(item);
            var options = {
                body: item.alertContent,
                icon: "../resource/images/bell-icon.png"
            };
            var notify = self.create(item.title, options).subscribe();
        });
    };
    NotificationService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], NotificationService);
    return NotificationService;
}());
export { NotificationService };
//# sourceMappingURL=notification.service.js.map