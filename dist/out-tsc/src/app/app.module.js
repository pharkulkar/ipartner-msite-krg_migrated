import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { OrderByPipe } from './pipes/order-by.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule, MatFormFieldModule, MatInputModule, MatNativeDateModule, MAT_DATE_LOCALE, MatStepperModule } from '@angular/material';
// import { MDBBootstrapModule } from 'angular-bootstrap-md';
// import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
// import { MDBBootstrapModule } from 'angular-bootstrap-md';
// import { MaterialModule } from '@angular/material';
// import { DatePickerModule } from 'angular-material-datepicker';
// import { MatDatepickerModule } from '@angular/material';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { MdlDatePickerModule } from '@angular-mdl/datepicker';
// import { DatePickerModule } from 'angular-material-datepicker';
import { IonRangeSliderModule } from 'ng2-ion-range-slider';
//import {MatDatepickerModule} from '@angular/material/datepicker';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib_1.__decorate([
        NgModule({
            declarations: [AppComponent, OrderByPipe],
            entryComponents: [],
            imports: [
                CommonModule,
                BrowserModule,
                HttpModule,
                HttpClientModule,
                FormsModule,
                ReactiveFormsModule,
                MatInputModule,
                MatFormFieldModule,
                MatDatepickerModule,
                BrowserAnimationsModule,
                MatNativeDateModule,
                MatStepperModule,
                IonRangeSliderModule,
                // BrowserAnimationsModule,
                // DatePickerModule,
                // BsDatepickerModule.forRoot(),
                // MDBBootstrapModule.forRoot(),
                IonicModule.forRoot(),
                AppRoutingModule
            ],
            providers: [
                StatusBar,
                SplashScreen,
                { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
                { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map