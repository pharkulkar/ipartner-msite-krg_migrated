import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { FormGroup, FormControl } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { NgxSpinnerService } from 'ngx-spinner';
var HomePage = /** @class */ (function () {
    function HomePage(router, loadingCtrl, spinner, cs) {
        this.router = router;
        this.loadingCtrl = loadingCtrl;
        this.spinner = spinner;
        this.cs = cs;
    }
    HomePage.prototype.ngOnInit = function () {
        this.createLoginForm();
        localStorage.removeItem('userData');
        console.log("Loader", this.cs.showLoader);
    };
    HomePage.prototype.createLoginForm = function () {
        this.loginForm = new FormGroup({
            //sanity
            // userName: new FormControl('ankur.saxena@icicilombard.com'),
            // passWord: new FormControl('hello123')
            userName: new FormControl('IM-165736'),
            passWord: new FormControl('lombard111')
            // userName: new FormControl('IM-46232'),
            // passWord: new FormControl('Lombard123')  
            // uat
            // userName: new FormControl('ankur.saxena@icicilombard.com'),
            // passWord: new FormControl('testing123')
            // prod
            // userName: new FormControl('IM-546438'),
            // passWord: new FormControl('Lombard2018')
            // userName: new FormControl('IM-501267'),
            // passWord: new FormControl('manita12345')
            //health travel mapped but travel product not working
            // userName: new FormControl('IM-566367'),
            // passWord: new FormControl('lombard123') 
            // userName: new FormControl('IM-552006'),
            // passWord: new FormControl('assure4542') 
            // userName: new FormControl('IM-556246'),
            // passWord: new FormControl('Asc360217') 
            // userName: new FormControl('IM-635185'),
            // passWord: new FormControl('secure20')  
            // userName: new FormControl('IM-532796'),
            // passWord: new FormControl('travel123')
            // userName: new FormControl('IM-542002'),
            // passWord: new FormControl('harman12345')
            // userName: new FormControl('IM-546438'),
            // passWord: new FormControl('Lombard2018') 
            // userName: new FormControl(),
            // passWord: new FormControl()
        });
    };
    // getDisabledFeaturesList(token:any):Promise<any>{
    //   return new Promise((resolve:any) => {
    //     this.cs.showLoader = true;
    //     this.cs.getWithParams1('/api/Agent/GetDisableFeatures', token).then((res:any) => {
    //       this.disabledFeatureData = res.DisableResponseList;
    //       let isKeralacess=this.disabledFeatureData.find((x:any) => x.Key == 'isKeralaCessEnabled').Value;
    //       localStorage.setItem('isKeralaCessEnabled',isKeralacess);
    //       this.cs.showLoader = false;
    //       resolve();
    //     });
    //   });
    // }
    HomePage.prototype.login = function (form) {
        var _this = this;
        console.log(form.value);
        // this.spinner.show();
        this.cs.showLoader = true;
        this.cs.getHomeAuth('/api/agent/AgentAppMasterData', form.value).then(function (data) {
            var userCredentials = form.value.userName + ":" + form.value.passWord;
            var authToken = "Basic " + btoa(userCredentials);
            localStorage.setItem('authToken', JSON.stringify(authToken));
            _this.pageAuth = data.MappedProduct;
            console.log("Response", _this.pageAuth, data);
            localStorage.setItem('userData', JSON.stringify(data));
            var isKeralacess = data.DisableFeatures.isKeralaCessEnabled;
            localStorage.setItem('isKeralaCessEnabled', isKeralacess);
            _this.router.navigateByUrl('dash-board');
            // this.spinner.hide();
            _this.cs.showLoader = false;
        }).catch(function (err) {
            console.log("Something went wrong....");
            // this.spinner.hide();
            _this.cs.showLoader = false;
        });
    };
    HomePage.prototype.goToHealth = function () {
        console.log("Health");
        this.router.navigateByUrl('cal-health-premium');
    };
    HomePage.prototype.goToTravel = function () {
        console.log("Travel");
        this.router.navigateByUrl('cal-travel-premium');
    };
    HomePage = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: 'home.page.html',
            styleUrls: ['home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            LoadingController,
            NgxSpinnerService,
            CommonService])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map