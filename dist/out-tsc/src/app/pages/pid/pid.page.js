import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { Router } from '@angular/router';
var PIDPage = /** @class */ (function () {
    function PIDPage(cs, router) {
        this.cs = cs;
        this.router = router;
        this.isHealthShow = false;
        this.isTravelShow = false;
        this.isCustomer = false;
        this.showPIDList = false;
        this.isLoader = false;
        this.showCustList = false;
        this.isNewPID = false;
        this.custType = 'Customer';
        this.productType = '0';
        this.healthArray = [];
        this.travelArray = [];
    }
    PIDPage.prototype.ngOnInit = function () {
        localStorage.removeItem('newPID');
        this.getData();
    };
    PIDPage.prototype.getData = function () {
        var _this = this;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.isHealthRights(this.userData.MappedProduct.Health).then(function () {
            console.log(_this.healthArray);
            if (_this.healthArray.indexOf(true) > -1) {
                console.log("Valid Data");
                _this.isHealthShow = true;
            }
            else {
                console.log("Invalid Data");
                _this.isHealthShow = false;
            }
        });
        this.isTravelRights(this.userData.MappedProduct.Travel).then(function () {
            console.log(_this.travelArray);
            if (_this.travelArray.indexOf(true) > -1) {
                console.log("Valid Data");
                _this.isTravelShow = true;
            }
            else {
                console.log("Invalid Data");
                _this.isTravelShow = false;
            }
        });
    };
    PIDPage.prototype.isHealthRights = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            Object.keys(data).forEach(function (key) {
                console.log(key);
                if (key == 'isHealthBoosterMapped' || key == 'isHealthCHIMapped' || key == 'isHealthPPAPMapped') {
                    console.log(data[key], key);
                    _this.healthArray.push(data[key]);
                }
            });
            resolve();
        });
    };
    PIDPage.prototype.isTravelRights = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            Object.keys(data).forEach(function (key) {
                console.log(key);
                if (key == 'isTRAVELINTERNATIONALMapped' || key == 'isTRAVELSTUDENTMapped') {
                    console.log(data[key], key);
                    _this.travelArray.push(data[key]);
                }
            });
            resolve();
        });
    };
    PIDPage.prototype.showPIDData = function (ev) {
        console.log(ev);
        this.selectProduct(ev);
        // .then(() =>{
        //   this.getPIDList();
        // });
    };
    PIDPage.prototype.selectProduct = function (ev) {
        var _this = this;
        return new Promise(function (resolve) {
            var product = _this.productType;
            console.log(product);
            if (product == 'travel') {
                _this.selectedProduct = 'Travel';
            }
            else {
                _this.selectedProduct = 'Health';
                _this.custType = 'Customer';
            }
            resolve();
        });
    };
    PIDPage.prototype.showType = function (ev) {
        console.log(this.checkbox.nativeElement.checked);
        if (this.checkbox.nativeElement.checked) {
            this.isCustomer = true;
            this.custType = 'Intermediary';
            this.custIpartId = "";
            this.getPIDList();
        }
        else {
            this.isCustomer = false;
            this.custType = 'Customer';
        }
        console.log(this.custType);
    };
    PIDPage.prototype.searchCustData = function (ev) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log(_this.serachData);
            var req = {
                "CustomerName": _this.serachData,
                "CustomerEmail": "",
                "CustomerMobile": ""
            };
            var str = JSON.stringify(req);
            _this.cs.showLoader = true;
            _this.cs.postWithParams('/api/Customer/SearchCustomer', str).then(function (res) {
                console.log(res);
                _this.custData = res;
                _this.showCustList = true;
                _this.showPIDList = false;
                _this.cs.showLoader = false;
                resolve();
            }).catch(function (err) {
                console.log(err);
                _this.cs.showLoader = false;
                resolve();
            });
        });
    };
    PIDPage.prototype.searchPID = function (ev) {
        console.log(ev.target.value);
        var searchData = ev.target.value;
        this.searchData = this.pidData.filter(function (x) { return (x.PaymentID).includes(searchData); });
        console.log("Filetr Data", this.searchData);
    };
    PIDPage.prototype.selectedCust = function (data) {
        console.log(data);
        this.custIpartId = data.CustomerID;
        this.custEmail = data.CustomerEmail;
        this.custMobile = data.CustomerMobile;
        this.custName = data.CustomerName;
        this.getPIDList();
    };
    PIDPage.prototype.getPIDList = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var req = {
                "PayerType": _this.custType,
                "PF_CustomerId": "",
                "iPartnerCustomerId": _this.custIpartId,
                "PolicyType": _this.selectedProduct
            };
            var str = JSON.stringify(req);
            // this.cs.showLoader = true;
            _this.isLoader = true;
            _this.cs.postWithParams('/api/Payment/GetPidList', str).then(function (res) {
                console.log("PID", res);
                _this.pidData = res;
                _this.searchData = res;
                _this.showCustList = false;
                _this.showPIDList = true;
                // this.cs.showLoader = false;
                _this.isLoader = false;
                resolve();
            }).catch(function (err) {
                console.log(err);
                // this.cs.showLoader = false;
                _this.isLoader = false;
                resolve();
            });
        });
    };
    PIDPage.prototype.createPID = function () {
        this.isNewPID = true;
        var objPID = {
            'isNew': this.isNewPID,
            'pidAmount': (this.pidAmount).toString(),
            'CustomerId': this.custIpartId,
            'policyType': this.selectedProduct,
            'PayerType': this.custType,
            'custEmail': this.custEmail,
            'custMobile': this.custMobile,
            'custName': this.custName
        };
        console.log("AMOUNT", objPID);
        localStorage.setItem('newPID', JSON.stringify(objPID));
        this.router.navigateByUrl('payment');
    };
    PIDPage.prototype.selectedPID = function (pidData) {
        var _this = this;
        console.log(pidData);
        this.cs.postWithParams('/api/payment/GetPaymentIDTaggedReport?PaymentID=' + pidData.PaymentID, '').then(function (res) {
            console.log(res);
            _this.selectedPIDDetails = res;
            document.getElementById("pidDetails").style.height = "100%";
        }).catch(function (err) {
            console.log(err);
            document.getElementById("pidDetails").style.height = "0%";
        });
    };
    PIDPage.prototype.close = function () {
        document.getElementById("pidDetails").style.height = "0%";
    };
    PIDPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    tslib_1.__decorate([
        ViewChild('checkbox'),
        tslib_1.__metadata("design:type", Object)
    ], PIDPage.prototype, "checkbox", void 0);
    PIDPage = tslib_1.__decorate([
        Component({
            selector: 'app-pid',
            templateUrl: './pid.page.html',
            styleUrls: ['./pid.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, Router])
    ], PIDPage);
    return PIDPage;
}());
export { PIDPage };
//# sourceMappingURL=pid.page.js.map