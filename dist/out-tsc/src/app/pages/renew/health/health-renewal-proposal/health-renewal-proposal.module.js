import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IonRangeSliderModule } from 'ng2-ion-range-slider';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HealthRenewalProposalPage } from './health-renewal-proposal.page';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
var routes = [
    {
        path: '',
        component: HealthRenewalProposalPage
    }
];
var HealthRenewalProposalPageModule = /** @class */ (function () {
    function HealthRenewalProposalPageModule() {
    }
    HealthRenewalProposalPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                IonicModule,
                MatNativeDateModule,
                MatFormFieldModule,
                IonRangeSliderModule,
                MatInputModule,
                MatDatepickerModule,
                RouterModule.forChild(routes)
            ],
            declarations: [HealthRenewalProposalPage]
        })
    ], HealthRenewalProposalPageModule);
    return HealthRenewalProposalPageModule;
}());
export { HealthRenewalProposalPageModule };
//# sourceMappingURL=health-renewal-proposal.module.js.map