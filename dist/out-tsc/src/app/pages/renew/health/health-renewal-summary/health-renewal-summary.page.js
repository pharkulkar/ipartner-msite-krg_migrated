import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { config } from 'src/app/config.properties';
var HealthRenewalSummaryPage = /** @class */ (function () {
    function HealthRenewalSummaryPage(cs, router, alertCtrl) {
        this.cs = cs;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.isPlanDetails = false;
        this.isInsured1 = false;
        this.isInsured2 = false;
        this.isInsured3 = false;
        this.isNomineeDetails = false;
        this.isappointeeDetails = false;
        this.isInsured4 = false;
        this.isInsured5 = false;
        this.isInsured6 = false;
        this.isApplicantDetails = false;
        this.showInsured1 = false;
        this.isNomineeFormValid = false;
        this.showInsured2 = false;
        this.showInsured3 = false;
        this.showInsured4 = false;
        this.showInsured5 = false;
        this.showInsured6 = false;
        this.isBreakUp = false;
        this.isAppointeeFormValid = false;
        this.below18 = false;
        this.breakUpData = {};
        this.titles = [{ "id": "0", "val": "Mrs." }, { "id": "1", "val": "Mr." }, { "id": "2", "val": "Ms." }];
        this.isDeclarationAccepted = true;
        this.isInsuredChange = false;
        this.applNomBody = {};
        this.memberObj = [];
        this.isRenewalEdited = false;
        this.memberObj1 = {};
    }
    HealthRenewalSummaryPage.prototype.ngOnInit = function () {
        var _this = this;
        var date1 = new Date;
        this.date = moment(date1).format('DD MMMM YYYY');
        this.minDate = moment(date1).format('YYYY-MM-DD');
        this.policyType = JSON.parse(localStorage.getItem('product'));
        this.nomRelations = JSON.parse(localStorage.getItem('relations'));
        console.log(this.nomRelations);
        localStorage.removeItem('isDataEdit');
        localStorage.setItem('isRenewal', 'true');
        this.getRelation();
        this.createRenewNomineeForm();
        this.createRenewAppointeeForm();
        this.getStateData();
        this.setData().then(function () {
            _this.getData().then(function () {
                _this.getBreakUpData();
                // this.payNow();
            });
        });
    };
    HealthRenewalSummaryPage.prototype.getStateData = function () {
        var _this = this;
        this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then(function (res) {
            _this.states = res;
            _this.renewalSummaryResponse.StateName = _this.states.find(function (x) { return x.StateId == _this.renewalSummaryResponse.StateID; }).StateName;
        }).catch(function (err) {
            _this.displayAlert(err.error.ExceptionMessage);
        });
    };
    HealthRenewalSummaryPage.prototype.getStateforHealth = function () {
        var _this = this;
        this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then(function (res) {
            _this.states = res;
            _this.applNomBody.StateName = _this.states.find(function (x) { return x.StateId == _this.renewalSummaryResponse.StateID; }).StateName;
        }).catch(function (err) {
            _this.displayAlert(err.error.ExceptionMessage);
        });
    };
    HealthRenewalSummaryPage.prototype.createRenewNomineeForm = function () {
        this.renewalNomForm = new FormGroup({
            nomTitle: new FormControl(),
            nomName: new FormControl(),
            nomRelation: new FormControl(),
            nomDOB: new FormControl()
        });
    };
    HealthRenewalSummaryPage.prototype.createRenewAppointeeForm = function () {
        this.renewalAppoForm = new FormGroup({
            appoTitle: new FormControl(),
            appoName: new FormControl(),
            appoRelation: new FormControl(),
            appoDOB: new FormControl()
        });
    };
    HealthRenewalSummaryPage.prototype.setData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (_this.policyType.productId != 30) {
                _this.localData = JSON.parse(localStorage.getItem('renewalPolicyData'));
                _this.renewalBody = JSON.parse(localStorage.getItem('renewalBody'));
                _this.renewalSummaryResponse = _this.localData;
                var isrenewalPremium = _this.renewalBody.isRenewalthroughPremium;
                if (_this.renewalBody.isInsuredChange || _this.renewalBody.isRenewalEdited) {
                    if (isrenewalPremium == true) {
                        _this.isInsuredChange = false;
                        _this.isRenewalEdited = true;
                        _this.applNomBody.SumInsured = _this.renewalBody.SumInsured;
                        _this.renewalSummaryResponse.Premium1YearFlag = _this.renewalBody.Premium1YearFlag;
                        _this.renewalSummaryResponse.Premium2YearFlag = _this.renewalBody.Premium2YearFlag;
                        _this.renewalSummaryResponse.Premium3YearFlag = _this.renewalBody.Premium3YearFlag;
                        _this.renewalSummaryResponse.Premium1Year = _this.renewalBody.TotalPremium1Year;
                        _this.renewalSummaryResponse.Premium2Year = _this.renewalBody.TotalPremium2Year;
                        _this.renewalSummaryResponse.Premium3Year = _this.renewalBody.TotalPremium3Year;
                        _this.applNomBody.Email = _this.localData.Email;
                        _this.applNomBody.Mobile = _this.localData.Mobile;
                        var addon = JSON.parse(localStorage.getItem('RenewalPremiumAddons'));
                        _this.applNomBody.AddOnCovers1 = addon.AddOnCovers1;
                        _this.applNomBody.AddOnCovers1Flag = addon.AddOnCovers1Flag;
                        _this.applNomBody.AddOnCovers6 = addon.AddOnCovers6;
                        _this.applNomBody.AddOnCovers6Flag = addon.AddOnCovers6Flag;
                        _this.applNomBody.StateName = _this.localData.StateName;
                        _this.renewalBody = JSON.parse(localStorage.getItem('renewalPolicyData'));
                    }
                    else {
                        _this.isInsuredChange = true;
                        _this.isRenewalEdited = true;
                        _this.applNomBody.SumInsured = _this.renewalBody.SumInsured;
                        _this.applNomBody.AddOnCovers1 = _this.renewalBody.AddOnCovers1;
                        _this.applNomBody.AddOnCovers1Flag = _this.renewalBody.AddOnCovers1Flag;
                        _this.applNomBody.AddOnCovers6 = _this.renewalBody.AddOnCovers6;
                        _this.applNomBody.AddOnCovers6Flag = _this.renewalBody.AddOnCovers6Flag;
                        _this.applNomBody.StateName = _this.renewalBody.CustStateName;
                        _this.applNomBody.Email = _this.renewalBody.EmailID;
                        _this.applNomBody.Mobile = _this.renewalBody.MobileNo;
                    }
                    if (_this.renewalSummaryResponse.Premium1YearFlag == 'true') {
                        _this.selectedCoveragePeriod = '1';
                    }
                    if (_this.renewalSummaryResponse.Premium2YearFlag == 'true') {
                        _this.selectedCoveragePeriod = '2';
                    }
                    if (_this.renewalSummaryResponse.Premium3YearFlag == 'true') {
                        _this.selectedCoveragePeriod = '3';
                    }
                    console.log("Inside", _this.renewalBody, _this.localData);
                    if (isrenewalPremium == true) {
                        _this.applNomBody.noOfInsured = _this.renewalBody.InsuredDetails.length;
                    }
                    else {
                        _this.applNomBody.noOfInsured = _this.renewalBody.Members.Member.length;
                    }
                    _this.applNomBody.sublimit = _this.renewalBody.SubLimitApplicable;
                    _this.applNomBody.AdditionalSI = _this.renewalBody.AdditionalSI;
                    _this.applNomBody.NameOfApplicant = _this.renewalBody.NameOfApplicant;
                    _this.applNomBody.AadhaarNumber = _this.renewalBody.AadhaarNumber;
                    _this.applNomBody.PANNumber = _this.renewalBody.PANNumber;
                    _this.applNomBody.AddressLine1 = _this.renewalBody.AddressLine1;
                    _this.applNomBody.AddressLine2 = _this.renewalBody.AddressLine2;
                    _this.applNomBody.PincodeID = _this.renewalBody.PincodeID;
                    _this.applNomBody.Pincode = _this.renewalBody.Pincode;
                    _this.applNomBody.CityID = _this.renewalBody.CityID;
                    _this.applNomBody.StateID = _this.renewalBody.StateID;
                    _this.applNomBody.CityName = _this.renewalBody.CityName;
                    _this.applNomBody.AddOnCovers2And4 = _this.renewalBody.AddOnCovers2And4;
                    _this.applNomBody.AddOnCovers2And4Flag = _this.renewalBody.AddOnCovers2And4Flag;
                    _this.applNomBody.AddOnCovers2And4Type = _this.renewalBody.AddOnCovers2And4Type;
                    _this.applNomBody.AddOnCovers3 = _this.renewalBody.AddOnCovers3;
                    _this.applNomBody.AddOnCovers3Flag = _this.renewalBody.AddOnCovers3Flag;
                    _this.applNomBody.AddOnCovers3Type = _this.renewalBody.AddOnCovers3Type;
                    _this.applNomBody.AddOnCovers5 = _this.renewalBody.AddOnCovers5;
                    _this.applNomBody.AddOnCovers5Flag = _this.renewalBody.AddOnCovers5Flag;
                    _this.applNomBody.VoluntaryDeductible = _this.renewalBody.VoluntaryDeductible;
                    _this.applNomBody.AgeOfEldest = _this.renewalBody.AgeOfEldest;
                    _this.applNomBody.NoOfAdults = _this.renewalBody.NoOfAdults;
                    _this.applNomBody.NoOfKids = _this.renewalBody.NoOfKids;
                    _this.applNomBody.NomineeTitle = _this.renewalBody.NomineeTitle;
                    _this.applNomBody.NomineeRelationShipID = _this.renewalBody.NomineeRelationShipID;
                    _this.applNomBody.NomineeRelationShip = _this.renewalBody.NomineeRelationShip;
                    _this.applNomBody.NomineeName = _this.renewalBody.NomineeName;
                    _this.applNomBody.NomineeDOB = _this.renewalBody.NomineeDOB;
                    _this.applNomBody.AppointeeLastName = _this.renewalBody.AppointeeLastName;
                    _this.applNomBody.AppointeeTitleID = _this.renewalBody.AppointeeTitleID;
                    _this.applNomBody.AppointeeRelationShipID = _this.renewalBody.AppointeeRelationShipID;
                    _this.applNomBody.AppointeeRelationShipName = _this.renewalBody.AppointeeRelationShipName;
                    _this.applNomBody.AppointeeDOB = _this.renewalBody.AppointeeDOB;
                    _this.applNomBody.AddressLine1 = _this.renewalBody.AddressLine1;
                    _this.applNomBody.AddressLine2 = _this.renewalBody.AddressLine2;
                    _this.applNomBody.PFCustomerId = _this.renewalBody.PfCustomerId;
                    if (isrenewalPremium == true) {
                        for (var i = 0; i < _this.renewalBody.InsuredDetails.length; i++) {
                            _this.memberObj.push({
                                "Ailments": "",
                                "DOB": _this.renewalBody.InsuredDetails[i].DateofBirth,
                                "Height": _this.renewalBody.InsuredDetails[i].Height + '.' + _this.renewalBody.InsuredDetails[i].HeightInInches,
                                "isExisting": "true",
                                "MemberType": _this.renewalBody.InsuredDetails[i].KidAdultType,
                                "Name": _this.renewalBody.InsuredDetails[i].FullName,
                                "OtherDisease": '',
                                "RelationshipID": _this.renewalBody.InsuredDetails[i].RelationShipID.toString(),
                                "RelationshipName": _this.renewalBody.InsuredDetails[i].RelationwithApplicant == null ? '' : _this.renewalBody.InsuredDetails[i].RelationwithApplicant,
                                "TitleID": _this.renewalBody.InsuredDetails[i].Title,
                                "Weight": _this.renewalBody.InsuredDetails[i].Weight
                            });
                        }
                        // this.memberObj = this.renewalBody.InsuredDetails;
                    }
                    else {
                        _this.memberObj = _this.renewalBody.Members.Member;
                    }
                    _this.renewalNomForm.patchValue({ 'nomName': _this.renewalBody.NomineeName });
                    _this.renewalNomForm.patchValue({ 'nomDOB': moment(_this.renewalBody.NomineeDOB).format('YYYY-MM-DD') });
                    if (_this.renewalBody.NomineeTitle == null || _this.renewalBody.NomineeTitle == undefined || _this.renewalBody.NomineeTitle == '') {
                        _this.renewalNomForm.patchValue({ 'nomTitle': '' });
                    }
                    else {
                        _this.renewalNomForm.patchValue({ 'nomTitle': _this.renewalBody.NomineeTitle });
                        _this.nomineeTitle = _this.titles.find(function (x) { return x.id == _this.renewalBody.NomineeTitle; });
                        _this.nomineeTitle.id = _this.renewalBody.NomineeTitle;
                    }
                    if (_this.renewalBody.NomineeRelationShip == null || _this.renewalBody.NomineeRelationShip == undefined || _this.renewalBody.NomineeRelationShip == '') {
                        _this.renewalNomForm.patchValue({ 'nomRelation': '' });
                    }
                    else {
                        _this.renewalNomForm.patchValue({ 'nomRelation': _this.renewalBody.NomineeRelationShip });
                        _this.nomRela = _this.nomRelations.NomineeAppointeeRelationship.find(function (x) { return x.RelationshipName == _this.renewalBody.NomineeRelationShip; });
                        _this.nomRela.RelationshipID = _this.renewalBody.NomineeRelationShipID;
                        _this.nomRela.RelationshipName = _this.renewalBody.NomineeRelationShip;
                    }
                    resolve();
                }
                else {
                    // need to change   
                    _this.isInsuredChange = false;
                    _this.isRenewalEdited = false;
                    _this.localData = JSON.parse(localStorage.getItem('renewalPolicyData'));
                    _this.renewalSummaryResponse = _this.localData;
                    _this.applNomBody.noOfInsured = _this.renewalSummaryResponse.InsuredDetails.length;
                    _this.applNomBody.sublimit = _this.renewalSummaryResponse.SubLimitApplicable;
                    _this.applNomBody.AdditionalSI = _this.renewalSummaryResponse.AdditionalSI;
                    _this.applNomBody.SumInsured = _this.renewalSummaryResponse.SumInsured;
                    _this.applNomBody.NameOfApplicant = _this.renewalSummaryResponse.NameOfApplicant;
                    _this.applNomBody.AadhaarNumber = _this.renewalSummaryResponse.AadhaarNumber;
                    _this.applNomBody.PANNumber = _this.renewalSummaryResponse.PANNumber;
                    _this.applNomBody.AddressLine1 = _this.renewalSummaryResponse.AddressLine1;
                    _this.applNomBody.AddressLine2 = _this.renewalSummaryResponse.AddressLine2;
                    _this.applNomBody.PincodeID = _this.renewalSummaryResponse.PincodeID;
                    _this.applNomBody.Pincode = _this.renewalSummaryResponse.Pincode;
                    _this.applNomBody.CityID = _this.renewalSummaryResponse.CityID;
                    _this.applNomBody.StateID = _this.renewalSummaryResponse.StateID;
                    _this.applNomBody.CityName = _this.renewalSummaryResponse.CityName;
                    _this.applNomBody.StateName = _this.renewalSummaryResponse.StateName;
                    _this.applNomBody.Email = _this.renewalSummaryResponse.Email;
                    _this.applNomBody.Mobile = _this.renewalSummaryResponse.Mobile;
                    _this.applNomBody.AddOnCovers1 = _this.renewalSummaryResponse.AddOnCovers1;
                    _this.applNomBody.AddOnCovers1Flag = _this.renewalSummaryResponse.AddOnCovers1Flag;
                    _this.applNomBody.AddOnCovers2And4 = _this.renewalSummaryResponse.AddOnCovers2And4;
                    _this.applNomBody.AddOnCovers2And4Flag = _this.renewalSummaryResponse.AddOnCovers2And4Flag;
                    _this.applNomBody.AddOnCovers2And4Type = _this.renewalSummaryResponse.AddOnCovers2And4Type;
                    _this.applNomBody.AddOnCovers3 = _this.renewalSummaryResponse.AddOnCovers3;
                    _this.applNomBody.AddOnCovers3Flag = _this.renewalSummaryResponse.AddOnCovers3Flag;
                    _this.applNomBody.AddOnCovers3Type = _this.renewalSummaryResponse.AddOnCovers3Type;
                    _this.applNomBody.AddOnCovers5 = _this.renewalSummaryResponse.AddOnCovers5;
                    _this.applNomBody.AddOnCovers5Flag = _this.renewalSummaryResponse.AddOnCovers5Flag;
                    _this.applNomBody.AddOnCovers6 = _this.renewalSummaryResponse.AddOnCovers6;
                    _this.applNomBody.AddOnCovers6Flag = _this.renewalSummaryResponse.AddOnCovers6Flag;
                    _this.applNomBody.VoluntaryDeductible = _this.renewalSummaryResponse.VoluntaryDeductible;
                    _this.applNomBody.AgeOfEldest = _this.renewalSummaryResponse.AgeOfEldest;
                    _this.applNomBody.NoOfAdults = _this.renewalSummaryResponse.NoOfAdults;
                    _this.applNomBody.NoOfKids = _this.renewalSummaryResponse.NoOfKids;
                    _this.applNomBody.AppointeeLastName = _this.renewalSummaryResponse.AppointeeLastName;
                    _this.applNomBody.AppointeeTitleID = _this.renewalSummaryResponse.AppointeeTitleID;
                    _this.applNomBody.AppointeeRelationShipID = _this.renewalSummaryResponse.AppointeeRelationShipID;
                    _this.applNomBody.AppointeeRelationShipName = _this.renewalSummaryResponse.AppointeeRelationShipName;
                    _this.applNomBody.AppointeeDOB = _this.renewalSummaryResponse.AppointeeDOB;
                    _this.applNomBody.AddressLine1 = _this.renewalSummaryResponse.AddressLine1;
                    _this.applNomBody.AddressLine2 = _this.renewalSummaryResponse.AddressLine2;
                    _this.applNomBody.PFCustomerId = _this.renewalSummaryResponse.PfCustomerId;
                    _this.memberObj = _this.renewalSummaryResponse.InsuredDetails;
                    _this.renewalNomForm.patchValue({ 'nomName': _this.renewalSummaryResponse.NomineeName });
                    _this.renewalNomForm.patchValue({ 'nomDOB': moment(_this.renewalSummaryResponse.NomineeDOB).format('YYYY-MM-DD') });
                    if (_this.renewalSummaryResponse.NomineeTitle == null || _this.renewalSummaryResponse.NomineeTitle == undefined || _this.renewalSummaryResponse.NomineeTitle == '') {
                        _this.renewalSummaryResponse.patchValue({ 'nomTitle': '' });
                    }
                    else {
                        _this.renewalNomForm.patchValue({ 'nomTitle': _this.renewalBody.NomineeTitle });
                    }
                    if (_this.renewalSummaryResponse.NomineeRelationShip == null || _this.renewalSummaryResponse.NomineeRelationShip == undefined || _this.renewalSummaryResponse.NomineeRelationShip == '') {
                        _this.renewalNomForm.patchValue({ 'nomRelation': '' });
                    }
                    else {
                        _this.renewalNomForm.patchValue({ 'nomRelation': _this.renewalSummaryResponse.NomineeRelationShip });
                    }
                    resolve();
                }
            }
            else {
                _this.isInsuredChange = false;
                _this.isRenewalEdited = false;
                _this.renewalBody = JSON.parse(localStorage.getItem('renewalPolicyData'));
                _this.renewalSummaryResponse = _this.renewalBody;
                _this.applNomBody.NameOfApplicant = _this.renewalBody.NameOfApplicant;
                _this.applNomBody.AadhaarNumber = _this.renewalBody.AadhaarNumber;
                _this.applNomBody.PANNumber = _this.renewalBody.PANNumber;
                _this.panNo = _this.applNomBody.PANNumber;
                _this.applNomBody.AddressLine1 = _this.renewalBody.AddressLine1;
                _this.applNomBody.AddressLine2 = _this.renewalBody.AddressLine2;
                _this.applNomBody.Pincode = _this.renewalBody.Pincode;
                _this.applNomBody.CityName = _this.renewalBody.CityName;
                _this.applNomBody.StateName = _this.renewalBody.StateName;
                _this.applNomBody.Email = _this.renewalBody.Email;
                _this.applNomBody.Mobile = _this.renewalBody.Mobile;
                _this.applNomBody.totalPremium = _this.renewalBody.TotalPremium;
                _this.applNomBody.tenure = _this.renewalBody.TenureInMonths / 12;
                _this.applNomBody.SumInsured = _this.renewalBody.SumInsured;
                _this.applNomBody.noOfInsured = _this.renewalBody.InsuredDetails.length;
                _this.applNomBody.PFCustomerId = _this.renewalBody.PfCustomerId;
                _this.renewalNomForm.patchValue({ 'nomName': _this.renewalBody.NomineeName });
                _this.renewalNomForm.patchValue({ 'nomDOB': moment(_this.renewalBody.NomineeDOB).format('YYYY-MM-DD') });
                if (_this.renewalBody.NomineeTitle == null || _this.renewalBody.NomineeTitle == undefined || _this.renewalBody.NomineeTitle == '') {
                    _this.renewalNomForm.patchValue({ 'nomTitle': '' });
                }
                else {
                    _this.renewalNomForm.patchValue({ 'nomTitle': _this.renewalBody.NomineeTitle });
                }
                if (_this.renewalBody.NomineeRelationShip == null || _this.renewalBody.NomineeRelationShip == undefined || _this.renewalBody.NomineeRelationShip == '') {
                    _this.renewalNomForm.patchValue({ 'nomRelation': '' });
                }
                else {
                    _this.renewalNomForm.patchValue({ 'nomRelation': _this.renewalBody.NomineeRelationShip });
                }
                _this.memberObj.push({
                    "Ailments": "",
                    "DOB": _this.renewalBody.InsuredDetails[0].DateofBirth,
                    "Height": _this.renewalBody.InsuredDetails[0].Height + '.' + _this.renewalBody.InsuredDetails[0].HeightInInches,
                    "isExisting": "true",
                    "MemberType": _this.renewalBody.InsuredDetails[0].KidAdultType,
                    "Name": _this.renewalBody.InsuredDetails[0].FullName,
                    "OtherDisease": '',
                    "RelationshipID": _this.renewalBody.InsuredDetails[0].RelationShipID.toString(),
                    "RelationshipName": _this.renewalBody.InsuredDetails[0].RelationwithApplicant == null ? '' : _this.renewalBody.InsuredDetails[0].RelationwithApplicant,
                    "TitleID": _this.renewalBody.InsuredDetails[0].Title,
                    "Weight": _this.renewalBody.InsuredDetails[0].Weight
                });
                if (_this.memberObj[0].MemberType == null || _this.memberObj[0].MemberType == undefined) {
                    var tempAge = moment().diff(_this.memberObj[0].DOB, 'years');
                    if (tempAge > 18) {
                        _this.memberObj[0].MemberType = 'Adult';
                    }
                    else {
                        _this.memberObj[0].MemberType = 'Child';
                    }
                }
                resolve();
            }
        });
    };
    HealthRenewalSummaryPage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            console.log("IMP DAta", _this.renewalSummaryResponse, _this.applNomBody);
            _this.aadharNo = _this.applNomBody.AadhaarNumber;
            _this.panNo = _this.applNomBody.PANNumber;
            if (_this.memberObj.length == 1) {
                _this.showInsured1 = true;
                _this.showInsured2 = false;
                _this.showInsured3 = false;
                _this.showInsured4 = false;
                _this.showInsured5 = false;
                _this.showInsured6 = false;
            }
            else if (_this.memberObj.length == 2) {
                _this.showInsured1 = true;
                _this.showInsured2 = true;
                _this.showInsured3 = false;
                _this.showInsured4 = false;
                _this.showInsured5 = false;
                _this.showInsured6 = false;
            }
            else if (_this.memberObj.length == 3) {
                _this.showInsured1 = true;
                _this.showInsured2 = true;
                _this.showInsured3 = true;
                _this.showInsured4 = false;
                _this.showInsured5 = false;
                _this.showInsured6 = false;
            }
            else if (_this.memberObj.length == 4) {
                _this.showInsured1 = true;
                _this.showInsured2 = true;
                _this.showInsured3 = true;
                _this.showInsured4 = true;
                _this.showInsured5 = false;
                _this.showInsured6 = false;
            }
            else if (_this.memberObj.length == 5) {
                _this.showInsured1 = true;
                _this.showInsured2 = true;
                _this.showInsured3 = true;
                _this.showInsured4 = true;
                _this.showInsured5 = true;
                _this.showInsured6 = false;
            }
            else {
                _this.showInsured1 = true;
                _this.showInsured2 = true;
                _this.showInsured3 = true;
                _this.showInsured4 = true;
                _this.showInsured5 = true;
                _this.showInsured6 = true;
            }
            if (!_this.applNomBody.PANNumber || !_this.applNomBody.AadhaarNumber) {
                _this.isApplicantDetails = true;
            }
            resolve();
        });
    };
    HealthRenewalSummaryPage.prototype.getBreakUpData = function () {
        console.log(this.policyType);
        if (this.policyType.productId !== '30') {
            if (this.renewalSummaryResponse.Premium1YearFlag == "true") {
                this.breakUpData['totalPremium'] = this.renewalSummaryResponse.Premium1Year;
                this.breakUpData['basicPremium'] = this.renewalSummaryResponse.BasicPremium1Year;
                this.breakUpData['optionalCover'] = this.renewalSummaryResponse.TotalCoverPremium1Year;
                this.breakUpData['sublimit'] = this.renewalSummaryResponse.SublimitDiscount1Year;
                this.breakUpData['GST'] = this.renewalSummaryResponse.TotalGSTAmt1Year;
            }
            else if (this.renewalSummaryResponse.Premium2YearFlag == "true") {
                console.log(this.renewalSummaryResponse);
                this.breakUpData['totalPremium'] = this.renewalSummaryResponse.Premium2Year;
                this.breakUpData['basicPremium'] = this.renewalSummaryResponse.BasicPremium2Year;
                this.breakUpData['optionalCover'] = this.renewalSummaryResponse.TotalCoverPremium2Year;
                this.breakUpData['sublimit'] = this.renewalSummaryResponse.SublimitDiscount2Year;
                this.breakUpData['GST'] = this.renewalSummaryResponse.TotalGSTAmt2Year;
            }
            else if (this.renewalSummaryResponse.Premium3YearFlag == "true") {
                this.breakUpData['totalPremium'] = this.renewalSummaryResponse.Premium3Year;
                this.breakUpData['basicPremium'] = this.renewalSummaryResponse.BasicPremium3Year;
                this.breakUpData['optionalCover'] = this.renewalSummaryResponse.TotalCoverPremium3Year;
                this.breakUpData['sublimit'] = this.renewalSummaryResponse.SublimitDiscount3Year;
                this.breakUpData['GST'] = this.renewalSummaryResponse.TotalGSTAmt2Year;
            }
        }
        else {
            console.log(this.policyType.productId);
            this.breakUpData['totalPremium'] = this.renewalSummaryResponse.TotalPremium;
            this.breakUpData['basicPremium'] = this.renewalSummaryResponse.BasicPremium;
            this.breakUpData['salesTax'] = this.renewalSummaryResponse.TotalTax;
        }
        console.log("BreakUp Data", this.breakUpData);
    };
    HealthRenewalSummaryPage.prototype.getPrem = function (ev) {
        console.log(ev.target.id, this.renewalSummaryResponse);
        if (ev.target.id == "premOneYr") {
            console.log(this.renewalSummaryResponse, this.breakUpData);
            this.breakUpData['totalPremium'] = this.renewalSummaryResponse.Premium1Year;
            this.breakUpData['basicPremium'] = this.renewalSummaryResponse.BasicPremium1Year;
            this.breakUpData['optionalCover'] = this.renewalSummaryResponse.TotalCoverPremium1Year;
            this.breakUpData['sublimit'] = this.renewalSummaryResponse.SublimitDiscount1Year;
            this.breakUpData['GST'] = this.renewalSummaryResponse.TotalGSTAmt1Year;
            this.selectedCoveragePeriod = '1';
        }
        else if (ev.target.id == "premTwoYr") {
            console.log(this.renewalSummaryResponse);
            this.breakUpData['totalPremium'] = this.renewalSummaryResponse.Premium2Year;
            this.breakUpData['basicPremium'] = this.renewalSummaryResponse.BasicPremium2Year;
            this.breakUpData['optionalCover'] = this.renewalSummaryResponse.TotalCoverPremium2Year;
            this.breakUpData['sublimit'] = this.renewalSummaryResponse.SublimitDiscount2Year;
            this.breakUpData['GST'] = this.renewalSummaryResponse.TotalGSTAmt2Year;
            this.selectedCoveragePeriod = '2';
        }
        else if (ev.target.id == "premThreeYr") {
            this.breakUpData['totalPremium'] = this.renewalSummaryResponse.Premium3Year;
            this.breakUpData['basicPremium'] = this.renewalSummaryResponse.BasicPremium3Year;
            this.breakUpData['optionalCover'] = this.renewalSummaryResponse.TotalCoverPremium3Year;
            this.breakUpData['sublimit'] = this.renewalSummaryResponse.SublimitDiscount3Year;
            this.breakUpData['GST'] = this.renewalSummaryResponse.TotalGSTAmt2Year;
            this.selectedCoveragePeriod = '3';
        }
        console.log("BreakUp Data on change", this.breakUpData);
    };
    HealthRenewalSummaryPage.prototype.getNomineeAge = function (ev) {
        var timeDiff = Math.abs(Date.now() - new Date(ev.target.value).getTime());
        this.nomineeAge = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
        console.log(this.nomineeAge);
        if (this.nomineeAge < 18) {
            this.below18 = true;
        }
        else {
            this.below18 = false;
        }
    };
    HealthRenewalSummaryPage.prototype.payForRenewal = function (ev) {
        var _this = this;
        this.validateNomineeForm().then(function () {
            if (_this.isNomineeFormValid) {
                if (_this.nomineeAge < 18) {
                    _this.validateAppointeeForm();
                    if (_this.isAppointeeFormValid) {
                        if (_this.isDeclarationAccepted == false) {
                            _this.displayAlert('Please Accept Declaration');
                        }
                        else {
                            _this.payNow();
                        }
                        // Pay Now Function Call //
                    }
                    else {
                        _this.displayAlert('Please enter all appointee details');
                    }
                }
                else {
                    if (_this.isDeclarationAccepted == false) {
                        _this.displayAlert('Please Accept Declaration');
                    }
                    else {
                        _this.payNow();
                    }
                }
            }
            else {
                _this.displayAlert('Please enter all nominee details');
            }
        });
    };
    HealthRenewalSummaryPage.prototype.validateAdhaarNo = function (addharno) {
        var re = /^[0-9]{12}$/;
        return re.test(addharno);
    };
    HealthRenewalSummaryPage.prototype.validatePanNo = function (panno) {
        var re = /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
        return re.test(panno);
    };
    HealthRenewalSummaryPage.prototype.payNow = function () {
        var _this = this;
        if (this.aadharNo != undefined && this.aadharNo != null && this.aadharNo != '' && !this.validateAdhaarNo(this.aadharNo)) {
            this.displayAlert('Kindly Enter Valid Applicant Aadhaar Number');
        }
        else if (this.panNo != undefined && this.panNo != null && this.panNo != '' && !this.validatePanNo(this.panNo)) {
            this.displayAlert('Kindly Enter Valid Applicant Pan Card Number');
        }
        else {
            // this.cs.showLoader = true;
            if (this.aadharNo == undefined || this.aadharNo == null) {
                this.aadharNo == '';
            }
            if (this.panNo == undefined || this.panNo == null) {
                this.panNo == '';
            }
            if (this.policyType.productId != 30) {
                this.cs.showLoader = true;
                var relID = void 0, relName = void 0, relTitle = void 0, nomDate = void 0, appTitle = void 0, apprelID = void 0, apprelName = void 0, appDate = void 0;
                console.log("Bind this data", this.applNomBody);
                if (this.renewalNomForm.value.nomRelation == '') {
                    relID = '';
                    relName = '';
                }
                else {
                    relID = this.nomRela.RelationshipID;
                    relName = this.nomRela.RelationshipName;
                }
                if (this.renewalAppoForm.value.appoRelation == '' || this.renewalAppoForm.value.appoRelation == null || this.renewalAppoForm.value.appoRelation == undefined) {
                    apprelID = '';
                    apprelName = '';
                }
                else {
                    apprelID = this.appoRela.RelationshipID;
                    apprelName = this.appoRela.RelationshipName;
                }
                if (this.renewalNomForm.value.nomTitle == '') {
                    relTitle = '';
                }
                else {
                    relTitle = this.nomineeTitle.id;
                }
                if (this.renewalAppoForm.value.appoTitle == '' || this.renewalAppoForm.value.appoTitle == null || this.renewalAppoForm.value.appoTitle == undefined) {
                    appTitle = '';
                }
                else {
                    appTitle = this.appoTitleDetails.id;
                }
                if (this.applNomBody.AddOnCovers2And4 == undefined || this.applNomBody.AddOnCovers2And4 == null) {
                    this.applNomBody.AddOnCovers2And4 = '';
                }
                if (this.applNomBody.AddOnCovers2And4Flag == undefined || this.applNomBody.AddOnCovers2And4Flag == null) {
                    this.applNomBody.AddOnCovers2And4Flag = '';
                }
                if (this.applNomBody.AddOnCovers2And4Type == undefined || this.applNomBody.AddOnCovers2And4Type == null) {
                    this.applNomBody.AddOnCovers2And4Type = '';
                }
                if (this.applNomBody.AddOnCovers3 == undefined || this.applNomBody.AddOnCovers3 == null) {
                    this.applNomBody.AddOnCovers3 = '';
                }
                if (this.applNomBody.AddOnCovers3Flag == undefined || this.applNomBody.AddOnCovers3Flag == null) {
                    this.applNomBody.AddOnCovers3Flag = '';
                }
                if (this.applNomBody.AddOnCovers3Type == undefined || this.applNomBody.AddOnCovers3Type == null) {
                    this.applNomBody.AddOnCovers3Type = '';
                }
                if (this.applNomBody.AddOnCovers5 == undefined || this.applNomBody.AddOnCovers5 == null) {
                    this.applNomBody.AddOnCovers5 = '';
                }
                if (this.applNomBody.AddOnCovers5Flag == undefined || this.applNomBody.AddOnCovers5Flag == null) {
                    this.applNomBody.AddOnCovers5Flag = '';
                }
                if (this.applNomBody.AddOnCovers6 == undefined || this.applNomBody.AddOnCovers6 == null) {
                    this.applNomBody.AddOnCovers6 = '';
                }
                if (this.applNomBody.AddOnCovers6Flag == undefined || this.applNomBody.AddOnCovers6Flag == null) {
                    this.applNomBody.AddOnCovers6Flag = '';
                }
                if (this.renewalNomForm.value.nomDOB != undefined && this.renewalNomForm.value.nomDOB != null && this.renewalNomForm.value.nomDOB != '') {
                    nomDate = moment(this.renewalNomForm.value.nomDOB).format('DD-MMMM-YYYY');
                }
                else {
                    nomDate = '';
                }
                if (this.renewalAppoForm.value.appoDOB != undefined && this.renewalAppoForm.value.appoDOB != null && this.renewalAppoForm.value.appoDOB != '') {
                    appDate = moment(this.renewalAppoForm.value.appoDOB).format('DD-MMMM-YYYY');
                }
                else {
                    appDate = '';
                }
                if (this.applNomBody.AppointeeLastName == null || this.applNomBody.AppointeeLastName == undefined || this.applNomBody.AppointeeLastName == '') {
                    this.applNomBody.AppointeeLastName = '';
                }
                var data = {
                    "IsCustomerPID": "true",
                    "UserType": "Agent",
                    "ipaddress": config.ipAddress,
                    "IPGPayment": "true",
                    "PolicyNo": this.renewalSummaryResponse.PolicyNo,
                    "AddOnCovers1": this.applNomBody.AddOnCovers1,
                    "AddOnCovers1Flag": this.applNomBody.AddOnCovers1Flag,
                    "AddOnCovers2And4": this.applNomBody.AddOnCovers2And4,
                    "AddOnCovers2And4Flag": this.applNomBody.AddOnCovers2And4Flag,
                    "AddOnCovers2And4Type": this.applNomBody.AddOnCovers2And4Type,
                    "AddOnCovers3": this.applNomBody.AddOnCovers3,
                    "AddOnCovers3Flag": this.applNomBody.AddOnCovers3Flag,
                    "AddOnCovers3Type": this.applNomBody.AddOnCovers3Type,
                    "AddOnCovers5": this.applNomBody.AddOnCovers5,
                    "AddOnCovers5Flag": this.applNomBody.AddOnCovers5Flag,
                    "AddOnCovers6": this.applNomBody.AddOnCovers6,
                    "AddOnCovers6Flag": this.applNomBody.AddOnCovers6Flag,
                    "VoluntaryDeductible": this.applNomBody.VoluntaryDeductible,
                    "AgeOfEldest": this.applNomBody.AgeOfEldest,
                    "NoOfAdults": this.applNomBody.NoOfAdults,
                    "NoOfKids": this.applNomBody.NoOfKids,
                    "NomineeDOB": nomDate,
                    "NomineeName": this.renewalNomForm.value.nomName,
                    "NomineeRelationShip": relName,
                    "NomineeRelationShipID": relID,
                    "NomineeTitle": relTitle,
                    "IS_IBANK_RELATIONSHIP": "NO",
                    "APS_ID": "",
                    "CUSTOMER_REF_NO": "",
                    "SubLimitApplicable": this.applNomBody.sublimit,
                    "SumInsured": this.applNomBody.SumInsured,
                    "Tenure": this.selectedCoveragePeriod,
                    "PaymentMode": "RAZORPAY",
                    "CardType": "",
                    "ModeID": "0",
                    "CardHolderName": "",
                    "CardNumber": "",
                    "CVVNumber": "",
                    "CardExpiryMonth": "",
                    "CardExpiryYear": "",
                    "PANNumber": this.applNomBody.PANNumber,
                    "AadhaarNumber": this.applNomBody.AadhaarNumber,
                    "AppointeeLastName": this.applNomBody.AppointeeLastName,
                    "AppointeeTitleID": appTitle,
                    "AppointeeRelationShipID": apprelID,
                    "AppointeeRelationShipName": apprelName,
                    "AppointeeDOB": appDate,
                    "AddressLine1": this.applNomBody.AddressLine1,
                    "AddressLine2": this.applNomBody.AddressLine2,
                    "Landmark": "",
                    "CityID": this.applNomBody.CityID,
                    "StateID": this.applNomBody.StateID,
                    "PincodeID": this.applNomBody.PincodeID,
                    "EmailID": this.applNomBody.Email,
                    "MobileNo": this.applNomBody.Mobile,
                    "AreaVillageID": "0",
                    "isInsuredChange": this.isInsuredChange.toString(),
                    "isRenewalEdited": this.isRenewalEdited.toString(),
                    "Members": this.memberObj,
                    "GSTApplicable": "false",
                    "UINApplicable": "false",
                    "isGHD": "Yes"
                };
                var str = JSON.stringify(data); // to be used wherever necessary //
                console.log(str);
                localStorage.setItem('proposalRequest', str);
                this.cs.postWithParams('/api/Renewal/HealthPolicyRenewalPayment', str).then(function (res) {
                    _this.PPAPproposalData = res;
                    _this.cs.showLoader = false;
                    if (_this.PPAPproposalData.StatusCode == 0) {
                        _this.displayAlert(_this.PPAPproposalData.StatusMessage);
                    }
                    else {
                        var tempArray = [];
                        tempArray.push({ SavePolicy: _this.PPAPproposalData.FinalPolicy });
                        tempArray[0].SavePolicy[0].PF_CustomerID = _this.applNomBody.PFCustomerId;
                        localStorage.setItem('proposalResponse', JSON.stringify(tempArray[0]));
                        _this.router.navigateByUrl('payment');
                    }
                    console.log(res);
                }).catch(function (err) {
                    _this.cs.showLoader = false;
                    _this.displayAlert(err.error.ExceptionMessage);
                });
            }
            else {
                this.cs.showLoader = true;
                var tempDate = moment(this.renewalSummaryResponse.InsuredDetails[0].DateofBirth).format('YYYY-MMM-DD');
                this.requestBody = {
                    "IsCustomerPID": "true",
                    "UserType": "Agent",
                    "ipaddress": config.ipAddress,
                    "IPGPayment": "true",
                    "PolicyNo": this.renewalSummaryResponse.PolicyNo,
                    "DOB": tempDate,
                    "IS_IBANK_RELATIONSHIP": "NO",
                    "PaymentMode": "RAZORPAY",
                    "ModeID": "0",
                    "PANNumber": this.panNo,
                    "AadhaarNumber": this.aadharNo,
                    "GSTApplicable": this.renewalSummaryResponse.GSTApplicable.toString(),
                    "UINApplicable": "false"
                };
                var str = JSON.stringify(this.requestBody); // to be used wherever necessary //
                console.log(str);
                localStorage.setItem('proposalRequest', str);
                this.cs.postWithParams('/api/Renewal/PPAPPolicyRenewalPayment', str).then(function (res) {
                    _this.PPAPproposalData = res;
                    _this.cs.showLoader = false;
                    if (_this.PPAPproposalData.StatusCode == 0) {
                        _this.displayAlert(_this.PPAPproposalData.StatusMessage);
                    }
                    else {
                        var tempArray = [];
                        tempArray.push({ SavePolicy: _this.PPAPproposalData.FinalPolicy });
                        tempArray[0].SavePolicy[0].PF_CustomerID = _this.applNomBody.PFCustomerId;
                        localStorage.setItem('proposalResponse', JSON.stringify(tempArray[0]));
                        _this.router.navigateByUrl('payment');
                    }
                    console.log(res);
                }).catch(function (err) {
                    _this.cs.showLoader = false;
                    _this.displayAlert(err.error.ExceptionMessage);
                });
            }
        }
    };
    HealthRenewalSummaryPage.prototype.getNomTitle = function (ev) {
        this.nomineeTitle = this.titles.find(function (x) { return x.id == ev.target.value; });
    };
    HealthRenewalSummaryPage.prototype.validateNomineeForm = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.cs.isDataNullorUndefined(_this.renewalNomForm.value);
            if (_this.cs.validArray.indexOf("Invalid") > -1) {
                console.log("Invalid Data");
                _this.isNomineeFormValid = false;
            }
            else {
                console.log("Valid Data");
                _this.isNomineeFormValid = true;
            }
            resolve();
        });
    };
    HealthRenewalSummaryPage.prototype.validateAppointeeForm = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.cs.isDataNullorUndefined(_this.renewalAppoForm.value);
            if (_this.cs.validArray.indexOf("Invalid") > -1) {
                console.log("Invalid Data");
                _this.isAppointeeFormValid = false;
            }
            else {
                console.log("Valid Data");
                _this.isAppointeeFormValid = true;
            }
            resolve();
        });
    };
    HealthRenewalSummaryPage.prototype.showPlanDetails = function (ev) {
        this.isPlanDetails = !this.isPlanDetails;
        if (this.isPlanDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("planDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("planDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("planDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("planDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalSummaryPage.prototype.showApplicantDetails = function (ev) {
        this.isApplicantDetails = !this.isApplicantDetails;
        if (this.isApplicantDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("applDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("applDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("applDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("applDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalSummaryPage.prototype.showAppointeeDetails = function (ev) {
        this.isappointeeDetails = !this.isappointeeDetails;
        if (this.isappointeeDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("appointeeDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("appointeeDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("appointeeDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("appointeeDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalSummaryPage.prototype.showNomineeDetails = function (ev) {
        this.isNomineeDetails = !this.isNomineeDetails;
        if (this.isNomineeDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("nomineeDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("nomineeDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("nomineeDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("nomineeDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalSummaryPage.prototype.showInsured1Details = function (ev) {
        this.isInsured1 = !this.isInsured1;
        if (this.isInsured1) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("insured1").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured1").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("insured1").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured1").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalSummaryPage.prototype.showInsured2Details = function (ev) {
        this.isInsured2 = !this.isInsured2;
        if (this.isInsured2) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("insured2").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured2").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("insured2").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured2").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalSummaryPage.prototype.showInsured3Details = function (ev) {
        this.isInsured3 = !this.isInsured3;
        if (this.isInsured3) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("insured3").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured3").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("insured3").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured3").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalSummaryPage.prototype.showInsured4Details = function (ev) {
        this.isInsured4 = !this.isInsured4;
        if (this.isInsured4) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("insured4").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured4").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("insured4").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured4").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalSummaryPage.prototype.showInsured5Details = function (ev) {
        this.isInsured5 = !this.isInsured5;
        if (this.isInsured5) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("insured5").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured5").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("insured5").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured5").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalSummaryPage.prototype.showInsured6Details = function (ev) {
        this.isInsured6 = !this.isInsured6;
        if (this.isInsured6) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("insured6").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured6").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("insured6").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured6").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalSummaryPage.prototype.edit = function (ev) {
        this.renewalSummaryResponse.NomineeName = this.renewalNomForm.value.nomName;
        this.renewalSummaryResponse.NomineeDOB = moment(this.renewalNomForm.value.nomDOB).format('DD-MMM-YYYY');
        if (this.nomRela) {
            this.renewalSummaryResponse.NomineeRelationShip = this.nomRela.RelationshipID;
        }
        else {
            this.renewalSummaryResponse.NomineeRelationShip = '';
        }
        if (this.insuTitleDetails) {
            this.renewalSummaryResponse.NomineeTitle = this.insuTitleDetails.id;
        }
        else {
            this.renewalSummaryResponse.NomineeTitle = '';
        }
        if (this.nomineeAge < 18) {
            this.renewalSummaryResponse.AppointeeName = this.renewalAppoForm.value.appoName;
            this.renewalSummaryResponse.AppointeeDOB = moment(this.renewalAppoForm.value.appoDOB).format('DD-MMM-YYYY');
            if (this.appoRela) {
                this.renewalSummaryResponse.AppointeeRelationship = this.appoRela.RelationshipID;
            }
            else {
                this.renewalSummaryResponse.AppointeeRelationship = '';
            }
            if (this.appoTitleDetails) {
                this.renewalSummaryResponse.AppointeeTitle = this.appoTitleDetails.id;
            }
            else {
                this.renewalSummaryResponse.AppointeeTitle = '';
            }
        }
        console.log(this.renewalSummaryResponse);
        localStorage.setItem('isDataEdit', JSON.stringify('Yes'));
        localStorage.setItem('editedData', JSON.stringify(this.renewalSummaryResponse));
        this.router.navigateByUrl('health-renewal-proposal');
    };
    HealthRenewalSummaryPage.prototype.getInsuTitle = function (ev) {
        this.insuTitleDetails = this.titles.find(function (x) { return x.val == ev.target.value; });
        console.log(this.insuTitleDetails);
    };
    HealthRenewalSummaryPage.prototype.getAppoTitle = function (ev) {
        this.appoTitleDetails = this.titles.find(function (x) { return x.val == ev.target.value; });
        console.log(this.appoTitleDetails);
    };
    HealthRenewalSummaryPage.prototype.showBreakUp = function (ev) {
        console.log("Break Up", ev);
        this.isBreakUp = true;
        document.getElementById("myNav7").style.height = "100%";
    };
    HealthRenewalSummaryPage.prototype.close = function (ev) {
        this.isBreakUp = false;
        document.getElementById("myNav7").style.height = "0%";
    };
    HealthRenewalSummaryPage.prototype.displayAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HealthRenewalSummaryPage.prototype.getRelation = function () {
        var _this = this;
        this.cs.postWithParams('/api/healthmaster/GetHealthProposalRelationships?Product=CHI', '').then(function (res) {
            if (res.StatusCode == 1) {
                _this.nomRelations = res;
            }
            else {
            }
        }).catch(function (err) {
        });
    };
    HealthRenewalSummaryPage.prototype.getNomRel = function (ev) {
        console.log(ev.target.value, this.nomRelations);
        this.nomRela = this.nomRelations.NomineeAppointeeRelationship.find(function (x) { return x.RelationshipName == ev.target.value; });
        console.log(this.nomRela.RelationshipID);
    };
    HealthRenewalSummaryPage.prototype.getAppoRel = function (ev) {
        console.log(ev.target.value);
        this.appoRela = this.nomRelations.NomineeAppointeeRelationship.find(function (x) { return x.RelationshipName == ev.target.value; });
        console.log(this.appoRela.RelationshipID);
    };
    HealthRenewalSummaryPage.prototype.getDeclarationApproval = function (event) {
        this.isDeclarationAccepted = event.target.checked;
    };
    HealthRenewalSummaryPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    HealthRenewalSummaryPage = tslib_1.__decorate([
        Component({
            selector: 'app-health-renewal-summary',
            templateUrl: './health-renewal-summary.page.html',
            styleUrls: ['./health-renewal-summary.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, Router, AlertController])
    ], HealthRenewalSummaryPage);
    return HealthRenewalSummaryPage;
}());
export { HealthRenewalSummaryPage };
//# sourceMappingURL=health-renewal-summary.page.js.map