import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { config } from 'src/app/config.properties';
var SwapPage = /** @class */ (function () {
    function SwapPage(cs, router, toastController, loadingCtrl, activeRoute) {
        this.cs = cs;
        this.router = router;
        this.toastController = toastController;
        this.loadingCtrl = loadingCtrl;
        this.activeRoute = activeRoute;
    }
    SwapPage.prototype.ngOnInit = function () {
        this.getParamas();
    };
    SwapPage.prototype.getParamas = function () {
        var _this = this;
        this.showHome = false;
        this.cs.showSwapLoader = true;
        this.activeRoute.queryParams.forEach(function (params) {
            console.log("Par", params);
            _this.token = params.token;
            _this.plan = (params.productType).toUpperCase();
            localStorage.setItem('planFormURL', JSON.stringify(_this.plan));
            console.log(_this.plan);
            if (_this.token != undefined) {
                _this.getToken(_this.token).then(function () {
                    _this.getDisabledFeaturesList(_this.authToken).then(function () {
                        _this.getCustomerDetails(_this.authToken);
                    });
                });
            }
            else {
                _this.presentToast();
                _this.cs.showSwapLoader = false;
            }
        });
    };
    SwapPage.prototype.getToken = function (token) {
        var _this = this;
        console.log("Token", token);
        return new Promise(function (resolve) {
            var body = {
                TOKEN: token,
                IPARTNER_USER_ID: "",
                AMS_ID: ""
            };
            var str = JSON.stringify(body);
            console.log(str);
            _this.cs.showSwapLoader = true;
            _this.cs.getAuthToken('/api/common/AgentLogData', str).then(function (res) {
                console.log("", res);
                if (res != '' && res != 'FAILURE') {
                    _this.authToken = res;
                    localStorage.setItem('authToken', JSON.stringify(_this.authToken));
                    resolve();
                }
                else {
                    window.location.href = config.loginurl;
                }
            }).catch(function (err) {
                console.log("Error", err);
                if (err.status == 404) {
                    _this.presentToast();
                    window.location.href = config.loginurl;
                }
                _this.cs.showSwapLoader = false;
                resolve();
            });
        });
    };
    SwapPage.prototype.getDisabledFeaturesList = function (token) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.cs.showSwapLoader = true;
            _this.cs.getWithParams1('/api/Agent/GetDisableFeatures', token).then(function (res) {
                _this.disabledFeatureData = res.DisableResponseList;
                var isKeralacess = _this.disabledFeatureData.find(function (x) { return x.Key == 'isKeralaCessEnabled'; }).Value;
                localStorage.setItem('isKeralaCessEnabled', isKeralacess);
                resolve();
            });
        });
    };
    SwapPage.prototype.getCustomerDetails = function (token) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.cs.showSwapLoader = true;
            _this.cs.swapAppMaster('/api/agent/AgentAppMasterData', token).then(function (res) {
                console.log("Auth", res);
                _this.showHome = true;
                localStorage.setItem('userData', JSON.stringify(res));
                _this.agentData = res;
                if ((_this.agentData.MappedProduct.Health.isHealthBoosterMapped || _this.agentData.MappedProduct.Health.isHealthCHIMapped || _this.agentData.MappedProduct.Health.isHealthPPAPMapped) && ((_this.plan == 'HEALTH') || (_this.plan == 'HEALTHRENEWAL'))) {
                    console.log("Health Show");
                    if (_this.plan == 'HEALTH') {
                        _this.router.navigateByUrl('health');
                        _this.cs.showSwapLoader = false;
                        resolve();
                    }
                    else {
                        _this.router.navigateByUrl('health-renewal');
                        _this.cs.showSwapLoader = false;
                        resolve();
                    }
                }
                else if (_this.plan == 'PAYINSLIP') {
                    _this.router.navigateByUrl('health-renewal');
                    _this.cs.showSwapLoader = false;
                    resolve();
                }
                else if (_this.plan == 'PID') {
                    _this.router.navigateByUrl('pid');
                    _this.cs.showSwapLoader = false;
                    resolve();
                }
                else {
                    console.log("Travel Show");
                    _this.router.navigateByUrl('travel-quote');
                    _this.cs.showSwapLoader = false;
                    resolve();
                }
            });
        });
    };
    SwapPage.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Failed To autherized user...Please try again',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    SwapPage.prototype.home = function (ev) {
        this.cs.goToHome();
    };
    SwapPage = tslib_1.__decorate([
        Component({
            selector: 'app-swap',
            templateUrl: './swap.page.html',
            styleUrls: ['./swap.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, Router, ToastController, LoadingController, ActivatedRoute])
    ], SwapPage);
    return SwapPage;
}());
export { SwapPage };
//# sourceMappingURL=swap.page.js.map