import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SwapPage } from './swap.page';
var routes = [
    {
        path: '',
        component: SwapPage
    }
];
var SwapPageModule = /** @class */ (function () {
    function SwapPageModule() {
    }
    SwapPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [SwapPage]
        })
    ], SwapPageModule);
    return SwapPageModule;
}());
export { SwapPageModule };
//# sourceMappingURL=swap.module.js.map