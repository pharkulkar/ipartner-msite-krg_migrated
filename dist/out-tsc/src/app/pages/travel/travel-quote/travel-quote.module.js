import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TravelQuotePage } from './travel-quote.page';
// import { MDBBootstrapModule } from 'angular-bootstrap-md';
// import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
var routes = [
    {
        path: '',
        component: TravelQuotePage
    }
];
var TravelQuotePageModule = /** @class */ (function () {
    function TravelQuotePageModule() {
    }
    TravelQuotePageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                MatNativeDateModule,
                MatFormFieldModule,
                MatInputModule,
                MatDatepickerModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [TravelQuotePage]
        })
    ], TravelQuotePageModule);
    return TravelQuotePageModule;
}());
export { TravelQuotePageModule };
//# sourceMappingURL=travel-quote.module.js.map