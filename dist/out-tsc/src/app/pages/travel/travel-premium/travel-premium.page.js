import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { CommonService } from 'src/app/services/common.service';
var TravelPremiumPage = /** @class */ (function () {
    function TravelPremiumPage(router, location, cs) {
        this.router = router;
        this.location = location;
        this.cs = cs;
        this.isRecalculate = "false";
    }
    TravelPremiumPage.prototype.ngOnInit = function () {
        this.getData();
    };
    TravelPremiumPage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.request = JSON.parse(localStorage.getItem('quoteRequest'));
            _this.response = JSON.parse(localStorage.getItem('quoteResponse'));
            _this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
            console.log(_this.request);
            console.log(_this.response);
            console.log(_this.planDetails);
            resolve();
        });
    };
    TravelPremiumPage.prototype.convertToPolicy = function (ev) {
        console.log("Hello");
        this.router.navigateByUrl('travel-proposal');
    };
    TravelPremiumPage.prototype.recalculate = function (ev) {
        this.location.back();
        this.cs.isBackFormTravelPremium = true;
        this.isRecalculate = "true";
        localStorage.setItem('reCalculate', this.isRecalculate);
    };
    TravelPremiumPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    TravelPremiumPage = tslib_1.__decorate([
        Component({
            selector: 'app-travel-premium',
            templateUrl: './travel-premium.page.html',
            styleUrls: ['./travel-premium.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router, Location, CommonService])
    ], TravelPremiumPage);
    return TravelPremiumPage;
}());
export { TravelPremiumPage };
//# sourceMappingURL=travel-premium.page.js.map