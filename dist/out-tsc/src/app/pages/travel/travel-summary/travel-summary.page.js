import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { config } from 'src/app/config.properties';
var TravelSummaryPage = /** @class */ (function () {
    function TravelSummaryPage(router, cs) {
        this.router = router;
        this.cs = cs;
        this.isJourneyDetails = false;
        this.isInsuredDetails = false;
        this.isApplicantDetails = false;
        this.isInstaPay = false;
    }
    TravelSummaryPage.prototype.ngOnInit = function () {
        localStorage.removeItem('newPID');
        this.getData();
    };
    TravelSummaryPage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            localStorage.setItem('isRenewal', 'false');
            _this.request = JSON.parse(localStorage.getItem('quoteRequest'));
            _this.response = JSON.parse(localStorage.getItem('quoteResponse'));
            _this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
            _this.travelProp = JSON.parse(localStorage.getItem('proposalResponse'));
            _this.travelPropReq = JSON.parse(localStorage.getItem('proposalRequest'));
            _this.createCustReq = JSON.parse(localStorage.getItem('customerRequest'));
            if (_this.request.IsVisitingUSCanadaYes == "Yes") {
                _this.subPlanName = "USA or CANADA";
            }
            else if (_this.request.IsVisitingSCHENGENYes == "Yes") {
                _this.subPlanName = "SCHENGEN";
            }
            else if (_this.request.IsVisitingOtherCountries == "Yes") {
                _this.subPlanName = "OTHER";
            }
            console.log(_this.travelProp);
            console.log(_this.request);
            console.log(_this.response);
            console.log(_this.planDetails);
            console.log(_this.travelPropReq);
            console.log("Map Data", _this.createCustReq);
            if (_this.travelProp.SavePolicy[0].InstaDetails == null || _this.travelProp.SavePolicy[0].InstaDetails == '' || _this.travelProp.SavePolicy[0].InstaDetails == undefined) {
                _this.isInstaPay = false;
            }
            else {
                console.log(_this.travelProp.SavePolicy[0].InstaDetails.isInstaAvailable);
                _this.instaPaydata = _this.travelProp.SavePolicy[0].InstaDetails;
                if (_this.travelProp.SavePolicy[0].InstaDetails.isInstaAvailable) {
                    _this.isInstaPay = true;
                }
                else {
                    _this.isInstaPay = false;
                }
            }
            resolve();
        });
    };
    TravelSummaryPage.prototype.showJourneyDetails = function (ev) {
        console.log(ev);
        this.isJourneyDetails = !this.isJourneyDetails;
        if (this.isJourneyDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("journey").style.background = "url(" + imgUrl + ")";
            document.getElementById("journey").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("journey").style.background = "url(" + imgUrl + ")";
            document.getElementById("journey").style.backgroundRepeat = 'no-repeat';
        }
    };
    TravelSummaryPage.prototype.showInsuredDetails = function (ev) {
        this.isInsuredDetails = !this.isInsuredDetails;
        if (this.isInsuredDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("insured").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("insured").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured").style.backgroundRepeat = 'no-repeat';
        }
    };
    TravelSummaryPage.prototype.showApplicantDetails = function (ev) {
        this.isApplicantDetails = !this.isApplicantDetails;
        if (this.isApplicantDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("applicant").style.background = "url(" + imgUrl + ")";
            document.getElementById("applicant").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("applicant").style.background = "url(" + imgUrl + ")";
            document.getElementById("applicant").style.backgroundRepeat = 'no-repeat';
        }
    };
    TravelSummaryPage.prototype.payNow = function () {
        console.log("Prop Resp", this.travelProp);
        this.router.navigateByUrl('payment');
    };
    TravelSummaryPage.prototype.instaPay = function (ev) {
        console.log("Insta Pay", this.instaPaydata);
        document.getElementById("myNav2").style.height = "100%";
    };
    TravelSummaryPage.prototype.instaPayment = function (ev) {
        var _this = this;
        if (this.response.PremiumPayable < this.instaPaydata.Balance) {
            var body = {
                "PolicyID": this.travelProp.SavePolicy[0].PolicyID,
                "IPAddress": config.ipAddress,
                "UserRole": "AGENT",
                "IsIAS": ""
            };
            var str = JSON.stringify(body);
            console.log("STR", str);
            this.cs.showLoader = true;
            this.cs.postWithParams('/api/Payment/TravelInstaPayment', str).then(function (res) {
                console.log(res);
                if (res.Message == undefined || res.Message == '' || res.Message == null) {
                    console.log("Correct", res);
                }
                else {
                    console.log("Wrong", res);
                }
                _this.cs.showLoader = false;
            }).catch(function (err) {
                console.log("Error", err);
                _this.cs.showLoader = false;
            });
        }
        else {
            console.log("Insufficient Balance");
        }
    };
    TravelSummaryPage.prototype.close = function (ev) {
        document.getElementById("myNav2").style.height = "0%";
    };
    TravelSummaryPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    TravelSummaryPage = tslib_1.__decorate([
        Component({
            selector: 'app-travel-summary',
            templateUrl: './travel-summary.page.html',
            styleUrls: ['./travel-summary.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router, CommonService])
    ], TravelSummaryPage);
    return TravelSummaryPage;
}());
export { TravelSummaryPage };
//# sourceMappingURL=travel-summary.page.js.map