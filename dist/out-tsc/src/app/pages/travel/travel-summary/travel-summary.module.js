import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TravelSummaryPage } from './travel-summary.page';
var routes = [
    {
        path: '',
        component: TravelSummaryPage
    }
];
var TravelSummaryPageModule = /** @class */ (function () {
    function TravelSummaryPageModule() {
    }
    TravelSummaryPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [TravelSummaryPage]
        })
    ], TravelSummaryPageModule);
    return TravelSummaryPageModule;
}());
export { TravelSummaryPageModule };
//# sourceMappingURL=travel-summary.module.js.map