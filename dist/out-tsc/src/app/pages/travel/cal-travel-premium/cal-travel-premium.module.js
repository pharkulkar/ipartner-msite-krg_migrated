import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CalTravelPremiumPage } from './cal-travel-premium.page';
var routes = [
    {
        path: '',
        component: CalTravelPremiumPage
    }
];
var CalTravelPremiumPageModule = /** @class */ (function () {
    function CalTravelPremiumPageModule() {
    }
    CalTravelPremiumPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CalTravelPremiumPage]
        })
    ], CalTravelPremiumPageModule);
    return CalTravelPremiumPageModule;
}());
export { CalTravelPremiumPageModule };
//# sourceMappingURL=cal-travel-premium.module.js.map