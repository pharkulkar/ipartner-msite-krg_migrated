import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
var DashBoardPage = /** @class */ (function () {
    function DashBoardPage(router, toastCtrl) {
        this.router = router;
        this.toastCtrl = toastCtrl;
        this.isHealth = false;
        this.isTravel = false;
        this.isMotor = false;
        this.isTwoWheeler = false;
        this.isPayInSlips = true;
    }
    DashBoardPage.prototype.ngOnInit = function () {
        this.agentDetails = JSON.parse(localStorage.getItem('userData'));
        this.pageAuth(this.agentDetails);
    };
    DashBoardPage.prototype.pageAuth = function (data) {
        var _this = this;
        console.log(data.MappedProduct);
        var mappedProduct = data.MappedProduct;
        this.showHealth(mappedProduct).then(function () {
            _this.showTravel(mappedProduct).then(function () {
                _this.showCar(mappedProduct).then(function () {
                    _this.showTwoWheeler(mappedProduct);
                });
            });
        });
    };
    DashBoardPage.prototype.showHealth = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log("Health", data);
            if (data.Health.isHealthBoosterMapped || data.Health.isHealthCHIMapped || data.Health.isHealthPPAPMapped) {
                _this.isHealth = true;
            }
            resolve();
        });
    };
    DashBoardPage.prototype.showTravel = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log("Travel", data);
            if (data.Travel.isTRAVELINTERNATIONALMapped || data.Travel.isTRAVELSTUDENTMapped) {
                _this.isTravel = true;
            }
            resolve();
        });
    };
    DashBoardPage.prototype.showCar = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log("Car", data);
            if (data.Motor.isFWShopMapped || data.Motor.isFourWheelerMapped || data.Motor.isGCVMapped || data.Motor.isPCVMapped || data.Motor.isTWShopMapped) {
                _this.isMotor = true;
            }
            resolve();
        });
    };
    DashBoardPage.prototype.showTwoWheeler = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log("Two Wheeler", data);
            if (data.Motor.isTwoWheelerMapped) {
                _this.isTwoWheeler = true;
            }
            resolve();
        });
    };
    DashBoardPage.prototype.goToHealth = function () {
        this.router.navigateByUrl('health');
    };
    DashBoardPage.prototype.goToTravel = function () {
        this.router.navigateByUrl('travel-quote');
    };
    DashBoardPage.prototype.goToCar = function () {
        this.presentToast();
    };
    DashBoardPage.prototype.goToRenewal = function () {
        this.router.navigateByUrl('health-renewal');
    };
    DashBoardPage.prototype.goToSaveQuotes = function () {
        this.router.navigateByUrl("savequotes");
    };
    DashBoardPage.prototype.goToTwoWheeler = function () {
        this.presentToast();
    };
    DashBoardPage.prototype.goToPayInSlip = function () {
        this.router.navigateByUrl('pay-in-slips');
    };
    DashBoardPage.prototype.goToPID = function () {
        this.router.navigateByUrl('pid');
    };
    DashBoardPage.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: 'Work In Progress....',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    DashBoardPage = tslib_1.__decorate([
        Component({
            selector: 'app-dash-board',
            templateUrl: './dash-board.page.html',
            styleUrls: ['./dash-board.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router, ToastController])
    ], DashBoardPage);
    return DashBoardPage;
}());
export { DashBoardPage };
//# sourceMappingURL=dash-board.page.js.map