import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CommonService } from "../../../services/common.service";
import { AlertController } from '@ionic/angular';
import { LoadingService } from "../../../services/loading.service";
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
var PedQuestionaireComponent = /** @class */ (function () {
    function PedQuestionaireComponent(commonservice, modalController, routes, alertController, apiloading) {
        this.commonservice = commonservice;
        this.modalController = modalController;
        this.routes = routes;
        this.alertController = alertController;
        this.apiloading = apiloading;
        this.pedSelectedData = {};
    }
    PedQuestionaireComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.apiloading.present();
        this.commonservice.postWithParams('/api/Agent/GetPEDQuestion', '').then(function (res) {
            _this.result = res;
            _this.pedData = _this.result.Questions;
            for (var i = 0; i < _this.pedData.length; i++) {
                _this.pedData[i].isChecked = false;
            }
            console.log(_this.pedData);
            _this.apiloading.dismiss();
        }, function (err) {
            _this.presentAlert('Sorry Something went wrong');
            _this.apiloading.dismiss();
        });
    };
    // function to call alert //
    PedQuestionaireComponent.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PedQuestionaireComponent.prototype.messageAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Alert',
                            message: message,
                            buttons: [{
                                    text: 'Okay',
                                    handler: function () {
                                        _this.commonservice.goToHome();
                                    }
                                }]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PedQuestionaireComponent.prototype.gotoProposal = function () {
        this.modalController.dismiss();
        var isDisease = 0;
        localStorage.setItem('pedDetails', this.pedData);
        for (var i = 0; i < this.pedData.length; i++) {
            if (this.pedData[i].isChecked == true) {
                isDisease = isDisease + 1;
            }
        }
        if (isDisease > 0) {
            this.messageAlert('Online booking is not available for this proposal. Kindly contact the nearest branch or your MO for further assistance.');
        }
        else {
            this.routes.navigateByUrl('health-proposal');
        }
    };
    PedQuestionaireComponent.prototype.closeModal = function () {
        this.modalController.dismiss();
    };
    PedQuestionaireComponent = tslib_1.__decorate([
        Component({
            selector: 'app-ped-questionaire',
            templateUrl: './ped-questionaire.component.html',
            styleUrls: ['./ped-questionaire.component.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, ModalController, Router, AlertController, LoadingService])
    ], PedQuestionaireComponent);
    return PedQuestionaireComponent;
}());
export { PedQuestionaireComponent };
//# sourceMappingURL=ped-questionaire.component.js.map