import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CommonService } from "../../../services/common.service";
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { LoadingController } from '@ionic/angular';
var HealthBoosterPage = /** @class */ (function () {
    function HealthBoosterPage(cs, xml2JsonService, loadingCtrl) {
        this.cs = cs;
        this.xml2JsonService = xml2JsonService;
        this.loadingCtrl = loadingCtrl;
        this.healthMasterRequestXML = "<Request>\n\t<UserType>AGENT</UserType>\n\t<UserID>MTY0MTI1NDRfc011b2JtUWRmTDNPV3BReHlGTTJZZEx5YkZrVU1mSVVIVWpaT0UrT3cxVT0=</UserID>\n\t<SubType>19</SubType>\n  </Request>";
        this.submitted = false;
        this.age = 21;
    }
    HealthBoosterPage.prototype.ngOnInit = function () {
        this.getStateMaster();
        this.getHealthMaster();
        this.createQuoteForm();
    };
    HealthBoosterPage.prototype.getAdult = function (ev) {
        console.log(ev.detail.value);
        this.noOfAudult = ev.detail.value;
        var child = this.adultChildMap.get(ev.detail.value);
        this.childArray = child;
        console.log(child);
    };
    HealthBoosterPage.prototype.addOnYesNo = function (event, type) {
        if (!event.detail.checked) {
            if (type == 'AddOn2') {
                this.quoteForm.controls.Addon2Members['controls'].Member['controls'].AdultType1.setValue(null);
                this.quoteForm.controls.Addon2Members['controls'].Member['controls'].AdultType2.setValue(null);
                this.quoteForm.controls.AddOn2Varient.setValue(null);
            }
            if (type == 'AddOn3') {
                this.quoteForm.controls.Addon3Members['controls'].Member['controls'].AdultType1.setValue(null);
                this.quoteForm.controls.Addon3Members['controls'].Member['controls'].AdultType2.setValue(null);
                this.quoteForm.controls.AddOn3Varient.setValue(null);
            }
        }
    };
    HealthBoosterPage.prototype.getVDValues = function (ev) {
        console.log(ev.detail.value);
        var arr = this.vdMap.get(ev.detail.value);
        this.vDMapArray = arr;
        console.log(this.vDMapArray);
    };
    HealthBoosterPage.prototype.showLoading = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                spinner: 'lines',
                                // duration: 5000,
                                message: 'Please wait...',
                                translucent: true
                                // cssClass: 'custom-class custom-loading'
                            })];
                    case 1:
                        _a.loading = _b.sent();
                        return [4 /*yield*/, this.loading.present()];
                    case 2: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    HealthBoosterPage.prototype.getProductName = function (event) {
        var _this = this;
        this.showLoading();
        return new Promise(function (resolve) {
            _this.refreshForm(event.detail.value);
            // console.log(event.details.value);
            if (event.detail.value == "HBOOSTER_TOPUP") {
                _this.productCode = 19;
            }
            else {
                _this.productCode = 20;
            }
            _this.getHealthMaster();
            resolve();
        });
    };
    // getSoftCopy(event:any):Promise<any>{
    //   return new Promise((resolve:any) => {
    //     // console.log(event.details.value);
    //     if(event.detail.value == "topUpPlan"){
    //       this.productCode = 19;
    //     }else{
    //       this.productCode = 20;
    //     }
    //     this.getHealthMaster();
    //     resolve();
    //   });
    // }
    HealthBoosterPage.prototype.getStateMaster = function () {
        var _this = this;
        this.cs.postStateMaster('/api/MotorMaster/GetAllStates', '').then(function (res) {
            _this.stateMaster = res;
            console.log(_this.stateMaster);
        });
    };
    HealthBoosterPage.prototype.getHealthMaster = function () {
        var _this = this;
        var healthMasterRequestXML = "<Request>\n\t                                <UserType>" + "AGENT" + "</UserType>\n\t                                <UserID>MTY0MTI1NDRfc011b2JtUWRmTDNPV3BReHlGTTJZZEx5YkZrVU1mSVVIVWpaT0UrT3cxVT0=</UserID>\n\t                                <SubType>" + this.productCode + "</SubType>\n                                  </Request>";
        this.cs.post('/HealthMaster.svc/HealthPlanMaster', healthMasterRequestXML).then(function (res) {
            console.log("Response", res);
        }).catch(function (err) {
            if (err.status != 200) {
                console.log("OK");
            }
            _this.healthMasterResponse = _this.xml2JsonService.xmlToJson(err.error.text);
            // this.adultDataLength = this.healthMasterResponse.HealthPremiumResponse.AdultResponse.AdultDetails.length;
            // console.log(this.healthMasterResponse.HealthPremiumResponse.AdultResponse.AdultDetails.length);
            //get adult and child
            _this.adultChildMap = new Map();
            _this.healthMasterResponse.HealthPremiumResponse.AdultResponse.AdultDetails.forEach(function (adult) {
                // console.log("adult LENGTH", this.adultDataLength);
                console.log("Adult VALUE", adult.AdultValue);
                console.log("Child COUNT", adult.ChildDetails.Child.length);
                _this.adultChildMap.set(adult.AdultValue, adult.ChildDetails.Child);
                console.log("MAP", _this.adultChildMap);
            });
            // for VD details
            _this.vdMap = new Map();
            _this.healthMasterResponse.HealthPremiumResponse.SumInsuredDetails.SumInsured.forEach(function (amount) {
                console.log("VD Amount", amount);
                console.log("VD KEY", amount.SumAmount);
                console.log("VD VALUE", amount.VDValues.HealthVoluntaryDedutible);
                _this.vdMap.set(amount.SumAmount, amount.VDValues.HealthVoluntaryDedutible);
                console.log("VD MAp", _this.vdMap);
            });
            _this.loading.dismiss();
        });
    };
    // getChild()
    // {
    //   this.healthMasterResponse.HealthPremiumResponse.AdultResponse.AdultDetails[this.i].AdultValue==this.adultValue;
    //   console.log(this.adultValue);
    // }
    HealthBoosterPage.prototype.getDate = function (event) {
        console.log(event.detail.value);
        this.dob = moment(event.detail.value).format('DD-MMM-YYYY');
        console.log(this.dob);
        this.quoteForm.patchValue({ "Adult1Age": this.dob });
    };
    HealthBoosterPage.prototype.getDate1 = function (event) {
        console.log(event.detail.value);
        this.dob1 = moment(event.detail.value).format('DD-MMM-YYYY');
        console.log(this.dob1);
        this.quoteForm.patchValue({ "Adult2Age": this.dob1 });
    };
    HealthBoosterPage.prototype.createQuoteForm = function () {
        this.quoteForm = new FormGroup({
            productName: new FormControl(),
            state: new FormControl(),
            adultNo: new FormControl(),
            childNo: new FormControl(),
            annualSumInsured: new FormControl(),
            VDValues: new FormControl(),
            Adult1Age: new FormControl(),
            Adult2Age: new FormControl(),
            eldestMemAge: new FormControl(),
            deductibleSumInsured: new FormControl(),
            AddOn1: new FormControl(),
            AddOn1Varient: new FormControl(),
            AddOn2: new FormControl(),
            AddOn2Varient: new FormControl(),
            Addon2Members: new FormGroup({
                Member: new FormGroup({
                    AdultType1: new FormControl(),
                    AdultType2: new FormControl(),
                })
            }),
            AddOn3: new FormControl(),
            AddOn3Varient: new FormControl(),
            Addon3Members: new FormGroup({
                Member: new FormGroup({
                    AdultType1: new FormControl(),
                    AdultType2: new FormControl(),
                })
            }),
            SoftCopy: new FormControl()
        });
    };
    HealthBoosterPage.prototype.refreshForm = function (prodName) {
        this.quoteForm = new FormGroup({
            productName: new FormControl(prodName),
            state: new FormControl(),
            adultNo: new FormControl(),
            childNo: new FormControl(),
            annualSumInsured: new FormControl(),
            VDValues: new FormControl(),
            Adult1Age: new FormControl(),
            Adult2Age: new FormControl(),
            eldestMemAge: new FormControl(),
            deductibleSumInsured: new FormControl(),
            AddOn1: new FormControl(),
            AddOn1Varient: new FormControl(),
            AddOn2: new FormControl(),
            AddOn2Varient: new FormControl(),
            Addon2Members: new FormGroup({
                Member: new FormGroup({
                    AdultType1: new FormControl(),
                    AdultType2: new FormControl(),
                })
            }),
            AddOn3: new FormControl(),
            AddOn3Varient: new FormControl(),
            Addon3Members: new FormGroup({
                Member: new FormGroup({
                    AdultType1: new FormControl(),
                    AdultType2: new FormControl(),
                })
            }),
            SoftCopy: new FormControl()
        });
    };
    HealthBoosterPage.prototype.changeAddonValue = function (AddonMember) {
        if (this.quoteForm.controls[AddonMember]['controls'].Member['controls'].AdultType1.value) {
            this.quoteForm.controls[AddonMember]['controls'].Member['controls'].AdultType1.setValue('Adult1');
        }
        else {
            this.quoteForm.controls[AddonMember]['controls'].Member['controls'].AdultType1.setValue(null);
        }
        if (this.quoteForm.controls[AddonMember]['controls'].Member['controls'].AdultType2.value) {
            this.quoteForm.controls[AddonMember]['controls'].Member['controls'].AdultType2.setValue('Adult2');
        }
        else {
            this.quoteForm.controls[AddonMember]['controls'].Member['controls'].AdultType2.setValue(null);
        }
    };
    HealthBoosterPage.prototype.calculateQuote = function (formData1) {
        var _this = this;
        var formData = formData1.value;
        Object.keys(this.quoteForm.value.Addon2Members.Member).map(function (member) {
            _this.changeAddonValue('Addon2Members');
        });
        Object.keys(this.quoteForm.value.Addon3Members.Member).map(function (member) {
            _this.changeAddonValue('Addon3Members');
        });
        console.log(this.quoteForm.value);
        var xmlRequest1 = "<HBoosterRequest><ProductType>HBOOSTER</ProductType><UserID>MTY0MTI1NDRfc011b2JtUWRmTDNPV3BReHlGTTJZZEx5YkZrVU1mSVVIVWpaT0UrT3cxVT0=</UserID><UserType>Agent</UserType><ipaddress>MOBILE-GENESYS</ipaddress><NoOfAdults>" + formData.adultNo + "</NoOfAdults><NoOfKids>" + formData.childNo + "</NoOfKids><Adult1Age>" + formData.Adult1Age + "</Adult1Age><Adult2Age>" + formData.Adult2Age + "</Adult2Age><ProductName>" + formData.productName + "</ProductName><InsuredAmount>" + formData.annualSumInsured + "</InsuredAmount><VolDedutible>" + formData.VDValues + "</VolDedutible><SoftLessCopy>" + formData.SoftCopy + "</SoftLessCopy><IsJammuKashmir>false</IsJammuKashmir><AddOn1>" + formData.AddOn1 + "</AddOn1><AddOn1Varient>" + formData.AddOn1Varient + "</AddOn1Varient><AddOn2>" + formData.AddOn2 + "</AddOn2><AddOn2Varient>" + formData.AddOn2Varient + "</AddOn2Varient><Addon2Members><Member><AdultType>Adult1</AdultType></Member></Addon2Members><AddOn3>" + formData.AddOn3 + "</AddOn3><AddOn3Varient>" + formData.AddOn3Varient + "</AddOn3Varient><Addon3Members><Member><AdultType>" + formData.AdultType1 + "</AdultType></Member></Addon3Members><GSTStateCode></GSTStateCode><GSTStateName>" + formData.state + "</GSTStateName><YearPremium>3</YearPremium></HBoosterRequest>";
        // let xmlRequest = `<HBoosterRequest><ProductType>HBOOSTER</ProductType><UserID>MTY0MTI1NDRfc011b2JtUWRmTDNPV3BReHlGTTJZZEx5YkZrVU1mSVVIVWpaT0UrT3cxVT0=</UserID><UserType>Agent</UserType><ipaddress>MOBILE-GENESYS</ipaddress><NoOfAdults>1</NoOfAdults><NoOfKids>0</NoOfKids><Adult1Age>11-Apr-1978</Adult1Age><Adult2Age></Adult2Age><ProductName>HBOOSTER_TOPUP</ProductName><InsuredAmount>1000000</InsuredAmount><VolDedutible>400000</VolDedutible><SoftLessCopy>false</SoftLessCopy><IsJammuKashmir>false</IsJammuKashmir><AddOn1>true</AddOn1><AddOn1Varient>GOLD</AddOn1Varient><AddOn2>true</AddOn2><AddOn2Varient>SILVER</AddOn2Varient><Addon2Members><Member><AdultType>Adult1</AdultType></Member></Addon2Members><AddOn3>false</AddOn3><AddOn3Varient></AddOn3Varient><Addon3Members></Addon3Members><GSTStateCode></GSTStateCode><GSTStateName>MAHARASHTRA</GSTStateName><YearPremium>3</YearPremium></HBoosterRequest>`;
        console.log(xmlRequest1);
        this.cs.post('/Quote.svc/GetQuote/Health/NThiM2E5NWMtZTkyMy00MWEyLWIwOWMtZGFhOWFiYTJiZDNk', xmlRequest1).then(function (response) {
            console.log(_this.xml2JsonService.xmlToJson(response));
        }).catch(function (error) {
            if (error.status != 200) {
                console.log("OK");
            }
            _this.quateTotalPremium = _this.xml2JsonService.xmlToJson(error.error.text);
            console.log(_this.quateTotalPremium.QuoteResponse.HealthDetails.HealthOutputResponse.TotalPremium);
        });
    };
    HealthBoosterPage.prototype.getSubType = function (data) {
        console.log("Selected Data", data);
    };
    // generateQuote()
    // {
    //   let postData=
    //   {
    //     'state':this.selectedState,
    //   }
    //   console.log(postData);
    // }
    HealthBoosterPage.prototype.changesoftcopyVal = function (ev) {
        console.log(ev.detail.value);
    };
    HealthBoosterPage = tslib_1.__decorate([
        Component({
            selector: 'app-health-booster',
            templateUrl: './health-booster.page.html',
            styleUrls: ['./health-booster.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, XmlToJsonService, LoadingController])
    ], HealthBoosterPage);
    return HealthBoosterPage;
}());
export { HealthBoosterPage };
//# sourceMappingURL=health-booster.page.js.map