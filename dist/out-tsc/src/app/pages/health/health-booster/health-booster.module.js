import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HealthBoosterPage } from './health-booster.page';
// import { Subscription } from 'rxjs';
var routes = [
    {
        path: '',
        component: HealthBoosterPage
    }
];
var HealthBoosterPageModule = /** @class */ (function () {
    function HealthBoosterPageModule() {
    }
    HealthBoosterPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [HealthBoosterPage]
        })
    ], HealthBoosterPageModule);
    return HealthBoosterPageModule;
}());
export { HealthBoosterPageModule };
//# sourceMappingURL=health-booster.module.js.map