import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PremiumPage } from './premium.page';
//import {PedQuestionaireComponent} from '../ped-questionaire/ped-questionaire.component'
var routes = [
    {
        path: '',
        component: PremiumPage
    }
];
var PremiumPageModule = /** @class */ (function () {
    function PremiumPageModule() {
    }
    PremiumPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PremiumPage],
        })
    ], PremiumPageModule);
    return PremiumPageModule;
}());
export { PremiumPageModule };
//# sourceMappingURL=premium.module.js.map