import * as tslib_1 from "tslib";
import { Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { EcryDecryService } from 'src/app/services/ecry-decry.service';
import { ModalController, ToastController } from '@ionic/angular';
import { LoadingService } from "../../../services/loading.service";
import { AlertController } from '@ionic/angular';
var SearchCustComponent = /** @class */ (function () {
    function SearchCustComponent(cs, toastCtrl, modalCtrl, alertController, xml2Json, enCnService, apiloading) {
        this.cs = cs;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.alertController = alertController;
        this.xml2Json = xml2Json;
        this.enCnService = enCnService;
        this.apiloading = apiloading;
        this.searchData = [];
        this.isShowUserList = false;
        this.isPfSearch = false;
        this.newCustomerEnable = new EventEmitter();
    }
    SearchCustComponent.prototype.ngOnInit = function () {
        this.createForm();
        this.agentData = JSON.parse(localStorage.getItem('userData'));
        console.log("Agent Data", this.agentData);
    };
    SearchCustComponent.prototype.createForm = function () {
        this.searchCustForm = new FormGroup({
            custName: new FormControl(),
            custEmailId: new FormControl(),
            custMobileNo: new FormControl(),
            custID: new FormControl()
        });
    };
    // searchCustomer(form:any){
    // let body = `<SearchCustomer>
    //   <strPassKey>`+this.agentData.EnPassKey+`</strPassKey>
    //   <UserID>`+this.agentData.UserID+`</UserID>
    //   <CustomerName>`+form.value.custName+`</CustomerName>
    //   <MobileNo>`+form.value.custMobileNo+`</MobileNo>
    //   <EmailID>`+form.value.custEmailId+`</EmailID>
    //   </SearchCustomer>`;
    //   console.log(body);
    //   // let requestBody = this.ErDrService.encrypt(body);
    //   let requestBody = "\""+"uZoREihVEi%2FlvnsaYHjEW6lXQ37QcGK877a8tasnp6eRXvw5UPu4xZ6X9LGKSr6WvyOXuaqzK5lRhkrbj4DD764o6mgN9SDYxF02sqpMsdFQAsjtap9VhosPne%2FT%2ByreHS7D6tcj24POBpl8cFeU98Wj7HKDE3lZBPjHo2UZrUVcRN8dCoCMjJk1KpqAKEqCX3nplqzAzxAkQNktTurRbMKDruXJ2dCSjtJEx7PppXLEAIMIT49bPkrHhsaho%2BoagWIIJtOERCzOjMWgg7qs7gGTlGaTz4vopQn6e5t6Ka%2F9%2FuP0krKh8WbxlT5kd%2FB2HHJ499wKqmI22%2BvETeKS71DBjALvp%2FyAeNKtlf0EPpY%3D"+"\"";    
    //   console.log("ENc Data", requestBody);
    //   this.cs.postCustSearch('/Customer.svc/SearchCustomerV1',requestBody).then((res) => {
    //     console.log("Cust Res",res);
    //   }).catch((err:any) => {
    //     this.data = this.xml2Json.xmlToJson(err.error.text);
    //     this.searchData = this.data.CustomerDetailsResponse.CustDetails.CustomerDetailsOutputResponse;
    //     console.log(this.searchData);
    //     if(this.searchData.length > 0){
    //       this.isShowUserList = true;
    //     }else{
    //       this.isShowUserList = false;
    //     }
    //   });
    // }
    SearchCustComponent.prototype.formValidate = function (form) {
        var message;
        if ((this.searchCustForm.value.custName == undefined || this.searchCustForm.value.custName == null || this.searchCustForm.value.custName == '')
            && (this.searchCustForm.value.custEmailId == undefined || this.searchCustForm.value.custEmailId == null || this.searchCustForm.value.custEmailId == '')
            && (this.searchCustForm.value.custMobileNo == undefined || this.searchCustForm.value.custMobileNo == null || this.searchCustForm.value.custMobileNo == '')
            && (this.searchCustForm.value.custID == undefined || this.searchCustForm.value.custID == null || this.searchCustForm.value.custID == '')) {
            this.presentAlert('Please enter atleast one field for searching');
            return false;
        }
        else {
            return true;
        }
    };
    SearchCustComponent.prototype.presentToast = function (msg) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    SearchCustComponent.prototype.searchCustomer = function (form) {
        var _this = this;
        var valid = this.formValidate(form);
        if (valid) {
            this.cs.showLoader = true;
            if (form.value.custName == null || form.value.custName == undefined) {
                form.value.custName = '';
            }
            if (form.value.custEmailId == null || form.value.custEmailId == undefined) {
                form.value.custEmailId = '';
            }
            if (form.value.custMobileNo == null || form.value.custMobileNo == undefined) {
                form.value.custMobileNo = '';
            }
            if (form.value.custID == null || form.value.custID == undefined) {
                form.value.custID = '';
            }
            var request = {
                "CustomerName": form.value.custName,
                "CustomerEmail": form.value.custEmailId,
                "CustomerMobile": form.value.custMobileNo,
                "CustomerStateCode": form.value.custID
            };
            var str = JSON.stringify(request);
            if (form.value.custID != null && form.value.custID != undefined && form.value.custID != '') {
                this.cs.postWithParams('/api/Customer/FetchCustomerDetails?pFCustomerID=' + form.value.custID, form.value.custID).then(function (res) {
                    var temapArray = [];
                    if (res.data != null) {
                        temapArray.push(res.data);
                        _this.searchData = temapArray;
                        if (_this.searchData.length > 0) {
                            _this.isShowUserList = true;
                            _this.isPfSearch = true;
                            _this.searchType = '1';
                        }
                        else {
                            _this.isShowUserList = false;
                        }
                        _this.cs.showLoader = false;
                    }
                    else {
                        _this.isShowUserList = false;
                        if (res.StatusMessage != undefined) {
                            _this.presentAlert(res.StatusMessage);
                        }
                        _this.cs.showLoader = false;
                    }
                }, function (err) {
                    _this.presentAlert(err.error.ExceptionMessage);
                    _this.cs.showLoader = false;
                });
            }
            else {
                this.cs.postWithParams('/api/Customer/SearchCustomer', str).then(function (res) {
                    console.log("search user", res);
                    _this.searchData = res;
                    if (res.length > 0) {
                        if (_this.searchData.length > 0) {
                            _this.isShowUserList = true;
                            _this.cs.showLoader = false;
                            _this.isPfSearch = false;
                            _this.searchType = '0';
                        }
                        else {
                            _this.isShowUserList = false;
                        }
                        _this.cs.showLoader = false;
                    }
                    else {
                        _this.isShowUserList = false;
                        if (res.StatusMessage != undefined) {
                            _this.presentAlert(res.StatusMessage);
                        }
                        _this.cs.showLoader = false;
                    }
                }, function (err) {
                    _this.presentAlert(err.error.ExceptionMessage);
                    _this.cs.showLoader = false;
                });
            }
        }
    };
    SearchCustComponent.prototype.selectedUser = function (data) {
        console.log(data);
        this.selectedData = data;
        localStorage.setItem('selectedCust', this.selectedData);
        this.modalCtrl.dismiss(this.selectedData, this.searchType);
    };
    SearchCustComponent.prototype.closeModal = function () {
        this.modalCtrl.dismiss();
        //this.newCustomerEnable = true;
        // this.newCustomerEnable.emit(true);
    };
    SearchCustComponent.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], SearchCustComponent.prototype, "newCustomerEnable", void 0);
    SearchCustComponent = tslib_1.__decorate([
        Component({
            selector: 'app-search-cust',
            templateUrl: './search-cust.component.html',
            styleUrls: ['./search-cust.component.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, ToastController, ModalController, AlertController, XmlToJsonService, EcryDecryService, LoadingService])
    ], SearchCustComponent);
    return SearchCustComponent;
}());
export { SearchCustComponent };
//# sourceMappingURL=search-cust.component.js.map