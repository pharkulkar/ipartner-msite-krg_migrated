import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController, ModalController, ToastController } from '@ionic/angular';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { CommonService } from 'src/app/services/common.service';
import { FormGroup, FormControl } from '@angular/forms';
import * as _ from 'underscore';
import * as moment from 'moment';
import { Location } from '@angular/common';
import { PedQuestionaireComponent } from '../ped-questionaire/ped-questionaire.component';
import { config } from 'src/app/config.properties';
var CalHealthPremiumPage = /** @class */ (function () {
    function CalHealthPremiumPage(routes, cs, xml2Json, loadingCtrl, toastCtrl, router, alertCtrl, _location, modalController) {
        this.routes = routes;
        this.cs = cs;
        this.xml2Json = xml2Json;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this._location = _location;
        this.modalController = modalController;
        this.showCHI = false;
        this.showHealthBooster = false;
        this.showPPAP = false;
        this.adult1checked = false;
        this.adult2checked = false;
        this.adultArray = [];
        this.showAdultTable = false;
        this.adult2AgeArray = [];
        this.toggleButton = false;
        this.showAddOnCover1 = false;
        this.showAddOnCover3 = false;
        this.showAddOnCover5 = false;
        this.showAddOnCover6 = false;
        this.addOnCoverBasedOnAge = false;
        this.button1 = false;
        this.button2 = false;
        this.button3 = false;
        this.subilimitA = false;
        this.subilimitB = false;
        this.subilimitC = false;
        this.subilimitN = false;
        this.isJammuKashmir = false;
        this.isKeralaCess = false;
        this.show = false;
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.annualSumInsuredArray = [];
        this.customPopoverOptions = {};
        this.showHB = false;
        this.showaddon2HB = false;
        this.showaddon3HB = false;
        this.isJammuKashmirHB = false;
        this.isKeralaCessHB = false;
        this.dataValuesHB = [];
        this.noOfAudultHB = 0;
        this.noOfAudult1HB = 0;
        this.submittedHB = false;
        this.ageHB = 21;
        this.showAddOn1HB = false;
        this.customPopoverOptionsHB = {};
        this.statusHB = [];
        this.showSliderHB = false;
        this.adult1checkedHB = false;
        this.adult2checkedHB = false;
        this.index = 0;
        this.moreDetails = false;
        this.isPPAPPC = false;
        this.isPPA = false;
        this.isPPC = false;
        this.isEarning = false;
        this.canShowOptions = false;
        this.canShowOptionsDetails = false;
        this.optionsArray = [];
        this.OptionDetails = [];
        this.OptionsDetailsArray = [];
        this.showPPASumInsured = false;
        this.lumpsum = [];
        this.showPPCSumInsured = false;
        this.disableQuote = true;
        this.isRegWithGST = false;
        this.isQuoteDetails = true;
        this.isPremiumDetails = true;
        this.isQuoteGenerated = false;
        this.isRegWithGSTCHI = false;
        this.isRegWithGSTHB = false;
        this.isChecked = false;
        this.isChecked1 = false;
        this.showSlider = false;
        this.showMaxInsured = true;
        this.inputLoadingPlan = false;
        // @ViewChild('tenureCheck2')tenureCheck2;
        //Premium variables
        this.isHealthBooster = false;
        this.isCHI = false;
        this.isPPAP = false;
        this.isCHIRecalculate = false;
        this.isHBoosterRecalculate = false;
        this.isPPAPRecalculate = false;
        // select dropdown popvers configurations //
        this.productPopoveroptions = {
            header: 'Select Product Sub Type'
        };
        this.occuptionPopoverOptions = {
            header: 'Select Occupation'
        };
        this.statePopoverOptions = {
            header: 'Select State'
        };
        this.annualIncomePopoverOptions = {
            header: 'Select Annual Income'
        };
        this.policyDurationPopoverOptions = {
            header: 'Select Policy Duration'
        };
        this.sumInsuredPopoverOptions = {
            header: 'Select Sum Insured'
        };
        this.sumOptipnsPopoverOptions = {
            header: 'Select Options'
        };
        this.payoutAmountPopoverOptions = {
            header: 'Select Payout Amount'
        };
        this.payoutTenurePopoverOptions = {
            header: 'Select Payout Tenure'
        };
        this.lumSumPopoverOptions = {
            header: 'Select Lump Sum Amount'
        };
    }
    CalHealthPremiumPage.prototype.ngOnInit = function () {
        var _this = this;
        // this.routes.navigate(['../health/complete-health-insurance']);
        // this.cs.postWithParams('/api/agent/AgentAppMasterData').then((res:any)=> {
        //   console.log("res cal health");
        //   console.log(res);
        //   localStorage.setItem('userData', JSON.stringify(res));
        //   this.agentData = res;
        // });
        this.agentData = JSON.parse(localStorage.getItem('userData'));
        var iskeralaCessFeature = localStorage.getItem('isKeralaCessEnabled');
        if (iskeralaCessFeature == 'Y') {
            this.isKeralaCessEnabled = true;
        }
        else {
            this.isKeralaCessEnabled = false;
        }
        console.log(this.agentData.MappedProduct.Health.isHealthBoosterMapped);
        localStorage.removeItem('requestForProp');
        localStorage.removeItem('Request');
        if (this.agentData.MappedProduct.Health.isHealthCHIMapped) {
            this.showCHI = true;
            this.showHealthBooster = false;
            this.showPPAP = false;
        }
        else if (this.agentData.MappedProduct.Health.isHealthBoosterMapped) {
            this.showCHI = false;
            this.showHealthBooster = true;
            this.showPPAP = false;
        }
        else if (this.agentData.MappedProduct.Health.isHealthPPAPMapped) {
            this.showCHI = false;
            this.showHealthBooster = false;
            this.showPPAP = true;
        }
        else {
            this.presentAlert('Sorry Health is not mapped...');
        }
        this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then(function (res) {
            _this.states = res;
            _this.states = _.sortBy(_this.states, 'StateName');
            _this.selectedState = 55;
        }).catch(function (err) {
            _this.presentAlert(err.error.ExceptionMessage);
        });
        this.createQuoteForm();
        this.getStateMaster();
        this.getStateMasterHB();
        this.createQuoteFormHB();
        this.selectedSubType = '';
        this.selectedOccupationType = '';
        this.maxRiskDate = moment().format('YYYY-MM-DD');
        var today = new Date();
        this.memmaxDOBHB = new Date(today.setFullYear(new Date().getFullYear() - 21));
        this.meminDOBHB = new Date(today.setFullYear(new Date().getFullYear() - 100));
        this.memmaxDOBHB = moment(this.memmaxDOBHB).format('YYYY-MM-DD');
        this.meminDOBHB = moment(this.meminDOBHB).format('YYYY-MM-DD');
    };
    CalHealthPremiumPage.prototype.navigate = function (type) {
        if (type == 0) {
            //this.routes.navigate(['../health/complete-health-insurance']);
            this.showCHI = true;
            this.showHealthBooster = false;
            this.showPPAP = false;
        }
        else if (type == 1) {
            //this.routes.navigate(['../health/health-booster']);
            this.showCHI = false;
            this.showHealthBooster = true;
            this.showPPAP = false;
        }
        else {
            //this.routes.navigate(['../health/ppap']);
            this.showCHI = false;
            this.showHealthBooster = false;
            this.showPPAP = true;
        }
    };
    // CHI Code Begins //
    CalHealthPremiumPage.prototype.onFinishSliding = function (e) {
        console.log(e.from_value);
    };
    CalHealthPremiumPage.prototype.isGSTRegCHI = function (val) {
        if (val) {
            this.isRegWithGSTCHI = true;
        }
        else {
            this.isRegWithGSTCHI = false;
        }
    };
    CalHealthPremiumPage.prototype.isGSTRegHB = function (val) {
        if (val) {
            this.isRegWithGSTHB = true;
        }
        else {
            this.isRegWithGSTHB = false;
        }
    };
    CalHealthPremiumPage.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CalHealthPremiumPage.prototype.getAdult = function (ev) {
        var _this = this;
        this.noOfAudult = ev.target.value;
        this.showSlider = true;
        console.log("Get Adult", this.noOfAudult, ev);
        var audult1 = document.getElementById('oneAdult1');
        var audult2 = document.getElementById('oneAdult2');
        var child1 = document.getElementById('oneChild1');
        var child2 = document.getElementById('oneChild2');
        if (this.noOfAudult == 2) {
            audult1.checked = true;
            audult2.checked = true;
        }
        else if (this.noOfAudult == 1) {
            if (audult1.checked == true && audult2.checked == false && this.noOfAudult == 1) {
                this.noOfAudult = 1;
                audult1.checked = true;
                audult2.checked = false;
            }
            else if (audult1.checked == false && audult2.checked == true && this.noOfAudult == 1) {
                this.noOfAudult = 1;
                audult2.checked = false;
                audult1.checked = true;
            }
            else {
                this.noOfAudult = 0;
                child1.checked = true;
                child2.checked = false;
            }
        }
        else if (this.noOfAudult == 0) {
            audult1.checked = false;
            audult2.checked = false;
        }
        else {
            this.noOfAudult = 0;
            audult1.checked = false;
            audult2.checked = false;
        }
        if (this.noOfAudult > 0) {
            this.showAdultTable = true;
            this.addOnCoverBasedOnAge = false;
        }
        else {
            this.showAdultTable = false;
        }
        var child = this.adultChildMap.get(ev.target.value);
        this.childArray = child;
        var adultAge = this.adultAgeMap.get(ev.target.value);
        this.adultAgeArray = adultAge;
        if (this.noOfAudult == 2 && ev.target.checked == true) {
            this.childArray = this.adultChildMap.get(ev.target.value);
            this.healthMasterResponse.AdultResponse.forEach(function (adult) {
                adult.checked = true;
            });
        }
        if (this.noOfAudult == 2 && ev.target.checked == false) {
            this.childArray = this.adultChildMap.get("1");
        }
        else if (this.noOfAudult == 1 && ev.target.checked == true) {
            this.healthMasterResponse.AdultResponse.forEach(function (adult) {
                if (adult.AdultValue == 1) {
                    adult.checked = true;
                    _this.childArray = _this.adultChildMap.get("1");
                }
            });
        }
        else if (this.noOfAudult == 1 && ev.target.checked == false) {
            this.healthMasterResponse.AdultResponse.forEach(function (adult) {
                if (adult.AdultValue == 1) {
                    adult.checked = false;
                }
                _this.childArray = [{ NoofKids: "1", SumInsuredDetails: null }];
            });
        }
        console.log(this.adultAgeArray);
        if (this.noOfAudult > 0) {
            this.dataValues = [];
            this.adultAgeArray.forEach(function (element) {
                // console.log("age",element.AgeValue);
                _this.dataValues.push(element.AgeValue);
            });
            this.quoteForm.patchValue({ 'eldestMemAge': this.dataValues[0] });
        }
        else {
            console.log("No Old");
            // audult1.checked = false;
            // audult2.checked = false;
            this.noOfChildHB = 1;
            this.noOfChild = 1;
            this.quoteForm.patchValue({ 'childNo': "1" });
            this.dataValues = ['06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'];
        }
        console.log("CHI No Of Adult", this.noOfAudult);
        console.log("CHI No Of Child", this.noOfChild);
        this.button1 = true;
        this.subilimitN = true;
        this.quoteForm.patchValue({ "policyTenure": "1" });
        this.quoteForm.patchValue({ "subLimit": null });
    };
    CalHealthPremiumPage.prototype.getChild = function (ev, child) {
        var child1 = document.getElementById('oneChild1');
        var child2 = document.getElementById('oneChild2');
        var child3 = document.getElementById('oneChild3');
        this.noOfChild = child;
        this.showSlider = true;
        // if (ev.target.checked == true) {
        //   this.noOfChild = child;
        // }else if (this.noOfChild == 1 && ev.target.checked == false) {
        //   this.noOfChild = 0;
        // }
        // else if (this.noOfChild == 2 && ev.target.checked == false) {
        //   this.noOfChild = 0;
        // }
        // else if (this.noOfChild == 3 && ev.target.checked == false) {
        //   this.noOfChild = 0;
        // }
        console.log(this.noOfAudult, this.noOfChild, child1, child2, child3);
        if (this.noOfAudult == undefined || this.noOfAudult == null || this.noOfAudult < 1) {
            this.quoteForm.patchValue({ 'childNo': "1" });
            this.dataValues = ['06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'];
        }
        if (this.noOfChild == 1) {
            console.log("Child", this.noOfChild);
            child1.checked = true;
            child2.checked = false;
            child3.checked = false;
        }
        else if (this.noOfChild == 2) {
            child1.checked = true;
            child2.checked = true;
            child3.checked = false;
            console.log("Child", this.noOfChild);
        }
        else if (this.noOfChild == 3) {
            child1.checked = true;
            child2.checked = true;
            child3.checked = true;
            console.log("Child", this.noOfChild);
        }
        else {
            child1.checked = false;
            child2.checked = false;
            child3.checked = false;
        }
        console.log("CHI No Of Child", this.noOfChild);
    };
    CalHealthPremiumPage.prototype.addOnYesNo = function (event) {
        var _this = this;
        this.toggleButton = !this.toggleButton;
        console.log(event);
        this.quoteForm.patchValue({ "memberAge1": this.quoteForm.value.eldestMemAge });
        if (this.quoteForm.value.memberAge2 == null || this.quoteForm.value.memberAge2 == undefined) {
            this.quoteForm.value.memberAge2 = '';
        }
        if (this.quoteForm.value.addOnCover6 == false) {
            this.quateCalculation(this.quoteForm.value).then(function () {
                _this.quateCalculationCHI1(_this.nextAmountPremRequ);
                _this.isChecked = true;
                _this.isQuoteGenerated = false;
                _this.isPremiumDetails = false;
                if (_this.isChecked == true) {
                    _this.isChecked = false;
                }
                else if (_this.isChecked1 == true) {
                    _this.isChecked1 = false;
                }
            });
        }
    };
    CalHealthPremiumPage.prototype.isAdult = function (ev) {
        // console.log(ev.detail.checked, ev.detail.value);
        var _this = this;
        this.quateCalculation(this.quoteForm.value).then(function () {
            _this.quateCalculationCHI1(_this.nextAmountPremRequ);
            _this.isChecked = true;
            _this.isQuoteGenerated = false;
            _this.isPremiumDetails = false;
            if (_this.isChecked == true) {
                _this.isChecked = false;
            }
            else if (_this.isChecked1 == true) {
                _this.isChecked1 = false;
            }
        });
    };
    CalHealthPremiumPage.prototype.selectTenure = function (ev) {
        var _this = this;
        console.log("Tenure", ev.target.innerText);
        if (this.productCode == 14 || this.productCode == 17) {
            if (this.quoteForm.value.annualSumInsured == undefined || this.quoteForm.value.annualSumInsured == '' || this.quoteForm.value.annualSumInsured == undefined == null) {
                this.cs.showLoader = false;
                if (ev.target.innerText == '1 Year') {
                    this.quoteForm.patchValue({ "policyTenure": "1" });
                    this.button1 = true;
                    this.button2 = false;
                    this.button3 = false;
                }
                else if (ev.target.innerText == '2 Years') {
                    this.button1 = false;
                    this.button2 = true;
                    this.button3 = false;
                    this.quoteForm.patchValue({ "policyTenure": "2" });
                }
                else {
                    this.button1 = false;
                    this.button2 = false;
                    this.button3 = true;
                    this.quoteForm.patchValue({ "policyTenure": "3" });
                }
            }
            else {
                if (ev.target.innerText == '1 Year') {
                    this.quoteForm.patchValue({ "policyTenure": "1" });
                    this.button1 = true;
                    this.button2 = false;
                    this.button3 = false;
                    this.quateCalculation(this.quoteForm.value).then(function () {
                        _this.quateCalculationCHI1(_this.nextAmountPremRequ);
                        _this.isChecked = true;
                    });
                    this.isQuoteGenerated = false;
                    this.isPremiumDetails = false;
                    if (this.isChecked == true) {
                        this.isChecked = false;
                    }
                    else if (this.isChecked1 == true) {
                        this.isChecked1 = false;
                    }
                }
                else if (ev.target.innerText == '2 Years') {
                    this.button1 = false;
                    this.button2 = true;
                    this.button3 = false;
                    this.quoteForm.patchValue({ "policyTenure": "2" });
                    this.quateCalculation(this.quoteForm.value).then(function () {
                        _this.quateCalculationCHI1(_this.nextAmountPremRequ);
                        _this.isChecked = true;
                    });
                    this.isQuoteGenerated = false;
                    this.isPremiumDetails = false;
                    if (this.isChecked == true) {
                        this.isChecked = false;
                    }
                    else if (this.isChecked1 == true) {
                        this.isChecked1 = false;
                    }
                }
                else {
                    this.button1 = false;
                    this.button2 = false;
                    this.button3 = true;
                    this.quoteForm.patchValue({ "policyTenure": "3" });
                    this.quateCalculation(this.quoteForm.value).then(function () {
                        _this.quateCalculationCHI1(_this.nextAmountPremRequ);
                        _this.isChecked = true;
                    });
                    this.isQuoteGenerated = false;
                    this.isPremiumDetails = false;
                    if (this.isChecked == true) {
                        this.isChecked = false;
                    }
                    else if (this.isChecked1 == true) {
                        this.isChecked1 = false;
                    }
                }
            }
        }
        else {
            if (ev.target.innerText == '1 Year') {
                this.quoteForm.patchValue({ "policyTenure": "1" });
                this.button1 = true;
                this.button2 = false;
                this.button3 = false;
            }
            else if (ev.target.innerText == '2 Years') {
                this.button1 = false;
                this.button2 = true;
                this.button3 = false;
                this.quoteForm.patchValue({ "policyTenure": "2" });
            }
            else {
                this.button1 = false;
                this.button2 = false;
                this.button3 = true;
                this.quoteForm.patchValue({ "policyTenure": "3" });
            }
        }
    };
    CalHealthPremiumPage.prototype.selectSublimit = function (ev) {
        var _this = this;
        var subLimit = [{ name: 'A' }, { name: 'B' }, { name: 'C' }];
        console.log("Sublimit", ev.target.innerText);
        if (ev.target.innerText == 'A') {
            this.subilimitA = true;
            this.subilimitB = false;
            this.subilimitC = false;
            this.subilimitN = false;
            this.quoteForm.patchValue({ "subLimit": "A" });
            this.quateCalculation(this.quoteForm.value).then(function () {
                _this.quateCalculationCHI1(_this.nextAmountPremRequ);
                _this.isChecked = true;
            });
            this.isQuoteGenerated = false;
            this.isPremiumDetails = false;
            if (this.isChecked == true) {
                this.isChecked = false;
            }
            else if (this.isChecked1 == true) {
                this.isChecked1 = false;
            }
        }
        else if (ev.target.innerText == 'B') {
            this.subilimitA = false;
            this.subilimitB = true;
            this.subilimitC = false;
            this.subilimitN = false;
            this.quoteForm.patchValue({ "subLimit": "B" });
            this.quateCalculation(this.quoteForm.value).then(function () {
                _this.quateCalculationCHI1(_this.nextAmountPremRequ);
                _this.isChecked = true;
            });
            this.isQuoteGenerated = false;
            this.isPremiumDetails = false;
            if (this.isChecked == true) {
                this.isChecked = false;
            }
            else if (this.isChecked1 == true) {
                this.isChecked1 = false;
            }
        }
        else if (ev.target.innerText == 'C') {
            this.subilimitA = false;
            this.subilimitB = false;
            this.subilimitC = true;
            this.subilimitN = false;
            this.quoteForm.patchValue({ "subLimit": "C" });
            this.quateCalculation(this.quoteForm.value).then(function () {
                _this.quateCalculationCHI1(_this.nextAmountPremRequ);
                _this.isChecked = true;
            });
            this.isQuoteGenerated = false;
            this.isPremiumDetails = false;
            if (this.isChecked == true) {
                this.isChecked = false;
            }
            else if (this.isChecked1 == true) {
                this.isChecked1 = false;
            }
        }
        else {
            this.subilimitA = false;
            this.subilimitB = false;
            this.subilimitC = false;
            this.subilimitN = true;
            this.quoteForm.patchValue({ "subLimit": null });
            this.quateCalculation(this.quoteForm.value).then(function () {
                _this.quateCalculationCHI1(_this.nextAmountPremRequ);
                _this.isChecked = true;
            });
            this.isQuoteGenerated = false;
            this.isPremiumDetails = false;
            if (this.isChecked == true) {
                this.isChecked = false;
            }
            else if (this.isChecked1 == true) {
                this.isChecked1 = false;
            }
        }
    };
    CalHealthPremiumPage.prototype.getStateMaster = function () {
        var _this = this;
        this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then(function (res) {
            _this.stateMaster = res;
            _this.stateMaster = _.sortBy(_this.stateMaster, 'StateName');
            var statename = _this.stateMaster.find(function (x) { return x.StateId == 55; }).StateName;
            _this.quoteForm.patchValue({ 'state': statename });
        });
    };
    CalHealthPremiumPage.prototype.createQuoteForm = function () {
        this.quoteForm = new FormGroup({
            productName: new FormControl(''),
            // zone: new FormControl(),
            state: new FormControl(''),
            adultNo: new FormControl(),
            childNo: new FormControl(),
            annualSumInsured: new FormControl(''),
            policyTenure: new FormControl(),
            eldestMemAge: new FormControl(),
            // deductibleSumInsured: new FormControl(),
            // coPayment: new FormControl(),
            subLimit: new FormControl(),
            addOnCover1: new FormControl(),
            addOnCover3: new FormControl(),
            addOnCover5: new FormControl(),
            addOnCover6: new FormControl(),
            memberAge1: new FormControl(),
            memberAge2: new FormControl('')
        });
    };
    CalHealthPremiumPage.prototype.refreshForm = function (prodName) {
        this.addOnCoverBasedOnAge = false;
        this.showAdultTable = false;
        this.button1 = false;
        this.button2 = false;
        this.button3 = false;
        this.dataValues = [];
        this.subilimitA = false;
        this.subilimitB = false;
        this.subilimitC = false;
        this.subilimitN = false;
        this.quoteForm = new FormGroup({
            productName: new FormControl(prodName),
            // zone: new FormControl(),
            state: new FormControl(),
            adultNo: new FormControl(),
            childNo: new FormControl(),
            annualSumInsured: new FormControl(''),
            policyTenure: new FormControl(),
            eldestMemAge: new FormControl(),
            // deductibleSumInsured: new FormControl(),
            // coPayment: new FormControl(),
            subLimit: new FormControl(),
            addOnCover1: new FormControl(),
            addOnCover3: new FormControl(),
            addOnCover5: new FormControl(),
            addOnCover6: new FormControl(),
            memberAge1: new FormControl(),
            memberAge2: new FormControl('')
        });
    };
    CalHealthPremiumPage.prototype.getProductName = function (event) {
        var _this = this;
        console.log(this.quoteForm);
        return new Promise(function (resolve) {
            _this.isQuoteGenerated = false;
            _this.isPremiumDetails = false;
            if (_this.isChecked == true) {
                _this.isChecked = false;
            }
            else if (_this.isChecked1 == true) {
                _this.isChecked1 = false;
            }
            _this.refreshForm(event.target.value);
            _this.showSlider = false;
            console.log('Values', event.target.value);
            _this.productName = event.target.value;
            var statename = _this.stateMaster.find(function (x) { return x.StateId == 55; }).StateName;
            _this.quoteForm.patchValue({ 'state': statename });
            if (event.target.value == "PROTECT") {
                _this.productCode = 14;
            }
            else if (event.target.value == "PROTECTPLUS") {
                _this.productCode = 12;
            }
            else if (event.target.value == "SMART") {
                _this.productCode = 16;
            }
            else if (event.target.value == "SMARTPLUS") {
                _this.productCode = 15;
            }
            else if (event.target.value == "iHealth") {
                _this.productCode = 17;
            }
            _this.getHealthMaster();
            _this.quoteForm.patchValue({ 'subLimit': '' });
            _this.setAddOnCover(event.target.value);
            // this.loading.dismiss();
            resolve();
        });
    };
    CalHealthPremiumPage.prototype.getState = function (e) {
        console.log(e.target.value);
        if (e.target.value == 'Kerala' || e.target.value == 'KERALA' || e.target.value == 'kerala') {
            this.isKeralaCess = true;
        }
        else {
            this.isKeralaCess = false;
            this.isRegWithGSTCHI = false;
        }
    };
    CalHealthPremiumPage.prototype.getHealthMaster = function () {
        var _this = this;
        this.cs.showLoader = true;
        var req = {
            "UserID": "5924600",
            "SubType": this.productCode
        };
        var str = JSON.stringify(req);
        this.cs.postWithParams('/api/healthmaster/HealthPlanMaster', str).then(function (res) {
            console.log("CHI HealthplanMaster", res);
            if (res.StatusCode == 1) {
                _this.healthMasterResponse = res;
                _this.adultChildMap = new Map();
                _this.adultAgeMap = new Map();
                _this.healthMasterResponse.AdultResponse.forEach(function (adult) {
                    _this.adultChildMap.set(adult.AdultValue, adult.ChildDetails);
                    _this.adultAgeMap.set(adult.AdultValue, adult.AgeEldestDetails);
                });
                _this.childArray = [{
                        NoofKids: "1",
                        SumInsuredDetails: null
                    }];
                _this.noOfChildHB = "0";
                _this.cs.showLoader = false;
            }
            else {
                console.log("Something went wrong");
            }
        }).catch(function (err) {
            console.log("Error", err);
        });
    };
    CalHealthPremiumPage.prototype.showFormValidate = function (form) {
        var message;
        if (!form.productName) {
            this.presentAlert("Please select Product");
            return false;
        }
        else if (!form.state) {
            this.presentAlert("Please select State");
            return false;
        }
        else if ((this.noOfAudultHB <= 0 || this.noOfAudultHB == undefined) &&
            (this.noOfChildHB <= 0 || this.noOfChildHB == undefined) &&
            (this.noOfAudult <= 0 || this.noOfAudult == undefined) &&
            (this.noOfChild <= 0 || this.noOfChild == undefined)) {
            this.presentAlert("Please atleast 1 Adult or 1 Child");
            return false;
        }
        else if (!form.eldestMemAge) {
            this.presentAlert("Please select Member Age Range");
            return false;
        }
        else if (!form.policyTenure) {
            this.presentAlert("Please select Policy Tenure");
            return false;
        }
        else if (!form.annualSumInsured) {
            this.presentAlert("Please select Annual Sum Insured");
            return false;
        }
        else {
            return true;
        }
    };
    CalHealthPremiumPage.prototype.toggle = function () {
        var _this = this;
        this.show = !this.show;
        console.log(this.show);
        this.quoteForm.patchValue({ addOnCover1: this.show });
        this.quateCalculation(this.quoteForm.value).then(function () {
            _this.quateCalculationCHI1(_this.nextAmountPremRequ);
            _this.isChecked = true;
            _this.cs.showLoader = false;
        });
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
            this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
            this.isChecked1 = false;
        }
    };
    CalHealthPremiumPage.prototype.toggle1 = function () {
        this.show1 = !this.show1;
        console.log(this.show1);
        this.quoteForm.patchValue({ addOnCover3: this.show1 });
    };
    CalHealthPremiumPage.prototype.toggle2 = function () {
        this.show2 = !this.show2;
        console.log(this.show2);
        this.quoteForm.patchValue({ addOnCover5: this.show2 });
    };
    CalHealthPremiumPage.prototype.toggle3 = function (val) {
        this.show3 = val;
        console.log(this.show3);
        this.quoteForm.patchValue({ addOnCover6: this.show3 });
    };
    CalHealthPremiumPage.prototype.quateCalculation = function (form) {
        var _this = this;
        return new Promise(function (resolve) {
            var valid = _this.showFormValidate(form);
            if (valid) {
                _this.cs.showLoader = true;
                if (form.childNo == '' || form.childNo == undefined || form.childNo == null) {
                    form.childNo = "0";
                }
                _this.stateid = _this.stateMaster.find(function (x) { return x.StateName == form.state; }).StateId;
                console.log(_this.stateid);
                if (_this.stateid != 62) {
                    _this.isRegWithGSTCHI = false;
                }
                if (form.addOnCover1 == '' || form.addOnCover1 == undefined || form.addOnCover1 == null) {
                    form.addOnCover1 = "false";
                }
                if (form.addOnCover3 == '' || form.addOnCover3 == undefined || form.addOnCover3 == null) {
                    form.addOnCover3 = "false";
                }
                if (form.addOnCover5 == '' || form.addOnCover5 == undefined || form.addOnCover5 == null) {
                    form.addOnCover5 = "false";
                }
                if (form.addOnCover6 == '' || form.addOnCover6 == undefined || form.addOnCover6 == null) {
                    form.addOnCover6 = false;
                }
                console.log("CHI", _this.noOfChild, _this.noOfAudult);
                if (_this.noOfChild == undefined || _this.noOfChild == null) {
                    _this.noOfChild = '0';
                }
                if (_this.noOfAudult == undefined || _this.noOfAudult == null) {
                    _this.noOfAudult = '0';
                }
                var request = {
                    "ProductType": "CHI",
                    "UserID": "NTkyNDYwMF9qUy9MSUhpdzNoUmxHRlpyaStoS2gzZDRsbU1DVnNnVFdIYU1ibkxxSDNJPQ==",
                    "UserType": "Agent",
                    "NoOfAdults": _this.noOfAudult.toString(),
                    "NoOfKids": _this.noOfChild.toString(),
                    "AgeGroup1": form.eldestMemAge,
                    "AgeGroup2": form.memberAge2,
                    "AgeGroup": form.eldestMemAge,
                    "CHIProductName": form.productName,
                    "SubLimit": form.subLimit,
                    "YearPremium": form.policyTenure,
                    "IsJammuKashmir": _this.isJammuKashmir.toString(),
                    "VolDedutible": "0",
                    "GSTStateCode": _this.stateid.toString(),
                    "AddOn1": form.addOnCover1.toString(),
                    "InsuredAmount": form.annualSumInsured,
                    "AddOn6": form.addOnCover6,
                    "AddOn3": form.addOnCover3.toString(),
                    "AddOn5": form.addOnCover5,
                    "GSTStateName": form.state,
                    "isGSTRegistered": _this.isRegWithGSTCHI,
                    "Members": null,
                    "AddOn6Members": [
                        {
                            "AddOnName": "Adult1",
                            "AddOnAgeGroup": form.memberAge1
                        },
                        {
                            "AddOnName": "Adult2",
                            "AddOnAgeGroup": form.memberAge2
                        }
                    ]
                };
                var stateData = _this.states.find(function (x) { return x.StateName == form.state; });
                localStorage.setItem('selectedStateData', JSON.stringify(stateData));
                var req = JSON.stringify(request);
                _this.nextAmountPremRequ = request;
                localStorage.setItem('quoteRequest', req);
                console.log("JSON Request", req);
                _this.cs.postWithParams('/api/Health/SaveEditQuote', req).then(function (res) {
                    if (res.StatusCode == 1) {
                        _this.oldQuoteRes = res;
                        if (_this.productCode == 14 || _this.productCode == 17) {
                            if (_this.isChecked == true || _this.isChecked1 == true) {
                                localStorage.setItem('quoteResponse', JSON.stringify(res));
                                _this.isQuoteGenerated = true;
                                _this.isPremiumDetails = true;
                            }
                            else if (_this.isChecked == false || _this.isChecked1 == false) {
                                localStorage.setItem('quoteResponse', JSON.stringify(res));
                                _this.isQuoteGenerated = false;
                                _this.isPremiumDetails = false;
                            }
                        }
                        else if (_this.productCode == 12 || _this.productCode == 16 || _this.productCode == 15) {
                            localStorage.setItem('quoteResponse', JSON.stringify(res));
                            _this.isQuoteGenerated = true;
                            _this.isPremiumDetails = true;
                        }
                        _this.isCHIRecalculate = true;
                        _this.getRequest().then(function () {
                            _this.getDataFromReq(_this.responseTemp);
                        });
                        //this.router.navigateByUrl('premium');
                        _this.cs.showLoader = false;
                        resolve();
                    }
                    else {
                        _this.presentAlert(res.StatusMessage);
                        console.log("CHI Get quote Error");
                        _this.cs.showLoader = false;
                        _this.isQuoteGenerated = false;
                        _this.isPremiumDetails = false;
                        resolve();
                    }
                }).catch(function (err) {
                    console.log(err);
                    _this.presentAlert(err.error.ExceptionMessage);
                    _this.cs.showLoader = false;
                    _this.isQuoteGenerated = false;
                    _this.isPremiumDetails = false;
                    resolve();
                });
            }
        });
    };
    CalHealthPremiumPage.prototype.reCalculateQuote = function () {
        var _this = this;
        this.cs.showLoader = true;
        this.quateCalculation(this.quoteForm.value).then(function () {
            _this.quateCalculationCHI1(_this.nextAmountPremRequ);
            _this.isChecked = true;
            _this.cs.showLoader = false;
        });
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
            this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
            this.isChecked1 = false;
        }
    };
    CalHealthPremiumPage.prototype.calculateQuote = function (formData) {
        var _this = this;
        // this.router.navigateByUrl('health-proposal');
        console.log(formData.value);
        if (formData.value.state == 'JAMMU & KASHMIR') {
            this.isJammuKashmir = true;
        }
        else {
            this.isJammuKashmir = false;
        }
        if (this.productCode == 14 || this.productCode == 17) {
            if (this.isChecked == true && this.isChecked1 == false) {
                this.quateCalculation(formData.value);
            }
            else if (this.isChecked1 == true && this.isChecked == false) {
                this.quateCalculation(this.quoteForm.value).then(function () {
                    _this.quateCalculationCHI1(_this.nextAmountPremRequ);
                    _this.isChecked = true;
                });
            }
            else if ((this.noOfAudult <= 0 || this.noOfAudult == undefined) && (this.noOfChild <= 0 || this.noOfChild == undefined)) {
                var message = "Please select atleast 1 adult or 1 child";
                this.presentAlert2(message);
            }
            else {
                if (this.quoteForm.get('eldestMemAge').value == '') {
                    this.presentAlert('Please select member age range');
                    return;
                }
                if (this.tenureCheck.nativeElement.checked == false &&
                    this.tenureCheck1.nativeElement.checked == false) {
                    this.presentAlert('Please select policy tenure');
                    return;
                }
                else {
                    this.presentAlert('Please select atleast one Annual sum insured');
                    return;
                }
            }
        }
        else {
            this.quateCalculation(this.quoteForm.value);
        }
    };
    CalHealthPremiumPage.prototype.quateCalculationCHI1 = function (data) {
        var _this = this;
        console.log(this.nextSumInsured);
        this.inputLoadingPlan = true;
        var req1 = data;
        if (this.nextSumInsured != undefined) {
            req1.InsuredAmount = this.nextSumInsured.toString();
        }
        // req1.InsuredAmount = this.nextSumInsured.toString();
        var str = JSON.stringify(req1);
        console.log(str);
        this.cs.postWithParams('/api/Health/SaveEditQuote', str).then(function (res) {
            if (res.StatusCode == 1) {
                _this.nextAmountPremRes = res;
                _this.inputLoadingPlan = false;
                console.log("NEW Next Amount Response", res);
                // localStorage.setItem('quoteResponse', JSON.stringify(res));
                _this.cs.showLoader = false;
            }
            else {
                _this.presentAlert(res.StatusMessage);
                console.log("CHI Get quate Error");
                _this.cs.showLoader = false;
            }
        }).catch(function (err) {
            console.log(err);
            _this.presentAlert(err.error.ExceptionMessage);
            _this.cs.showLoader = false;
        });
    };
    CalHealthPremiumPage.prototype.getSumInsured = function (event) {
        var _this = this;
        if (this.productName == "PROTECT") {
            if (event.target.value < '500000') {
                if (this.noOfAudult == 0 && this.noOfChild < 2) {
                    this.quoteForm.patchValue({ 'addOnCover1': "true" });
                    this.showAddOnCover1 = false;
                    this.showAddOnCover3 = false;
                    this.showAddOnCover5 = false;
                    this.showAddOnCover6 = false;
                }
                else if (this.noOfAudult == 1 && this.noOfChild < 1) {
                    this.quoteForm.patchValue({ 'addOnCover1': "true" });
                    this.showAddOnCover1 = false;
                    this.showAddOnCover3 = false;
                    this.showAddOnCover5 = false;
                    this.showAddOnCover6 = false;
                }
                else {
                    this.quoteForm.patchValue({ 'addOnCover1': "false" });
                    this.showAddOnCover1 = true;
                    this.showAddOnCover3 = false;
                    this.showAddOnCover5 = false;
                    this.showAddOnCover6 = false;
                }
            }
            else {
                this.quoteForm.patchValue({ 'addOnCover1': "true" });
                this.showAddOnCover1 = true;
                this.showAddOnCover3 = false;
                this.showAddOnCover5 = false;
                this.showAddOnCover6 = false;
            }
        }
        else if (this.productName == "PROTECTPLUS") {
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover3 = false;
            this.showAddOnCover6 = false;
        }
        else if (this.productName == "SMART") {
            this.showAddOnCover3 = true;
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover6 = false;
        }
        else if (this.productName == "SMARTPLUS") {
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover3 = false;
            this.showAddOnCover6 = false;
        }
        else if (this.productName == "iHealth") {
            if (event.target.value >= '500000') {
                if (this.noOfAudult == 0 && this.noOfChild < 2) {
                    this.quoteForm.patchValue({ 'addOnCover1': "true" });
                    this.showAddOnCover1 = false;
                    this.showAddOnCover6 = true;
                    this.showAddOnCover3 = false;
                    this.showAddOnCover5 = false;
                }
                else if (this.noOfAudult == 1 && this.noOfChild < 1) {
                    this.quoteForm.patchValue({ 'addOnCover1': "true" });
                    this.showAddOnCover1 = false;
                    this.showAddOnCover6 = true;
                    this.showAddOnCover3 = false;
                    this.showAddOnCover5 = false;
                }
                else {
                    this.quoteForm.patchValue({ 'addOnCover1': "true" });
                    this.show = true;
                    this.showAddOnCover1 = true;
                    this.showAddOnCover6 = true;
                    this.showAddOnCover3 = false;
                    this.showAddOnCover5 = false;
                }
            }
            else {
                this.quoteForm.patchValue({ 'addOnCover1': "true" });
                this.show = true;
                this.showAddOnCover1 = true;
                this.showAddOnCover6 = true;
                this.showAddOnCover3 = false;
                this.showAddOnCover5 = false;
            }
        }
        if (this.productCode == 14 || this.productCode == 17) {
            // INSURED PREMIUM TABLE CODE
            this.quoteForm.value;
            console.log("New Premium", this.quoteForm.value);
            // next amount
            var insuredIndex = _.findIndex(this.healthMasterResponse.SumInsuredDetails, { SumAmount: parseInt(this.quoteForm.value.annualSumInsured) });
            console.log(insuredIndex + 1);
            if (insuredIndex + 1 == this.healthMasterResponse.SumInsuredDetails.length) {
                this.showMaxInsured = false;
            }
            else {
                this.showMaxInsured = true;
                this.nextSumInsured = this.healthMasterResponse.SumInsuredDetails[insuredIndex + 1].SumAmount;
            }
            console.log(this.nextSumInsured);
            console.log("New Premium Changed", this.quoteForm.value);
            this.cs.showLoader = true;
            this.quateCalculation(this.quoteForm.value).then(function () {
                _this.quateCalculationCHI1(_this.nextAmountPremRequ);
                _this.isChecked = true;
            });
            this.isQuoteGenerated = false;
            this.isPremiumDetails = false;
            if (this.isChecked == true) {
                this.isChecked = false;
            }
            else if (this.isChecked1 == true) {
                this.isChecked1 = false;
            }
            // this.cs.showLoader = false;
        }
    };
    CalHealthPremiumPage.prototype.getAgeStatus = function (event) {
        console.log(event);
        if (event.from_value) {
            this.addOnCoverBasedOnAge = true;
        }
        else {
            this.addOnCoverBasedOnAge = false;
        }
        this.quoteForm.patchValue({ 'memberAge1': event.from_value });
        this.quoteForm.patchValue({ 'eldestMemAge': event.from_value });
        console.log(this.quoteForm.value, this.quoteForm.controls.memberAge1);
        if (this.adultAgeArray && this.noOfAudult > 0) {
            var index = this.adultAgeArray.findIndex(function (data) { return data.AgeValue == event.from_value; });
            this.adult2AgeArray = [];
            for (var i = 0; i <= index; i++) {
                this.adult2AgeArray.push(this.adultAgeArray[i]);
            }
        }
        if (this.noOfChild == 1 && this.noOfAudult == 0) {
            this.quoteForm.patchValue({ 'eldestMemAge': '06-20' });
        }
    };
    CalHealthPremiumPage.prototype.setAddOnCover = function (prodName) {
        if (prodName == "PROTECT") {
            this.showAddOnCover1 = true;
            this.showAddOnCover3 = false;
            this.showAddOnCover5 = false;
            this.showAddOnCover6 = false;
            this.quoteForm.patchValue({ 'addOnCover3': 'false' });
            this.quoteForm.patchValue({ 'addOnCover5': 'false' });
            this.quoteForm.patchValue({ 'addOnCover6': 'false' });
        }
        else if (prodName == "PROTECTPLUS") {
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover3 = false;
            this.showAddOnCover6 = false;
            this.quoteForm.patchValue({ 'addOnCover3': 'false' });
            this.quoteForm.patchValue({ 'addOnCover1': 'false' });
            this.quoteForm.patchValue({ 'addOnCover6': 'false' });
        }
        else if (prodName == "SMART") {
            this.showAddOnCover3 = true;
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover6 = false;
            this.quoteForm.patchValue({ 'addOnCover1': 'false' });
            this.quoteForm.patchValue({ 'addOnCover6': 'false' });
        }
        else if (prodName == "SMARTPLUS") {
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover3 = false;
            this.showAddOnCover6 = false;
            this.quoteForm.patchValue({ 'addOnCover3': 'false' });
            this.quoteForm.patchValue({ 'addOnCover1': 'false' });
            this.quoteForm.patchValue({ 'addOnCover6': 'false' });
        }
        else {
            this.showAddOnCover1 = false;
            this.showAddOnCover6 = true;
            this.showAddOnCover3 = false;
            this.showAddOnCover5 = false;
            this.quoteForm.patchValue({ 'addOnCover1': 'true' });
            this.quoteForm.patchValue({ 'addOnCover3': 'false' });
            this.quoteForm.patchValue({ 'addOnCover5': 'false' });
        }
    };
    CalHealthPremiumPage.prototype.getPremium = function (ev) {
        this.isChecked = true;
        this.isChecked1 = false;
        console.log(ev.target.checked, this.isChecked, this.isChecked1, this.nextAmountPremRes, this.oldQuoteRes);
        if (this.isChecked && !this.isChecked1) {
            console.log("Check 1");
            localStorage.setItem('quoteResponse', JSON.stringify(this.nextAmountPremRes));
            this.quoteResponse = this.nextAmountPremRes;
        }
        else {
            console.log("Check 2");
            localStorage.setItem('quoteResponse', JSON.stringify(this.oldQuoteRes));
            this.quoteResponse = this.oldQuoteRes;
        }
        // localStorage.setItem('quoteResponse', JSON.stringify(res));
    };
    CalHealthPremiumPage.prototype.getPremium1 = function (ev) {
        this.isChecked = false;
        this.isChecked1 = true;
        console.log(ev.target.checked, this.nextAmountPremRes, this.oldQuoteRes);
        // localStorage.setItem('quoteResponse', JSON.stringify(res));  
        if (!this.isChecked && this.isChecked1) {
            console.log("Check 2");
            localStorage.setItem('quoteResponse', JSON.stringify(this.oldQuoteRes));
            this.quoteResponse = this.oldQuoteRes;
        }
        else {
            console.log("Check 1");
            localStorage.setItem('quoteResponse', JSON.stringify(this.nextAmountPremRes));
            this.quoteResponse = this.nextAmountPremRes;
        }
    };
    CalHealthPremiumPage.prototype.showAlert = function (response) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("Alert", response.QuoteResponse.HealthDetails.HealthOutputResponse);
                        return [4 /*yield*/, this.alertCtrl.create({
                                header: 'Premium Amount for' + response.QuoteResponse.HealthDetails.HealthOutputResponse.Tenure + 'Years',
                                message: response.QuoteResponse.HealthDetails.HealthOutputResponse.TotalPremium,
                                buttons: [
                                    {
                                        text: 'Cancel',
                                        role: 'cancel',
                                        cssClass: 'secondary',
                                        handler: function (blah) {
                                            console.log('Confirm Cancel: blah');
                                        }
                                    }, {
                                        text: 'Okay',
                                        handler: function () {
                                            console.log('Confirm Okay');
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // CHI code ends here //
    // Health Booster Code Begins //
    CalHealthPremiumPage.prototype.toggleHB = function () {
        this.showHB = !this.showHB;
        console.log(this.showHB);
        this.quoteFormHB.patchValue({ AddOn1: this.showHB });
    };
    CalHealthPremiumPage.prototype.toggle1HB = function () {
        this.showaddon2HB = !this.showaddon2HB;
        console.log(this.showaddon2HB);
        this.quoteFormHB.patchValue({ AddOn2: this.showaddon2HB });
    };
    CalHealthPremiumPage.prototype.toggle2HB = function () {
        this.showaddon3HB = !this.showaddon3HB;
        console.log(this.showaddon3HB);
        this.quoteFormHB.patchValue({ AddOn3: this.showaddon3HB });
    };
    CalHealthPremiumPage.prototype.getAdultHB = function (ev) {
        var _this = this;
        this.noOfAudultHB = ev.target.value;
        console.log("Get Adult", this.noOfAudultHB, ev);
        var audult1 = document.getElementById('oneAdult1');
        var audult2 = document.getElementById('oneAdult2');
        var child1 = document.getElementById('oneChild1');
        var child2 = document.getElementById('oneChild2');
        if (this.noOfAudultHB == 2) {
            audult1.checked = true;
            audult2.checked = true;
        }
        else if (this.noOfAudultHB == 1) {
            if (audult1.checked == true && audult2.checked == false && this.noOfAudultHB == 1) {
                this.noOfAudultHB = 1;
                audult1.checked = true;
                audult2.checked = false;
            }
            else if (audult1.checked == false && audult2.checked == true && this.noOfAudultHB == 1) {
                this.noOfAudultHB = 1;
                audult2.checked = false;
                audult1.checked = true;
            }
            else {
                this.noOfAudultHB = 0;
                child1.checked = true;
                child2.checked = false;
            }
        }
        else if (this.noOfAudultHB == 0) {
            audult1.checked = false;
            audult2.checked = false;
        }
        else {
            this.noOfAudultHB = 0;
            audult1.checked = false;
            audult2.checked = false;
        }
        console.log(ev);
        console.log(this.noOfAudult1HB);
        this.noOfAudultHB = ev.target.value;
        if (this.noOfAudultHB == 2 && ev.target.checked == true) {
            this.childArrayHB = this.adultChildMapHB.get(ev.target.value);
            this.healthMasterResponseHB.AdultResponse.forEach(function (adult) {
                adult.checked = true;
            });
        }
        if (this.noOfAudultHB == 2 && ev.target.checked == false) {
            this.childArrayHB = this.adultChildMapHB.get("1");
        }
        else if (this.noOfAudultHB == 1 && ev.target.checked == true) {
            this.healthMasterResponseHB.AdultResponse.forEach(function (adult) {
                if (adult.AdultValue == 1) {
                    adult.checked = true;
                    _this.childArrayHB = _this.adultChildMapHB.get("1");
                }
            });
        }
        else if (this.noOfAudultHB == 1 && ev.target.checked == false) {
            this.healthMasterResponseHB.AdultResponse.forEach(function (adult) {
                if (adult.AdultValue == 1) {
                    adult.checked = false;
                }
                _this.childArrayHB = [{ NoofKids: "1", SumInsuredDetails: null }];
            });
        }
    };
    CalHealthPremiumPage.prototype.getChildHB = function (ev, child) {
        // let child1 = document.getElementById('oneChild1') as HTMLInputElement;
        // let child2 = document.getElementById('oneChild2') as HTMLInputElement;
        // let child3 = document.getElementById('oneChild3') as HTMLInputElement;
        // this.noOfChildHB = ev.target.value;
        // if (ev.target.checked == true) {
        //   this.noOfChildHB = child;
        // }
        // else if (this.noOfChildHB == 1 && ev.target.checked == false) {
        //   this.noOfChildHB = 0;
        // }
        // else if (this.noOfChildHB == 2 && ev.target.checked == false) {
        //   this.noOfChildHB = 0;
        // }
        // else if (this.noOfChildHB == 3 && ev.target.checked == false) {
        //   this.noOfChildHB = 0;
        // }
        // console.log(this.noOfAudult, this.noOfChild);
        var child1 = document.getElementById('oneChild1');
        var child2 = document.getElementById('oneChild2');
        var child3 = document.getElementById('oneChild3');
        this.noOfChildHB = child;
        console.log(this.noOfAudultHB, this.noOfChildHB, child1, child2, child3);
        if (this.noOfChildHB == 1) {
            console.log("Child", this.noOfChildHB);
            child1.checked = true;
            child2.checked = false;
            child3.checked = false;
        }
        else if (this.noOfChildHB == 2) {
            child1.checked = true;
            child2.checked = true;
            child3.checked = false;
            console.log("Child", this.noOfChildHB);
        }
        else if (this.noOfChildHB == 3) {
            child1.checked = true;
            child2.checked = true;
            child3.checked = true;
            console.log("Child", this.noOfChildHB);
        }
        else {
            child1.checked = false;
            child2.checked = false;
            child3.checked = false;
        }
        console.log("CHI No Of Child", this.noOfChildHB);
    };
    CalHealthPremiumPage.prototype.toggleSegmentHB = function (ev) {
        var _this = this;
        this.statusHB[ev.target.innerText] = !this.statusHB[ev.target.innerText];
        this.statusHB.forEach(function (ele, eleIndex) {
            console.log((eleIndex).toString(), ev.target.innerText);
            if (eleIndex.toString() != ev.target.innerText) {
                _this.statusHB[ev.target.innerText] = false;
            }
        });
    };
    CalHealthPremiumPage.prototype.addOnYesNoHB = function (event, type) {
        if (!event.target.checked) {
            if (type == 'AddOn2') {
                this.quoteFormHB.controls.Addon2Members['controls'].Member['controls'].AdultType.setValue(false);
                this.quoteFormHB.controls.Addon2Members['controls'].Member['controls'].AdultType2.setValue(false);
                this.quoteFormHB.controls.AddOn2Varient.setValue(null);
                // this.quoteForm.controls.AddOn1Varient.setValue(false);
            }
            if (type == 'AddOn3') {
                this.quoteFormHB.controls.Addon3Members['controls'].Member['controls'].AdultType.setValue(false);
                this.quoteFormHB.controls.Addon3Members['controls'].Member['controls'].AdultType2.setValue(false);
                this.quoteFormHB.controls.AddOn3Varient.setValue(null);
            }
        }
    };
    CalHealthPremiumPage.prototype.setAddOnHB = function (prodName) {
        if (prodName == "HBOOSTER_TOPUP" || prodName == "HBOOSTER_SUPERUP") {
            this.showAddOn1HB = false;
            this.quoteFormHB.patchValue({ 'AddOn1': 'false' });
            this.quoteFormHB.patchValue({ 'AddOn2': 'false' });
            this.quoteFormHB.patchValue({ 'AddOn3': 'false' });
        }
    };
    CalHealthPremiumPage.prototype.getVDValuesHB = function (ev) {
        this.quoteFormHB.patchValue({ 'annualSumInsured': ev.from_value });
        var arr = this.vdMap.get(ev.from_value);
        this.vDMapArray = arr;
    };
    CalHealthPremiumPage.prototype.showLoadingHB = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Loading',
                                translucent: true
                            })];
                    case 1:
                        _a.loadingHB = _b.sent();
                        return [4 /*yield*/, this.loadingHB.present()];
                    case 2: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    CalHealthPremiumPage.prototype.getProductNameHB = function (event) {
        var _this = this;
        this.cs.showLoader = true;
        return new Promise(function (resolve) {
            _this.refreshFormHB(event.target.value);
            if (event.target.value == "HBOOSTER_TOPUP") {
                _this.productCodeHB = 19;
            }
            else {
                _this.productCodeHB = 20;
            }
            _this.getHealthMasterHB();
            _this.setAddOnHB(event.target.value);
            resolve();
        });
    };
    CalHealthPremiumPage.prototype.getStateMasterHB = function () {
        var _this = this;
        this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then(function (res) {
            _this.stateMasterHB = res;
            _this.stateMasterHB = _.sortBy(_this.stateMasterHB, 'StateName');
            var statename = _this.stateMasterHB.find(function (x) { return x.StateId == 55; }).StateName;
            _this.quoteFormHB.patchValue({ 'state': statename });
            //this.selectedState = 55;
            console.log(_this.stateMasterHB);
        });
    };
    CalHealthPremiumPage.prototype.getStateHB = function (e) {
        console.log(e.target.value);
        if (e.target.value == 'Kerala' || e.target.value == 'KERALA' || e.target.value == 'kerala') {
            this.isKeralaCessHB = true;
        }
        else {
            this.isKeralaCessHB = false;
            this.isRegWithGSTHB = false;
        }
    };
    CalHealthPremiumPage.prototype.getHealthMasterHB = function () {
        var _this = this;
        var req = {
            "UserID": "5924600",
            "SubType": this.productCodeHB
        };
        var str = JSON.stringify(req);
        this.cs.postWithParams('/api/healthmaster/HealthPlanMaster', str).then(function (res) {
            console.log("HBoosterplanMaster", res);
            if (res.StatusCode == 1) {
                _this.healthMasterResponseHB = res;
                _this.adultChildMapHB = new Map();
                _this.adultAgeMapHB = new Map();
                _this.healthMasterResponseHB.AdultResponse.forEach(function (adult) {
                    _this.adultChildMapHB.set(adult.AdultValue, adult.ChildDetails);
                    _this.adultAgeMapHB.set(adult.AdultValue, adult.AgeEldestDetails);
                    adult['checked'] = false;
                });
                _this.childArrayHB = [{
                        NoofKids: "1",
                        SumInsuredDetails: null
                    }];
                _this.noOfChildHB = "0";
                _this.dataValuesHB = [];
                _this.healthMasterResponseHB.SumInsuredDetails.forEach(function (sum) {
                    _this.dataValuesHB.push(sum.SumAmount.toString());
                });
                _this.showSliderHB = true;
                // for VD details
                _this.vdMap = new Map();
                _this.healthMasterResponseHB.SumInsuredDetails.forEach(function (amount) {
                    console.log("VD Amount", amount);
                    console.log("VD KEY", amount.SumAmount);
                    console.log("VD VALUE", amount.VDValues);
                    _this.vdMap.set(amount.SumAmount, amount.VDValues);
                    console.log("VD MAp", _this.vdMap);
                });
                _this.cs.showLoader = false;
            }
            else {
                console.log("Something went wrong");
                _this.cs.showLoader = false;
            }
        }).catch(function (err) {
            console.log("Error", err);
            _this.cs.showLoader = false;
        });
    };
    CalHealthPremiumPage.prototype.getDateHB = function (event) {
        this.quoteFormHB.patchValue({ "Adult1Age": event.value });
        // console.log("Date", moment(event.value).format('DD-MMM-YYYY'));
    };
    CalHealthPremiumPage.prototype.getDate1HB = function (event) {
        this.quoteFormHB.patchValue({ "Adult2Age": event.value });
    };
    CalHealthPremiumPage.prototype.createQuoteFormHB = function () {
        this.quoteFormHB = new FormGroup({
            productName: new FormControl(''),
            state: new FormControl(''),
            adultNo: new FormControl(),
            childNo: new FormControl(),
            annualSumInsured: new FormControl(),
            VDValues: new FormControl(''),
            Adult1Age: new FormControl(),
            Adult2Age: new FormControl(),
            eldestMemAge: new FormControl(),
            deductibleSumInsured: new FormControl(),
            AddOn1: new FormControl(),
            AddOn1Varient: new FormControl(),
            AddOn2: new FormControl(),
            AddOn2Varient: new FormControl(),
            Addon2Members: new FormGroup({
                Member: new FormGroup({
                    AdultType: new FormControl(false),
                    AdultType2: new FormControl(false),
                })
            }),
            AddOn3: new FormControl(),
            AddOn3Varient: new FormControl(),
            Addon3Members: new FormGroup({
                Member: new FormGroup({
                    AdultType: new FormControl(false),
                    AdultType2: new FormControl(false),
                })
            }),
            SoftLessCopy: new FormControl('')
        });
    };
    CalHealthPremiumPage.prototype.refreshFormHB = function (prodName) {
        this.quoteFormHB = new FormGroup({
            productName: new FormControl(prodName),
            state: new FormControl(''),
            adultNo: new FormControl(),
            childNo: new FormControl(),
            annualSumInsured: new FormControl(),
            VDValues: new FormControl(''),
            Adult1Age: new FormControl(),
            Adult2Age: new FormControl(),
            eldestMemAge: new FormControl(),
            deductibleSumInsured: new FormControl(),
            AddOn1: new FormControl(),
            AddOn1Varient: new FormControl(),
            AddOn2: new FormControl(),
            AddOn2Varient: new FormControl(),
            Addon2Members: new FormGroup({
                Member: new FormGroup({
                    AdultType: new FormControl(false),
                    AdultType2: new FormControl(false),
                })
            }),
            AddOn3: new FormControl(),
            AddOn3Varient: new FormControl(),
            Addon3Members: new FormGroup({
                Member: new FormGroup({
                    AdultType: new FormControl(false),
                    AdultType2: new FormControl(false),
                })
            }),
            SoftLessCopy: new FormControl('')
        });
    };
    CalHealthPremiumPage.prototype.showFormValidateHB = function (formData1) {
        var message;
        if (this.quoteFormHB.value.productName == undefined || this.quoteFormHB.value.productName == null || this.quoteFormHB.value.productName == '') {
            message = "Please select product";
            this.presentAlert2(message);
            return false;
        }
        else if (this.quoteFormHB.value.state == undefined || this.quoteFormHB.value.state == null || this.quoteFormHB.value.state == '') {
            message = "Please select state";
            this.presentAlert2(message);
            return false;
        }
        else if (this.quoteFormHB.value.annualSumInsured == undefined || this.quoteFormHB.value.annualSumInsured == null || this.quoteFormHB.value.annualSumInsured == '') {
            message = "Please select Annual Sum Insured";
            this.presentAlert2(message);
            return false;
        }
        else if (this.quoteFormHB.value.VDValues == undefined || this.quoteFormHB.value.VDValues == null || this.quoteFormHB.value.VDValues == '') {
            message = "Please select Voluntarily Deductible Value";
            this.presentAlert2(message);
            return false;
        }
        else if (this.noOfAudultHB == 2 && (this.quoteFormHB.value.Adult2Age == undefined || this.quoteFormHB.value.Adult2Age == null || this.quoteFormHB.value.Adult2Age == '')) {
            message = "Please select Date of Birth of 2";
            this.presentAlert2(message);
            return false;
        }
        else if (this.quoteFormHB.value.adultNo != null && this.quoteFormHB.value.adultNo != undefined && this.quoteFormHB.value.adultNo != '') {
            if (this.quoteFormHB.value.Adult1Age == undefined || this.quoteFormHB.value.Adult1Age == null || this.quoteFormHB.value.Adult1Age == '') {
                message = "Please select Date of Birth";
                this.presentAlert2(message);
                return false;
            }
            else
                return true;
        }
        else if (this.noOfAudultHB <= 0 && this.noOfChildHB <= 0) {
            message = "Please select atleast one child or one adult";
            this.presentAlert2(message);
            return false;
        }
        else {
            return true;
        }
    };
    CalHealthPremiumPage.prototype.presentToast = function (msg) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CalHealthPremiumPage.prototype.calculateQuoteHB = function (formData1) {
        var _this = this;
        if (formData1.value.state == 'JAMMU & KASHMIR') {
            this.isJammuKashmirHB = true;
        }
        else {
            this.isJammuKashmirHB = false;
        }
        var valid = this.showFormValidateHB(formData1);
        if (valid) {
            var formData_1 = formData1.value;
            console.log(formData1);
            Object.keys(this.quoteFormHB.value.Addon2Members.Member).map(function (member) {
            });
            Object.keys(this.quoteFormHB.value.Addon3Members.Member).map(function (member) {
            });
            console.log(this.quoteFormHB.value);
            if (formData_1.SoftLessCopy == '' || formData_1.SoftLessCopy == undefined || formData_1.SoftLessCopy == null) {
                formData_1.SoftLessCopy = "false";
            }
            if (formData_1.childNo == '' || formData_1.childNo == undefined || formData_1.childNo == null) {
                formData_1.childNo = "0";
            }
            this.stateidHB = this.stateMasterHB.find(function (x) { return x.StateName == formData_1.state; }).StateId;
            console.log(this.stateidHB);
            if (this.stateidHB != 62) {
                this.isRegWithGSTHB = false;
            }
            this.cs.showLoader = true;
            var request = void 0;
            var addon2 = [];
            var addon3 = [];
            console.log(formData_1.Addon2Members.Member.AdultType);
            console.log(formData_1.Addon2Members.Member.AdultType2);
            if (formData_1.Addon2Members.Member.AdultType == true && formData_1.Addon2Members.Member.AdultType2 == true) {
                addon2.push("Adult1");
                addon2.push("Adult2");
            }
            else if (formData_1.Addon2Members.Member.AdultType == true && formData_1.Addon2Members.Member.AdultType2 == false) {
                addon2.push("Adult1");
            }
            else if (formData_1.Addon2Members.Member.AdultType == false && formData_1.Addon2Members.Member.AdultType2 == true) {
                addon2.push("Adult2");
            }
            else {
                addon2 = [];
            }
            if (formData_1.Addon3Members.Member.AdultType == true && formData_1.Addon3Members.Member.AdultType2 == true) {
                addon3.push("Adult1");
                addon3.push("Adult2");
            }
            else if (formData_1.Addon3Members.Member.AdultType == true && formData_1.Addon3Members.Member.AdultType2 == false) {
                addon3.push("Adult1");
            }
            else if (formData_1.Addon3Members.Member.AdultType == false && formData_1.Addon3Members.Member.AdultType2 == true) {
                addon3.push("Adult2");
            }
            else {
                addon3 = [];
            }
            var adultDOB1 = void 0;
            var adultDOB2 = void 0;
            if (formData_1.Adult1Age != undefined && formData_1.Adult1Age != null && formData_1.Adult1Age != '') {
                adultDOB1 = moment(formData_1.Adult1Age).format('DD-MMM-YYYY');
                console.log(adultDOB1);
            }
            else {
                adultDOB1 = null;
            }
            if (formData_1.Adult2Age != undefined && formData_1.Adult2Age != null && formData_1.Adult2Age != '') {
                adultDOB2 = moment(formData_1.Adult2Age).format('DD-MMM-YYYY');
                console.log(adultDOB2);
            }
            else {
                adultDOB2 = null;
            }
            if (this.noOfChildHB == undefined || this.noOfChildHB == null) {
                this.noOfChildHB = '0';
            }
            if (this.noOfAudultHB == undefined || this.noOfAudultHB == null) {
                this.noOfAudultHB = 0;
            }
            request = {
                "ProductType": "HBOOSTER",
                "UserType": "Agent",
                "ipaddress": config.ipAddress,
                "NoOfAdults": this.noOfAudultHB,
                "NoOfKids": this.noOfChildHB,
                "Adult1Age": adultDOB1,
                "Adult2Age": adultDOB2,
                "ProductName": formData_1.productName,
                "SumInsured": formData_1.annualSumInsured,
                "VolDedutible": formData_1.VDValues,
                "SoftLessCopy": formData_1.SoftLessCopy,
                "IsJammuKashmir": this.isJammuKashmirHB,
                "AddOn1": formData_1.AddOn1,
                "AddOn1Varient": formData_1.AddOn1Varient,
                "AddOn2": formData_1.AddOn2,
                "AddOn2Varient": formData_1.AddOn2Varient,
                "Addon2Members": addon2,
                "AddOn3": formData_1.AddOn3,
                "AddOn3Varient": formData_1.AddOn3Varient,
                "Addon3Members": addon3,
                "GSTStateCode": this.stateidHB,
                "GSTStateName": formData_1.state,
                "YearPremium": "3",
                "isGSTRegistered": this.isRegWithGSTHB,
            };
            var stateData = this.states.find(function (x) { return x.StateName == formData_1.state; });
            localStorage.setItem('selectedStateData', JSON.stringify(stateData));
            localStorage.setItem('quoteRequest', JSON.stringify(request));
            console.log("Request", JSON.stringify(request));
            var stringifyReq = JSON.stringify(request);
            this.cs.postWithParams('/api/HBooster/SaveEditQuote', stringifyReq).then(function (res) {
                console.log("New Response", res);
                if (res.StatusCode == 1) {
                    localStorage.setItem('quoteResponse', JSON.stringify(res));
                    _this.isQuoteGenerated = true;
                    _this.isHBoosterRecalculate = true;
                    _this.getRequest().then(function () {
                        _this.getDataFromReq(_this.responseTemp);
                    });
                    _this.cs.showLoader = false;
                    _this.isQuoteDetails = !_this.isQuoteDetails;
                }
                else {
                    if (res.StatusMessage != undefined) {
                        _this.presentAlert2(res.StatusMessage);
                        _this.cs.showLoader = false;
                    }
                    else {
                        _this.presentAlert2('Something went wrong...');
                        _this.cs.showLoader = false;
                    }
                }
            }).catch(function (err) {
                _this.presentAlert2(err.error.ExceptionMessage);
                _this.cs.showLoader = false;
            });
        }
    };
    CalHealthPremiumPage.prototype.presentAlert2 = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CalHealthPremiumPage.prototype.getSubTypeHB = function (data) {
        console.log("Selected Data", data);
    };
    CalHealthPremiumPage.prototype.changesoftcopyValHB = function (ev) {
        console.log(ev.target.value);
        ev.target.value;
        var yesNO = ev.target.value;
        console.log('value', yesNO);
        if (yesNO == 'true') {
            this.softCopyAlert();
        }
    };
    CalHealthPremiumPage.prototype.softCopyAlert = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            // header: 'Age',
                            subHeader: 'iPartner Mobile ',
                            message: 'You have opted for an e-policy instead of a hard copy.Your Premium shall be revised with a deduction of 100₹',
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // Health Booster Code Ends //
    // PPAP code begins //
    // intializeDate() {
    //   let elems = document.querySelectorAll('#dobValidator');
    //   let options = {}
    //   let instances = M.Datepicker.init(elems, options);
    // }
    // function getting triggered on changing annual income //
    CalHealthPremiumPage.prototype.getAnnualIncome = function (selectedAnuualIncome) {
        this.sum_insured = '';
        this.policy_duration = '';
        this.insured_options = '';
        this.payoutTenure = [];
        this.payout_amount = '';
        this.payout_tenure = '';
        this.optionsArray = [];
        this.OptionDetails = [];
        this.canShowOptions = false;
        this.canShowOptionsDetails = false;
        this.showPPASumInsured = false;
        this.lumpsum = [];
        this.lump_sum_payment = '';
        this.showPPCSumInsured = false;
        this.disableQuote = true;
    };
    //function to perform activity on duartion //
    CalHealthPremiumPage.prototype.getPolicyDuration = function (policyTenure) {
        this.sumInsuredArray = {};
        if (this.selectedPP == 'PP') {
            for (var i = 0; i < this.policyData.length; i++) {
                if (this.policyData[i].Value == policyTenure) {
                    this.insuredArray = this.policyData[i].SumInsureds;
                }
            }
            if (this.insuredArray.length > 1) {
                this.sumInsuredArray = _.uniq(this.insuredArray, false, function (p) { return p.Value; });
            }
            else {
                this.sumInsuredArray = this.insuredArray;
            }
        }
        else if (this.selectedPP == 'PPA' || this.selectedPP == 'PPC') {
            for (var i = 0; i < this.policyData.length; i++) {
                if (this.policyData[i].Value == policyTenure) {
                    this.payoutAmount = this.policyData[i].MonthlyPayouts;
                }
            }
        }
        this.disableQuote = true;
        this.sum_insured = '';
        this.payoutTenure = [];
        this.payout_amount = '';
        this.payout_tenure = '';
        this.optionsArray = [];
        this.OptionDetails = [];
        this.canShowOptions = false;
        this.canShowOptionsDetails = false;
        this.showPPASumInsured = false;
        this.lumpsum = [];
        this.lump_sum_payment = '';
        this.showPPCSumInsured = false;
        this.insured_options = '';
    };
    // function getting triggered on changing payout amount //
    CalHealthPremiumPage.prototype.getPayoutAmount = function (payoutamt) {
        this.payoutTenure = {};
        for (var i = 0; i < this.payoutAmount.length; i++) {
            if (this.payoutAmount[i].Value == payoutamt) {
                this.payoutTenure = this.payoutAmount[i].PayoutTenures;
                //this.ppa_sum_insured = this.payoutTenure[0].SumInsured;
                // if (this.payoutAmount[i].PayoutTenures.length >=1) {
                //   this.payoutTenure.push(this.payoutAmount[i].PayoutTenures);
                // }
                // else {
                //   this.payoutTenure.push(this.payoutAmount[i].PayoutTenures);
                //   this.ppa_sum_insured = this.payoutAmount[i].PayoutTenures.PayoutTenure.SumInsured;
                // }
            }
        }
        this.payoutTenure = _.uniq(this.payoutTenure, false, function (p) { return p.Value; });
        this.payout_tenure = '';
        this.canShowOptions = false;
        this.canShowOptionsDetails = false;
        this.showPPASumInsured = false;
        this.lumpsum = [];
        this.lump_sum_payment = '';
        this.showPPCSumInsured = false;
        this.disableQuote = true;
    };
    // function getting triggered on changing payout tenure //
    CalHealthPremiumPage.prototype.getPayoutTenure = function (payoutTenure) {
        //this.showPPASumInsured = true; 
        this.OptionsDetailsArray = [];
        this.optionsArray = [];
        this.canShowOptions = false;
        this.canShowOptionsDetails = false;
        this.lumpsum = [];
        this.lump_sum_payment = '';
        this.showPPCSumInsured = false;
        this.disableQuote = true;
        if (this.selectedPP == 'PPA') {
            for (var i = 0; i < this.payoutAmount.length; i++) {
                if (this.payoutAmount[i].Value == this.payout_amount) {
                    if (this.payoutAmount[i].PayoutTenures.length == 0) {
                        this.OptionsDetailsArray.push(this.payoutAmount[i].PayoutTenures[0].BenefitSummary);
                        this.ppaPlancode = this.payoutAmount[i].PayoutTenures[0].PlanCode;
                        this.ppa_sum_insured = this.payoutAmount[i].PayoutTenures[0].SumInsured;
                        this.showPPASumInsured = true;
                        this.disableQuote = false;
                    }
                    if (this.payoutAmount[i].PayoutTenures.length == 1) {
                        for (var j = 0; j < this.payoutAmount[i].PayoutTenures[0].BenefitSummary.length; j++) {
                            this.OptionsDetailsArray.push(this.payoutAmount[i].PayoutTenures[0].BenefitSummary[j]);
                        }
                        this.ppaPlancode = this.payoutAmount[i].PayoutTenures[0].PlanCode;
                        this.disableQuote = false;
                        this.ppa_sum_insured = this.payoutAmount[i].PayoutTenures[0].SumInsured;
                        this.showPPASumInsured = true;
                    }
                    else if (this.payoutAmount[i].PayoutTenures.length > 1) {
                        for (var j = 0; j < this.payoutAmount[i].PayoutTenures.length; j++) {
                            this.optionsArray.push(this.payoutAmount[i].PayoutTenures[j]);
                            this.canShowOptions = true;
                        }
                    }
                    // else {
                    //   if (this.payoutAmount[i].PayoutTenures[0].BenefitSummary.length == 0) {
                    //     this.ppaPlancode = this.payoutAmount[i].PayoutTenures[0].PlanCode; 
                    //     this.ppa_sum_insured = this.payoutAmount[i].PayoutTenures[0].SumInsured;
                    //     this.disableQuote = false;  this.showPPASumInsured = true;
                    //   }
                    //   else {
                    //     this.canShowOptions = false; this.canShowOptionsDetails = false; this.disableQuote = false;
                    //   }
                    //   this.canShowOptions = false;
                    // }
                }
            }
            this.canShowOptionsDetails = true;
        }
        else {
            this.lumpsum = [];
            this.disableQuote = true;
            for (var i = 0; i < this.payoutAmount.length; i++) {
                if (this.payoutAmount[i].Value == this.payout_amount) {
                    if (this.payoutAmount[i].PayoutTenures[0].LumpSums.length != undefined) {
                        this.lumpsum.push(this.payoutAmount[i].PayoutTenures[0].LumpSums[0].Value);
                        this.ppc_sum_insured = this.payoutAmount[i].PayoutTenures[0].LumpSums[0].SumInsured;
                    }
                    else {
                        this.lumpsum.push(this.payoutAmount[i].PayoutTenures.PayoutTenure.LumpSums.Value);
                        this.ppc_sum_insured = this.payoutAmount[i].PayoutTenures.PayoutTenure.LumpSums.SumInsured;
                    }
                }
            }
        }
    };
    // function getting triggered on changing lump sump amount //
    CalHealthPremiumPage.prototype.getLumPSumpAmt = function (lumpsumpamt) {
        for (var i = 0; i < this.payoutAmount.length; i++) {
            if (this.payoutAmount[i].Value == this.payout_amount) {
                if (this.payoutAmount[i].PayoutTenures[0].LumpSums.length != undefined) {
                    this.getPPCDteails(this.payoutAmount[i]);
                }
                else {
                    this.getPPCDteails(this.payoutAmount[i]);
                }
            }
        }
        //this.showPPCSumInsured = true;
    };
    CalHealthPremiumPage.prototype.getPPCDteails = function (array) {
        if (array.PayoutTenures[0].LumpSums[0].BenefitSummary != undefined && array.PayoutTenures[0].LumpSums[0].BenefitSummary != null && array.PayoutTenures[0].LumpSums[0].BenefitSummary != "") {
            if (array.PayoutTenures[0].LumpSums.length == 1) {
                for (var j = 0; j < array.PayoutTenures[0].LumpSums[0].BenefitSummary.length; j++) {
                    this.OptionsDetailsArray.push(array.PayoutTenures[0].LumpSums[0].BenefitSummary[j]);
                }
                this.ppaPlancode = array.PayoutTenures[0].LumpSums[0].PlanCode;
                this.showPPCSumInsured = true;
                this.disableQuote = false;
            }
            else if (array.PayoutTenures[0].LumpSums.length > 1) {
                for (var j = 0; j < array.PayoutTenures[0].LumpSums.length; j++) {
                    {
                        this.optionsArray.push(array.PayoutTenures[0].LumpSums[j]);
                    }
                }
                this.canShowOptions = true;
            }
        }
        else {
            this.canShowOptions = false;
            this.canShowOptionsDetails = false;
            this.disableQuote = false;
        }
        this.canShowOptionsDetails = true;
    };
    //function to perform activity on changing insurance //
    CalHealthPremiumPage.prototype.getPPAPSumInsured = function (sumInsured) {
        this.optionsArray = [];
        this.OptionDetails = [];
        this.disableQuote = true;
        this.insured_options = '';
        if (_.isArray(this.insuredArray)) {
            for (var i = 0; i < this.insuredArray.length; i++) {
                if (this.insuredArray[i].Value == sumInsured) {
                    this.optionsArray.push(this.insuredArray[i]);
                }
            }
        }
        else {
            this.optionsArray.push(this.insuredArray);
        }
        if (this.optionsArray.length == 1) {
            if (this.optionsArray[0].BenefitSummary != undefined && this.optionsArray[0].BenefitSummary != null && this.optionsArray[0].BenefitSummary != "") {
                if (this.optionsArray[0].BenefitSummary == undefined) {
                    this.canShowOptions = false;
                    this.OptionDetails.push(this.optionsArray[0].BenefitSummary);
                    this.OptionsDetailsArray = this.OptionDetails;
                    this.insured_options = this.optionsArray[0].PlanCode;
                    this.canShowOptionsDetails = true;
                    this.disableQuote = false;
                }
                if (this.optionsArray[0].BenefitSummary.length >= 1) {
                    this.canShowOptions = false;
                    this.OptionsDetailsArray = this.optionsArray[0].BenefitSummary;
                    this.insured_options = this.optionsArray[0].PlanCode;
                    this.canShowOptionsDetails = true;
                    this.disableQuote = false;
                }
            }
        }
        else {
            this.canShowOptions = true;
            this.canShowOptionsDetails = false;
        }
    };
    // function to perform activity on changing options if applicable //
    CalHealthPremiumPage.prototype.getOptions = function (selectedOption) {
        this.OptionDetails = [];
        for (var i = 0; i < this.optionsArray.length; i++) {
            if (this.optionsArray[i].PlanCode == selectedOption) {
                this.OptionDetails.push(this.optionsArray[i].BenefitSummary);
                if (this.selectedPP == 'PPA') {
                    this.ppaPlancode = selectedOption;
                    this.ppa_sum_insured = this.optionsArray[i].SumInsured;
                    this.showPPASumInsured = true;
                }
                if (this.selectedPP == 'PPC') {
                    this.ppaPlancode = selectedOption;
                    this.ppc_sum_insured = this.optionsArray[i].SumInsured;
                    this.showPPCSumInsured = true;
                }
            }
        }
        if (this.OptionDetails[0].length == undefined) {
            this.OptionsDetailsArray = this.OptionDetails;
            if (this.selectedPP == 'PPA') {
                this.ppaPlancode = selectedOption;
                this.ppa_sum_insured = this.OptionDetails[0].SumInsured;
                this.showPPASumInsured = true;
            }
            if (this.selectedPP == 'PPC') {
                this.ppaPlancode = selectedOption;
                this.ppc_sum_insured = this.optionsArray[0].SumInsured;
                this.showPPCSumInsured = true;
            }
        }
        else {
            this.OptionsDetailsArray = this.OptionDetails[0];
        }
        this.canShowOptionsDetails = true;
        this.disableQuote = false;
    };
    // function to perform activity on changing states //
    CalHealthPremiumPage.prototype.getStatePPAP = function (state) {
        this.selectedState = state;
    };
    // function to perform activity on changing product subtype //
    CalHealthPremiumPage.prototype.getSubType = function (subtype) {
        this.moreDetails = false;
        this.selectedOccupationType = '';
        this.disableQuote = true;
    };
    // function to perform activity on changing occupationtype //
    CalHealthPremiumPage.prototype.getOccupation = function (selectedoccupation) {
        var _this = this;
        this.moreDetails = false;
        this.annual_income = '';
        if (this.selectedSubType == '0') {
            this.selectedPP = 'PP';
            this.isPPAPPC = false;
            this.isPPC = false;
            this.isPPA = false;
        }
        else if (this.selectedSubType == '1') {
            this.selectedPP = 'PPA';
            this.isPPAPPC = true;
            this.isPPA = true;
            this.isPPC = false;
        }
        else {
            this.selectedPP = 'PPC';
            this.isPPAPPC = true;
            this.isPPC = true;
            this.isPPA = false;
        }
        if (selectedoccupation == 0 || selectedoccupation == 1) {
            this.isEarning = true;
        }
        else {
            this.isEarning = false;
        }
        switch (selectedoccupation) {
            case "0":
                this.occType = 'Salaried';
                this.salariedType = 'salaried';
                break;
            case "1":
                this.occType = 'Self Employed';
                this.salariedType = 'salaried';
                break;
            case "2":
                this.occType = 'Housewife';
                this.salariedType = 'nonsalaried';
                break;
            case "3":
                this.occType = 'Student';
                this.salariedType = 'nonsalaried';
                break;
            case "4":
                this.occType = 'Retired';
                this.salariedType = 'nonsalaried';
                break;
            case "5":
                this.occType = 'Business man';
                this.salariedType = 'BUSINESS MAN';
                break;
        }
        // if ((selectedoccupation == '2' || selectedoccupation == '3' || selectedoccupation == '4') && (this.selectedSubType == '1' || this.selectedSubType == '2')) {
        //   this.presentAlert('No plan available for Monthly Payment Option'); this.isPPAPPC = false; this.isPPC = false; this.moreDetails = false;
        // }
        // else {
        var postData = {
            "SubProduct": this.selectedPP,
            "OccupationType": this.salariedType,
        };
        // this.apiloading.present();
        this.cs.showLoader = true;
        this.cs.postWithParams('/api/healthmaster/GetPPAPMasterDetails', postData).then(function (res) {
            _this.masterDataPPAP = res;
            _this.incomeData = _this.masterDataPPAP.Incomes;
            _this.policyData = _this.masterDataPPAP.PolicyTenures;
            if (_this.incomeData.length < 1 || _this.policyData.length < 1) {
                if (_this.selectedPP == 'PPA') {
                    _this.cs.showLoader = false;
                    _this.moreDetails = false;
                    _this.presentAlert('No plan available for Monthly Payment Option');
                }
                else if (_this.selectedPP == 'PPC') {
                    _this.cs.showLoader = false;
                    _this.moreDetails = false;
                    _this.presentAlert('No plan available for Lump Sum Payment Option with Monthly Payout');
                }
                else {
                    _this.cs.showLoader = false;
                    _this.moreDetails = false;
                    _this.presentAlert('No plan available');
                }
            }
            else {
                _this.moreDetails = true;
                _this.disableQuote = true;
                _this.sum_insured = '';
                _this.policy_duration = '';
                _this.annual_income = '';
                _this.payoutTenure = [];
                _this.payout_amount = null;
                _this.payout_tenure = '';
                _this.optionsArray = [];
                _this.OptionDetails = [];
                _this.canShowOptions = false;
                _this.canShowOptionsDetails = false;
                _this.showPPASumInsured = false;
                _this.lumpsum = [];
                _this.lump_sum_payment = '';
                _this.showPPCSumInsured = false;
                // this.apiloading.dismiss();
                _this.cs.showLoader = false;
            }
            // setTimeout(function () {
            //   let elems = document.querySelectorAll('#dobValidator');
            //   let sinsuredDOB = new Date();
            //   let einsuredDOB = new Date();
            //   sinsuredDOB.setFullYear(new Date().getFullYear() - 100);
            //   let today = new Date()
            //   let priorDate = new Date().setFullYear(new Date().getFullYear()-18); 
            //   let defDate = new Date(priorDate)
            //   let options = { format: 'dd-mm-yyyy', autoClose: true, disableWeekends: false, defaultDate: defDate, minDate: sinsuredDOB, maxDate: defDate, yearRange: 100 }
            //   let instances = M.Datepicker.init(elems, options);
            // }, 1000);
        }, function (err) {
            _this.presentAlert('Sorry Something went wrong');
            // this.apiloading.dismiss();
            _this.cs.showLoader = false;
        });
    };
    CalHealthPremiumPage.prototype.generateQuote = function (form) {
        if (this.incomeData.length < 1 || this.policyData.length < 1) {
            if (this.selectedPP == 'PPA') {
                this.presentAlert('No plan available for Monthly Payment Option');
            }
            else if (this.selectedPP == 'PPC') {
                this.presentAlert('No plan available for Lump Sum Payment Option with Monthly Payout');
            }
            else {
                this.presentAlert('No plan available');
            }
        }
        else {
            this.saveGetQuote(form);
        }
    };
    CalHealthPremiumPage.prototype.saveGetQuote = function (form) {
        var _this = this;
        var isJammuKashmir;
        var tempstate = this.selectedState;
        if (this.selectedState != 62) {
            this.isRegWithGST = false;
        }
        if (this.selectedState == 149) {
            isJammuKashmir = true;
        }
        else {
            isJammuKashmir = false;
        }
        if (this.selectedState == undefined || this.selectedState == '' || this.selectedState == null) {
            this.presentAlert('Please select state');
        }
        else if (this.dob == undefined || this.dob == '' || this.dob == null) {
            this.presentAlert('Please select date of insured');
        }
        else if (this.dob != undefined || this.dob != '' || this.dob != null) {
            var tempAgeDate = moment().diff(this.dob, 'years');
            if (tempAgeDate < 1) {
                this.presentAlert('Please select valid date of insured');
            }
            else {
                var statename_1 = _.find(this.states, function (s) { return s.StateId == tempstate; }).StateName;
                var gstStateCode = _.find(this.states, function (s) { return s.StateId == tempstate; }).StateId;
                var dateofbirth = moment(this.dob).format('DD-MMMM-YYYY');
                var occupation = this.occType.toUpperCase();
                var plancode = void 0;
                if (this.selectedPP == 'PP') {
                    plancode = this.insured_options;
                    localStorage.setItem('SumInsured', this.sum_insured.toString());
                }
                else if (this.selectedPP == 'PPA') {
                    plancode = this.ppaPlancode;
                    localStorage.setItem('SumInsured', this.ppa_sum_insured.toString());
                }
                else if (this.selectedPP == 'PPC') {
                    plancode = this.ppaPlancode;
                    localStorage.setItem('SumInsured', this.ppc_sum_insured.toString());
                }
                else {
                    alert('Plancode not found');
                }
                // debugger
                var postData = {
                    "UserType": "AGENT",
                    "ProductType": "PPAP",
                    "ipaddress": config.ipAddress,
                    "SubProduct": this.selectedPP,
                    "DOB": dateofbirth,
                    "Tenure": this.policy_duration,
                    "IsJammuKashmir": isJammuKashmir,
                    "Occupation": occupation,
                    "PlanCode": plancode,
                    "GSTStateCode": gstStateCode,
                    "GSTStateName": statename_1,
                    "isGSTRegistered": this.isRegWithGST
                };
                var stateData = this.states.find(function (x) { return x.StateName == statename_1; });
                localStorage.setItem('selectedStateData', JSON.stringify(stateData));
                localStorage.setItem('quoteRequest', JSON.stringify(postData));
                localStorage.setItem('AnuualIncome', this.annual_income.toString());
                if (this.OptionsDetailsArray.length > 0) {
                    localStorage.setItem('PPAPCovers', JSON.stringify(this.OptionsDetailsArray));
                }
                this.cs.showLoader = true;
                this.cs.postWithParams('/api/PPAP/SaveEditQuote', postData).then(function (res) {
                    _this.saveQuoteData = res;
                    // form.controls['dobValidator'].reset(); form.controls['subProuctType'].reset();
                    // this.moreDetails = false; this.disableQuote = true;
                    // this.sum_insured = ''; this.policy_duration = ''; this.annual_income = '';
                    // this.payoutTenure = []; this.payout_amount = null; this.payout_tenure = '';
                    // this.optionsArray = []; this.OptionDetails = []; this.canShowOptions = false; this.canShowOptionsDetails = false;
                    // this.showPPASumInsured = false; this.lumpsum = []; this.lump_sum_payment = ''; this.showPPCSumInsured = false;
                    // this.disableQuote = true; this.dob = null; this.selectedSubType = null; this.selectedOccupationType = null;
                    // this.selectedState = 55; this.payoutAmount = []; this.policyData = [];
                    if (_this.saveQuoteData.StatusCode == 1) {
                        _this.cs.showLoader = false;
                        localStorage.setItem('quoteResponse', JSON.stringify(_this.saveQuoteData));
                        _this.isQuoteGenerated = true;
                        _this.isPPAPRecalculate = true;
                        _this.getRequest().then(function () {
                            _this.getDataFromReq(_this.responseTemp);
                        });
                        _this.isQuoteDetails = !_this.isQuoteDetails;
                    }
                    else {
                        _this.presentAlert(_this.saveQuoteData.StatusMessage);
                        _this.cs.showLoader = false;
                    }
                }, function (err) {
                    _this.presentAlert(err.error.ExceptionMessage);
                    _this.cs.showLoader = false;
                });
            }
        }
    };
    CalHealthPremiumPage.prototype.gotosummary = function () {
        this.routes.navigate(['../cal-health-premium/health_summary']);
    };
    CalHealthPremiumPage.prototype.convertAmountToCurrency = function (num) {
        if (num != undefined && num != null && num != '') {
            return num.toLocaleString('en-IN', { style: 'currency', currency: "INR", minimumFractionDigits: 0, maximumFractionDigits: 0 });
        }
    };
    CalHealthPremiumPage.prototype.getDate = function (event) {
        console.log(moment(this.dob).format('DD-MMMM-YYYY'));
    };
    // customPopoverOptions: any = {
    //   header: 'Select'
    // };
    CalHealthPremiumPage.prototype.showLoading = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Loading',
                                translucent: true
                            })];
                    case 1:
                        _a.loading = _b.sent();
                        return [4 /*yield*/, this.loading.present()];
                    case 2: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    CalHealthPremiumPage.prototype.isGSTReg = function (val) {
        if (val) {
            this.isRegWithGST = true;
        }
        else {
            this.isRegWithGST = false;
        }
    };
    // PPAP code ends //
    CalHealthPremiumPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    CalHealthPremiumPage.prototype.showQuoteDetails = function (ev) {
        this.isQuoteDetails = !this.isQuoteDetails;
        if (this.isQuoteDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("quoteDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("quoteDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    CalHealthPremiumPage.prototype.showPremiumDetails = function (ev) {
        this.isPremiumDetails = !this.isPremiumDetails;
        if (this.isPremiumDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("premiumDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("premiumDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("premiumDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("premiumDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    // Premium Code Begins Here //
    CalHealthPremiumPage.prototype.getRequest = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.responseTemp = JSON.parse(localStorage.getItem('quoteRequest'));
            console.log("Preetam", _this.responseTemp);
            resolve();
        });
    };
    CalHealthPremiumPage.prototype.getDataFromReq = function (data) {
        console.log(data);
        this.productType = data.ProductType;
        if (data.ProductType == 'HBOOSTER') {
            console.log("HB", this.responseTemp);
            this.isHealthBooster = true;
            this.isCHI = false;
            this.isPPAP = false;
            this.PremiumForThreeYear(1, '');
            var SoftLessCopyBool = void 0;
            this.quoteRequest = this.responseTemp;
            SoftLessCopyBool = this.responseTemp.SoftLessCopy;
            if (SoftLessCopyBool == "true") {
                this.SoftLessCopyValue = 100;
            }
            else {
                this.SoftLessCopyValue = 0;
            }
            if (this.responseTemp.ProductName == "HBOOSTER_TOPUP") {
                this.HBProductName = "Top Up Plan";
            }
            else {
                this.HBProductName = "Super Top Up Plan";
            }
        }
        else if (data.ProductType == 'CHI') {
            this.isHealthBooster = false;
            this.isCHI = true;
            this.isPPAP = false;
            console.log("CHI", this.responseTemp);
            this.quoteRequest = this.responseTemp;
            var res = JSON.parse(localStorage.getItem('quoteResponse'));
            this.quoteResponse = res;
            console.log(this.quoteResponse);
        }
        else {
            this.isHealthBooster = false;
            this.isCHI = false;
            this.isPPAP = true;
            this.quoteRequest = this.responseTemp;
            var res = JSON.parse(localStorage.getItem('quoteResponse'));
            this.quoteResponse = res;
            if (this.quoteRequest.SubProduct == "PP") {
                this.SubProductName = "Lum Sum Payment Option";
            }
            else if (this.quoteRequest.SubProduct == "PPA") {
                this.SubProductName = "Monthly Payment Option";
            }
            else if (this.quoteRequest.SubProduct == "PPC") {
                this.SubProductName = "Lump Sum Payment Option With Monthly Payout";
            }
            else {
                console.log("Error");
            }
        }
    };
    CalHealthPremiumPage.prototype.ConvertToPolicy = function () {
        if (this.productType == 'PPAP') {
            this.openPEDModal();
        }
        else {
            this.router.navigateByUrl('health-proposal');
        }
    };
    CalHealthPremiumPage.prototype.openPEDModal = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var gstModal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: PedQuestionaireComponent,
                            showBackdrop: false,
                            backdropDismiss: false
                        })];
                    case 1:
                        gstModal = _a.sent();
                        gstModal.onDidDismiss()
                            .then(function (data) {
                            var gstData = data['data'];
                            console.log(gstData); // Here's your selected user!
                        });
                        return [4 /*yield*/, gstModal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    CalHealthPremiumPage.prototype.PremiumForOneYear = function (data) {
        var _this = this;
        console.log("for 1 Year", data);
        data.YearPremium = '1';
        console.log(data);
        var req = JSON.stringify(data);
        this.showLoading();
        localStorage.setItem('Request', req);
        this.cs.postWithParams('/api/HBooster/SaveEditQuote', req).then(function (res) {
            console.log("New Response", res);
            if (res.StatusCode == 1) {
                console.log(res);
                _this.quoteResponse = res;
                localStorage.setItem('quoteResponse', JSON.stringify(res));
                _this.loading.dismiss();
            }
            else {
                _this.loading.dismiss();
                _this.presentAlert(res.StatusMessage);
            }
        }).catch(function (err) {
            _this.presentAlert(err);
            _this.loading.dismiss();
        });
    };
    CalHealthPremiumPage.prototype.PremiumForTwoYear = function (data) {
        var _this = this;
        console.log("for 2 Year", data);
        data.YearPremium = '2';
        console.log(data);
        var req = JSON.stringify(data);
        this.showLoading();
        localStorage.setItem('Request', req);
        this.cs.postWithParams('/api/HBooster/SaveEditQuote', req).then(function (res) {
            console.log("New Response", res);
            if (res.StatusCode == 1) {
                console.log(res);
                _this.quoteResponse = res;
                localStorage.setItem('quoteResponse', JSON.stringify(res));
                _this.loading.dismiss();
            }
            else {
                _this.loading.dismiss();
                _this.presentAlert(res.StatusMessage);
            }
        }).catch(function (err) {
            _this.presentAlert(err);
            _this.loading.dismiss();
        });
    };
    CalHealthPremiumPage.prototype.PremiumForThreeYear = function (call, data) {
        var _this = this;
        console.log("Premium");
        if (call == 1) {
            var res = JSON.parse(localStorage.getItem('quoteResponse'));
            this.quoteResponse = res;
        }
        else {
            data.YearPremium = '3';
            console.log(data);
            var req = JSON.stringify(data);
            this.showLoading();
            localStorage.setItem('Request', req);
            this.cs.postWithParams('/api/HBooster/SaveEditQuote', req).then(function (res) {
                console.log("New Response", res);
                if (res.StatusCode == 1) {
                    console.log(res);
                    _this.quoteResponse = res;
                    localStorage.setItem('quoteResponse', JSON.stringify(res));
                    _this.loading.dismiss();
                }
                else {
                    _this.loading.dismiss();
                    _this.presentAlert(res.StatusMessage);
                }
            }).catch(function (err) {
                _this.presentAlert(err);
                _this.loading.dismiss();
            });
        }
        console.log(this.quoteResponse);
    };
    CalHealthPremiumPage.prototype.getHBData = function (year) {
        this.hbyearwiseData = year;
        if (year == 1) {
            this.PremiumForOneYear(this.responseTemp);
        }
        else if (year == 2) {
            this.PremiumForTwoYear(this.responseTemp);
        }
        else if (year == 3) {
            this.PremiumForThreeYear(2, this.responseTemp);
        }
    };
    CalHealthPremiumPage.prototype.showPremium = function () {
        this.isQuoteGenerated = true;
        this.isPremiumDetails = true;
        this.isQuoteDetails = !this.isQuoteDetails;
        if (this.isChecked && !this.isChecked1) {
            this.quoteResponse = this.nextAmountPremRes;
            this.quoteRequest = this.nextAmountPremRequ;
        }
        else if (!this.isChecked && this.isChecked1) {
            this.quoteResponse = this.oldQuoteRes;
            this.quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
        }
    };
    tslib_1.__decorate([
        ViewChild('tenureCheck'),
        tslib_1.__metadata("design:type", Object)
    ], CalHealthPremiumPage.prototype, "tenureCheck", void 0);
    tslib_1.__decorate([
        ViewChild('tenureCheck1'),
        tslib_1.__metadata("design:type", Object)
    ], CalHealthPremiumPage.prototype, "tenureCheck1", void 0);
    CalHealthPremiumPage = tslib_1.__decorate([
        Component({
            selector: 'app-cal-health-premium',
            templateUrl: './cal-health-premium.page.html',
            styleUrls: ['./cal-health-premium.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            CommonService,
            XmlToJsonService,
            LoadingController,
            ToastController,
            Router,
            AlertController,
            Location,
            ModalController])
    ], CalHealthPremiumPage);
    return CalHealthPremiumPage;
}());
export { CalHealthPremiumPage };
//# sourceMappingURL=cal-health-premium.page.js.map