import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HealthSummaryPage } from './health-summary.page';
var routes = [
    {
        path: '',
        component: HealthSummaryPage
    }
];
var HealthSummaryPageModule = /** @class */ (function () {
    function HealthSummaryPageModule() {
    }
    HealthSummaryPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [HealthSummaryPage]
        })
    ], HealthSummaryPageModule);
    return HealthSummaryPageModule;
}());
export { HealthSummaryPageModule };
//# sourceMappingURL=health-summary.module.js.map