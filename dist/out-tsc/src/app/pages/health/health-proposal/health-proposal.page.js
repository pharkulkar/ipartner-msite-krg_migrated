import * as tslib_1 from "tslib";
import { Component, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
// import { SearchCustComponent } from '../search-cust/search-cust.component';
import { ModalController, ToastController, LoadingController, AlertController } from '@ionic/angular';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
import { EcryDecryService } from 'src/app/services/ecry-decry.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { SearchCustComponent } from '../search-cust/search-cust.component';
import * as _ from 'underscore';
import { config } from 'src/app/config.properties';
var HealthProposalPage = /** @class */ (function () {
    function HealthProposalPage(router, cdRef, alertController, cs, toastCtrl, modalCtrl, xml2Json, ErDrService, loadingCtrl) {
        this.router = router;
        this.cdRef = cdRef;
        this.alertController = alertController;
        this.cs = cs;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.xml2Json = xml2Json;
        this.ErDrService = ErDrService;
        this.loadingCtrl = loadingCtrl;
        this.showOtherPolicyDetails = false;
        this.showClaimsDetails = false;
        this.height_feet = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        this.height_inch = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        this.adultRelationShip = [];
        this.adultRelationArray = [];
        this.childRelationShip = [];
        this.childRelationArray = [];
        this.questionList = [];
        this.titles = [{ "id": "0", "val": "Mrs." }, { "id": "1", "val": "Mr." }, { "id": "2", "val": "Ms." }];
        // requestFromPremium: any;
        // jsonData: any;
        this.isAdult1 = false;
        this.isAdult2 = false;
        this.isChild1 = false;
        this.isChild2 = false;
        this.isChild3 = false;
        this.isBasicDetails = true;
        this.isInsuredDetails = false;
        this.isApplicantDetails = false;
        this.validityyears = [];
        this.validityfromyears = [];
        this.isPermanent = false;
        this.isOther = false;
        this.isNomineeDeatils = false;
        this.isNew = true;
        // titleId1: any; titleId2: any;titleId3: any;  minDate: any;  relation: any;agentData: any;
        this.isCorraddSame = false;
        this.isNewCust = 'NO';
        this.isRegWithGST = false;
        this.isExistPolicy1 = false;
        this.isExistPolicy2 = false;
        this.isExistPolicy3 = false;
        this.isExistPolicy4 = false;
        this.isExistPolicy5 = false;
        this.isDisease = false;
        this.isDisease2 = false;
        this.isDisease3 = false;
        this.isDisease4 = false;
        this.isDisease5 = false;
        this.isOtherPol = 'NO';
        this.isGSTN = true;
        this.isUIN = false;
        this.showApointee = false;
        this.apointee = false;
        this.ailmentArray1 = [];
        this.ailmentArray2 = [];
        this.ailmentArray3 = [];
        this.ailmentArray4 = [];
        this.ailmentArray5 = [];
        this.iskeralaCess = false;
    }
    HealthProposalPage.prototype.ngOnInit = function () {
        var _this = this;
        console.log(localStorage.getItem('requestForProp'));
        var imgUrl = './assets/images/minus.png';
        document.getElementById("basicDetails").style.background = "url(" + imgUrl + ")";
        document.getElementById("basicDetails").style.backgroundRepeat = 'no-repeat';
        this.getCall().then(function () {
            _this.router.navigate(['../health-proposal']);
        });
        this.getBackData();
        this.agentData = JSON.parse(localStorage.getItem('userData'));
        this.quoteRequestData = JSON.parse(localStorage.getItem('quoteRequest'));
        if (this.quoteRequestData.GSTStateCode == '62') {
            if (this.quoteRequestData.isGSTRegistered == true) {
                this.iskeralaCess = true;
                this.isRegWithGST = true;
            }
            else {
                this.iskeralaCess = false;
                this.isRegWithGST = false;
            }
        }
        else {
            this.iskeralaCess = false;
            this.isRegWithGST = false;
        }
        this.createBasicForm();
        this.getJsonData().then(function () {
            _this.patchValue(_this.requestFromPremium);
        });
        var date = new Date();
        this.minDate = moment(date).format('YYYY-MM-DD');
        var today = new Date();
        var currentYear = date.getFullYear();
        var currentTillYear = date.getFullYear() + 2;
        var validFromYear = date.getFullYear() - 30;
        for (var i = validFromYear; i < currentYear; i++) {
            this.validityyears.push(i);
        }
        for (var i = validFromYear; i < currentTillYear; i++) {
            this.validityfromyears.push(i);
        }
        this.getRelations();
        this.getQuestionList();
        this.createApplForm();
        this.getAnnualIncome();
        this.getMaritalStatus();
        this.getOccupation();
        this.getRelation();
        this.getNomineeRelation();
        this.getInsuranceData();
        this.result = JSON.parse(localStorage.getItem('quoteRequest'));
        this.childmaxDOB = new Date(today.setMonth(new Date().getMonth() - 3));
        this.childminDOB = new Date(today.setFullYear(new Date().getFullYear() - 20));
        this.childmaxDOB = moment(this.childmaxDOB).format('YYYY-MM-DD');
        this.childminDOB = moment(this.childminDOB).format('YYYY-MM-DD');
        this.productType = this.result.ProductType;
        if (this.productType == 'CHI') {
            this.createInsuredForm();
            var ageArray = this.result.AgeGroup1.split("-");
            this.eldestMax = new Date(today.setFullYear(new Date().getFullYear() - ageArray[0]));
            this.eldestMin = new Date(today.setFullYear(new Date().getFullYear() - ageArray[1]));
            this.eldestMax = moment(this.eldestMax).format('YYYY-MM-DD');
            this.eldestMin = moment(this.eldestMin).format('YYYY-MM-DD');
            localStorage.setItem('isOtherPolicyDetails', this.isOtherPol);
        }
        else if (this.productType == 'HBOOSTER') {
            this.createInsuredForm();
            localStorage.setItem('isOtherPolicyDetails', this.isOtherPol);
        }
        else {
            this.ppapDOB = this.result.DOB;
            var tempAgeDate = moment().diff(this.ppapDOB, 'years');
            this.minRiskDate = moment().add(1, 'days').format('YYYY-MM-DD');
            this.maxRiskDate = moment().add(15, 'days').format('YYYY-MM-DD');
            if (tempAgeDate < 18) {
                this.ppapMemeberType = 'Child';
            }
            else {
                this.ppapMemeberType = 'Adult';
            }
            this.ppapOccupation = this.result.Occupation;
            this.createPPAPInsuranceForm();
        }
        this.getJsonData().then(function () {
            _this.showSegments(_this.jsonData);
        });
    };
    //-Proposal Applicant Code Begins--//
    HealthProposalPage.prototype.radio = function (val) {
        if (val == true) {
            this.isGSTN = true;
            this.isUIN = false;
        }
        else {
            this.isGSTN = false;
            this.isUIN = true;
        }
    };
    HealthProposalPage.prototype.validateAlternateMobileLength = function (e) {
        if (e.target.value.length > 10) {
            this.presentAlert('Alterante Mobile No cannot more than 10 digits');
            this.applicantForm.patchValue({ 'applaltMobNo': null });
        }
    };
    HealthProposalPage.prototype.validateMobileLength = function (e) {
        if (e.target.value.length > 10) {
            this.presentAlert('Mobile No cannot more than 10 digits');
            this.applicantForm.patchValue({ 'applMobNo': null });
        }
    };
    HealthProposalPage.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    HealthProposalPage.prototype.validateMobile = function (mobile) {
        var mob = /^[0-9]{10}$/;
        return mob.test(mobile);
    };
    HealthProposalPage.prototype.validateAdhaarNo = function (addharno) {
        var re = /^[0-9]{12}$/;
        return re.test(addharno);
    };
    HealthProposalPage.prototype.validatePanNo = function (panno) {
        var re = /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
        return re.test(panno);
    };
    HealthProposalPage.prototype.validateGST = function (gst) {
        var re = /^[0-9]{2}[A-Z]{3}[PCHABGJLEFT][A-Z][0-9]{4}[A-Z]{1}[0-9A-Z]{1}[Z]{1}[0-9A-Z]{1}$/;
        var gstvalid = re.test(gst);
        if (gstvalid) {
            var gstcodeObtained = gst.substring(0, 2);
            var stateData = JSON.parse(localStorage.getItem('selectedStateData'));
            var gstcode = stateData.GSTStateCode;
            if (gstcodeObtained == gstcode) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return gstvalid;
        }
    };
    HealthProposalPage.prototype.calculateMemberAge = function (val) {
        var age = moment().diff(moment(val).format('DD-MMM-YYYY'), 'years');
    };
    HealthProposalPage.prototype.ValidateGSTSection = function () {
        if (this.isRegWithGST == true) {
            if (this.applicantForm.value.constitution == undefined || this.applicantForm.value.constitution == null || this.applicantForm.value.constitution == '') {
                this.presentAlert("Please select constitution of business for GST section");
                return false;
            }
            else if (this.applicantForm.value.panNo == undefined || this.applicantForm.value.panNo == null || this.applicantForm.value.panNo == '') {
                this.presentAlert("Please enter pan card number for GST section");
                return false;
            }
            else if (this.applicantForm.value.panNo != undefined && this.applicantForm.value.panNo != null && this.applicantForm.value.panNo != '' && !this.validatePanNo(this.applicantForm.value.panNo)) {
                this.presentAlert('Please enter valid pan card number for GST section');
                return false;
            }
            else if (this.applicantForm.value.customerType == undefined || this.applicantForm.value.customerType == null || this.applicantForm.value.customerType == '') {
                this.presentAlert("Please select customer type for GST section");
                return false;
            }
            else if (this.applicantForm.value.gstRegStatus == undefined || this.applicantForm.value.gstRegStatus == null || this.applicantForm.value.gstRegStatus == '') {
                this.presentAlert("Please select registration status for GST section");
                return false;
            }
            else if ((this.applicantForm.value.GSTIN == undefined || this.applicantForm.value.GSTIN == null || this.applicantForm.value.GSTIN == '') && this.isGSTN) {
                this.presentAlert("Please enter GSTIN number");
                return false;
            }
            else if (!this.validateGST(this.applicantForm.value.GSTIN) && this.isGSTN) {
                this.presentAlert("Please enter valid GSTIN number");
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    };
    HealthProposalPage.prototype.ValidationApplicant = function () {
        var isGSTSectionValid = this.ValidateGSTSection();
        if (!isGSTSectionValid) {
            return false;
        }
        else if (this.applicantForm.value.aadhaarNo != undefined && this.applicantForm.value.aadhaarNo != null && this.applicantForm.value.aadhaarNo != '' && !this.validateAdhaarNo(this.applicantForm.value.aadhaarNo)) {
            this.presentAlert('Kindly Enter Valid Applicant Aadhaar Number');
            return false;
        }
        else if (this.applicantForm.value.panNumber != undefined && this.applicantForm.value.panNumber != null && this.applicantForm.value.panNumber != '' && !this.validatePanNo(this.applicantForm.value.panNumber)) {
            this.presentAlert('Kindly Enter Valid Applicant Pan Card Number');
            return false;
        }
        else if (this.titleId1 == undefined || this.titleId1 == null || this.titleId1 == '') {
            this.presentAlert("Kindly Insert Applicant Title");
            return false;
        }
        else if (this.applicantForm.value.applName == undefined || this.applicantForm.value.applName == null || this.applicantForm.value.applName == '') {
            this.presentAlert("Kindly Insert Applicant Name");
            return false;
        }
        else if (this.applicantForm.value.applDOB == undefined || this.applicantForm.value.applDOB == null || this.applicantForm.value.applDOB == '') {
            this.presentAlert("Kindly Enter Applicant Date of Birth");
            return false;
        }
        else if (this.applicantForm.value.applAdd1 == undefined || this.applicantForm.value.applAdd1 == null || this.applicantForm.value.applAdd1 == '') {
            this.presentAlert("Kindly Insert Applicant Correspondence Address");
            return false;
        }
        else if (this.applicantForm.value.applPinCode == undefined || this.applicantForm.value.applPinCode == null || this.applicantForm.value.applPinCode == '') {
            this.presentAlert("Kindly Insert Applicant Pincode");
            return false;
        }
        else if (this.applicantForm.value.applCity == undefined || this.applicantForm.value.applCity == null || this.applicantForm.value.applCity == '') {
            this.presentAlert("Applicant City not found");
            return false;
        }
        else if (this.applicantForm.value.applState == undefined || this.applicantForm.value.applState == null || this.applicantForm.value.applState == '') {
            this.presentAlert("Applicant State not found");
            return false;
        }
        else if (!this.validateEmail(this.applicantForm.value.applEmail)) {
            this.presentAlert("Kindly Insert Applicant Email ID");
            return false;
        }
        else if (this.applicantForm.value.applMarital == undefined || this.applicantForm.value.applMarital == null || this.applicantForm.value.applMarital == '') {
            this.presentAlert("Kindly Select Applicant Marital Status");
            return false;
        }
        // else if (this.applicantForm.value.applOccu == undefined || this.applicantForm.value.applOccu == null || this.applicantForm.value.applOccu == '') {
        //   this.presentAlert("Kindly Select Applicant Occupation"); return false;
        // } else if (this.applicantForm.value.annuIncome == undefined || this.applicantForm.value.annuIncome == null || this.applicantForm.value.annuIncome == '') {
        //   this.presentAlert("Kindly Select Applicant Annual Income"); return false;
        // } 
        else if (this.applicantForm.value.permAdd1 == undefined || this.applicantForm.value.permAdd1 == null || this.applicantForm.value.permAdd1 == '') {
            this.presentAlert("Kindly Insert Applicant Permanent Address");
            return false;
        }
        else if (this.applicantForm.value.permPinCode == undefined || this.applicantForm.value.permPinCode == null || this.applicantForm.value.permPinCode == '') {
            this.presentAlert("Kindly Insert Applicant Permanent Pincode");
            return false;
        }
        else if (this.applicantForm.value.permCity == undefined || this.applicantForm.value.permCity == null || this.applicantForm.value.permCity == '') {
            this.presentAlert("Applicant Permanent City not found");
            return false;
        }
        else if (this.applicantForm.value.permState == undefined || this.applicantForm.value.permState == null || this.applicantForm.value.permState == '') {
            this.presentAlert("Applicant Permanent State not found");
            return false;
        }
        else if (this.applicantForm.value.nomTitle == undefined || this.applicantForm.value.nomTitle == null || this.applicantForm.value.nomTitle == '') {
            this.presentAlert("Kindly Select Nominee Title");
            return false;
        }
        else if (this.applicantForm.value.nomName == undefined || this.applicantForm.value.nomName == null || this.applicantForm.value.nomName == '') {
            this.presentAlert("Kindly Insert Nominee Name");
            return false;
        }
        else if (this.applicantForm.value.nomRelation == undefined || this.applicantForm.value.nomRelation == null || this.applicantForm.value.nomRelation == '') {
            this.presentAlert("Kindly Select Nominee Relation");
            return false;
        }
        else if (this.applicantForm.value.nomDOB == undefined || this.applicantForm.value.nomDOB == null || this.applicantForm.value.nomDOB == '') {
            this.presentAlert("Kindly Select Nominee Date of Birth");
            return false;
        }
        else if (this.applicantForm.value.applMobNo == undefined || this.applicantForm.value.applMobNo == null || this.applicantForm.value.applMobNo == '') {
            this.presentAlert("Kindly Insert Your Mobile Number");
            return false;
        }
        else if (!this.validateMobile(this.applicantForm.value.applMobNo)) {
            this.presentAlert("Mobile Number should be 10 digit");
            return false;
        }
        else if (this.applicantForm.value.applaltMobNo != undefined && this.applicantForm.value.applaltMobNo != null && this.applicantForm.value.applaltMobNo != '' && !this.validateMobile(this.applicantForm.value.applaltMobNo)) {
            this.presentAlert("Alternate Mobile Number should be 10 digit");
            return false;
        }
        else if (this.applicantForm.value.applaltEmail != undefined && this.applicantForm.value.applaltEmail != null && this.applicantForm.value.applaltEmail != '' && !this.validateEmail(this.applicantForm.value.applaltEmail)) {
            this.presentAlert("Alternate Email should be valid");
            return false;
        }
        else {
            return true;
        }
    };
    HealthProposalPage.prototype.validatedInsuredMembers = function () {
        var totalAdults, totalKids, isAdult1Valid = true, isAdult2Valid = true, isChild1Valid = true, isChild2Valid = true, isChild3Valid = true;
        if (this.quoteRequestData.NoOfAdults != undefined && this.quoteRequestData.NoOfAdults != null && this.quoteRequestData.NoOfAdults != '') {
            totalAdults = this.quoteRequestData.NoOfAdults;
        }
        else {
            totalAdults = 0;
        }
        if (this.quoteRequestData.NoOfKids != undefined && this.quoteRequestData.NoOfKids != null && this.quoteRequestData.NoOfKids != '') {
            totalKids = this.quoteRequestData.NoOfKids;
        }
        else {
            totalKids = 0;
        }
        if (totalAdults == 1 || totalAdults == 2) {
            if (this.insuredForm.value.member1Rela == undefined || this.insuredForm.value.member1Rela == null || this.insuredForm.value.member1Rela == '') {
                this.presentAlert("Kindly Select Relationship of Adult 1 under Insured");
                isAdult1Valid = false;
            }
            else if (this.insuredForm.value.member1Title == undefined || this.insuredForm.value.member1Title == null || this.insuredForm.value.member1Title == '') {
                this.presentAlert("Kindly Select Title of Adult 1 under Insured");
                isAdult1Valid = false;
            }
            else if (this.insuredForm.value.member1Name == undefined || this.insuredForm.value.member1Name == null || this.insuredForm.value.member1Name == '') {
                this.presentAlert("Kindly Insert Name of Adult 1 under Insured");
                isAdult1Valid = false;
            }
            else if (this.insuredForm.value.member1Weight == undefined || this.insuredForm.value.member1Weight == null || this.insuredForm.value.member1Weight == '') {
                this.presentAlert("Kindly Insert Weight of Adult 1 under Insured");
                isAdult1Valid = false;
            }
            else if (this.insuredForm.value.member1Feet == undefined || this.insuredForm.value.member1Feet == null || this.insuredForm.value.member1Feet == '') {
                this.presentAlert("Kindly Insert Height in Feet of Adult 1 under Insured");
                isAdult1Valid = false;
            }
            else if (this.insuredForm.value.member1Inches == undefined || this.insuredForm.value.member1Inches == null || this.insuredForm.value.member1Inches == '') {
                this.presentAlert("Kindly Insert Height in Inches of Adult 1 under Insured");
                isAdult1Valid = false;
            }
        }
        else {
            isAdult1Valid = true;
        }
        if (totalAdults == 2) {
            if (this.insuredForm.value.member2Rela == undefined || this.insuredForm.value.member2Rela == null || this.insuredForm.value.member2Rela == '') {
                this.presentAlert("Kindly Select Relationship for Adult 2 under Insured");
                isAdult2Valid = false;
            }
            else if (this.insuredForm.value.member2Title == undefined || this.insuredForm.value.member2Title == null || this.insuredForm.value.member2Title == '') {
                this.presentAlert("Kindly Select Title for Adult 2 under Insured");
                isAdult2Valid = false;
            }
            else if (this.insuredForm.value.member2Name == undefined || this.insuredForm.value.member2Name == null || this.insuredForm.value.member2Name == '') {
                this.presentAlert("Kindly Insert Name for Adult 2 under Insured");
                isAdult2Valid = false;
            }
            else if (this.insuredForm.value.member2Weight == undefined || this.insuredForm.value.member2Weight == null || this.insuredForm.value.member2Weight == '') {
                this.presentAlert("Kindly Insert Weight for Adult 2 under Insured");
                isAdult2Valid = false;
            }
            else if (this.insuredForm.value.member2Feet == undefined || this.insuredForm.value.member2Feet == null || this.insuredForm.value.member2Feet == '') {
                this.presentAlert("Kindly Insert Height in Feet for Adult 2 under Insured");
                isAdult2Valid = false;
            }
            else if (this.insuredForm.value.member2Inches == undefined || this.insuredForm.value.member2Inches == null || this.insuredForm.value.member2Inches == '') {
                this.presentAlert("Kindly Insert Height in Inches for Adult 2 under Insured");
                isAdult2Valid = false;
            }
        }
        else {
            isAdult2Valid = true;
        }
        if (totalKids == 1 || totalKids == 2 || totalKids == 3) {
            if (this.insuredForm.value.child1Rela == undefined || this.insuredForm.value.child1Rela == null || this.insuredForm.value.child1Rela == '') {
                this.presentAlert("Kindly Select Relationship for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1Title == undefined || this.insuredForm.value.child1Title == null || this.insuredForm.value.child1Title == '') {
                this.presentAlert("Kindly Select Title for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1Name == undefined || this.insuredForm.value.child1Name == null || this.insuredForm.value.child1Name == '') {
                this.presentAlert("Kindly Insert Name for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1DOB == undefined || this.insuredForm.value.child1DOB == null || this.insuredForm.value.child1DOB == '') {
                this.presentAlert("Kindly Insert DOB for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1Weight == undefined || this.insuredForm.value.child1Weight == null || this.insuredForm.value.child1Weight == '') {
                this.presentAlert("Kindly Insert Weight for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1Feet == undefined || this.insuredForm.value.child1Feet == null || this.insuredForm.value.child1Feet == '') {
                this.presentAlert("Kindly Insert Height in Feet for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1Inches == undefined || this.insuredForm.value.child1Inches == null || this.insuredForm.value.child1Inches == '') {
                this.presentAlert("Kindly Insert Height in Inches for Child 1 under Insured");
                isChild1Valid = false;
            }
        }
        else {
            isChild1Valid = true;
        }
        if (totalKids == 2 || totalKids == 3) {
            if (this.insuredForm.value.child2Rela == undefined || this.insuredForm.value.child2Rela == null || this.insuredForm.value.child2Rela == '') {
                this.presentAlert("Kindly Select  Relationship Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2Rela == undefined || this.insuredForm.value.child2Rela == null || this.insuredForm.value.child2Rela == '') {
                this.presentAlert("Kindly Select Title for Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2Name == undefined || this.insuredForm.value.child2Name == null || this.insuredForm.value.child2Name == '') {
                this.presentAlert("Kindly Insert Name for Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2DOB == undefined || this.insuredForm.value.child2DOB == null || this.insuredForm.value.child2DOB == '') {
                this.presentAlert("Kindly Insert DOB for Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2Weight == undefined || this.insuredForm.value.child2Weight == null || this.insuredForm.value.child2Weight == '') {
                this.presentAlert("Kindly Insert Weight for Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2Feet == undefined || this.insuredForm.value.child2Feet == null || this.insuredForm.value.child2Feet == '') {
                this.presentAlert("Kindly Insert Height in Feet for Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2Inches == undefined || this.insuredForm.value.child2Inches == null || this.insuredForm.value.child2Inches == '') {
                this.presentAlert("Kindly Insert Height in Inches for Child 2 under Insured");
                isChild2Valid = false;
            }
        }
        else {
            isChild2Valid = true;
        }
        if (totalKids == 3) {
            if (this.insuredForm.value.child1Rela == undefined || this.insuredForm.value.child1Rela == null || this.insuredForm.value.child1Rela == '') {
                this.presentAlert("Kindly Select Relationship for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3Title == undefined || this.insuredForm.value.child3Title == null || this.insuredForm.value.child3Title == '') {
                this.presentAlert("Kindly Select Title for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3Name == undefined || this.insuredForm.value.child3Name == null || this.insuredForm.value.child3Name == '') {
                this.presentAlert("Kindly Insert Name for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3DOB == undefined || this.insuredForm.value.child3DOB == null || this.insuredForm.value.child3DOB == '') {
                this.presentAlert("Kindly Insert DOB for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3Weight == undefined || this.insuredForm.value.child3Weight == null || this.insuredForm.value.child3Weight == '') {
                this.presentAlert("Kindly Insert Weight for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3Feet == undefined || this.insuredForm.value.child3Feet == null || this.insuredForm.value.child3Feet == '') {
                this.presentAlert("Kindly Insert Height in Feet for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3Inches == undefined || this.insuredForm.value.child3Inches == null || this.insuredForm.value.child3Inches == '') {
                this.presentAlert("Kindly Insert Height in Inches for Child 3 under Insured");
                isChild3Valid = false;
            }
        }
        else {
            isChild3Valid = true;
        }
        if (isAdult1Valid == true && isAdult2Valid == true && isChild1Valid == true) {
            return true;
        }
        else {
            return false;
        }
    };
    HealthProposalPage.prototype.SimilarFieldsValidator = function (formData) {
        // if (formData.value.insuredpanno == null) {
        //   this.presentAlert('Please select pan card number for insured member');
        //   return false;
        // }
        if (formData.value.insuredpanno != undefined && formData.value.insuredpanno != null && formData.value.insuredpanno != '' && !this.validatepanCard(formData.value.insuredpanno)) {
            this.presentAlert('Please select valid pan card number for insured member');
            return false;
        }
        if (formData.value.member1Cat == null) {
            this.presentAlert('Member Type not present');
            return false;
        }
        else if (formData.value.member1Rela == null || formData.value.member1Rela == '') {
            this.presentAlert('Please select relationship with applicant for insured member');
            return false;
        }
        else if (formData.value.member1Title == null || formData.value.member1Title == '') {
            this.presentAlert('Please select  title for insured member');
            return false;
        }
        else if (formData.value.member1Name == null) {
            this.presentAlert('Please select full name for insured member');
            return false;
        }
        else if (formData.value.member1DOB == null) {
            this.presentAlert('Date of birth not present for insured member');
            return false;
        }
        else if (formData.value.riskStartDate == null) {
            this.presentAlert('Please select Risk Date for insured member');
            return false;
        }
        else if (formData.value.insuredOccupation == null) {
            this.presentAlert('Occupation not present for insured member');
            return false;
        }
        else {
            return true;
        }
    };
    HealthProposalPage.prototype.ValidationCheckPPAP = function (formData) {
        if (formData.value.aiPolicy == true) {
            var isBasicFeildsValid = this.SimilarFieldsValidator(formData);
            if (!isBasicFeildsValid) {
                return false;
            }
            else if (formData.value.policyValidFrom == null && formData.value.policyValidTill == null) {
                this.presentAlert('Please select Both policy valid from and policy valid till for insured member');
                return false;
            }
            else if (formData.value.policyValidFrom == null) {
                this.presentAlert('Please select policy valid from for insured member');
                return false;
            }
            else if (formData.value.policyValidTill == null) {
                this.presentAlert('Please select policy valid till for insured member');
                return false;
            }
            else if (formData.value.policyValidFrom > formData.value.policyValidTill) {
                this.presentAlert('Please select policy valid till field must be less than policy valid from field');
                return false;
            }
            else if (formData.value.insuranceComapny == null) {
                this.presentAlert('Please select Insurance Company for insured member');
                return false;
            }
            else if (formData.value.insurancePolicy == null) {
                this.presentAlert('Please select Insurance Policy for insured member');
                return false;
            }
            else if (formData.value.policySumInsured == null) {
                this.presentAlert('Please select Sum Insured for insured member');
                return false;
            }
            else {
                return true;
            }
        }
        else {
            var isBasicFeildsValid = this.SimilarFieldsValidator(formData);
            if (!isBasicFeildsValid) {
                return false;
            }
            else {
                return true;
            }
        }
    };
    HealthProposalPage.prototype.showFormValidate = function (form) {
        var message;
        // if (!this.insuredForm.value.member1Rela) {
        //   message = "Kindly Select Relationship with applicant in Insured Page";
        //   this.presentToast(message);
        // } 
        var Htype = JSON.parse(localStorage.getItem('quoteRequest'));
        if (Htype.ProductType == 'HBOOSTER' || Htype.ProductType == 'CHI') {
            var isapplicantFeildsValid = this.ValidationApplicant();
            var isinsuredFeildsValid = this.validatedInsuredMembers();
            if (isapplicantFeildsValid == true && isinsuredFeildsValid == true) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            if (this.showOtherPolicyDetails == true) {
                var isBasicFeildsValid = this.SimilarFieldsValidator(this.insuredPPAPForm);
                var isapplicantFeildsValid = this.ValidationApplicant();
                if (isBasicFeildsValid == true && isapplicantFeildsValid == true) {
                    if (this.insuredPPAPForm.value.policyValidFrom == '' && this.insuredPPAPForm.value.policyValidTill == '') {
                        this.presentAlert('Please select Both policy valid from and policy valid to');
                        return false;
                    }
                    else if (this.insuredPPAPForm.value.policyValidFrom == '') {
                        this.presentAlert('Please select policy valid from');
                        return false;
                    }
                    else if (this.insuredPPAPForm.value.policyValidTill == '') {
                        this.presentAlert('Please select policy valid to');
                        return false;
                    }
                    else if (this.insuredPPAPForm.value.policyValidFrom > this.insuredPPAPForm.value.policyValidTill) {
                        this.presentAlert('Please select policy valid to field must be less than policy valid from field');
                        return false;
                    }
                    else if (this.insuredPPAPForm.value.insuranceComapny == null && this.insuredPPAPForm.value.insuranceComapny == '') {
                        this.presentAlert('Please select Insurance Company');
                        return false;
                    }
                    else if (this.insuredPPAPForm.value.insurancePolicy == null) {
                        this.presentAlert('Please select Insurance Policy');
                        return false;
                    }
                    else if (this.insuredPPAPForm.value.policySumInsured == null) {
                        this.presentAlert('Please select Sum Insured');
                        return false;
                    }
                    else if (this.applicantForm.value.applTitle == undefined || this.applicantForm.value.applTitle == null || this.applicantForm.value.applTitle == '') {
                        this.presentAlert("Kindly Select Applicant Title");
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return false;
                }
            }
            else {
                var isBasicFeildsValid = this.SimilarFieldsValidator(this.insuredPPAPForm);
                var isapplicantFeildsValid = this.ValidationApplicant();
                if (isBasicFeildsValid == true && isapplicantFeildsValid == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    };
    HealthProposalPage.prototype.createApplForm = function () {
        this.applicantForm = new FormGroup({
            aadhaarNo: new FormControl(),
            panNumber: new FormControl(),
            applTitle: new FormControl(''),
            applName: new FormControl(),
            applDOB: new FormControl(),
            applMobNo: new FormControl(),
            applAdd1: new FormControl(),
            applAdd2: new FormControl(),
            applLandMark: new FormControl(),
            applPinCode: new FormControl(),
            applCity: new FormControl(),
            applState: new FormControl(),
            // isRegWithGST: new FormControl(),
            applMarital: new FormControl(''),
            applOccu: new FormControl(''),
            annuIncome: new FormControl(''),
            applEmail: new FormControl(),
            applaltEmail: new FormControl(),
            applaltMobNo: new FormControl(),
            landLine: new FormControl(),
            isCorrAndPerSame: new FormControl(),
            permAdd1: new FormControl(),
            permAdd2: new FormControl(),
            permLandMark: new FormControl(),
            permPinCode: new FormControl(),
            permCity: new FormControl(),
            permState: new FormControl(),
            // isOtherPol: new FormControl(),
            nomTitle: new FormControl(''),
            nomName: new FormControl(),
            nomRelation: new FormControl(''),
            nomDOB: new FormControl(),
            apsId: new FormControl(),
            custRefNo: new FormControl(),
            sourcingCode: new FormControl(),
            //
            GSTIN: new FormControl(),
            UIN: new FormControl(),
            gstRegStatus: new FormControl(''),
            customerType: new FormControl(''),
            panNo: new FormControl(),
            constitution: new FormControl(''),
            appTitle: new FormControl(''),
            appName: new FormControl(),
            appRelation: new FormControl(''),
            appDOB: new FormControl()
        });
    };
    // searchUser(form: any) {
    //   this.searchCust();
    //   console.log(form.value);
    // }
    HealthProposalPage.prototype.isNewCustomer = function (ev) {
        // console.log(ev.detail.checked);
        if (ev == true) {
            this.isNew = true;
            this.isNewCust = 'YES';
            this.modalCtrl.dismiss();
            this.applicantForm.reset();
            this.applicantForm.patchValue({ 'applTitle': '' });
            this.applicantForm.patchValue({ 'annuIncome': '' });
            this.applicantForm.patchValue({ 'applOccu': '' });
            this.applicantForm.patchValue({ 'applMarital': '' });
            this.applicantForm.patchValue({ 'nomTitle': '' });
            this.applicantForm.patchValue({ 'nomRelation': '' });
            this.applicantForm.patchValue({ 'nomRelation': '' });
            this.applicantForm.patchValue({ 'customerType': '' });
            this.applicantForm.patchValue({ 'constitution': '' });
            this.applicantForm.patchValue({ 'gstRegStatus': '' });
            this.applicantForm.patchValue({ 'appTitle': '' });
            this.applicantForm.patchValue({ 'appRelation': '' });
        }
        else {
            this.applicantForm.reset();
            this.applicantForm.patchValue({ 'applTitle': '' });
            this.applicantForm.patchValue({ 'annuIncome': '' });
            this.applicantForm.patchValue({ 'applOccu': '' });
            this.applicantForm.patchValue({ 'applMarital': '' });
            this.applicantForm.patchValue({ 'nomTitle': '' });
            this.applicantForm.patchValue({ 'nomRelation': '' });
            this.applicantForm.patchValue({ 'nomRelation': '' });
            this.applicantForm.patchValue({ 'customerType': '' });
            this.applicantForm.patchValue({ 'constitution': '' });
            this.applicantForm.patchValue({ 'gstRegStatus': '' });
            this.applicantForm.patchValue({ 'appTitle': '' });
            this.applicantForm.patchValue({ 'appRelation': '' });
            this.isNew = false;
            this.isNewCust = 'NO';
            this.searchCust();
        }
    };
    HealthProposalPage.prototype.isOtherPolicy = function (val) {
        if (val == 'YES') {
            this.isOtherPol = 'YES';
        }
        else {
            this.isOtherPol = 'NO';
        }
        localStorage.setItem('isOtherPolicyDetails', this.isOtherPol);
    };
    HealthProposalPage.prototype.getPincodeDetailsifSame = function (val) {
        var _this = this;
        if (val.length == 6) {
            var body_1 = val;
            this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body_1).then(function (res) {
                _this.pinDataPer = res;
                if (_this.pinDataPer.StatusCode == 1) {
                    _this.applicantForm.patchValue({ 'permCity': _this.pinDataPer.CityList[0].CityName });
                    _this.applicantForm.patchValue({ 'permState': _this.pinDataPer.StateName });
                    var proposalStateCity = { pin: body_1, city: _this.pinDataPer.CityList[0].CityName, state: _this.pinDataPer.StateName };
                    localStorage.setItem('PermanentStateCity', JSON.stringify(proposalStateCity));
                }
                else {
                    _this.applicantForm.patchValue({ 'permCity': null });
                    _this.applicantForm.patchValue({ 'permState': null });
                    var proposalStateCity = { pin: body_1, city: null, state: null };
                    _this.presentAlert(_this.pinDataPer.StatusMessage);
                }
            });
        }
    };
    HealthProposalPage.prototype.corrFunction = function (val) {
        //console.log(ev.detail.checked);
        if (val) {
            this.isCorraddSame = true;
            this.applicantForm.patchValue({ 'permAdd1': this.applicantForm.value.applAdd1 });
            this.applicantForm.patchValue({ 'permAdd2': this.applicantForm.value.applAdd2 });
            this.applicantForm.patchValue({ 'permLandMark': this.applicantForm.value.applLandMark });
            this.applicantForm.patchValue({ 'permPinCode': this.applicantForm.value.applPinCode });
            this.getPincodeDetailsifSame(this.applicantForm.value.applPinCode);
        }
        else {
            this.isCorraddSame = false;
            this.applicantForm.patchValue({ 'permAdd1': '' });
            this.applicantForm.patchValue({ 'permAdd2': '' });
            this.applicantForm.patchValue({ 'permLandMark': '' });
            this.applicantForm.patchValue({ 'permPinCode': '' });
            this.applicantForm.patchValue({ 'permCity': '' });
            this.applicantForm.patchValue({ 'permState': '' });
        }
    };
    HealthProposalPage.prototype.isGSTReg = function (val) {
        if (val) {
            this.isRegWithGST = true;
        }
        else {
            this.isRegWithGST = false;
        }
    };
    HealthProposalPage.prototype.isExPolicy1 = function (val) {
        if (val) {
            this.isExistPolicy1 = true;
        }
        else {
            this.isExistPolicy1 = false;
        }
    };
    HealthProposalPage.prototype.isExPolicy2 = function (val) {
        if (val) {
            this.isExistPolicy2 = true;
        }
        else {
            this.isExistPolicy2 = false;
        }
    };
    HealthProposalPage.prototype.isExPolicy3 = function (val) {
        if (val) {
            this.isExistPolicy3 = true;
        }
        else {
            this.isExistPolicy3 = false;
        }
    };
    HealthProposalPage.prototype.isExPolicy4 = function (val) {
        if (val) {
            this.isExistPolicy4 = true;
        }
        else {
            this.isExistPolicy4 = false;
        }
    };
    HealthProposalPage.prototype.isExPolicy5 = function (val) {
        if (val) {
            this.isExistPolicy5 = true;
        }
        else {
            this.isExistPolicy5 = false;
        }
    };
    HealthProposalPage.prototype.isDiseaseVal = function (val) {
        if (val) {
            this.isDisease = true;
        }
        else {
            this.isDisease = false;
        }
    };
    HealthProposalPage.prototype.isDiseaseVal2 = function (val) {
        if (val) {
            this.isDisease2 = true;
        }
        else {
            this.isDisease2 = false;
        }
    };
    HealthProposalPage.prototype.isDiseaseVal3 = function (val) {
        if (val) {
            this.isDisease3 = true;
        }
        else {
            this.isDisease3 = false;
        }
    };
    HealthProposalPage.prototype.isDiseaseVal4 = function (val) {
        if (val) {
            this.isDisease4 = true;
        }
        else {
            this.isDisease4 = false;
        }
    };
    HealthProposalPage.prototype.isDiseaseVal5 = function (val) {
        if (val) {
            this.isDisease5 = true;
        }
        else {
            this.isDisease5 = false;
        }
    };
    HealthProposalPage.prototype.getRelations = function () {
        var _this = this;
        this.cs.getWithParams('/api/healthmaster/GetRelations').then(function (res) {
            _this.ppapRealtions = res;
            _this.relation = res;
        }).catch(function (err) {
            console.log(err);
        });
    };
    HealthProposalPage.prototype.getNomineeRelation = function () {
        var _this = this;
        var producttype;
        if (this.quoteRequestData.ProductType == 'CHI') {
            producttype = 'CHI';
        }
        else if (this.quoteRequestData.ProductType == 'HBOOSTER') {
            producttype = 'HBOOSTER';
        }
        else if (this.quoteRequestData.ProductType == "PPAP") {
            producttype = 'PPAP';
        }
        else {
            console.log('Invalid Product');
        }
        this.cs.postWithParams('/api/healthmaster/GetHealthProposalRelationships?Product=' + producttype, '').then(function (res) {
            console.log("Rel", res);
            _this.nomineeData = res;
            _this.nomineeRelation = _this.nomineeData.NomineeAppointeeRelationship;
        }).catch(function (err) {
            console.log(err);
        });
    };
    HealthProposalPage.prototype.nomineeRelId = function (ev) {
        this.nomineeRelationId = this.nomineeRelation.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    HealthProposalPage.prototype.nomineePPAPRelId = function (ev) {
        this.nomineeRelationId = this.ppapRealtions.find(function (x) { return x.Name == ev.target.value; });
    };
    HealthProposalPage.prototype.appointeeRelId = function (ev) {
        this.appointeeRelationId = this.nomineeRelation.find(function (x) { return x.RelationshipName == ev.target.value; }).RelationshipID;
    };
    HealthProposalPage.prototype.getTitle = function (ev) {
        this.titleId1 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthProposalPage.prototype.getNomTitle = function (ev) {
        this.nomineeTitle = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthProposalPage.prototype.getAppTitle = function (ev) {
        this.appointeeTitle = this.titles.find(function (x) { return x.val == ev.target.value; }).id;
    };
    HealthProposalPage.prototype.showPermanent = function () {
        this.isPermanent = !this.isPermanent;
    };
    HealthProposalPage.prototype.showAppointeeDetail = function () {
        this.apointee = !this.apointee;
    };
    HealthProposalPage.prototype.showOther = function () {
        this.isOther = !this.isOther;
    };
    HealthProposalPage.prototype.showNomineeDetail = function () {
        this.isNomineeDeatils = !this.isNomineeDeatils;
    };
    HealthProposalPage.prototype.getPincodeDetails = function (ev) {
        var _this = this;
        // 
        if (ev.target.value.length == 6) {
            //this.showLoading();
            var body_2 = ev.target.value;
            this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body_2).then(function (res) {
                _this.pinData = res;
                if (_this.pinData.StatusCode == 1) {
                    _this.applicantForm.patchValue({ 'applCity': _this.pinData.CityList[0].CityName });
                    _this.applicantForm.patchValue({ 'applState': _this.pinData.StateName });
                    var proposalStateCity = { pin: body_2, city: _this.pinData.CityList[0].CityName, state: _this.pinData.StateName };
                    localStorage.setItem('CoresspondentStateCity', JSON.stringify(proposalStateCity));
                    //  this.loading.dismiss();
                }
                else {
                    _this.applicantForm.patchValue({ 'applCity': null });
                    _this.applicantForm.patchValue({ 'applState': null });
                    var proposalStateCity = { pin: body_2, city: null, state: null };
                    localStorage.setItem('CoresspondentStateCity', JSON.stringify(proposalStateCity));
                    _this.presentAlert(_this.pinData.StatusMessage);
                    //  this.loading.dismiss();
                }
            });
        }
    };
    HealthProposalPage.prototype.getPincodeDetailsPer = function (ev) {
        var _this = this;
        if (ev.target.value.length == 6) {
            var body_3 = ev.target.value;
            //this.showLoading();
            this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body_3).then(function (res) {
                _this.pinDataPer = res;
                if (_this.pinDataPer.StatusCode == 1) {
                    _this.applicantForm.patchValue({ 'permCity': _this.pinDataPer.CityList[0].CityName });
                    _this.applicantForm.patchValue({ 'permState': _this.pinDataPer.StateName });
                    var proposalStateCity = { pin: body_3, city: _this.pinDataPer.CityList[0].CityName, state: _this.pinDataPer.StateName };
                    localStorage.setItem('PermanentStateCity', JSON.stringify(proposalStateCity));
                    // this.loading.dismiss();
                }
                else {
                    _this.applicantForm.patchValue({ 'permCity': null });
                    _this.applicantForm.patchValue({ 'permState': null });
                    var proposalStateCity = { pin: body_3, city: null, state: null };
                    _this.presentAlert(_this.pinDataPer.StatusMessage);
                    // this.loading.dismiss();
                }
            });
        }
    };
    HealthProposalPage.prototype.getMaritalStatus = function () {
        var _this = this;
        this.cs.getWithParams('/api/healthmaster/getMaritalStatus').then(function (res) {
            _this.maritalStatus = res;
        });
    };
    HealthProposalPage.prototype.showMaritalStatus = function (ev) {
        this.maritalStatusId = this.maritalStatus.MaritalStatusList.find(function (x) { return x.MaritalStatusDesc == ev.target.value; });
    };
    HealthProposalPage.prototype.getAnnualIncome = function () {
        var _this = this;
        this.cs.postWithParams('/api/agent/GetAnnualIncome', '').then(function (res) {
            _this.annualIncome = res;
        }).catch(function (err) {
            console.log(err);
        });
    };
    HealthProposalPage.prototype.getAnnualIncId = function (ev) {
        this.annualIncomeId = this.annualIncome.Values.find(function (x) { return x.Name == ev.target.value; });
    };
    HealthProposalPage.prototype.getInsuredRelation = function (ev) {
        this.relationId1 = this.adultRelationArray.find(function (x) { return x.RelationshipName == ev.target.value; }).RelationshipID;
    };
    HealthProposalPage.prototype.getInsuredPPAPRelation = function (ev) {
        this.relationId1 = this.ppapRealtions.find(function (x) { return x.Name == ev.target.value; }).Value;
    };
    HealthProposalPage.prototype.getOccupation = function () {
        var _this = this;
        this.cs.getWithParams('/api/healthmaster/GetOccupation').then(function (res) {
            _this.occupation = res;
            _this.occupation = _.sortBy(_this.occupation.OccupationLists, 'OCCUPATION');
        });
    };
    HealthProposalPage.prototype.getInsuranceData = function () {
        var _this = this;
        this.cs.getWithParams('/api/healthmaster/GetInsuranceCompany').then(function (res) {
            _this.InsuranceData = res;
            _this.InsuranceData = _.sortBy(_this.InsuranceData, 'Name');
        });
    };
    HealthProposalPage.prototype.getOccId = function (ev) {
        this.occupationId = this.occupation.find(function (x) { return x.OCCUPATION == ev.target.value; });
    };
    HealthProposalPage.prototype.getConstName = function (ev) {
        if (ev.target.value == "0") {
            this.constName = "Constitution of Business";
        }
        else if (ev.target.value == "1") {
            this.constName = "Non Resident Entity";
        }
        else if (ev.target.value == "2") {
            this.constName = "Foreign company registered in India";
        }
        else if (ev.target.value == "3") {
            this.constName = "Foreign LLP";
        }
        else if (ev.target.value == "4") {
            this.constName = "Government Department";
        }
        else if (ev.target.value == "5") {
            this.constName = "Hindu Undivided Family";
        }
        else if (ev.target.value == "6") {
            this.constName = "LLP Partnership";
        }
        else if (ev.target.value == "7") {
            this.constName = "Local Authorities";
        }
        else if (ev.target.value == "8") {
            this.constName = "Partnership";
        }
        else if (ev.target.value == "9") {
            this.constName = "Private Limited Company";
        }
        else if (ev.target.value == "10") {
            this.constName = "Proprietorship";
        }
        else {
            this.constName = "Others";
        }
    };
    HealthProposalPage.prototype.getGSTName = function (ev) {
        if (ev.target.value == "0") {
            this.gstName = "GST Registration Status";
        }
        else if (ev.target.value == "41") {
            this.gstName = "ARN Generated";
        }
        else if (ev.target.value == "42") {
            this.gstName = "Provision ID Obtained";
        }
        else if (ev.target.value == "43") {
            this.gstName = "To be commenced";
        }
        else if (ev.target.value == "44") {
            this.gstName = "Enrolled";
        }
        else {
            this.gstName = "Not applicable";
        }
    };
    HealthProposalPage.prototype.getCustTypeName = function (ev) {
        if (ev.target.value == "0") {
            this.custType = "Customer Type";
        }
        else if (ev.target.value == "21") {
            this.custType = "General";
        }
        else if (ev.target.value == "22") {
            this.custType = "EOU/STP/EHTP";
        }
        else if (ev.target.value == "23") {
            this.custType = "Government";
        }
        else if (ev.target.value == "24") {
            this.custType = "Overseas";
        }
        else if (ev.target.value == "25") {
            this.custType = "Related parties";
        }
        else if (ev.target.value == "26") {
            this.custType = "SEZ";
        }
        else {
            this.custType = "Others";
        }
    };
    HealthProposalPage.prototype.getBackData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.requestBody = JSON.parse(localStorage.getItem('quoteRequest'));
            _this.responseBody = JSON.parse(localStorage.getItem('quoteResponse'));
            resolve();
        });
    };
    HealthProposalPage.prototype.calProposal = function (form) {
        var _this = this;
        console.log(form.value);
        var validForm = this.showFormValidate(form);
        if (validForm) {
            this.cs.showLoader = true;
            // this.getBackData().then(() => {
            this.getPinCodeData(form).then(function () {
                _this.getPinCodeDataPerm(form).then(function () {
                    _this.createCust(form).then(function () {
                        if (_this.requestBody.ProductType == 'CHI') {
                            _this.createCHIMembers(form).then(function () {
                                _this.proposalForCHI(form);
                            });
                        }
                        else if (_this.requestBody.ProductType == 'HBOOSTER') {
                            _this.createHBMembers(form).then(function () {
                                _this.proposalForHB(form);
                            });
                        }
                        else {
                            _this.proposalForPPAP(form);
                        }
                    });
                });
            });
            // });
        }
    };
    HealthProposalPage.prototype.saveProposal = function (form) {
        var _this = this;
        console.log(form.value);
        var validForm = this.showFormValidate(form);
        if (validForm) {
            this.cs.showLoader = true;
            this.getBackData().then(function () {
                console.log("Insured Data", _this.insuredDetails);
                console.log("Response Data", _this.responseBody);
                console.log("Request Data", _this.requestBody);
                _this.getPinCodeData(form).then(function () {
                    _this.getPinCodeDataPerm(form).then(function () {
                        _this.createCust(form).then(function () {
                            if (_this.requestBody.ProductType == 'CHI') {
                                _this.createCHIMembers(form).then(function () {
                                    _this.saveProposalForCHI(form);
                                });
                            }
                            else if (_this.requestBody.ProductType == 'HBOOSTER') {
                                _this.createHBMembers(form).then(function () {
                                    _this.saveProposalForHB(form);
                                });
                            }
                            else {
                                _this.saveProposalForPPAP(form);
                            }
                        });
                    });
                });
            });
        }
    };
    HealthProposalPage.prototype.getAge = function (ev) {
        var date = new Date();
        var age = date.getFullYear() - new Date(ev.value).getFullYear();
        console.log(age);
        if (age < 18) {
            this.showApointee = true;
        }
        else {
            this.applicantForm.patchValue({ 'appTitle': '' });
            this.applicantForm.patchValue({ 'appName': '' });
            this.applicantForm.patchValue({ 'appRelation': '' });
            this.applicantForm.patchValue({ 'appDOB': '' });
            if (this.appointeeRelationId == undefined) {
                this.appointeeRelationId = "";
            }
            if (this.appointeeTitle == undefined) {
                this.appointeeTitle = "";
            }
            this.showApointee = false;
        }
    };
    HealthProposalPage.prototype.getPinCodeData = function (form) {
        var _this = this;
        return new Promise(function (resolve) {
            var body = { "Pincode": form.value.applPinCode, "CityID": _this.pinData.CityList[0].CityID };
            _this.cs.postWithParams('/api/RTOList/GetPincodeID', body).then(function (res) {
                console.log("Get Pin Data ", res);
                _this.pinDataValue = res;
                resolve();
            }).catch(function (err) {
                _this.presentToast(err);
                _this.loading.dismiss();
                resolve();
            });
        });
    };
    HealthProposalPage.prototype.getPinCodeDataPerm = function (form) {
        var _this = this;
        return new Promise(function (resolve) {
            var body = { "Pincode": form.value.permPinCode, "CityID": _this.pinDataPer.CityList[0].CityID };
            _this.cs.postWithParams('/api/RTOList/GetPincodeID', body).then(function (res) {
                console.log("Get Pin Data ", res);
                _this.pinDataPermValue = res;
                resolve();
            }).catch(function (err) {
                _this.presentToast(err);
                _this.loading.dismiss();
                resolve();
            });
        });
    };
    HealthProposalPage.prototype.createCust = function (form) {
        var _this = this;
        return new Promise(function (resolve) {
            var occuid, annuid;
            if (form.value.panNumber == null || form.value.panNumber == undefined) {
                form.value.panNumber = '';
            }
            if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
                form.value.applAdd2 = '';
            }
            if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
                form.value.applLandMark = '';
            }
            if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
                form.value.permAdd2 = '';
            }
            if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
                form.value.permAdd2 = '';
            }
            if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
                form.value.applaltEmail = '';
            }
            if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
                form.value.applOccu = '';
                occuid = '';
            }
            else {
                occuid = _this.occupationId.OCCUPATIONID;
            }
            if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
                form.value.annuIncome == '';
                annuid = '';
            }
            else {
                annuid = _this.annualIncomeId.Value;
            }
            if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
                form.value.applaltMobNo = '';
            }
            var proofText, proofValue;
            if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined || form.value.aadhaarNo == '') {
                form.value.aadhaarNo = '';
                proofText = '';
                proofValue = '';
            }
            else {
                proofText = "Aadhaar Card";
                proofValue = '2';
            }
            var dob;
            dob = moment(form.value.applDOB, 'DD-MM-YYYY').format('DD-MMM-YYYY');
            var body = {
                "CustomerID": 0,
                "HasAddressChanged": true,
                "SetAsDefault": true,
                "TitleText": form.value.applTitle,
                "TitleValue": _this.titleId1.id,
                "Name": form.value.applName,
                "DateOfBirth": dob,
                "MaritalStatusValue": _this.maritalStatusId.MaritalStatusCode,
                "MaritalStatusText": form.value.applMarital,
                "OccupationValue": occuid,
                "OccupationText": form.value.applOccu,
                "AnnualIncomeValue": annuid,
                "AnnualIncomeText": form.value.annuIncome,
                "IdentityProofValue": proofValue,
                "IdentityProofText": proofText,
                "IdentityNumber": form.value.aadhaarNo,
                "PresentAddrLine1": form.value.applAdd1,
                "PresentAddrLine2": form.value.applAdd2,
                "PresentAddrLandmark": form.value.applLandMark,
                "PresentAddrCityValue": _this.pinData.CityList[0].CityID,
                "PresentAddrCityText": form.value.applCity,
                "PresentAddrStateValue": _this.pinData.StateId,
                "PresentAddrStateText": form.value.applState,
                "PresentAddrPincodeValue": _this.pinDataValue.Details[0].Value,
                "PresentAddrPincodeText": form.value.applPinCode,
                "EmailAddress": form.value.applEmail,
                "MobileNumber": form.value.applMobNo.toString(),
                "LandlineNumber": form.value.landLine,
                "EmailAlternate": form.value.applaltEmail,
                "MobileAlternate": form.value.applaltMobNo.toString(),
                "PANNumber": form.value.panNumber,
                "isPermanentAsPresent": true,
                "PermanentAddrLine1": form.value.permAdd1,
                "PermanentAddrLine2": form.value.permAdd2,
                "PermanentAddrLandmark": form.value.permLandMark,
                "PermanentAddrCityValue": _this.pinDataPer.CityList[0].CityID,
                "PermanentAddrCityText": form.value.permCity,
                "PermanentAddrStateValue": _this.pinDataPer.StateId,
                "PermanentAddrStateText": form.value.permState,
                "PermanentAddrPincodeValue": _this.pinDataPermValue.Details[0].Value,
                "PermanentAddrPincodeText": form.value.permPinCode,
                "isGSTINApplicable": false,
                "isUINApplicable": false,
                "GSTDetails": {
                    "GSTIN_NO": form.value.GSTIN,
                    "CONSTITUTION_OF_BUSINESS": form.value.constitution,
                    "CONSTITUTION_OF_BUSINESS_TEXT": _this.constName,
                    "CUSTOMER_TYPE": form.value.customerType,
                    "CUSTOMER_TYPE_TEXT": _this.custType,
                    "PAN_NO": form.value.panNo,
                    "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
                    "GST_REGISTRATION_STATUS_TEXT": _this.gstName
                }
            };
            console.log("Create Cust Body", body);
            var str = JSON.stringify(body);
            console.log("Create Cust stringify", str);
            localStorage.setItem('customerRequest', str);
            if (_this.requestBody.ProductType != 'CHI') {
                _this.cs.postWithParams('/api/customer/saveeditcustomer', str).then(function (res) {
                    _this.createCustData = res;
                    localStorage.setItem('customerResponse', JSON.stringify(res));
                    resolve();
                }).catch(function (err) {
                    _this.presentAlert(err.error.Message);
                    _this.cs.showLoader = false;
                    resolve();
                });
            }
            else {
                _this.cs.postWithParams('/api/customer/saveeditcustomer', str).then(function (res) {
                    _this.createCustData = res;
                    resolve();
                }).catch(function (err) {
                    _this.presentAlert(err.error.Message);
                    _this.cs.showLoader = false;
                    resolve();
                });
            }
            // resolve();
        });
    };
    // calculate proposal
    HealthProposalPage.prototype.proposalForCHI = function (form) {
        var _this = this;
        console.log("Request", this.requestBody, this.pinDataValue, this.pinDataPermValue);
        //this.showLoading();
        var appointeeDate;
        var occuid, annuid;
        if (this.isRegWithGST == false) {
            form.value.GSTIN = '';
            form.value.GSTIN = '';
            this.constName = '';
            form.value.customerType = '';
            this.custType = '';
            form.value.panNo = '';
            form.value.gstRegStatus = '';
            this.gstName = '';
        }
        if (this.showApointee == false) {
            form.value.appRelation = '';
            appointeeDate = '';
            form.value.appTitle = '';
            form.value.appName = '';
            this.appointeeTitle = '';
            this.appointeeRelationId = '';
        }
        else {
            appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY');
        }
        if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
            form.value.aadhaarNo = '';
        }
        if (form.value.panNumber == null || form.value.panNumber == undefined) {
            form.value.panNumber = '';
        }
        if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
            form.value.applAdd2 = '';
        }
        if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
            form.value.applLandMark = '';
        }
        if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
            form.value.applaltEmail = '';
        }
        if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
            form.value.applaltMobNo = '';
        }
        if (form.value.apsId == null || form.value.apsId == undefined) {
            form.value.apsId = '';
        }
        if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
            form.value.custRefNo = '';
        }
        if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
            form.value.applOccu = '';
            occuid = '';
        }
        else {
            occuid = this.occupationId.OCCUPATIONID;
        }
        if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
            form.value.annuIncome == '';
            annuid = '';
        }
        else {
            annuid = this.annualIncomeId.Value;
        }
        var Body = {
            "IsCustomerPID": "true",
            "UserType": "Agent",
            "ipaddress": config.ipAddress,
            "IPGPayment": "true",
            "ProductType": this.requestBody.ProductType,
            "ProductName": this.requestBody.CHIProductName,
            "GSTApplicable": "false",
            "UINApplicable": "false",
            "PolicyID": this.responseBody.HealthDetails[0].PolicyID,
            "DealID": this.responseBody.HealthDetails[0].DealId,
            "IS_IBANK_RELATIONSHIP": "NO",
            "APS_ID": form.value.apsId,
            "CUSTOMER_REF_NO": form.value.custRefNo,
            "Members": this.memberObj,
            "NomineeName": form.value.nomName,
            "NomineeTitleID": this.nomineeTitle.id,
            "NomineeRelationShipID": this.nomineeRelationId.RelationshipID,
            "NomineeRelationShipText": form.value.nomRelation,
            "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
            "HUF": "Y",
            "AppointeeLastName": form.value.appName,
            "AppointeeTitleID": this.appointeeTitle,
            "AppointeeRelationShipID": this.appointeeRelationId,
            "AppointeeRelationShip": form.value.appRelation,
            "AppointeeDOB": appointeeDate,
            "SetAsDefault": "NO",
            "AddressChange": true,
            "CustomerID": this.createCustData.CustomerID,
            "PF_CUSTOMERID": this.createCustData.PFCustomerID,
            "OccupationID": occuid,
            "OccupationDesc": form.value.applOccu,
            "IDProofID": -1,
            "IDProofValue": "",
            "AadharEKYCYesNo": "N",
            "Title": "2",
            "InsuredID": "4",
            "CustomerName": form.value.applName,
            "Address1": form.value.applAdd1,
            "Address2": form.value.applAdd2,
            "Landmark": form.value.applLandMark,
            "StateID": this.pinData.StateId,
            "CityID": this.pinData.CityList[0].CityID,
            "PincodeID": this.pinDataValue.Details[0].Value,
            "PermanentAddrLine1": form.value.permAdd1,
            "PermanentAddrLine2": form.value.permAdd2,
            "PermanentAddrLandmark": form.value.permLandMark,
            "PermanentAddrStateID": this.pinDataPer.StateId,
            "PermanentAddrCityID": this.pinDataPer.CityList[0].CityID,
            "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,
            "Mobile": form.value.applMobNo.toString(),
            "LandLineNo": '',
            "STD": "",
            "Email": form.value.applEmail,
            "FinancierBranch": "",
            "FinancierName": "",
            "PaymentMode": "NONE",
            "Cheque_Type": "",
            "ModeID": "",
            "CardExpiryYear": "",
            "CardExpiryMonth": "",
            "CardNumber": "",
            "CVVNumber": "",
            "CardType": "",
            "CardHolderName": "",
            "PANNumber": form.value.panNumber,
            "AadhaarNumber": form.value.aadhaarNo,
            "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
            "SubLimit": "",
            "AddOn1": this.requestBody.AddOn1,
            "AddOn3": this.requestBody.AddOn3,
            "AddOn5": this.requestBody.AddOn5,
            "AddOn6": this.requestBody.AddOn6,
            "MaritalStatus": "SINGLE",
            "AnnualIncome": annuid,
            "isHobbyOccupationVisible": "false"
        };
        console.log("CHI RequestBody", Body);
        var str = JSON.stringify(Body);
        console.log("Stringify", str);
        localStorage.setItem('proposalRequest', JSON.stringify(Body));
        this.cs.postWithParams('/api/Health/SaveEditProposal', str).then(function (res) {
            console.log("CHI Response", res);
            if (res.StatusCode == '1') {
                localStorage.setItem('proposalResponse', JSON.stringify(res));
                _this.cs.showLoader = false;
                _this.router.navigateByUrl('health-summary');
            }
            else {
                _this.cs.showLoader = false;
                _this.messageAlert(res.StatusMessage);
            }
            //this.loading.dismiss();
        }).catch(function (err) {
            _this.presentToast(err.error.Message);
            _this.cs.showLoader = false;
        });
    };
    HealthProposalPage.prototype.saveProposalForCHI = function (form) {
        var _this = this;
        console.log("Request", this.requestBody, this.pinDataValue, this.pinDataPermValue);
        //this.showLoading();
        var appointeeDate;
        var occuid, annuid;
        if (this.isRegWithGST == false) {
            form.value.GSTIN = '';
            form.value.GSTIN = '';
            this.constName = '';
            form.value.customerType = '';
            this.custType = '';
            form.value.panNo = '';
            form.value.gstRegStatus = '';
            this.gstName = '';
        }
        if (this.showApointee == false) {
            form.value.appRelation = '';
            appointeeDate = '';
            form.value.appTitle = '';
            form.value.appName = '';
            this.appointeeTitle = '';
            this.appointeeRelationId = '';
        }
        else {
            appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY');
        }
        if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
            form.value.aadhaarNo = '';
        }
        if (form.value.panNumber == null || form.value.panNumber == undefined) {
            form.value.panNumber = '';
        }
        if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
            form.value.applAdd2 = '';
        }
        if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
            form.value.applLandMark = '';
        }
        if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
            form.value.applaltEmail = '';
        }
        if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
            form.value.applaltMobNo = '';
        }
        if (form.value.apsId == null || form.value.apsId == undefined) {
            form.value.apsId = '';
        }
        if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
            form.value.custRefNo = '';
        }
        if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
            form.value.applOccu = '';
            occuid = '';
        }
        else {
            occuid = this.occupationId.OCCUPATIONID;
        }
        if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
            form.value.annuIncome == '';
            annuid = '';
        }
        else {
            annuid = this.annualIncomeId.Value;
        }
        var Body = {
            "IsCustomerPID": "true",
            "UserType": "Agent",
            "ipaddress": config.ipAddress,
            "IPGPayment": "true",
            "ProductType": this.requestBody.ProductType,
            "ProductName": this.requestBody.CHIProductName,
            "GSTApplicable": "false",
            "UINApplicable": "false",
            "PolicyID": this.responseBody.HealthDetails[0].PolicyID,
            "DealID": this.responseBody.HealthDetails[0].DealId,
            "IS_IBANK_RELATIONSHIP": "NO",
            "APS_ID": form.value.apsId,
            "CUSTOMER_REF_NO": form.value.custRefNo,
            'Members': this.memberObj,
            "NomineeName": form.value.nomName,
            "NomineeTitleID": this.nomineeTitle.id,
            "NomineeRelationShipID": this.nomineeRelationId.RelationshipID,
            "NomineeRelationShipText": form.value.nomRelation,
            "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
            "HUF": "Y",
            "AppointeeLastName": form.value.appName,
            "AppointeeTitleID": this.appointeeTitle,
            "AppointeeRelationShipID": this.appointeeRelationId,
            "AppointeeRelationShip": form.value.appRelation,
            "AppointeeDOB": appointeeDate,
            "SetAsDefault": "NO",
            "AddressChange": true,
            "CustomerID": this.createCustData.CustomerID,
            "PF_CUSTOMERID": this.createCustData.PFCustomerID,
            "OccupationID": occuid,
            "OccupationDesc": form.value.applOccu,
            "IDProofID": -1,
            "IDProofValue": "",
            "AadharEKYCYesNo": "N",
            "Title": this.titleId1.id,
            "InsuredID": "4",
            "CustomerName": form.value.applName,
            "Address1": form.value.applAdd1,
            "Address2": form.value.applAdd2,
            "Landmark": form.value.applLandMark,
            "StateID": this.pinData.StateId,
            "CityID": this.pinData.CityList[0].CityID,
            "PincodeID": this.pinDataValue.Details[0].Value,
            "PermanentAddrLine1": form.value.permAdd1,
            "PermanentAddrLine2": form.value.permAdd2,
            "PermanentAddrLandmark": form.value.permLandMark,
            "PermanentAddrStateID": this.pinDataPer.StateId,
            "PermanentAddrCityID": this.pinDataPer.CityList[0].CityID,
            "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,
            "Mobile": form.value.applMobNo.toString(),
            "LandLineNo": '',
            "STD": "",
            "Email": form.value.applEmail,
            "FinancierBranch": "",
            "FinancierName": "",
            "PaymentMode": "NONE",
            "Cheque_Type": "",
            "ModeID": "00001",
            "CardExpiryYear": "2014",
            "CardExpiryMonth": "07",
            "CardNumber": "5454454545544554",
            "CVVNumber": "123",
            "CardType": "MC",
            "CardHolderName": "xfgshg",
            "PANNumber": form.value.panNumber,
            "AadhaarNumber": form.value.aadhaarNo,
            "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
            "SubLimit": "",
            "AddOn1": this.requestBody.AddOn1,
            "AddOn3": this.requestBody.AddOn3,
            "AddOn5": this.requestBody.AddOn5,
            "AddOn6": this.requestBody.AddOn6,
            "MaritalStatus": "SINGLE",
            "AnnualIncome": annuid,
            "isHobbyOccupationVisible": "false",
            "SaveType": "PROPOSAL"
        };
        console.log("CHI RequestBody", Body);
        var str = JSON.stringify(Body);
        console.log("Stringify", str);
        localStorage.setItem('reqFromProp', JSON.stringify(Body));
        this.cs.postWithParams('/api/Health/SaveEditProposal', str).then(function (res) {
            console.log("CHI Response", res);
            if (res.StatusCode == '1') {
                localStorage.setItem('resFromProp', JSON.stringify(res));
                _this.cs.showLoader = false;
                _this.messageAlert('Your Proposal hasbeen saved with Policy Id: ' + res.SavePolicy[0].PolicyID + ' and will be expired in 7 days');
                _this.router.navigateByUrl('health');
            }
            else {
                _this.cs.showLoader = false;
                _this.messageAlert(res.StatusMessage);
            }
        }).catch(function (err) {
            _this.presentToast(err.error.Message);
            _this.cs.showLoader = false;
        });
    };
    HealthProposalPage.prototype.proposalForPPAP = function (form) {
        var _this = this;
        // this.showLoading();
        var appointeeDate;
        var occuid, annuid;
        var isotherPolicy, otherPolicyNo, claimsMade, PolicyValidFrom, PolicyValidTo, KidAdultType, InsuranceCompany, Gender;
        var insuredData = this.insuredPPAPForm.value;
        var quoteRequestData = JSON.parse(localStorage.getItem('quoteRequest'));
        if (this.showOtherPolicyDetails == false) {
            isotherPolicy = 'false';
            otherPolicyNo = '';
            PolicyValidFrom = -1;
            PolicyValidTo = -1;
            InsuranceCompany = '';
            claimsMade = false;
        }
        else {
            isotherPolicy = 'true';
            otherPolicyNo = insuredData.insurancePolicy;
            PolicyValidFrom = insuredData.policyValidFrom;
            PolicyValidTo = insuredData.policyValidTill;
            InsuranceCompany = insuredData.insuranceComapny;
            claimsMade = this.showClaimsDetails;
        }
        if (insuredData.member1Cat == 'Adult') {
            KidAdultType = '1';
        }
        else {
            KidAdultType = '2';
        }
        if (insuredData.member1Title == 'Mr.') {
            Gender = 'Male';
        }
        else {
            Gender = 'Female';
        }
        ;
        if (insuredData.insuredpanno == null || insuredData.insuredpanno == undefined || insuredData.insuredpanno == '') {
            insuredData.insuredpanno = '';
        }
        //  let proposalDate=moment().add(2, 'days').format('DD-MMM-YYYY');
        var proposalDate = moment(insuredData.riskStartDate).format('DD-MMM-YYYY');
        var insuredRelationName = insuredData.member1Rela;
        // let insuredRelationshipID = this.adultRelationArray.find((x: any) => x.Name == insuredRelationName);
        if (this.isRegWithGST == false) {
            form.value.GSTIN = '';
            form.value.GSTIN = '';
            this.constName = '';
            form.value.customerType = '';
            this.custType = '';
            form.value.panNo = '';
            form.value.gstRegStatus = '';
            this.gstName = '';
        }
        if (this.showApointee == false) {
            form.value.appRelation = '';
            appointeeDate = '';
            form.value.appTitle = '';
            form.value.appName = '';
            this.appointeeTitle = '';
            this.appointeeRelationId = '';
        }
        else {
            appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY');
        }
        if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
            form.value.aadhaarNo = '';
        }
        if (form.value.panNumber == null || form.value.panNumber == undefined) {
            form.value.panNumber = '';
        }
        if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
            form.value.applAdd2 = '';
        }
        if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
            form.value.applLandMark = '';
        }
        if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
            form.value.applaltEmail = '';
        }
        if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
            form.value.applaltMobNo = '';
        }
        if (form.value.apsId == null || form.value.apsId == undefined) {
            form.value.apsId = '';
        }
        if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
            form.value.custRefNo = '';
        }
        if (form.value.sourcingCode == null || form.value.sourcingCode == undefined) {
            form.value.sourcingCode = '';
        }
        if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
            form.value.applOccu = '';
            occuid = '';
        }
        else {
            occuid = this.occupationId.OCCUPATIONID;
        }
        if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
            form.value.annuIncome == '';
            annuid = '';
        }
        else {
            annuid = this.annualIncomeId.Value;
        }
        var Body = {
            "IsCustomerPID": "true",
            "UserType": "Agent",
            "ipaddress": config.ipAddress,
            "IPGPayment": "true",
            "ProductType": this.requestBody.ProductType,
            "SubProduct": this.requestBody.SubProduct,
            "ProposalDate": proposalDate,
            "GSTApplicable": "false",
            "UINApplicable": "false",
            "PolicyID": this.responseBody.HealthDetails[0].PolicyID,
            "DealID": this.responseBody.HealthDetails[0].DealId,
            "IS_IBANK_RELATIONSHIP": "NO",
            "APS_ID": form.value.apsId,
            "CUSTOMER_REF_NO": form.value.custRefNo,
            "CustomerID": this.createCustData.CustomerID,
            "PF_CUSTOMERID": this.createCustData.PFCustomerID,
            "SumInsured": localStorage.getItem('SumInsured'),
            "CustomerGSTDetails": {
                "GSTIN_NO": form.value.GSTIN,
                "CONSTITUTION_OF_BUSINESS": form.value.constitution,
                "CONSTITUTION_OF_BUSINESS_TEXT": this.constName,
                "CUSTOMER_TYPE": form.value.customerType,
                "CUSTOMER_TYPE_TEXT": this.custType,
                "PAN_NO": form.value.panNo,
                "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
                "GST_REGISTRATION_STATUS_TEXT": this.gstName
            },
            "Members": [
                {
                    "Name": insuredData.member1Name,
                    "Gender": Gender,
                    "RelationshipID": this.relationId1,
                    "RelationshipName": insuredRelationName,
                    "PanNumber": insuredData.insuredpanno,
                    "DOB": quoteRequestData.DOB,
                    "IsOtherPolicy": isotherPolicy,
                    "InsuranceCompany": InsuranceCompany,
                    "OtherPolicyNo": otherPolicyNo,
                    "PolicyValidFrom": PolicyValidFrom,
                    "PolicyValidTill": PolicyValidTo,
                    "ClaimsMade": claimsMade,
                    "KidAdultType": KidAdultType,
                    "FirstName": "",
                    "MiddleName": "",
                    "LastName": "",
                    "Height": "",
                    "Weight": ""
                }
            ],
            "NomineeName": form.value.nomName,
            "NomineeTitleID": this.nomineeTitle.id,
            "NomineeRelationShipID": this.nomineeRelationId.Value,
            "NomineeRelationShipText": form.value.nomRelation,
            "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
            "HUF": "Y",
            "AppointeeLastName": form.value.appName,
            "AppointeeTitleID": this.appointeeTitle,
            "AppointeeRelationShipID": this.appointeeRelationId,
            "AppointeeRelationShip": form.value.appRelation,
            "AppointeeDOB": appointeeDate,
            "SetAsDefault": "NO",
            "AddressChange": true,
            "OccupationID": occuid,
            "OccupationDesc": form.value.applOccu,
            "IDProofID": -1,
            "IDProofValue": "",
            "AadharEKYCYesNo": "N",
            "Title": this.titleId1.id,
            "InsuredID": "4",
            "CustomerName": form.value.applName,
            "Address1": form.value.applAdd1,
            "Address2": form.value.applAdd2,
            "Landmark": form.value.applLandMark,
            "StateID": this.pinData.StateId,
            "CityID": this.pinData.CityList[0].CityID,
            "PincodeID": this.pinDataValue.Details[0].Value,
            "PermanentAddrLine1": form.value.permAdd1,
            "PermanentAddrLine2": form.value.permAdd2,
            "PermanentAddrLandmark": form.value.permLandMark,
            "PermanentAddrStateID": this.pinDataPer.StateId,
            "PermanentAddrCityID": this.pinDataPer.CityList[0].CityID,
            "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,
            "Mobile": form.value.applMobNo.toString(),
            "LandLineNo": '',
            "STD": "",
            "Email": form.value.applEmail,
            "FinancierBranch": "",
            "FinancierName": "",
            "PaymentMode": "NONE",
            "CardType": "",
            "ModeID": "0",
            "CardHolderName": "",
            "CardNumber": "",
            "CVVNumber": "",
            "CardExpiryMonth": "",
            "CardExpiryYear": "",
            "PANNumber": form.value.panNumber,
            "AadhaarNumber": form.value.aadhaarNo,
            "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
            "MaritalStatus": this.maritalStatusId.MaritalStatusCode,
            "AnnualIncome": annuid,
            "HobbyID": "",
            "AlternateEMail": form.value.applaltEmail,
            "AlternateMobile": form.value.applaltMobNo.toString(),
            "SourcingCode": form.value.sourcingCode,
            "SaveType": ""
        };
        this.cs.showLoader = true;
        var stringifyReq = JSON.stringify(Body);
        localStorage.setItem('proposalRequest', JSON.stringify(Body));
        this.cs.postWithParams('/api/PPAP/SaveEditProposal', stringifyReq).then(function (res) {
            console.log("PPAP Response", res);
            if (res.StatusCode == '1') {
                localStorage.setItem('proposalResponse', JSON.stringify(res));
                _this.router.navigateByUrl('health-summary');
                _this.cs.showLoader = false;
            }
            else {
                _this.cs.showLoader = false;
                _this.messageAlert(res.StatusMessage);
            }
            //this.loading.dismiss();
        }).catch(function (err) {
            _this.presentToast(err.error.Message);
            _this.cs.showLoader = false;
        });
    };
    HealthProposalPage.prototype.saveProposalForPPAP = function (form) {
        var _this = this;
        var appointeeDate;
        var occuid, annuid;
        var isotherPolicy, otherPolicyNo, claimsMade, PolicyValidFrom, PolicyValidTo, KidAdultType, InsuranceCompany, Gender;
        var insuredData = this.insuredPPAPForm.value;
        var quoteRequestData = JSON.parse(localStorage.getItem('quoteRequest'));
        if (this.showOtherPolicyDetails == false) {
            isotherPolicy = 'false';
            otherPolicyNo = '';
            PolicyValidFrom = -1;
            PolicyValidTo = -1;
            InsuranceCompany = '';
            claimsMade = 'false';
        }
        else {
            isotherPolicy = 'true';
            otherPolicyNo = insuredData.insurancePolicy;
            PolicyValidFrom = insuredData.policyValidFrom;
            PolicyValidTo = insuredData.policyValidTill;
            InsuranceCompany = insuredData.insuranceComapny;
            claimsMade = this.showClaimsDetails;
        }
        if (insuredData.member1Cat == 'Adult') {
            KidAdultType = '1';
        }
        else {
            KidAdultType = '2';
        }
        if (insuredData.member1Title == 'Mr.') {
            Gender = 'Male';
        }
        else {
            Gender = 'Female';
        }
        ;
        //  let proposalDate=moment().add(2, 'days').format('DD-MMM-YYYY');
        var proposalDate = moment(insuredData.riskStartDate).format('DD-MMM-YYYY');
        var insuredRelationName = insuredData.member1Rela;
        // let insuredRelationshipID = this.adultRelationArray.find((x: any) => x.Name == insuredRelationName);
        if (this.isRegWithGST == false) {
            form.value.GSTIN = '';
            form.value.GSTIN = '';
            this.constName = '';
            form.value.customerType = '';
            this.custType = '';
            form.value.panNo = '';
            form.value.gstRegStatus = '';
            this.gstName = '';
        }
        if (this.showApointee == false) {
            form.value.appRelation = '';
            appointeeDate = '';
            form.value.appTitle = '';
            form.value.appName = '';
            this.appointeeTitle = '';
            this.appointeeRelationId = '';
        }
        else {
            appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY');
        }
        if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
            form.value.aadhaarNo = '';
        }
        if (form.value.panNumber == null || form.value.panNumber == undefined) {
            form.value.panNumber = '';
        }
        if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
            form.value.applAdd2 = '';
        }
        if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
            form.value.applLandMark = '';
        }
        if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
            form.value.applaltEmail = '';
        }
        if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
            form.value.applaltMobNo = '';
        }
        if (form.value.apsId == null || form.value.apsId == undefined) {
            form.value.apsId = '';
        }
        if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
            form.value.custRefNo = '';
        }
        if (form.value.sourcingCode == null || form.value.sourcingCode == undefined) {
            form.value.sourcingCode = '';
        }
        if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
            form.value.applOccu = '';
            occuid = '';
        }
        else {
            occuid = this.occupationId.OCCUPATIONID;
        }
        if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
            form.value.annuIncome == '';
            annuid = '';
        }
        else {
            annuid = this.annualIncomeId.Value;
        }
        var Body = {
            "IsCustomerPID": "true",
            "UserType": "Agent",
            "ipaddress": config.ipAddress,
            "IPGPayment": "true",
            "ProductType": this.requestBody.ProductType,
            "SubProduct": this.requestBody.SubProduct,
            "ProposalDate": proposalDate,
            "GSTApplicable": "false",
            "UINApplicable": "false",
            "PolicyID": this.responseBody.HealthDetails[0].PolicyID,
            "DealID": this.responseBody.HealthDetails[0].DealId,
            "IS_IBANK_RELATIONSHIP": "NO",
            "APS_ID": form.value.apsId,
            "CUSTOMER_REF_NO": form.value.custRefNo,
            "CustomerID": this.createCustData.CustomerID,
            "PF_CUSTOMERID": this.createCustData.PFCustomerID,
            "SumInsured": localStorage.getItem('SumInsured'),
            "CustomerGSTDetails": {
                "GSTIN_NO": form.value.GSTIN,
                "CONSTITUTION_OF_BUSINESS": form.value.constitution,
                "CONSTITUTION_OF_BUSINESS_TEXT": this.constName,
                "CUSTOMER_TYPE": form.value.customerType,
                "CUSTOMER_TYPE_TEXT": this.custType,
                "PAN_NO": form.value.panNo,
                "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
                "GST_REGISTRATION_STATUS_TEXT": this.gstName
            },
            "Members": [
                {
                    "Name": insuredData.member1Name,
                    "Gender": Gender,
                    "RelationshipID": this.relationId1,
                    "RelationshipName": insuredRelationName,
                    "PanNumber": insuredData.insuredpanno,
                    "DOB": quoteRequestData.DOB,
                    "IsOtherPolicy": isotherPolicy,
                    "InsuranceCompany": InsuranceCompany,
                    "OtherPolicyNo": otherPolicyNo,
                    "PolicyValidFrom": PolicyValidFrom,
                    "PolicyValidTill": PolicyValidTo,
                    "ClaimsMade": claimsMade,
                    "KidAdultType": KidAdultType,
                    "FirstName": "",
                    "MiddleName": "",
                    "LastName": "",
                    "Height": "",
                    "Weight": ""
                }
            ],
            "NomineeName": form.value.nomName,
            "NomineeTitleID": this.nomineeTitle.id,
            "NomineeRelationShipID": this.nomineeRelationId.Value,
            "NomineeRelationShipText": form.value.nomRelation,
            "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
            "HUF": "Y",
            "AppointeeLastName": form.value.appName,
            "AppointeeTitleID": this.appointeeTitle,
            "AppointeeRelationShipID": this.appointeeRelationId,
            "AppointeeRelationShip": form.value.appRelation,
            "AppointeeDOB": appointeeDate,
            "SetAsDefault": "NO",
            "AddressChange": true,
            "OccupationID": occuid,
            "OccupationDesc": form.value.applOccu,
            "IDProofID": -1,
            "IDProofValue": "",
            "AadharEKYCYesNo": "N",
            "Title": this.titleId1.id,
            "InsuredID": "4",
            "CustomerName": form.value.applName,
            "Address1": form.value.applAdd1,
            "Address2": form.value.applAdd2,
            "Landmark": form.value.applLandMark,
            "StateID": this.pinData.StateId,
            "CityID": this.pinData.CityList[0].CityID,
            "PincodeID": this.pinDataValue.Details[0].Value,
            "PermanentAddrLine1": form.value.permAdd1,
            "PermanentAddrLine2": form.value.permAdd2,
            "PermanentAddrLandmark": form.value.permLandMark,
            "PermanentAddrStateID": this.pinDataPer.StateId,
            "PermanentAddrCityID": this.pinDataPer.CityList[0].CityID,
            "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,
            "Mobile": form.value.applMobNo.toString(),
            "LandLineNo": '',
            "STD": "",
            "Email": form.value.applEmail,
            "FinancierBranch": "",
            "FinancierName": "",
            "PaymentMode": "NONE",
            "CardType": "",
            "ModeID": "0",
            "CardHolderName": "",
            "CardNumber": "",
            "CVVNumber": "",
            "CardExpiryMonth": "",
            "CardExpiryYear": "",
            "PANNumber": form.value.panNumber,
            "AadhaarNumber": form.value.aadhaarNo,
            "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
            "MaritalStatus": this.maritalStatusId.MaritalStatusCode,
            "AnnualIncome": annuid,
            "HobbyID": "",
            "AlternateEMail": form.value.applaltEmail,
            "AlternateMobile": form.value.applaltMobNo.toString(),
            "SourcingCode": form.value.sourcingCode,
            "SaveType": "PROPOSAL"
        };
        var stringifyReq = JSON.stringify(Body);
        localStorage.setItem('reqFromProp', JSON.stringify(Body));
        this.cs.postWithParams('/api/PPAP/SaveEditProposal', stringifyReq).then(function (res) {
            console.log("PPAP Response", res);
            if (res.StatusCode == '1') {
                localStorage.setItem('resFromProp', JSON.stringify(res));
                _this.cs.showLoader = false;
                _this.messageAlert('Your Proposal has been saved with Policy Id: ' + res.SavePolicy[0].PolicyID + ' and will be expired in 7 days');
                _this.router.navigateByUrl('health');
            }
            else {
                _this.cs.showLoader = false;
                _this.messageAlert(res.StatusMessage);
            }
            //this.loading.dismiss();
        }).catch(function (err) {
            _this.presentToast(err.error.Message);
            _this.cs.showLoader = false;
        });
    };
    HealthProposalPage.prototype.proposalForHB = function (form) {
        var _this = this;
        // this.showLoading();
        var appointeeDate;
        var occuid, annuid;
        if (this.isRegWithGST == false) {
            form.value.GSTIN = '';
            form.value.GSTIN = '';
            this.constName = '';
            form.value.customerType = '';
            this.custType = '';
            form.value.panNo = '';
            form.value.gstRegStatus = '';
            this.gstName = '';
        }
        if (this.isExistPolicy1 == false) {
            this.insuredForm.value.TypeofPolicy1 = '';
            this.insuredForm.value.PolicyDuration1 = '';
            this.insuredForm.value.InsuranceCompany1 = '';
            this.insuredForm.value.SumInsured1 = '';
        }
        if (this.isExistPolicy2 == false) {
            this.insuredForm.value.TypeofPolicy2 = '';
            this.insuredForm.value.PolicyDuration2 = '';
            this.insuredForm.value.InsuranceCompany2 = '';
            this.insuredForm.value.SumInsured2 = '';
        }
        if (this.isExistPolicy3 == false) {
            this.insuredForm.value.TypeofPolicy3 = '';
            this.insuredForm.value.PolicyDuration3 = '';
            this.insuredForm.value.InsuranceCompany3 = '';
            this.insuredForm.value.SumInsured3 = '';
        }
        if (this.isExistPolicy4 == false) {
            this.insuredForm.value.TypeofPolicy4 = '';
            this.insuredForm.value.PolicyDuration4 = '';
            this.insuredForm.value.InsuranceCompany4 = '';
            this.insuredForm.value.SumInsured4 = '';
        }
        if (this.isExistPolicy5 == false) {
            this.insuredForm.value.TypeofPolicy5 = '';
            this.insuredForm.value.PolicyDuration5 = '';
            this.insuredForm.value.InsuranceCompany5 = '';
            this.insuredForm.value.SumInsured5 = '';
        }
        if (this.showApointee == false) {
            form.value.appRelation = '';
            appointeeDate = '';
            form.value.appTitle = '';
            form.value.appName = '';
            this.appointeeTitle = '';
            this.appointeeRelationId = '';
        }
        else {
            appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY');
        }
        if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
            form.value.aadhaarNo = '';
        }
        if (form.value.panNumber == null || form.value.panNumber == undefined) {
            form.value.panNumber = '';
        }
        if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
            form.value.applAdd2 = '';
        }
        if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
            form.value.applLandMark = '';
        }
        if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
            form.value.applaltEmail = '';
        }
        if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
            form.value.applaltMobNo = '';
        }
        if (form.value.apsId == null || form.value.apsId == undefined) {
            form.value.apsId = '';
        }
        if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
            form.value.custRefNo = '';
        }
        if (form.value.sourcingCode == null || form.value.sourcingCode == undefined) {
            form.value.sourcingCode = '';
        }
        if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
            form.value.applOccu = '';
            occuid = '';
        }
        else {
            occuid = this.occupationId.OCCUPATIONID;
        }
        if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
            form.value.annuIncome == '';
            annuid = '';
        }
        else {
            annuid = this.annualIncomeId.Value;
        }
        var userDate = this.quoteRequestData.Adult1Age;
        var requestBodyHB = {
            "Product": "NA",
            "IsCustomerPID": "true",
            "UserType": "Agent",
            "ipaddress": config.ipAddress,
            "IPGPayment": "true",
            "ProductType": this.requestBody.ProductType,
            "ProductName": this.requestBody.ProductName,
            "GSTApplicable": "false",
            "UINApplicable": "false",
            "CustomerGSTDetails": {
                "GSTIN_NO": form.value.GSTIN,
                "CONSTITUTION_OF_BUSINESS": form.value.constitution,
                "CONSTITUTION_OF_BUSINESS_TEXT": this.constName,
                "CUSTOMER_TYPE": form.value.customerType,
                "CUSTOMER_TYPE_TEXT": this.custType,
                "PAN_NO": form.value.panNo,
                "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
                "GST_REGISTRATION_STATUS_TEXT": this.gstName
            },
            "PolicyID": this.responseBody.HealthDetails[0].PolicyID,
            "DealID": this.responseBody.HealthDetails[0].DealId,
            "IS_IBANK_RELATIONSHIP": "NO",
            "APS_ID": [form.value.apsId],
            'Members': this.memberObj,
            "NomineeName": form.value.nomName,
            "NomineeTitleID": this.nomineeTitle.id,
            "NomineeRelationShipID": this.nomineeRelationId.RelationshipID,
            "NomineeRelationShipText": form.value.nomRelation,
            "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
            "HUF": "Y",
            "AppointeeLastName": form.value.appName,
            "AppointeeTitleID": this.appointeeTitle,
            "AppointeeRelationShipID": this.appointeeRelationId,
            "AppointeeRelationShip": form.value.appRelation,
            "AppointeeDOB": appointeeDate,
            "SetAsDefault": "NO",
            "AddressChange": "YES",
            "CustomerID": this.createCustData.CustomerID,
            "OccupationID": occuid,
            "OccupationDesc": form.value.applOccu,
            "IDProofID": "2",
            "IDProofValue": "978456320164",
            "AadharEKYCYesNo": "N",
            "Title": this.titleId1.id,
            "InsuredID": "4",
            "CustomerName": form.value.applName,
            "Address1": form.value.applAdd1,
            "Address2": form.value.applAdd2,
            "Landmark": form.value.applLandMark,
            "StateID": this.pinData.StateId,
            "CityID": this.pinData.CityList[0].CityID,
            "PincodeID": this.pinDataValue.Details[0].Value,
            "PermanentAddrLine1": form.value.permAdd1,
            "PermanentAddrLine2": form.value.permAdd2,
            "PermanentAddrLandmark": form.value.permLandMark,
            "PermanentAddrStateID": this.pinDataPer.StateId,
            "PermanentAddrCityID": this.pinDataPer.CityList[0].CityID,
            "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,
            "Mobile": form.value.applMobNo.toString(),
            "LandLineNo": '',
            "STD": "",
            "Email": form.value.applEmail,
            "FinancierBranch": "",
            "FinancierName": "",
            "PaymentMode": "NONE",
            "Cheque_Type": "",
            "ModeID": "0001",
            "CardExpiryYear": "",
            "CardExpiryMonth": "",
            "CardNumber": "",
            "CVVNumber": "",
            "CardType": "",
            "CardHolderName": "",
            "PANNumber": form.value.panNumber,
            "AadhaarNumber": form.value.aadhaarNo,
            "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
            "SubLimit": "",
            "AddOn1": this.requestBody.AddOn1,
            "AddOn3": this.requestBody.AddOn3,
            "AddOn5": "false",
            "AddOn6": "false",
            "MaritalStatus": this.maritalStatusId.MaritalStatusCode,
            "AnnualIncome": annuid,
            "PF_CUSTOMERID": this.createCustData.PFCustomerID
        };
        console.log("Request for save Proposal", requestBodyHB);
        console.log("Stringify Request", JSON.stringify(requestBodyHB));
        localStorage.setItem('proposalRequest', JSON.stringify(requestBodyHB));
        var stringifyReq = JSON.stringify(requestBodyHB);
        this.cs.postWithParams('/api/HBooster/SaveEditProposal', stringifyReq).then(function (res) {
            console.log("HB Response", res);
            if (res.StatusCode == '1') {
                localStorage.setItem('proposalResponse', JSON.stringify(res));
                _this.cs.showLoader = false;
                _this.router.navigateByUrl('health-summary');
            }
            else {
                _this.cs.showLoader = false;
                _this.messageAlert(res.StatusMessage);
            }
        }).catch(function (err) {
            _this.presentToast(err.error.Message);
            _this.cs.showLoader = false;
        });
    };
    HealthProposalPage.prototype.saveProposalForHB = function (form) {
        var _this = this;
        var appointeeDate;
        var occuid, annuid;
        if (this.isRegWithGST == false) {
            form.value.GSTIN = '';
            form.value.GSTIN = '';
            this.constName = '';
            form.value.customerType = '';
            this.custType = '';
            form.value.panNo = '';
            form.value.gstRegStatus = '';
            this.gstName = '';
        }
        if (this.isExistPolicy1 == false) {
            this.insuredForm.value.TypeofPolicy1 = '';
            this.insuredForm.value.PolicyDuration1 = '';
            this.insuredForm.value.InsuranceCompany1 = '';
            this.insuredForm.value.SumInsured1 = '';
        }
        if (this.isExistPolicy2 == false) {
            this.insuredForm.value.TypeofPolicy2 = '';
            this.insuredForm.value.PolicyDuration2 = '';
            this.insuredForm.value.InsuranceCompany2 = '';
            this.insuredForm.value.SumInsured2 = '';
        }
        if (this.isExistPolicy3 == false) {
            this.insuredForm.value.TypeofPolicy3 = '';
            this.insuredForm.value.PolicyDuration3 = '';
            this.insuredForm.value.InsuranceCompany3 = '';
            this.insuredForm.value.SumInsured3 = '';
        }
        if (this.isExistPolicy4 == false) {
            this.insuredForm.value.TypeofPolicy4 = '';
            this.insuredForm.value.PolicyDuration4 = '';
            this.insuredForm.value.InsuranceCompany4 = '';
            this.insuredForm.value.SumInsured4 = '';
        }
        if (this.isExistPolicy5 == false) {
            this.insuredForm.value.TypeofPolicy5 = '';
            this.insuredForm.value.PolicyDuration5 = '';
            this.insuredForm.value.InsuranceCompany5 = '';
            this.insuredForm.value.SumInsured5 = '';
        }
        if (this.showApointee == false) {
            form.value.appRelation = '';
            appointeeDate = '';
            form.value.appTitle = '';
            form.value.appName = '';
            this.appointeeTitle = '',
                this.appointeeRelationId = '';
        }
        else {
            appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY');
        }
        if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
            form.value.aadhaarNo = '';
        }
        if (form.value.panNumber == null || form.value.panNumber == undefined) {
            form.value.panNumber = '';
        }
        if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
            form.value.applAdd2 = '';
        }
        if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
            form.value.applLandMark = '';
        }
        if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
            form.value.permAdd2 = '';
        }
        if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
            form.value.applaltEmail = '';
        }
        if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
            form.value.applaltMobNo = '';
        }
        if (form.value.apsId == null || form.value.apsId == undefined) {
            form.value.apsId = '';
        }
        if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
            form.value.custRefNo = '';
        }
        if (form.value.sourcingCode == null || form.value.sourcingCode == undefined) {
            form.value.sourcingCode = '';
        }
        if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
            form.value.applOccu = '';
            occuid = '';
        }
        else {
            occuid = this.occupationId.OCCUPATIONID;
        }
        if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
            form.value.annuIncome == '';
            annuid = '';
        }
        else {
            annuid = this.annualIncomeId.value;
        }
        var userDate = this.quoteRequestData.Adult1Age;
        var requestBodyHB = {
            "Product": "NA",
            "IsCustomerPID": "true",
            "UserType": "Agent",
            "ipaddress": config.ipAddress,
            "IPGPayment": "true",
            "ProductType": this.requestBody.ProductType,
            "ProductName": this.requestBody.ProductName,
            "GSTApplicable": "false",
            "UINApplicable": "false",
            "CustomerGSTDetails": {
                "GSTIN_NO": form.value.GSTIN,
                "CONSTITUTION_OF_BUSINESS": form.value.constitution,
                "CONSTITUTION_OF_BUSINESS_TEXT": this.constName,
                "CUSTOMER_TYPE": form.value.customerType,
                "CUSTOMER_TYPE_TEXT": this.custType,
                "PAN_NO": form.value.panNo,
                "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
                "GST_REGISTRATION_STATUS_TEXT": this.gstName
            },
            "PolicyID": this.responseBody.HealthDetails[0].PolicyID,
            "DealID": this.responseBody.HealthDetails[0].DealId,
            "IS_IBANK_RELATIONSHIP": "NO",
            "APS_ID": [form.value.apsId],
            'Members': this.memberObj,
            "NomineeName": form.value.nomName,
            "NomineeTitleID": this.nomineeTitle.id,
            "NomineeRelationShipID": this.nomineeRelationId.RelationshipID,
            "NomineeRelationShipText": form.value.nomRelation,
            "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
            "HUF": "Y",
            "AppointeeLastName": form.value.appName,
            "AppointeeTitleID": this.appointeeTitle,
            "AppointeeRelationShipID": this.appointeeRelationId,
            "AppointeeRelationShip": form.value.appRelation,
            "AppointeeDOB": appointeeDate,
            "SetAsDefault": "NO",
            "AddressChange": "YES",
            "CustomerID": this.createCustData.CustomerID,
            "OccupationID": occuid,
            "OccupationDesc": form.value.applOccu,
            "IDProofID": "2",
            "IDProofValue": "978456320164",
            "AadharEKYCYesNo": "N",
            "Title": this.titleId1.id,
            "InsuredID": "4",
            "CustomerName": form.value.applName,
            "Address1": form.value.applAdd1,
            "Address2": form.value.applAdd2,
            "Landmark": form.value.applLandMark,
            "StateID": this.pinData.StateId,
            "CityID": this.pinData.CityList[0].CityID,
            "PincodeID": this.pinDataValue.Details[0].Value,
            "PermanentAddrLine1": form.value.permAdd1,
            "PermanentAddrLine2": form.value.permAdd2,
            "PermanentAddrLandmark": form.value.permLandMark,
            "PermanentAddrStateID": this.pinDataPer.StateId,
            "PermanentAddrCityID": this.pinDataPer.CityList[0].CityID,
            "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,
            "Mobile": form.value.applMobNo.toString(),
            "LandLineNo": '',
            "STD": "",
            "Email": form.value.applEmail,
            "FinancierBranch": "",
            "FinancierName": "",
            "PaymentMode": "NONE",
            "Cheque_Type": "",
            "ModeID": "0001",
            "CardExpiryYear": "",
            "CardExpiryMonth": "",
            "CardNumber": "",
            "CVVNumber": "",
            "CardType": "",
            "CardHolderName": "",
            "PANNumber": form.value.panNumber,
            "AadhaarNumber": form.value.aadhaarNo,
            "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
            "SubLimit": "",
            "AddOn1": this.requestBody.AddOn1,
            "AddOn3": this.requestBody.AddOn3,
            "AddOn5": "false",
            "AddOn6": "false",
            "MaritalStatus": this.maritalStatusId.MaritalStatusCode,
            "AnnualIncome": annuid,
            "PF_CUSTOMERID": this.createCustData.PFCustomerID,
            "SaveType": "PROPOSAL"
        };
        console.log("Request for save Proposal", requestBodyHB);
        console.log("Stringify Request", JSON.stringify(requestBodyHB));
        localStorage.setItem('reqFromProp', JSON.stringify(requestBodyHB));
        var stringifyReq = JSON.stringify(requestBodyHB);
        this.cs.postWithParams('/api/HBooster/SaveEditProposal', stringifyReq).then(function (res) {
            console.log("HB Response", res);
            if (res.StatusCode == '1') {
                localStorage.setItem('resFromProp', JSON.stringify(res));
                _this.cs.showLoader = false;
                _this.messageAlert('Your Proposal hasbeen saved with Policy Id: ' + res.SavePolicy[0].PolicyID + ' and will be expired in 7 days');
                _this.router.navigateByUrl('health');
            }
            else {
                _this.cs.showLoader = false;
                ;
                _this.messageAlert(res.StatusMessage);
            }
        }).catch(function (err) {
            _this.presentToast(err.error.Message);
            _this.cs.showLoader = false;
        });
    };
    HealthProposalPage.prototype.searchCust = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        console.log("modal");
                        _a = this;
                        return [4 /*yield*/, this.modalCtrl.create({
                                component: SearchCustComponent,
                                cssClass: 'custom_modal',
                                componentProps: { users: this.users },
                            })];
                    case 1:
                        _a.searchCustModal = _b.sent();
                        this.searchCustModal.onDidDismiss().then(function (data, type) {
                            console.log(data.data);
                            if (!data.data) {
                                _this.isNew = true;
                            }
                            if (data.role == '0') {
                                _this.getCustomerDetails(data.data);
                            }
                            else if (data.role == '1') {
                                _this.mapDataToExistanceUserByPFId(data.data);
                            }
                        });
                        return [4 /*yield*/, this.searchCustModal.present()];
                    case 2: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    HealthProposalPage.prototype.getCustomerDetails = function (userData) {
        var _this = this;
        console.log("User Data", userData);
        this.cs.getWithParams('/api/Customer/GetCustomerDetails/' + userData.CustomerID).then(function (res) {
            console.log("Cust Details", res);
            _this.getDataName(res).then(function () {
                _this.mapDataToExistanceUser(res);
                //this.isRegWithGST = true;
            }, function (err) {
                _this.presentAlert('Sorry Something went wrong');
            });
        });
    };
    HealthProposalPage.prototype.getDataName = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.getTitleName(data);
            _this.getMaritalName(data);
            _this.getOccupationName(data);
            resolve();
        });
    };
    HealthProposalPage.prototype.getTitleName = function (data) {
        if (data.TitleValue == '0') {
            this.titleName = 'Mrs.';
        }
        else if (data.TitleValue == '1') {
            this.titleName = 'Mr.';
        }
        else {
            this.titleName = 'Ms.';
        }
    };
    HealthProposalPage.prototype.getMaritalName = function (data) {
        // this.marName = this.maritalStatus.MaritalStatusList.find(x => x.MaritalStatusCode = data.)
    };
    HealthProposalPage.prototype.getOccupationName = function (data) {
        // this.occName = this.occupation.OccupationLists.find(x => )
    };
    HealthProposalPage.prototype.mapDataToExistanceUserByPFId = function (data) {
        if (data.AadhaarNo != null && data.AadhaarNo != undefined && data.AadhaarNo != '') {
            this.applicantForm.patchValue({ 'aadhaarNo': data.AadhaarNo });
        }
        if (data.PAN_NO != null && data.PAN_NO != undefined && data.PAN_NO != '') {
            this.applicantForm.patchValue({ 'panNumber': data.PAN_NO });
        }
        if (data.TitleText != null && data.TitleText != undefined && data.TitleText != '') {
            this.applicantForm.patchValue({ 'applTitle': data.TitleText });
        }
        if (data.Name != null && data.Name != undefined && data.Name != '') {
            this.applicantForm.patchValue({ 'applName': data.Name });
        }
        if (data.DOB != null && data.DOB != undefined && data.DOB != '') {
            this.applicantForm.patchValue({ 'applDOB': moment(data.DOB).format('YYYY-MM-DD') });
        }
        if (data.Address1 != null && data.Address1 != undefined && data.Address1 != '') {
            this.applicantForm.patchValue({ 'applAdd1': data.Address1 });
        }
        if (data.Address2 != null && data.Address2 != undefined && data.Address2 != '') {
            this.applicantForm.patchValue({ 'applAdd2': data.Address2 });
        }
        if (data.PresentAddrLandmark != null && data.PresentAddrLandmark != undefined && data.PresentAddrLandmark != '') {
            this.applicantForm.patchValue({ 'applLandMark': data.PresentAddrLandmark });
        }
        if (data.PinCode != null && data.PinCode != undefined && data.PinCode != '') {
            this.applicantForm.patchValue({ 'applPinCode': data.PinCode });
        }
        if (data.MARITAL_STATUS != null && data.MARITAL_STATUS != undefined && data.MARITAL_STATUS != '') {
            this.applicantForm.patchValue({ 'applMarital': data.MARITAL_STATUS });
        }
        if (data.Occupation != null && data.Occupation != undefined && data.Occupation != '') {
            this.applicantForm.patchValue({ 'applOccu': data.Occupation });
        }
        if (data.AnnualIncomeText != null && data.AnnualIncomeText != undefined && data.AnnualIncomeText != '') {
            this.applicantForm.patchValue({ 'annuIncome': data.AnnualIncomeText });
        }
        if (data.EmailID != null && data.EmailID != undefined && data.EmailID != '') {
            this.applicantForm.patchValue({ 'applEmail': data.EmailID });
        }
        if (data.Mobile != null && data.Mobile != undefined && data.Mobile != '') {
            this.applicantForm.patchValue({ 'applMobNo': data.Mobile });
        }
        if (data.GSTDetails != null && data.GSTDetails != undefined) {
            this.isGSTReg(data.GSTApplicable);
            this.radio(!data.IS_UIN_APPLICABLE);
            if (data.GSTIN_UIN_NO != null && data.GSTIN_UIN_NO != undefined && data.GSTIN_UIN_NO != '') {
                this.applicantForm.patchValue({ 'GSTIN': data.GSTIN_UIN_NO });
                this.applicantForm.patchValue({ 'UIN': data.GSTIN_UIN_NO });
            }
            if (data.GST_REGISTRATION_STATUS != null && data.GST_REGISTRATION_STATUS != undefined && data.GST_REGISTRATION_STATUS != '') {
                this.applicantForm.patchValue({ 'gstRegStatus': data.GST_REGISTRATION_STATUS });
            }
            if (data.CUSTOMER_TYPE != null && data.CUSTOMER_TYPE != undefined && data.CUSTOMER_TYPE != '') {
                this.applicantForm.patchValue({ 'customerType': data.CUSTOMER_TYPE });
            }
            if (data.PAN_NO != null && data.PAN_NO != undefined && data.PAN_NO != '') {
                this.applicantForm.patchValue({ 'panNo': data.PAN_NO });
            }
            if (data.CONSTITUTION_OF_BUSINESS != null && data.CONSTITUTION_OF_BUSINESS != undefined && data.CONSTITUTION_OF_BUSINESS != '') {
                this.applicantForm.patchValue({ 'constitution': data.CONSTITUTION_OF_BUSINESS });
            }
        }
        if (data.PresentAddrStateValue == '62') {
            this.isRegWithGST = true;
            this.iskeralaCess = true;
        }
        else {
            this.iskeralaCess = false;
            this.isRegWithGST = false;
        }
    };
    HealthProposalPage.prototype.mapDataToExistanceUser = function (data) {
        // this.applicantForm.patchValue({'aadhaarNo': data });
        if (data.PANNumber != null && data.PANNumber != undefined && data.PANNumber != '') {
            this.applicantForm.patchValue({ 'panNumber': data.PANNumber });
        }
        if (data.TitleText != null && data.TitleText != undefined && data.TitleText != '') {
            this.applicantForm.patchValue({ 'applTitle': data.TitleText });
        }
        if (data.Name != null && data.Name != undefined && data.Name != '') {
            this.applicantForm.patchValue({ 'applName': data.Name });
        }
        if (data.DateOfBirth != null && data.DateOfBirth != undefined && data.DateOfBirth != '') {
            this.applicantForm.patchValue({ 'applDOB': moment(data.DateOfBirth, 'DD/MM/YYYY').format('DD-MM-YYYY') });
        }
        if (data.PresentAddrLine1 != null && data.PresentAddrLine1 != undefined && data.PresentAddrLine1 != '') {
            this.applicantForm.patchValue({ 'applMobNo': data.MobileNumber });
        }
        if (data.PresentAddrLine1 != null && data.PresentAddrLine1 != undefined && data.PresentAddrLine1 != '') {
            this.applicantForm.patchValue({ 'applAdd1': data.PresentAddrLine1 });
        }
        if (data.PresentAddrLine2 != null && data.PresentAddrLine2 != undefined && data.PresentAddrLine2 != '') {
            this.applicantForm.patchValue({ 'applAdd2': data.PresentAddrLine2 });
        }
        if (data.PresentAddrLandmark != null && data.PresentAddrLandmark != undefined && data.PresentAddrLandmark != '') {
            this.applicantForm.patchValue({ 'applLandMark': data.PresentAddrLandmark });
        }
        if (data.PresentAddrPincodeText != null && data.PresentAddrPincodeText != undefined && data.PresentAddrPincodeText != '') {
            this.applicantForm.patchValue({ 'applPinCode': data.PresentAddrPincodeText });
            this.getPincodeForExitingUser(data.PresentAddrPincodeText);
        }
        if (data.MaritalStatusText != undefined && data.MaritalStatusText != null && data.PresentAddrPincodeText != '') {
            this.applicantForm.patchValue({ 'applMarital': data.MaritalStatusText });
            this.showMaritalStatusForExisitng(data.MaritalStatusText);
        }
        if (data.OccupationText != undefined && data.OccupationText != null && data.OccupationText != '') {
            this.applicantForm.patchValue({ 'applOccu': data.OccupationText });
            this.getOccIdForExisitng(data.OccupationText);
        }
        if (data.AnnualIncomeText != undefined && data.AnnualIncomeText != null && data.AnnualIncomeText != '') {
            this.applicantForm.patchValue({ 'annuIncome': data.AnnualIncomeText });
            this.getAnnualIncIdForExisitng(data.AnnualIncomeText);
        }
        if (data.EmailAddress != undefined && data.EmailAddress != null && data.EmailAddress != '') {
            this.applicantForm.patchValue({ 'applEmail': data.EmailAddress });
        }
        if (data.LandlineNumber != undefined && data.LandlineNumber != null && data.AlternateEmail != '') {
            this.applicantForm.patchValue({ 'landLine': data.LandlineNumber });
        }
        if (data.AlternateEmail != undefined && data.AlternateEmail != null && data.EmailAddress != '') {
            this.applicantForm.patchValue({ 'applaltEmail': data.AlternateEmail });
        }
        if (data.AlternateMobile != undefined && data.AlternateMobile != null && data.AlternateMobile != '') {
            this.applicantForm.patchValue({ 'applaltMobNo': data.AlternateMobile });
        }
        if (data.PermanentAddrLine1 != undefined && data.PermanentAddrLine1 != null && data.PermanentAddrLine1 != '') {
            this.applicantForm.patchValue({ 'permAdd1': data.PermanentAddrLine1 });
        }
        if (data.PermanentAddrLine2 != undefined && data.PermanentAddrLine2 != null && data.PermanentAddrLine2 != '') {
            this.applicantForm.patchValue({ 'permAdd2': data.PermanentAddrLine2 });
        }
        if (data.PermanentAddrLandmark != undefined && data.PermanentAddrLandmark != null && data.PermanentAddrLandmark != '') {
            this.applicantForm.patchValue({ 'permLandMark': data.PermanentAddrLandmark });
        }
        if (data.PermanentAddrPincodeText != undefined && data.PermanentAddrPincodeText != null && data.EmailAddress != '') {
            this.applicantForm.patchValue({ 'permPinCode': data.PermanentAddrPincodeText });
            this.getPerPincodeForExitingUser(data.PresentAddrPincodeText);
        }
        if (data.GSTDetails != null && data.GSTDetails != undefined) {
            this.applicantForm.patchValue({ 'GSTIN': data.GSTDetails.GSTIN_NO });
            // this.applicantForm.patchValue({'UIN': data });
            this.applicantForm.patchValue({ 'gstRegStatus': data.GSTDetails.GST_REGISTRATION_STATUS_TEXT });
            this.applicantForm.patchValue({ 'customerType': data.GSTDetails.CUSTOMER_TYPE_TEXT });
            this.applicantForm.patchValue({ 'panNo': data.GSTDetails.PAN_NO });
            this.applicantForm.patchValue({ 'constitution': data.GSTDetails.CONSTITUTION_OF_BUSINESS_TEXT });
            this.isRegWithGST = true;
        }
        if (data.PresentAddrStateValue == '62') {
            this.isRegWithGST = true;
            this.iskeralaCess = true;
        }
        else {
            this.iskeralaCess = false;
        }
    };
    HealthProposalPage.prototype.showMaritalStatusForExisitng = function (ev) {
        this.maritalStatusId = this.maritalStatus.MaritalStatusList.find(function (x) { return x.MaritalStatusDesc == ev; });
    };
    HealthProposalPage.prototype.getAnnualIncIdForExisitng = function (ev) {
        this.annualIncomeId = this.annualIncome.Values.find(function (x) { return x.Name == ev; });
    };
    HealthProposalPage.prototype.getOccIdForExisitng = function (ev) {
        this.occupationId = this.occupation.find(function (x) { return x.OCCUPATION == ev; });
    };
    HealthProposalPage.prototype.getPincodeForExitingUser = function (val) {
        var _this = this;
        // 
        if (val.length == 6) {
            //this.showLoading();
            var body_4 = val;
            this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body_4).then(function (res) {
                _this.pinData = res;
                if (_this.pinData.StatusCode == 1) {
                    _this.applicantForm.patchValue({ 'applCity': _this.pinData.CityList[0].CityName });
                    _this.applicantForm.patchValue({ 'applState': _this.pinData.StateName });
                    var proposalStateCity = { pin: body_4, city: _this.pinData.CityList[0].CityName, state: _this.pinData.StateName };
                    localStorage.setItem('CoresspondentStateCity', JSON.stringify(proposalStateCity));
                    //  this.loading.dismiss();
                }
                else {
                    _this.applicantForm.patchValue({ 'applCity': null });
                    _this.applicantForm.patchValue({ 'applState': null });
                    var proposalStateCity = { pin: body_4, city: null, state: null };
                    localStorage.setItem('CoresspondentStateCity', JSON.stringify(proposalStateCity));
                    _this.presentAlert(_this.pinData.StatusMessage);
                    //  this.loading.dismiss();
                }
            });
        }
    };
    HealthProposalPage.prototype.getPerPincodeForExitingUser = function (val) {
        var _this = this;
        if (val.length == 6) {
            var body_5 = val;
            //this.showLoading();
            this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body_5).then(function (res) {
                _this.pinDataPer = res;
                if (_this.pinDataPer.StatusCode == 1) {
                    _this.applicantForm.patchValue({ 'permCity': _this.pinDataPer.CityList[0].CityName });
                    _this.applicantForm.patchValue({ 'permState': _this.pinDataPer.StateName });
                    var proposalStateCity = { pin: body_5, city: _this.pinDataPer.CityList[0].CityName, state: _this.pinDataPer.StateName };
                    localStorage.setItem('PermanentStateCity', JSON.stringify(proposalStateCity));
                    // this.loading.dismiss();
                }
                else {
                    _this.applicantForm.patchValue({ 'permCity': null });
                    _this.applicantForm.patchValue({ 'permState': null });
                    var proposalStateCity = { pin: body_5, city: null, state: null };
                    _this.presentAlert(_this.pinDataPer.StatusMessage);
                    // this.loading.dismiss();
                }
            });
        }
    };
    //--Proposal Applicant Code Begins--//
    //--Proposal Insured Code Begins--//
    HealthProposalPage.prototype.getCall = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.data = localStorage.getItem('requestForProp');
            resolve();
        });
    };
    // -- Proposal Insured Code Begins--//
    HealthProposalPage.prototype.showSegments = function (form) {
        console.log("form", form);
        if (form.ProductType == 'CHI') {
            var data = form;
            if (data.NoOfAdults == '0') {
                this.isAdult1 = false;
                this.isAdult2 = false;
                this.isChild1 = true;
                this.isChild2 = false;
                this.isChild3 = false;
            }
            else if (data.NoOfAdults == '1') {
                this.isAdult1 = true;
                this.isAdult2 = false;
                if (data.NoOfKids == '0' || data.NoOfKids == '') {
                    this.isChild1 = false;
                    this.isChild2 = false;
                    this.isChild3 = false;
                }
                else if (data.NoOfKids == '1') {
                    this.isChild1 = true;
                    this.isChild2 = false;
                    this.isChild3 = false;
                }
                else {
                    this.isChild1 = true;
                    this.isChild2 = true;
                    this.isChild3 = false;
                }
            }
            else {
                this.isAdult1 = true;
                this.isAdult2 = true;
                if (data.NoOfKids == '0' || data.NoOfKids == '') {
                    this.isChild1 = false;
                    this.isChild2 = false;
                    this.isChild3 = false;
                }
                else if (data.NoOfKids == '1') {
                    this.isChild1 = true;
                    this.isChild2 = false;
                    this.isChild3 = false;
                }
                else if (data.NoOfKids == '2') {
                    this.isChild1 = true;
                    this.isChild2 = true;
                    this.isChild3 = false;
                }
                else {
                    this.isChild1 = true;
                    this.isChild2 = true;
                    this.isChild3 = true;
                }
            }
        }
        else if (form.ProductType == 'HBOOSTER') {
            var data = form;
            if (data.NoOfAdults == '0') {
                this.isAdult1 = false;
                this.isAdult2 = false;
                this.isChild1 = true;
                this.isChild2 = false;
                this.isChild3 = false;
            }
            else if (data.NoOfAdults == '1') {
                this.isAdult1 = true;
                this.isAdult2 = false;
                if (data.NoOfKids == '0' || data.NoOfKids == '') {
                    this.isChild1 = false;
                    this.isChild2 = false;
                    this.isChild3 = false;
                }
                else if (data.NoOfKids == '1') {
                    this.isChild1 = true;
                    this.isChild2 = false;
                    this.isChild3 = false;
                }
                else {
                    this.isChild1 = true;
                    this.isChild2 = true;
                    this.isChild3 = false;
                }
            }
            else {
                this.isAdult1 = true;
                this.isAdult2 = true;
                if (data.NoOfKids == '0' || data.NoOfKids == '') {
                    this.isChild1 = false;
                    this.isChild2 = false;
                    this.isChild3 = false;
                }
                else if (data.NoOfKids == '1') {
                    this.isChild1 = true;
                    this.isChild2 = false;
                    this.isChild3 = false;
                }
                else if (data.NoOfKids == '2') {
                    this.isChild1 = true;
                    this.isChild2 = true;
                    this.isChild3 = false;
                }
                else {
                    this.isChild1 = true;
                    this.isChild2 = true;
                    this.isChild3 = true;
                }
            }
        }
    };
    HealthProposalPage.prototype.segmentChanged = function (ev) {
        console.log(ev.detail.value);
        this.showSegment = ev.detail.value;
    };
    HealthProposalPage.prototype.createInsuredForm = function () {
        this.insuredForm = new FormGroup({
            member1Cat: new FormControl('Adult'),
            member1Rela: new FormControl(''),
            member1Title: new FormControl(''),
            member1Name: new FormControl(),
            member1DOB: new FormControl(),
            member1Feet: new FormControl(''),
            member1Inches: new FormControl(''),
            member1Weight: new FormControl(),
            member1Disease: new FormControl(),
            // FOR HBOOSTER
            TypeofPolicy1: new FormControl(''),
            PolicyDuration1: new FormControl(),
            InsuranceCompany1: new FormControl(''),
            SumInsured1: new FormControl(),
            member2Cat: new FormControl('Adult'),
            member2Rela: new FormControl(''),
            member2Title: new FormControl(''),
            member2Name: new FormControl(),
            member2DOB: new FormControl(),
            member2Feet: new FormControl(''),
            member2Inches: new FormControl(''),
            member2Weight: new FormControl(),
            member2Disease: new FormControl(),
            // FOR HBOOSTER
            TypeofPolicy2: new FormControl(''),
            PolicyDuration2: new FormControl(),
            InsuranceCompany2: new FormControl(''),
            SumInsured2: new FormControl(),
            child1Cat: new FormControl('Child'),
            child1Rela: new FormControl(''),
            child1Title: new FormControl(''),
            child1Name: new FormControl(),
            child1DOB: new FormControl(),
            child1Feet: new FormControl(''),
            child1Inches: new FormControl(''),
            child1Weight: new FormControl(),
            child1Disease: new FormControl(),
            // FOR HBOOSTER
            TypeofPolicy3: new FormControl(''),
            PolicyDuration3: new FormControl(),
            InsuranceCompany3: new FormControl(''),
            SumInsured3: new FormControl(),
            child2Cat: new FormControl('Child'),
            child2Rela: new FormControl(''),
            child2Title: new FormControl(''),
            child2Name: new FormControl(),
            child2DOB: new FormControl(),
            child2Feet: new FormControl(''),
            child2Inches: new FormControl(''),
            child2Weight: new FormControl(),
            child2Disease: new FormControl(),
            // FOR HBOOSTER
            TypeofPolicy4: new FormControl(''),
            PolicyDuration4: new FormControl(),
            InsuranceCompany4: new FormControl(''),
            SumInsured4: new FormControl(),
            child3Cat: new FormControl('Child'),
            child3Rela: new FormControl(''),
            child3Title: new FormControl(''),
            child3Name: new FormControl(),
            child3DOB: new FormControl(),
            child3Feet: new FormControl(''),
            child3Inches: new FormControl(''),
            child3Weight: new FormControl(),
            child3Disease: new FormControl(),
            // FOR HBOOSTER
            TypeofPolicy5: new FormControl(''),
            PolicyDuration5: new FormControl(),
            InsuranceCompany5: new FormControl(''),
            SumInsured5: new FormControl(),
        });
    };
    HealthProposalPage.prototype.createPPAPInsuranceForm = function () {
        var dob = this.ppapDOB;
        var occ = this.ppapOccupation;
        var memberType = this.ppapMemeberType;
        this.insuredPPAPForm = new FormGroup({
            member1Cat: new FormControl(memberType),
            member1Rela: new FormControl(''),
            member1Title: new FormControl(''),
            member1Name: new FormControl(),
            member1DOB: new FormControl(dob),
            insuredpanno: new FormControl(),
            insuredOccupation: new FormControl(occ),
            riskStartDate: new FormControl(),
            aiPolicy: new FormControl(),
            insuranceComapny: new FormControl(''),
            insurancePolicy: new FormControl(),
            policyValidFrom: new FormControl(''),
            policyValidTill: new FormControl(''),
            policySumInsured: new FormControl(),
            claimsMade: new FormControl(),
        });
    };
    HealthProposalPage.prototype.acitrigger = function (val) {
        this.insuredPPAPForm.value.aiPolicy = val;
        this.showOtherPolicyDetails = this.insuredPPAPForm.value.aiPolicy;
    };
    HealthProposalPage.prototype.claimstrigger = function (val) {
        this.insuredPPAPForm.value.claimsMade = val;
        this.showClaimsDetails = this.insuredPPAPForm.value.claimsMade;
    };
    HealthProposalPage.prototype.getFeet = function (ev) {
        console.log(ev.target.value);
    };
    HealthProposalPage.prototype.getInches = function (ev) {
        console.log(ev.target.value);
    };
    HealthProposalPage.prototype.getRelation = function () {
        var _this = this;
        var producttype;
        if (this.quoteRequestData.ProductType == 'CHI') {
            producttype = 'CHI';
        }
        if (this.quoteRequestData.ProductType == 'HBOOSTER') {
            producttype = 'HBOOSTER';
        }
        if (this.quoteRequestData.ProductType == "PPAP") {
            producttype = 'PPAP';
        }
        // else
        // {
        //   producttype='PPAP'
        // }
        this.cs.postWithParams('/api/healthmaster/GetHealthProposalRelationships?Product=' + producttype, '').then(function (res) {
            console.log("Rel", res);
            _this.adultRelationArray = [];
            _this.childRelationArray = [];
            res.InsuredRelationship.forEach(function (relation) {
                if (relation.KidAdultType == 'Adult') {
                    _this.adultRelationArray.push(relation);
                }
                else {
                    _this.childRelationArray.push(relation);
                }
            });
            _this.adultRelationArray = _.sortBy(_this.adultRelationArray, 'RelationshipName');
            _this.childRelationArray = _.sortBy(_this.childRelationArray, 'RelationshipName');
        }).catch(function (err) {
            console.log(err);
        });
    };
    HealthProposalPage.prototype.isDisease1 = function (ev) {
        console.log(ev.detail);
    };
    HealthProposalPage.prototype.getQuestionList = function () {
        var _this = this;
        this.cs.getWithParams('/api/healthmaster/GetHealthAilmentList?isAgent=YES').then(function (res) {
            console.log("JSON Ailment", res);
            _this.questionList = res.Details;
            // for(let i=0;i<this.questionList.length;i++)
            // {
            //   this.questionList[i].isChecked=false;
            // }
        }).catch(function (err) {
            console.log(err);
        });
    };
    HealthProposalPage.prototype.getTitle1 = function (ev) {
        console.log(ev.target.value);
        this.titleId1 = this.titles.find(function (x) { return x.val == ev.target.value; });
        console.log(this.titleId1);
    };
    HealthProposalPage.prototype.getTitle2 = function (ev) {
        console.log(ev.target.value);
        this.titleId2 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthProposalPage.prototype.getTitle3 = function (ev) {
        console.log(ev.target.value);
        this.titleId3 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthProposalPage.prototype.getTitle4 = function (ev) {
        console.log(ev.target.value);
        this.titleId4 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthProposalPage.prototype.getTitle5 = function (ev) {
        console.log(ev.target.value);
        this.titleId5 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthProposalPage.prototype.getRelation1 = function (ev) {
        console.log(ev.target.value);
        this.relationId1 = this.adultRelationArray.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    HealthProposalPage.prototype.getRelation2 = function (ev) {
        console.log(ev.target.value);
        this.relationId2 = this.adultRelationArray.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    HealthProposalPage.prototype.getRelation3 = function (ev) {
        console.log(ev.target.value);
        this.relationId3 = this.childRelationArray.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    HealthProposalPage.prototype.getRelation4 = function (ev) {
        console.log(ev.target.value);
        this.relationId4 = this.childRelationArray.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    HealthProposalPage.prototype.getRelation5 = function (ev) {
        console.log(ev.target.value);
        this.relationId5 = this.childRelationArray.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    HealthProposalPage.prototype.getDiseases1 = function (d, ev) {
        if (ev.detail.checked) {
            for (var i = 0; i < this.ailmentArray1.length; i++) {
                if (this.ailmentArray1[i].AilmentName == d.Name) {
                    var index = this.ailmentArray1.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray1.splice(index, 1);
                }
            }
            this.ailmentArray1.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value });
        }
        else {
            for (var i = 0; i < this.ailmentArray1.length; i++) {
                if (this.ailmentArray1[i].AilmentName == d.Name) {
                    var index = this.ailmentArray1.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray1.splice(index, 1);
                }
            }
        }
    };
    HealthProposalPage.prototype.getDiseases2 = function (d, ev) {
        if (ev.detail.checked) {
            for (var i = 0; i < this.ailmentArray2.length; i++) {
                if (this.ailmentArray2[i].AilmentName == d.Name) {
                    var index = this.ailmentArray2.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray2.splice(index, 1);
                }
            }
            this.ailmentArray2.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value });
        }
        else {
            for (var i = 0; i < this.ailmentArray2.length; i++) {
                if (this.ailmentArray2[i].AilmentName == d.Name) {
                    var index = this.ailmentArray2.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray2.splice(index, 1);
                }
            }
        }
    };
    HealthProposalPage.prototype.getDiseases3 = function (d, ev) {
        if (ev.detail.checked) {
            for (var i = 0; i < this.ailmentArray3.length; i++) {
                if (this.ailmentArray3[i].AilmentName == d.Name) {
                    var index = this.ailmentArray3.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray3.splice(index, 1);
                }
            }
            this.ailmentArray3.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value });
        }
        else {
            for (var i = 0; i < this.ailmentArray3.length; i++) {
                if (this.ailmentArray3[i].AilmentName == d.Name) {
                    var index = this.ailmentArray3.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray3.splice(index, 1);
                }
            }
        }
    };
    HealthProposalPage.prototype.getDiseases4 = function (d, ev) {
        if (ev.detail.checked) {
            for (var i = 0; i < this.ailmentArray4.length; i++) {
                if (this.ailmentArray4[i].AilmentName == d.Name) {
                    var index = this.ailmentArray4.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray4.splice(index, 1);
                }
            }
            this.ailmentArray4.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value });
        }
        else {
            for (var i = 0; i < this.ailmentArray4.length; i++) {
                if (this.ailmentArray4[i].AilmentName == d.Name) {
                    var index = this.ailmentArray4.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray4.splice(index, 1);
                }
            }
        }
    };
    HealthProposalPage.prototype.getDiseases5 = function (d, ev) {
        if (ev.detail.checked) {
            for (var i = 0; i < this.ailmentArray5.length; i++) {
                if (this.ailmentArray5[i].AilmentName == d.Name) {
                    var index = this.ailmentArray5.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray5.splice(index, 1);
                }
            }
            this.ailmentArray5.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value });
        }
        else {
            for (var i = 0; i < this.ailmentArray5.length; i++) {
                if (this.ailmentArray5[i].AilmentName == d.Name) {
                    var index = this.ailmentArray5.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray5.splice(index, 1);
                }
            }
        }
    };
    HealthProposalPage.prototype.createHBMembers = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log("create Dummy obj", data);
            var isxistingPolicy1, isxistingPolicy2, isxistingPolicy3, isxistingPolicy4, isxistingPolicy5;
            if (_this.isExistPolicy1 == false) {
                _this.insuredForm.value.TypeofPolicy1 = '';
                _this.insuredForm.value.SumInsured1 = '';
                _this.insuredForm.value.PolicyDuration1 = '';
                _this.insuredForm.value.InsuranceCompany1 = '';
                isxistingPolicy1 = 'false';
            }
            else {
                isxistingPolicy1 = 'true';
            }
            if (_this.isExistPolicy2 == false) {
                _this.insuredForm.value.TypeofPolicy2 = '';
                _this.insuredForm.value.SumInsured2 = '';
                _this.insuredForm.value.PolicyDuration2 = '';
                _this.insuredForm.value.InsuranceCompany2 = '';
                isxistingPolicy2 = 'false';
            }
            else {
                isxistingPolicy2 = 'true';
            }
            if (_this.isExistPolicy3 == false) {
                _this.insuredForm.value.TypeofPolicy3 = '';
                _this.insuredForm.value.SumInsured3 = '';
                _this.insuredForm.value.PolicyDuration3 = '';
                _this.insuredForm.value.InsuranceCompany3 = '';
                isxistingPolicy3 = 'false';
            }
            else {
                isxistingPolicy3 = 'true';
            }
            if (_this.isExistPolicy4 == false) {
                _this.insuredForm.value.TypeofPolicy4 = '';
                _this.insuredForm.value.SumInsured4 = '';
                _this.insuredForm.value.PolicyDuration4 = '';
                _this.insuredForm.value.InsuranceCompany4 = '';
                isxistingPolicy4 = 'false';
            }
            else {
                isxistingPolicy4 = 'true';
            }
            if (_this.isExistPolicy5 == false) {
                _this.insuredForm.value.TypeofPolicy5 = '';
                _this.insuredForm.value.SumInsured5 = '';
                _this.insuredForm.value.PolicyDuration5 = '';
                _this.insuredForm.value.InsuranceCompany5 = '';
                isxistingPolicy5 = 'false';
            }
            else {
                isxistingPolicy5 = 'true';
            }
            if (_this.isDisease == false) {
                _this.ailmentArray1 = [];
            }
            if (_this.isDisease2 == false) {
                _this.ailmentArray2 = [];
            }
            if (_this.isDisease3 == false) {
                _this.ailmentArray3 = [];
            }
            if (_this.isDisease4 == false) {
                _this.ailmentArray4 = [];
            }
            if (_this.isDisease5 == false) {
                _this.ailmentArray5 = [];
            }
            if (_this.product == "HBOOSTER") {
                _this.insuredForm.value.member1DOB = _this.quoteRequestData.Adult1Age;
            }
            if (_this.product == "HBOOSTER") {
                _this.insuredForm.value.member2DOB = _this.quoteRequestData.Adult2Age;
            }
            if (_this.jsonData.NoOfAdults == '0') {
                _this.memberObj = [{
                        "MemberType": _this.insuredForm.value.child1Cat, "TitleID": _this.titleId3.id, "Name": _this.insuredForm.value.child1Name,
                        "RelationshipID": _this.relationId3.RelationshipID, "RelationshipName": _this.insuredForm.value.child1Rela,
                        "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                        "Weight": _this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                        "isExisting": isxistingPolicy3, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3, "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                        "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3, "SumInsured": _this.insuredForm.value.SumInsured3,
                        "Ailments": _this.ailmentArray3
                    }];
                resolve();
            }
            else if (_this.jsonData.NoOfAdults == '1') {
                if (_this.jsonData.NoOfKids == '0') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat, "TitleID": _this.titleId1.id, "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID, "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy1, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1, "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1, "SumInsured": _this.insuredForm.value.SumInsured1,
                            "Ailments": _this.ailmentArray1
                        }];
                    resolve();
                }
                else if (_this.jsonData.NoOfKids == '1') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat, "TitleID": _this.titleId1.id, "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID, "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy1, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1, "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1, "SumInsured": _this.insuredForm.value.SumInsured1,
                            "Ailments": _this.ailmentArray1
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat, "TitleID": _this.titleId3.id, "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID, "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy3, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3, "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3, "SumInsured": _this.insuredForm.value.SumInsured3,
                            "Ailments": _this.ailmentArray3
                        }];
                    resolve();
                }
                else if (_this.jsonData.NoOfKids == '2') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat, "TitleID": _this.titleId1.id, "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID, "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy1, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1, "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1, "SumInsured": _this.insuredForm.value.SumInsured1,
                            "Ailments": _this.ailmentArray1
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat, "TitleID": _this.titleId3.id, "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID, "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy3, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3, "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3, "SumInsured": _this.insuredForm.value.SumInsured3,
                            "Ailments": _this.ailmentArray3
                        }, {
                            "MemberType": _this.insuredForm.value.child2Cat, "TitleID": _this.titleId4.id, "Name": _this.insuredForm.value.child2Name,
                            "RelationshipID": _this.relationId4.RelationshipID, "RelationshipName": _this.insuredForm.value.child2Rela,
                            "DOB": moment(_this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child2Feet + '.' + _this.insuredForm.value.child2Inches,
                            "Weight": _this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy4, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy4, "PolicyDuration": _this.insuredForm.value.PolicyDuration4,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany4, "SumInsured": _this.insuredForm.value.SumInsured4,
                            "Ailments": _this.ailmentArray4
                        }];
                    resolve();
                }
            }
            else if (_this.jsonData.NoOfAdults == '2') {
                if (_this.jsonData.NoOfKids == '1') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat, "TitleID": _this.titleId1.id, "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID, "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy1, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1, "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1, "SumInsured": _this.insuredForm.value.SumInsured1,
                            "Ailments": _this.ailmentArray1
                        }, {
                            "MemberType": _this.insuredForm.value.member2Cat, "TitleID": _this.titleId2.id, "Name": _this.insuredForm.value.member2Name,
                            "RelationshipID": _this.relationId2.RelationshipID, "RelationshipName": _this.insuredForm.value.member2Rela,
                            "DOB": moment(_this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member2Feet + '.' + _this.insuredForm.value.member2Inches,
                            "Weight": _this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy2, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy2, "PolicyDuration": _this.insuredForm.value.PolicyDuration2,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany2, "SumInsured": _this.insuredForm.value.SumInsured2,
                            "Ailments": _this.ailmentArray2
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat, "TitleID": _this.titleId3.id, "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID, "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy3, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3, "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3, "SumInsured": _this.insuredForm.value.SumInsured3,
                            "Ailments": _this.ailmentArray3
                        }];
                    resolve();
                }
                else if (_this.jsonData.NoOfKids == '2') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat, "TitleID": _this.titleId1.id, "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID, "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy1, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1, "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1, "SumInsured": _this.insuredForm.value.SumInsured1,
                            "Ailments": _this.ailmentArray1
                        }, {
                            "MemberType": _this.insuredForm.value.member2Cat, "TitleID": _this.titleId2.id, "Name": _this.insuredForm.value.member2Name,
                            "RelationshipID": _this.relationId2.RelationshipID, "RelationshipName": _this.insuredForm.value.member2Rela,
                            "DOB": moment(_this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member2Feet + '.' + _this.insuredForm.value.member2Inches,
                            "Weight": _this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy2, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy2, "PolicyDuration": _this.insuredForm.value.PolicyDuration2,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany2, "SumInsured": _this.insuredForm.value.SumInsured2,
                            "Ailments": _this.ailmentArray2
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat, "TitleID": _this.titleId3.id, "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID, "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy3, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3, "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3, "SumInsured": _this.insuredForm.value.SumInsured3,
                            "Ailments": _this.ailmentArray3
                        }, {
                            "MemberType": _this.insuredForm.value.child2Cat, "TitleID": _this.titleId4.id, "Name": _this.insuredForm.value.child2Name,
                            "RelationshipID": _this.relationId4.RelationshipID, "RelationshipName": _this.insuredForm.value.child2Rela,
                            "DOB": moment(_this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child2Feet + '.' + _this.insuredForm.value.child2Inches,
                            "Weight": _this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy4, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy4, "PolicyDuration": _this.insuredForm.value.PolicyDuration4,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany4, "SumInsured": _this.insuredForm.value.SumInsured4,
                            "Ailments": _this.ailmentArray4
                        }];
                    resolve();
                }
                else if (_this.jsonData.NoOfKids == '2') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat, "TitleID": _this.titleId1.id, "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID, "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy1, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1, "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1, "SumInsured": _this.insuredForm.value.SumInsured1,
                            "Ailments": _this.ailmentArray1
                        }, {
                            "MemberType": _this.insuredForm.value.member2Cat, "TitleID": _this.titleId2.id, "Name": _this.insuredForm.value.member2Name,
                            "RelationshipID": _this.relationId2.RelationshipID, "RelationshipName": _this.insuredForm.value.member2Rela,
                            "DOB": moment(_this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member2Feet + '.' + _this.insuredForm.value.member2Inches,
                            "Weight": _this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy2, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy2, "PolicyDuration": _this.insuredForm.value.PolicyDuration2,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany2, "SumInsured": _this.insuredForm.value.SumInsured2,
                            "Ailments": _this.ailmentArray2
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat, "TitleID": _this.titleId3.id, "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID, "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy3, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3, "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3, "SumInsured": _this.insuredForm.value.SumInsured3,
                            "Ailments": _this.ailmentArray3
                        }, {
                            "MemberType": _this.insuredForm.value.child2Cat, "TitleID": _this.titleId4.id, "Name": _this.insuredForm.value.child2Name,
                            "RelationshipID": _this.relationId4.RelationshipID, "RelationshipName": _this.insuredForm.value.child2Rela,
                            "DOB": moment(_this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child2Feet + '.' + _this.insuredForm.value.child2Inches,
                            "Weight": _this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy4, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy4, "PolicyDuration": _this.insuredForm.value.PolicyDuration4,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany4, "SumInsured": _this.insuredForm.value.SumInsured4,
                            "Ailments": _this.ailmentArray4
                        }, {
                            "MemberType": _this.insuredForm.value.child3Cat, "TitleID": _this.titleId5.id, "Name": _this.insuredForm.value.child3Name,
                            "RelationshipID": _this.relationId5.RelationshipID, "RelationshipName": _this.insuredForm.value.child3Rela,
                            "DOB": moment(_this.insuredForm.value.child3DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child3Feet + '.' + _this.insuredForm.value.child3Inches,
                            "Weight": _this.insuredForm.value.child3Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "isExisting": isxistingPolicy5, "TypeofPolicy": data.value.TypeofPolicy5, "PolicyDuration": _this.insuredForm.value.PolicyDuration5,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany5, "SumInsured": _this.insuredForm.value.SumInsured5,
                            "Ailments": _this.ailmentArray5
                        }];
                    resolve();
                }
            }
        });
    };
    HealthProposalPage.prototype.createCHIMembers = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log("create Dummy obj", data);
            var isxistingPolicy1, isxistingPolicy2, isxistingPolicy3, isxistingPolicy4, isxistingPolicy5;
            if (_this.isExistPolicy1 == false) {
                _this.insuredForm.value.TypeofPolicy1 = '';
                _this.insuredForm.value.SumInsured1 = '';
                _this.insuredForm.value.PolicyDuration1 = '';
                _this.insuredForm.value.InsuranceCompany1 = '';
                isxistingPolicy1 = 'No';
            }
            else {
                isxistingPolicy1 = 'Yes';
            }
            if (_this.isExistPolicy2 == false) {
                _this.insuredForm.value.TypeofPolicy2 = '';
                _this.insuredForm.value.SumInsured2 = '';
                _this.insuredForm.value.PolicyDuration2 = '';
                _this.insuredForm.value.InsuranceCompany2 = '';
                isxistingPolicy2 = 'No';
            }
            else {
                isxistingPolicy2 = 'Yes';
            }
            if (_this.isExistPolicy3 == false) {
                _this.insuredForm.value.TypeofPolicy3 = '';
                _this.insuredForm.value.SumInsured3 = '';
                _this.insuredForm.value.PolicyDuration3 = '';
                _this.insuredForm.value.InsuranceCompany3 = '';
                isxistingPolicy3 = 'No';
            }
            else {
                isxistingPolicy3 = 'Yes';
            }
            if (_this.isExistPolicy4 == false) {
                _this.insuredForm.value.TypeofPolicy4 = '';
                _this.insuredForm.value.SumInsured4 = '';
                _this.insuredForm.value.PolicyDuration4 = '';
                _this.insuredForm.value.InsuranceCompany4 = '';
                isxistingPolicy4 = 'No';
            }
            else {
                isxistingPolicy4 = 'Yes';
            }
            if (_this.isExistPolicy5 == false) {
                _this.insuredForm.value.TypeofPolicy5 = '';
                _this.insuredForm.value.SumInsured5 = '';
                _this.insuredForm.value.PolicyDuration5 = '';
                _this.insuredForm.value.InsuranceCompany5 = '';
                isxistingPolicy5 = 'No';
            }
            else {
                isxistingPolicy5 = 'Yes';
            }
            if (_this.product == "HBOOSTER") {
                _this.insuredForm.value.member1DOB = _this.quoteRequestData.Adult1Age;
            }
            if (_this.product == "HBOOSTER") {
                _this.insuredForm.value.member2DOB = _this.quoteRequestData.Adult2Age;
            }
            if (_this.isDisease == false) {
                _this.ailmentArray1 = [];
            }
            if (_this.isDisease2 == false) {
                _this.ailmentArray2 = [];
            }
            if (_this.isDisease3 == false) {
                _this.ailmentArray3 = [];
            }
            if (_this.isDisease4 == false) {
                _this.ailmentArray4 = [];
            }
            if (_this.isDisease5 == false) {
                _this.ailmentArray5 = [];
            }
            if (_this.jsonData.NoOfAdults == '0') {
                _this.memberObj = [{
                        "MemberType": _this.insuredForm.value.child1Cat,
                        "TitleID": _this.titleId3.id,
                        "Name": _this.insuredForm.value.child1Name,
                        "RelationshipID": _this.relationId3.RelationshipID,
                        "RelationshipName": _this.insuredForm.value.child1Rela,
                        "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'),
                        "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                        "Weight": _this.insuredForm.value.child1Weight.toString(),
                        "OtherDisease": "",
                        "AddOnName": "",
                        "AddOnName6": "",
                        "isExistingPolicy": isxistingPolicy3,
                        "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3,
                        "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                        "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3,
                        "SumInsured": _this.insuredForm.value.SumInsured3,
                        "Ailments": _this.ailmentArray3
                    }];
                resolve();
            }
            else if (_this.jsonData.NoOfAdults == '1') {
                if (_this.jsonData.NoOfKids == '0') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat,
                            "TitleID": _this.titleId1.id,
                            "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(),
                            "OtherDisease": "",
                            "AddOnName": "",
                            "AddOnName6": "",
                            "isExistingPolicy": isxistingPolicy1,
                            "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1,
                            "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1,
                            "SumInsured": _this.insuredForm.value.SumInsured1,
                            "Ailments": _this.ailmentArray1
                        }];
                    resolve();
                }
                else if (_this.jsonData.NoOfKids == '1') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat, "TitleID": _this.titleId1.id, "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID, "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray1, "isExistingPolicy": isxistingPolicy1, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1, "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1, "SumInsured": _this.insuredForm.value.SumInsured1
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat, "TitleID": _this.titleId3.id, "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID, "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray3, "isExistingPolicy": isxistingPolicy3, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3, "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3, "SumInsured": _this.insuredForm.value.SumInsured3
                        }];
                    resolve();
                }
                else if (_this.jsonData.NoOfKids == '2') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat, "TitleID": _this.titleId1.id, "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID, "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray1, "isExistingPolicy": isxistingPolicy1, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1, "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1, "SumInsured": _this.insuredForm.value.SumInsured1
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat, "TitleID": _this.titleId3.id, "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID, "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray3, "isExistingPolicy": isxistingPolicy3, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3, "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3, "SumInsured": _this.insuredForm.value.SumInsured3
                        }, {
                            "MemberType": _this.insuredForm.value.child2Cat, "TitleID": _this.titleId4.id, "Name": _this.insuredForm.value.child2Name,
                            "RelationshipID": _this.relationId4.RelationshipID, "RelationshipName": _this.insuredForm.value.child2Rela,
                            "DOB": moment(_this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child2Feet + '.' + _this.insuredForm.value.child2Inches,
                            "Weight": _this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray4, "isExistingPolicy": isxistingPolicy4, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy4, "PolicyDuration": _this.insuredForm.value.PolicyDuration4,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany4, "SumInsured": _this.insuredForm.value.SumInsured4
                        }];
                    resolve();
                }
            }
            else if (_this.jsonData.NoOfAdults == '2') {
                if (_this.jsonData.NoOfKids == '1') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat, "TitleID": _this.titleId1.id, "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID, "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray1, "isExistingPolicy": isxistingPolicy1, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1, "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1, "SumInsured": _this.insuredForm.value.SumInsured1
                        }, {
                            "MemberType": _this.insuredForm.value.member2Cat, "TitleID": _this.titleId2.id, "Name": _this.insuredForm.value.member2Name,
                            "RelationshipID": _this.relationId2.RelationshipID, "RelationshipName": _this.insuredForm.value.member2Rela,
                            "DOB": moment(_this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member2Feet + '.' + _this.insuredForm.value.member2Inches,
                            "Weight": _this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray2, "isExistingPolicy": isxistingPolicy2, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy2, "PolicyDuration": _this.insuredForm.value.PolicyDuration2,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany2, "SumInsured": _this.insuredForm.value.SumInsured2
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat, "TitleID": _this.titleId3.id, "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID, "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray3, "isExistingPolicy": isxistingPolicy3, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3, "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3, "SumInsured": _this.insuredForm.value.SumInsured3
                        }];
                    resolve();
                }
                else if (_this.jsonData.NoOfKids == '2') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat, "TitleID": _this.titleId1.id, "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID, "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray1, "isExistingPolicy": isxistingPolicy1, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1, "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1, "SumInsured": _this.insuredForm.value.SumInsured1
                        }, {
                            "MemberType": _this.insuredForm.value.member2Cat, "TitleID": _this.titleId2.id, "Name": _this.insuredForm.value.member2Name,
                            "RelationshipID": _this.relationId2.RelationshipID, "RelationshipName": _this.insuredForm.value.member2Rela,
                            "DOB": moment(_this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member2Feet + '.' + _this.insuredForm.value.member2Inches,
                            "Weight": _this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray2, "isExistingPolicy": isxistingPolicy2, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy2, "PolicyDuration": _this.insuredForm.value.PolicyDuration2,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany2, "SumInsured": _this.insuredForm.value.SumInsured2
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat, "TitleID": _this.titleId3.id, "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID, "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray3, "isExistingPolicy": isxistingPolicy3, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3, "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3, "SumInsured": _this.insuredForm.value.SumInsured3
                        }, {
                            "MemberType": _this.insuredForm.value.child2Cat, "TitleID": _this.titleId4.id, "Name": _this.insuredForm.value.child2Name,
                            "RelationshipID": _this.relationId4.RelationshipID, "RelationshipName": _this.insuredForm.value.child2Rela,
                            "DOB": moment(_this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child2Feet + '.' + _this.insuredForm.value.child2Inches,
                            "Weight": _this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray4, "isExistingPolicy": isxistingPolicy4, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy4, "PolicyDuration": _this.insuredForm.value.PolicyDuration4,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany4, "SumInsured": _this.insuredForm.value.SumInsured4
                        }];
                    resolve();
                }
                else if (_this.jsonData.NoOfKids == '2') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat, "TitleID": _this.titleId1.id, "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID, "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray1, "isExistingPolicy": isxistingPolicy1, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy1, "PolicyDuration": _this.insuredForm.value.PolicyDuration1,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany1, "SumInsured": _this.insuredForm.value.SumInsured1
                        }, {
                            "MemberType": _this.insuredForm.value.member2Cat, "TitleID": _this.titleId2.id, "Name": _this.insuredForm.value.member2Name,
                            "RelationshipID": _this.relationId2.RelationshipID, "RelationshipName": _this.insuredForm.value.member2Rela,
                            "DOB": moment(_this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.member2Feet + '.' + _this.insuredForm.value.member2Inches,
                            "Weight": _this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray2, "isExistingPolicy": isxistingPolicy2, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy2, "PolicyDuration": _this.insuredForm.value.PolicyDuration2,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany2, "SumInsured": _this.insuredForm.value.SumInsured2
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat, "TitleID": _this.titleId3.id, "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID, "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray3, "isExistingPolicy": isxistingPolicy3, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy3, "PolicyDuration": _this.insuredForm.value.PolicyDuration3,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany3, "SumInsured": _this.insuredForm.value.SumInsured3
                        }, {
                            "MemberType": _this.insuredForm.value.child2Cat, "TitleID": _this.titleId4.id, "Name": _this.insuredForm.value.child2Name,
                            "RelationshipID": _this.relationId4.RelationshipID, "RelationshipName": _this.insuredForm.value.child2Rela,
                            "DOB": moment(_this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child2Feet + '.' + _this.insuredForm.value.child2Inches,
                            "Weight": _this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray4, "isExistingPolicy": isxistingPolicy4, "TypeofPolicy": _this.insuredForm.value.TypeofPolicy4, "PolicyDuration": _this.insuredForm.value.PolicyDuration4,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany4, "SumInsured": _this.insuredForm.value.SumInsured4
                        }, {
                            "MemberType": _this.insuredForm.value.child3Cat, "TitleID": _this.titleId5.id, "Name": _this.insuredForm.value.child3Name,
                            "RelationshipID": _this.relationId5.RelationshipID, "RelationshipName": _this.insuredForm.value.child3Rela,
                            "DOB": moment(_this.insuredForm.value.child3DOB).format('DD-MMM-YYYY'), "Height": _this.insuredForm.value.child3Feet + '.' + _this.insuredForm.value.child3Inches,
                            "Weight": _this.insuredForm.value.child3Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
                            "Ailments": _this.ailmentArray5, "isExistingPolicy": isxistingPolicy5, "TypeofPolicy": data.value.TypeofPolicy5, "PolicyDuration": _this.insuredForm.value.PolicyDuration5,
                            "InsuranceCompany": _this.insuredForm.value.InsuranceCompany5, "SumInsured": _this.insuredForm.value.SumInsured5
                        }];
                    resolve();
                }
            }
        });
    };
    HealthProposalPage.prototype.SavePPAPInsuredData = function (formData) {
        var isFormValid = this.ValidationCheckPPAP(formData);
        if (isFormValid) {
            if (formData.value.member1Cat == 'Adult') {
                for (var i = 0; i < this.adultRelationArray.length; i++) {
                    if (this.adultRelationArray[i].RelationshipName = formData.value.member1Rela) {
                        localStorage.setItem('InsuredRealtionData', JSON.stringify(this.adultRelationArray[i]));
                    }
                }
            }
            else {
                for (var i = 0; i < this.childRelationArray.length; i++) {
                    if (this.childRelationArray[i].RelationshipName = formData.value.member1Rela) {
                        localStorage.setItem('InsuredRealtionData', JSON.stringify(this.childRelationArray[i]));
                    }
                }
            }
            this.router.navigateByUrl('health-proposal/propApplicant');
            localStorage.setItem('PPAPInsuredData', JSON.stringify(formData.value));
        }
    };
    HealthProposalPage.prototype.validatepanCard = function (val) {
        if (new RegExp('[A-Z]{5}[0-9]{4}[A-Z]{1}').test(val)) {
            return true;
        }
        return false;
    };
    HealthProposalPage.prototype.presentToast = function (msg) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: 3000,
                            cssClass: "toastscheme ",
                            showCloseButton: true,
                            closeButtonText: "OK",
                            position: 'middle'
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HealthProposalPage.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //-- Proposal Insured Code Ends --//
    //-- Proposal Basic Code Begins --///
    HealthProposalPage.prototype.getJsonData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.requestFromPremium = JSON.parse(localStorage.getItem('quoteRequest'));
            _this.jsonData = _this.requestFromPremium;
            console.log(_this.requestFromPremium);
            resolve();
        });
    };
    HealthProposalPage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.requestFromPremium = localStorage.getItem('requestForProp');
            resolve();
        });
    };
    HealthProposalPage.prototype.xmlJson = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.jsonData = _this.xml2Json.xmlToJson(data);
            console.log(_this.jsonData);
            resolve();
        });
    };
    HealthProposalPage.prototype.createBasicForm = function () {
        this.basicPropForm = new FormGroup({
            productName: new FormControl(),
            subProductname: new FormControl(),
            noOfAdult: new FormControl(),
            eldestMembAge: new FormControl(),
            noOfChild: new FormControl(),
            policyTenure: new FormControl(),
            annualSumInsured: new FormControl(),
            subLimit: new FormControl(),
            JKProp: new FormControl(),
            addOnCover1: new FormControl(),
            addOnCover3: new FormControl(),
            addOnCover5: new FormControl(),
            addOnCover6: new FormControl(),
            ////// for HBooster
            softCopy: new FormControl(),
            addOnCover2: new FormControl(),
            deductible: new FormControl(),
            ///for PPAP
            premium: new FormControl(),
            sumInsured: new FormControl()
        });
    };
    HealthProposalPage.prototype.patchValue = function (form) {
        console.log(form);
        if (form.ProductType == 'CHI') {
            var data = form;
            this.product = form.ProductType;
            if (data.IsJammuKashmir == 'true') {
                this.basicPropForm.patchValue({ 'JKProp': 'Yes' });
            }
            else {
                this.basicPropForm.patchValue({ 'JKProp': 'No' });
            }
            if (data.AddOn1 == "true" || data.AddOn1 == true) {
                this.basicPropForm.patchValue({ 'addOnCover1': 'Yes' });
            }
            else {
                this.basicPropForm.patchValue({ 'addOnCover1': 'No' });
            }
            if (data.AddOn3 == "true" || data.AddOn3 == true) {
                this.basicPropForm.patchValue({ 'addOnCover3': 'Yes' });
            }
            else {
                this.basicPropForm.patchValue({ 'addOnCover3': 'No' });
            }
            if (data.AddOn5 == "true" || data.AddOn5 == true) {
                this.basicPropForm.patchValue({ 'addOnCover5': 'Yes' });
            }
            else {
                this.basicPropForm.patchValue({ 'addOnCover5': 'No' });
            }
            if (data.AddOn6 == true || data.AddOn6 == 'true') {
                this.basicPropForm.patchValue({ 'addOnCover6': 'Yes' });
            }
            else {
                this.basicPropForm.patchValue({ 'addOnCover6': 'No' });
            }
            if (!data.SubLimit) {
                this.basicPropForm.patchValue({ 'subLimit': 'None' });
            }
            else {
                this.basicPropForm.patchValue({ 'subLimit': data.SubLimit });
            }
            if (data.CHIProductName == "PROTECT") {
                this.basicPropForm.patchValue({ 'productName': 'HEALTH PROTECT' });
            }
            else if (data.CHIProductName == "PROTECTPLUS") {
                this.basicPropForm.patchValue({ 'productName': 'HEALTH PROTECT PLUS' });
            }
            else if (data.CHIProductName == "SMART") {
                this.basicPropForm.patchValue({ 'productName': 'HEALTH SMART' });
            }
            else if (data.CHIProductName == "SMARTPLUS") {
                this.basicPropForm.patchValue({ 'productName': 'HEALTH SMART PLUS' });
            }
            else if (data.CHIProductName == "iHealth") {
                this.basicPropForm.patchValue({ 'productName': data.CHIProductName });
            }
            this.basicPropForm.patchValue({ 'noOfAdult': data.NoOfAdults });
            this.basicPropForm.patchValue({ 'eldestMembAge': data.AgeGroup });
            this.basicPropForm.patchValue({ 'noOfChild': data.NoOfKids });
            this.basicPropForm.patchValue({ 'policyTenure': data.YearPremium });
            this.basicPropForm.patchValue({ 'annualSumInsured': data.InsuredAmount });
            // this.basicPropForm.patchValue({'subLimit':data.SubLimit});
            if (parseInt(data.NoOfAdults) > 0 && parseInt(data.NoOfKids) <= 0) {
                this.showSegment = 'adult1';
            }
            if (parseInt(data.NoOfAdults) <= 0 && parseInt(data.NoOfKids) > 0) {
                this.showSegment = 'isChild1';
            }
            if (parseInt(data.NoOfAdults) > 0 && parseInt(data.NoOfKids) > 0) {
                this.showSegment = 'adult1';
            }
        }
        else if (form.ProductType == 'HBOOSTER') {
            console.log("Health Booster");
            var data = form;
            this.product = form.ProductType;
            if (data.IsJammuKashmir == 'true') {
                this.basicPropForm.patchValue({ 'JKProp': 'Yes' });
            }
            else {
                this.basicPropForm.patchValue({ 'JKProp': 'No' });
            }
            if (data.AddOn1 == 'true' || data.AddOn1 == true) {
                this.basicPropForm.patchValue({ 'addOnCover1': 'Yes' });
            }
            else {
                this.basicPropForm.patchValue({ 'addOnCover1': 'No' });
            }
            if (data.AddOn2 == 'true' || data.AddOn2 == true) {
                this.basicPropForm.patchValue({ 'addOnCover2': 'Yes' });
            }
            else {
                this.basicPropForm.patchValue({ 'addOnCover2': 'No' });
            }
            if (data.AddOn3 == 'true' || data.AddOn3 == true) {
                this.basicPropForm.patchValue({ 'addOnCover3': 'Yes' });
            }
            else {
                this.basicPropForm.patchValue({ 'addOnCover3': 'No' });
            }
            if (data.SoftLessCopy == 'true') {
                this.basicPropForm.patchValue({ 'softCopy': 'Yes' });
            }
            else {
                this.basicPropForm.patchValue({ 'softCopy': 'No' });
            }
            this.basicPropForm.patchValue({ 'productName': 'Health Booster' });
            if (data.ProductName == 'HBOOSTER_TOPUP') {
                this.basicPropForm.patchValue({ 'subProductname': 'Health Booster-Top Up Plan' });
                this.basicPropForm.patchValue({ 'productName': 'Top Up Plan' });
            }
            else {
                this.basicPropForm.patchValue({ 'subProductname': 'Health Booster-Super Top Up Plan' });
                this.basicPropForm.patchValue({ 'productName': 'Super Top Up Plan' });
            }
            this.basicPropForm.patchValue({ 'noOfAdult': data.NoOfAdults });
            this.basicPropForm.patchValue({ 'eldestMembAge': data.Adult1Age });
            this.basicPropForm.patchValue({ 'noOfChild': data.NoOfKids });
            this.basicPropForm.patchValue({ 'policyTenure': this.responseBody.HealthDetails[0].Tenure });
            this.basicPropForm.patchValue({ 'annualSumInsured': data.SumInsured });
            this.basicPropForm.patchValue({ 'deductible': data.VolDedutible });
            if (parseInt(data.NoOfAdults) > 0 && parseInt(data.NoOfKids) <= 0) {
                this.showSegment = 'adult1';
            }
            if (parseInt(data.NoOfAdults) <= 0 && parseInt(data.NoOfKids) > 0) {
                this.showSegment = 'isChild1';
            }
            if (parseInt(data.NoOfAdults) > 0 && parseInt(data.NoOfKids) > 0) {
                this.showSegment = 'adult1';
            }
        }
        else {
            var data = form;
            var tempName = void 0;
            this.product = form.ProductType;
            var annual = localStorage.getItem('AnuualIncome');
            var sum = localStorage.getItem('SumInsured');
            var response = JSON.parse(localStorage.getItem('quoteResponse'));
            if (data.SubProduct == 'PP') {
                tempName = 'Lump Sum Payment Option';
            }
            else if (data.SubProduct == 'PPA') {
                tempName = 'Monthly Payment Option';
            }
            else if (data.SubProduct == 'PPC') {
                tempName = 'Lump Sum Payment Option With Monthly Payout';
            }
            this.basicPropForm.patchValue({ 'productName': data.ProductType });
            var tempTenure = parseInt(response.HealthDetails[0].Tenure);
            if (tempTenure == 1) {
                this.basicPropForm.patchValue({ 'policyTenure': response.HealthDetails[0].Tenure + ' year(s)' });
            }
            else if (tempTenure > 1) {
                this.basicPropForm.patchValue({ 'policyTenure': response.HealthDetails[0].Tenure + ' years' });
            }
            var tempASum = parseInt(annual);
            if (tempASum == 1) {
                this.basicPropForm.patchValue({ 'annualSumInsured': '₹ ' + annual + ' Lakh(s)' });
            }
            else if (tempASum > 1) {
                this.basicPropForm.patchValue({ 'annualSumInsured': '₹ ' + annual + ' Lacs' });
            }
            else {
                this.basicPropForm.patchValue({ 'annualSumInsured': '₹ ' + annual + ' Lacs' });
            }
            this.basicPropForm.patchValue({ 'subProductname': tempName });
            this.basicPropForm.patchValue({ 'sumInsured': '₹ ' + sum });
            this.basicPropForm.patchValue({ 'premium': '₹ ' + response.HealthDetails[0].TotalPremium });
        }
    };
    //-- Proposal Basic Code Ends --///
    HealthProposalPage.prototype.showLoading = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Loading',
                            })];
                    case 1:
                        _a.loading = _b.sent();
                        return [4 /*yield*/, this.loading.present()];
                    case 2: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    HealthProposalPage.prototype.messageAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HealthProposalPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    HealthProposalPage.prototype.showBasicDetails = function (ev) {
        this.isBasicDetails = !this.isBasicDetails;
        if (this.isBasicDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("basicDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("basicDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("basicDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("basicDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthProposalPage.prototype.showInsuredDetails = function (ev) {
        this.isInsuredDetails = !this.isInsuredDetails;
        if (this.isInsuredDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("insuredDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("insuredDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthProposalPage.prototype.showApplicantDetails = function (ev) {
        this.isApplicantDetails = !this.isApplicantDetails;
        if (this.isApplicantDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthProposalPage = tslib_1.__decorate([
        Component({
            selector: 'app-health-proposal',
            templateUrl: './health-proposal.page.html',
            styleUrls: ['./health-proposal.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router, ChangeDetectorRef, AlertController, CommonService, ToastController, ModalController, XmlToJsonService, EcryDecryService, LoadingController])
    ], HealthProposalPage);
    return HealthProposalPage;
}());
export { HealthProposalPage };
//# sourceMappingURL=health-proposal.page.js.map