import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
var CompleteHealthInsurancePage = /** @class */ (function () {
    function CompleteHealthInsurancePage(cs, xml2JsonService, loadingCtrl) {
        this.cs = cs;
        this.xml2JsonService = xml2JsonService;
        this.loadingCtrl = loadingCtrl;
        this.healthMasterRequestXML = "<Request>\n\t<UserType>AGENT</UserType>\n\t<UserID>MTY0MTI1NDRfc011b2JtUWRmTDNPV3BReHlGTTJZZEx5YkZrVU1mSVVIVWpaT0UrT3cxVT0=</UserID>\n\t<SubType>19</SubType>\n  </Request>";
        this.showAdultTable = false;
        this.adult2AgeArray = [];
        this.showAddOnCover1 = false;
        this.showAddOnCover3 = false;
        this.showAddOnCover5 = false;
        this.showAddOnCover6 = false;
        this.button1 = false;
        this.button2 = false;
        this.button3 = false;
        this.subilimitA = false;
        this.subilimitB = false;
        this.subilimitC = false;
        this.subilimitN = false;
    }
    CompleteHealthInsurancePage.prototype.ngOnInit = function () {
        this.createQuoteForm();
        this.getStateMaster();
        console.log("Validation", this.quoteForm.controls.childNo);
    };
    CompleteHealthInsurancePage.prototype.getAdult = function (ev) {
        console.log(ev.detail.value);
        this.noOfAudult = ev.detail.value;
        if (this.noOfAudult > 0) {
            this.showAdultTable = true;
        }
        var child = this.adultChildMap.get(ev.detail.value);
        this.childArray = child;
        var adultAge = this.adultAgeMap.get(ev.detail.value);
        this.adultAgeArray = adultAge;
        console.log(child);
        console.log("AGE", this.adultAgeArray);
    };
    CompleteHealthInsurancePage.prototype.addOnYesNo = function (event) {
        console.log(this.quoteForm.value.addOnCover6);
        console.log("Eldest Age", this.quoteForm.value.eldestMemAge);
        this.quoteForm.patchValue({ "memberAge1": this.quoteForm.value.eldestMemAge });
    };
    CompleteHealthInsurancePage.prototype.selectTenure = function (ev) {
        console.log(ev.target.innerText);
        if (ev.target.innerText == '1 YEAR') {
            this.button1 = true;
            this.button2 = false;
            this.button3 = false;
            console.log("1 year Tenure", this.button1);
            this.quoteForm.patchValue({ "policyTenure": "1" });
        }
        else if (ev.target.innerText == '2 YEAR') {
            this.button1 = false;
            this.button2 = true;
            this.button3 = false;
            this.quoteForm.patchValue({ "policyTenure": "2" });
        }
        else {
            this.button1 = false;
            this.button2 = false;
            this.button3 = true;
            this.quoteForm.patchValue({ "policyTenure": "3" });
        }
    };
    CompleteHealthInsurancePage.prototype.selectSublimit = function (ev) {
        if (ev.target.innerText == 'A') {
            // console.log("A sublimit");
            this.subilimitA = true;
            this.subilimitB = false;
            this.subilimitC = false;
            this.subilimitN = false;
            this.quoteForm.patchValue({ "subLimit": "A" });
        }
        else if (ev.target.innerText == 'B') {
            this.subilimitA = false;
            this.subilimitB = true;
            this.subilimitC = false;
            this.subilimitN = false;
            // console.log("B sublimit");
            this.quoteForm.patchValue({ "subLimit": "B" });
        }
        else if (ev.target.innerText == 'C') {
            this.subilimitA = false;
            this.subilimitB = false;
            this.subilimitC = true;
            this.subilimitN = false;
            // console.log("C sublimit");
            this.quoteForm.patchValue({ "subLimit": "C" });
        }
        else {
            this.subilimitA = false;
            this.subilimitB = false;
            this.subilimitC = false;
            this.subilimitN = true;
            // console.log("NONE sublimit");
            this.quoteForm.patchValue({ "subLimit": "" });
        }
    };
    CompleteHealthInsurancePage.prototype.getStateMaster = function () {
        var _this = this;
        this.cs.postStateMaster('/api/MotorMaster/GetAllStates', '').then(function (res) {
            _this.stateMaster = res;
        });
    };
    CompleteHealthInsurancePage.prototype.createQuoteForm = function () {
        this.quoteForm = new FormGroup({
            productName: new FormControl(['', Validators.required]),
            // zone: new FormControl(),
            state: new FormControl(),
            adultNo: new FormControl(),
            childNo: new FormControl(),
            annualSumInsured: new FormControl(),
            policyTenure: new FormControl(),
            eldestMemAge: new FormControl(),
            // deductibleSumInsured: new FormControl(),
            // coPayment: new FormControl(),
            subLimit: new FormControl(),
            addOnCover1: new FormControl(),
            addOnCover3: new FormControl(),
            addOnCover5: new FormControl(),
            addOnCover6: new FormControl(),
            memberAge1: new FormControl(),
            memberAge2: new FormControl()
        });
    };
    CompleteHealthInsurancePage.prototype.refreshForm = function (prodName) {
        this.quoteForm = new FormGroup({
            productName: new FormControl(prodName),
            // zone: new FormControl(),
            state: new FormControl(),
            adultNo: new FormControl(),
            childNo: new FormControl(),
            annualSumInsured: new FormControl(),
            policyTenure: new FormControl(),
            eldestMemAge: new FormControl(),
            // deductibleSumInsured: new FormControl(),
            // coPayment: new FormControl(),
            subLimit: new FormControl(),
            addOnCover1: new FormControl(),
            addOnCover3: new FormControl(),
            addOnCover5: new FormControl(),
            addOnCover6: new FormControl(),
            memberAge1: new FormControl(),
            memberAge2: new FormControl()
        });
    };
    CompleteHealthInsurancePage.prototype.getProductName = function (event) {
        var _this = this;
        this.showLoading();
        return new Promise(function (resolve) {
            _this.refreshForm(event.detail.value);
            console.log(event.detail.value);
            _this.productName = event.detail.value;
            if (event.detail.value == "PROTECT") {
                _this.productCode = 14;
            }
            else if (event.detail.value == "PROTECTPLUS") {
                _this.productCode = 12;
            }
            else if (event.detail.value == "SMART") {
                _this.productCode = 16;
            }
            else if (event.detail.value == "SMARTPLUS") {
                _this.productCode = 15;
            }
            else {
                _this.productCode = 17;
            }
            _this.getHealthMaster();
            _this.setAddOnCover(event.detail.value);
            console.log("Validation 1234", _this.quoteForm.get('productName'));
            resolve();
        });
    };
    CompleteHealthInsurancePage.prototype.getHealthMaster = function () {
        var _this = this;
        var healthMasterRequestXML = "<Request>\n\t                                <UserType>" + "AGENT" + "</UserType>\n\t                                <UserID>MTY0MTI1NDRfc011b2JtUWRmTDNPV3BReHlGTTJZZEx5YkZrVU1mSVVIVWpaT0UrT3cxVT0=</UserID>\n\t                                <SubType>" + this.productCode + "</SubType>\n                                  </Request>";
        this.cs.post('/HealthMaster.svc/HealthPlanMaster', healthMasterRequestXML).then(function (res) {
            console.log("Response", res);
        }).catch(function (err) {
            if (err.status != 200) {
                console.log("OK");
            }
            _this.healthMasterResponse = _this.xml2JsonService.xmlToJson(err.error.text);
            console.log('Health Plan Master', _this.healthMasterResponse);
            // this.quoteForm.patchValue({"adultNo": this.healthMasterResponse.HealthPremiumResponse.AdultResponse.AdultDetails[0].AdultValue});
            // this.quoteForm.patchValue({"childNo": this.healthMasterResponse.HealthPremiumResponse.AdultResponse.AdultDetails[0].AdultValue});
            //get adult and child
            _this.adultChildMap = new Map();
            _this.adultAgeMap = new Map();
            _this.healthMasterResponse.HealthPremiumResponse.AdultResponse.AdultDetails.forEach(function (adult) {
                // console.log("adult LENGTH", this.adultDataLength);
                console.log("Adult VALUE", adult.AdultValue);
                console.log("Child COUNT", adult.ChildDetails.Child.length);
                console.log("Adult AGE VALUE", adult.AgeEldestDetails.AgeEldest);
                _this.adultChildMap.set(adult.AdultValue, adult.ChildDetails.Child);
                _this.adultAgeMap.set(adult.AdultValue, adult.AgeEldestDetails.AgeEldest);
                console.log("MAP", _this.adultChildMap);
                console.log("AGE MAP", _this.adultAgeMap);
            });
            _this.loading.dismiss();
        });
    };
    CompleteHealthInsurancePage.prototype.quateCalculation = function (form) {
        var _this = this;
        console.log(form);
        var requestBody = "<CHI>\n    <ProductType>CHI</ProductType>\n    <UserID>MTY0MTI1NDRfc011b2JtUWRmTDNPV3BReHlGTTJZZEx5YkZrVU1mSVVIVWpaT0UrT3cxVT0=</UserID>\n    <UserType>Agent</UserType>\n    <NoOfAdults>" + form.adultNo + "</NoOfAdults>\n    <NoOfKids>" + form.childNo + "</NoOfKids>\n    <AgeGroup1>" + form.eldestMemAge + "</AgeGroup1>\n    <AgeGroup2>" + form.memberAge2 + "</AgeGroup2>\n    <AgeGroup>" + form.eldestMemAge + "</AgeGroup>\n    <CHIProductName>" + form.productName + "</CHIProductName>\n    <SubLimit>" + form.subLimit + "</SubLimit>\n    <YearPremium>" + form.policyTenure + "</YearPremium>\n    <IsJammuKashmir>false</IsJammuKashmir>\n    <VolDedutible>0</VolDedutible>\n    <GSTStateCode></GSTStateCode>\n    <AddOn1>" + form.addOnCover1 + "</AddOn1>\n    <InsuredAmount>" + form.annualSumInsured + "</InsuredAmount>\n    <AddOn6>" + form.addOnCover6 + "</AddOn6>\n    <AddOn3>" + form.addOnCover3 + "</AddOn3>\n    <AddOn5>" + form.addOnCover5 + "</AddOn5>\n    <GSTStateName>" + form.state + "</GSTStateName>\n    <Members>\n    <Member>\n    <AddOnName>Adult1</AddOnName>\n    <AddOnAgeGroup>" + form.memberAge1 + "</AddOnAgeGroup>\n    </Member>\n    <Member>\n    <AddOnName>Adult2</AddOnName>\n    <AddOnAgeGroup>" + form.memberAge2 + "</AddOnAgeGroup>\n    </Member>\n    </Members>\n    <AddOn6Members></AddOn6Members>\n  </CHI>";
        console.log("request", requestBody);
        this.cs.post('/Quote.svc/GetQuote/Health/NThiM2E5NWMtZTkyMy00MWEyLWIwOWMtZGFhOWFiYTJiZDNk', requestBody).then(function (response) {
            console.log(_this.xml2JsonService.xmlToJson(response));
        }).catch(function (error) {
            if (error.status != 200) {
                console.log("OK");
            }
            _this.quateTotalPremium = _this.xml2JsonService.xmlToJson(error.error.text);
            console.log(_this.quateTotalPremium.QuoteResponse.HealthDetails.HealthOutputResponse.TotalPremium);
        });
    };
    CompleteHealthInsurancePage.prototype.calculateQuote = function (formData) {
        console.log("on CLick", formData);
        this.quateCalculation(formData.value);
    };
    CompleteHealthInsurancePage.prototype.getSubType = function (data) {
        console.log("Selected Data", data);
    };
    CompleteHealthInsurancePage.prototype.getSumInsured = function (event) {
        console.log("Sum Insured", event.detail.value, this.productName);
        if (this.productName == "PROTECT") {
            if (event.detail.value < '500000') {
                console.log("ADD on Cover True");
                this.showAddOnCover1 = false;
                this.showAddOnCover3 = false;
                this.showAddOnCover5 = false;
                this.showAddOnCover6 = false;
                this.quoteForm.patchValue({ 'addOnCover1': true });
            }
            else {
                console.log("ADD on cover false");
                this.showAddOnCover1 = true;
                this.showAddOnCover3 = false;
                this.showAddOnCover5 = false;
                this.showAddOnCover6 = false;
            }
        }
        else if (this.productName == "PROTECTPLUS") {
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover3 = false;
            this.showAddOnCover6 = false;
        }
        else if (this.productName == "SMART") {
            this.showAddOnCover3 = true;
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover6 = false;
        }
        else if (this.productName == "SMARTPLUS") {
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover3 = false;
            this.showAddOnCover6 = false;
        }
        else {
            this.showAddOnCover1 = true;
            this.showAddOnCover6 = true;
            this.showAddOnCover3 = false;
            this.showAddOnCover5 = false;
        }
    };
    CompleteHealthInsurancePage.prototype.getAgeStatus = function (event) {
        if (this.adultAgeArray.length) {
            var index = this.adultAgeArray.findIndex(function (data) { return data.AgeValue == event.detail.value; });
            console.log(index);
            for (var i = 0; i < index; i++) {
                console.log(i);
                this.adult2AgeArray.push(this.adultAgeArray[i]);
            }
        }
        else {
            this.adult2AgeArray = this.adultAgeArray;
        }
        console.log("ageLimit", this.adult2AgeArray);
    };
    CompleteHealthInsurancePage.prototype.setAddOnCover = function (prodName) {
        if (prodName == "PROTECT") {
            this.showAddOnCover1 = true;
            this.showAddOnCover3 = false;
            this.showAddOnCover5 = false;
            this.showAddOnCover6 = false;
        }
        else if (prodName == "PROTECTPLUS") {
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover3 = false;
            this.showAddOnCover6 = false;
        }
        else if (prodName == "SMART") {
            this.showAddOnCover3 = true;
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover6 = false;
        }
        else if (prodName == "SMARTPLUS") {
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover3 = false;
            this.showAddOnCover6 = false;
        }
        else {
            this.showAddOnCover1 = true;
            this.showAddOnCover6 = true;
            this.showAddOnCover3 = false;
            this.showAddOnCover5 = false;
        }
    };
    CompleteHealthInsurancePage.prototype.showLoading = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                spinner: 'lines',
                                // duration: 5000,
                                message: 'Please wait...',
                                translucent: true
                                // cssClass: 'custom-class custom-loading'
                            })];
                    case 1:
                        _a.loading = _b.sent();
                        return [4 /*yield*/, this.loading.present()];
                    case 2: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    CompleteHealthInsurancePage = tslib_1.__decorate([
        Component({
            selector: 'app-complete-health-insurance',
            templateUrl: './complete-health-insurance.page.html',
            styleUrls: ['./complete-health-insurance.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, XmlToJsonService, LoadingController])
    ], CompleteHealthInsurancePage);
    return CompleteHealthInsurancePage;
}());
export { CompleteHealthInsurancePage };
//# sourceMappingURL=complete-health-insurance.page.js.map