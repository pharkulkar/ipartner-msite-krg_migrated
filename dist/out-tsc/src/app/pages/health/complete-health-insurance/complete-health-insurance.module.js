import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CompleteHealthInsurancePage } from './complete-health-insurance.page';
var routes = [
    {
        path: '',
        component: CompleteHealthInsurancePage
    }
];
var CompleteHealthInsurancePageModule = /** @class */ (function () {
    function CompleteHealthInsurancePageModule() {
    }
    CompleteHealthInsurancePageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CompleteHealthInsurancePage]
        })
    ], CompleteHealthInsurancePageModule);
    return CompleteHealthInsurancePageModule;
}());
export { CompleteHealthInsurancePageModule };
//# sourceMappingURL=complete-health-insurance.module.js.map