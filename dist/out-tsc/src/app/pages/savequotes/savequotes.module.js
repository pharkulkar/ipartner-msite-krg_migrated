import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SavequotesPage } from './savequotes.page';
var routes = [
    {
        path: '',
        component: SavequotesPage
    }
];
var SavequotesPageModule = /** @class */ (function () {
    function SavequotesPageModule() {
    }
    SavequotesPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [SavequotesPage]
        })
    ], SavequotesPageModule);
    return SavequotesPageModule;
}());
export { SavequotesPageModule };
//# sourceMappingURL=savequotes.module.js.map