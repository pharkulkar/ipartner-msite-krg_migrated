import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import * as moment from 'moment';
import { config } from 'src/app/config.properties';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
var PaymentModePage = /** @class */ (function () {
    function PaymentModePage(cs, toastCtrl, router) {
        this.cs = cs;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.transType = "POLICY_PAYMENT";
        this.totalAmount = 0;
        this.paymentDone = false;
        this.PIDArray = [];
        this.showMobile = false;
        this.showEmail = true;
        this.linkPayment = false;
        this.isNewPID = false;
    }
    PaymentModePage.prototype.ngOnInit = function () {
        var _this = this;
        this.createChequeForm();
        this.createDualCheckForm();
        this.createPIDForm();
        this.getBankName();
        this.getBranchName();
        this.getData().then(function () {
            _this.validationForChequeDate(_this.travelRequest);
            console.log("Hello", _this.travelProp);
            _this.travelPropReq = JSON.parse(localStorage.getItem('proposalRequest'));
            if (_this.travelPropReq.ProductType == 'CHI' || _this.travelPropReq.ProductType == 'HBOOSTER' || _this.travelPropReq.ProductType == 'PPAP') {
                _this.chequeForm.patchValue({ 'proposalNo': _this.travelProp.SavePolicy[0].ProposalNo });
                _this.dualCheckForm.patchValue({ 'proposalNo': _this.travelProp.SavePolicy[0].ProposalNo });
            }
            else {
                _this.chequeForm.patchValue({ 'proposalNo': _this.travelProp.SavePolicy[0].PF_ProsalNumber });
                _this.dualCheckForm.patchValue({ 'proposalNo': _this.travelProp.SavePolicy[0].PF_ProsalNumber });
            }
        });
    };
    PaymentModePage.prototype.createChequeForm = function () {
        this.chequeForm = new FormGroup({
            proposalNo: new FormControl(),
            chequeNo: new FormControl(),
            chequeDate: new FormControl(),
            bankName: new FormControl(),
            branchName: new FormControl(),
        });
    };
    PaymentModePage.prototype.createPIDForm = function () {
        this.PIDForm = new FormGroup({
            PID1: new FormControl(),
            PID2: new FormControl(),
            PID3: new FormControl(),
            PID4: new FormControl(),
            PID5: new FormControl()
        });
    };
    PaymentModePage.prototype.createDualCheckForm = function () {
        this.dualCheckForm = new FormGroup({
            proposalNo: new FormControl(),
            chequeNo: new FormControl(),
            chequeAmount: new FormControl(),
            chequeDate: new FormControl(),
            bankName: new FormControl(),
            branchName: new FormControl(),
            PID1: new FormControl(),
            PID2: new FormControl(),
            PID3: new FormControl(),
            PID4: new FormControl(),
            PID5: new FormControl()
        });
    };
    // getNewPID():Promise<any>{
    //   return new Promise((resolve) => {
    //     this.newPID = JSON.parse(localStorage.getItem('newPID'));
    //     if(this.newPID){
    //       if(this.newPID.CustomerId){ this.newPIDCustId = this.newPID.CustomerId;}else{ this.newPIDCustId = "" }
    //       if(this.newPID.custEmail){ this.custEmail = this.newPID.custEmail;}else{ this.custEmail = "" }
    //       if(this.newPID.custMobile){ this.custMobile = this.newPID.custMobile;}else{ this.custMobile = "" }
    //       if(this.newPID.custName){ this.custName = this.newPID.custName;}else{ this.custName = "" }
    //     }else{
    //       this.newPIDCustId = "";
    //       this.custEmail = "";
    //       this.custMobile = "";
    //       this.custName = "";
    //     }      
    //     resolve();
    //   });
    // }
    PaymentModePage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.travelRequest = JSON.parse(localStorage.getItem('quoteRequest'));
            _this.travelResponse = JSON.parse(localStorage.getItem('quoteResponse'));
            _this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
            _this.travelProp = JSON.parse(localStorage.getItem('proposalResponse'));
            _this.travelPropReq = JSON.parse(localStorage.getItem('proposalRequest'));
            _this.createCustReq = JSON.parse(localStorage.getItem('customerRequest'));
            _this.PIDData = JSON.parse(localStorage.getItem('PIDData'));
            _this.paymentMode = JSON.parse(localStorage.getItem('paymentMode'));
            _this.custType = JSON.parse(localStorage.getItem('custType'));
            _this.newPID = JSON.parse(localStorage.getItem('newPID'));
            if (_this.newPID) {
                if (_this.newPID.isNew) {
                    _this.isNewPID = true;
                }
                else {
                    _this.isNewPID = false;
                }
            }
            else {
                _this.isNewPID = false;
            }
            console.log("IMP", _this.travelProp);
            console.log(_this.travelRequest);
            console.log(_this.travelResponse);
            console.log(_this.planDetails);
            console.log(_this.travelPropReq);
            console.log(_this.createCustReq);
            console.log("PID", _this.PIDData);
            console.log("NEW PID", _this.newPID);
            resolve();
        });
    };
    PaymentModePage.prototype.validationForChequeDate = function (data) {
        console.log(data.LeavingIndia);
        if (this.travelRequest.ProductType == 'PPAP') {
            var date = new Date(data.LeavingIndia);
            var date1 = new Date(data.LeavingIndia);
            date.setDate(date.getDate() - parseInt('15'));
            this.minChequeDate = moment(date).format('YYYY-MM-DD');
            date1.setDate(date1.getDate() + parseInt('15'));
            this.minChequeDate = moment(date1).format('YYYY-MM-DD');
        }
        else {
            var date = new Date(data.LeavingIndia);
            date.setDate(date.getDate() - parseInt('90'));
            this.minChequeDate = moment(date).format('YYYY-MM-DD');
            console.log(this.minChequeDate);
            this.maxChequeDate = moment(data.LeavingIndia).format('YYYY-MM-DD');
        }
    };
    PaymentModePage.prototype.getBankName = function () {
        var _this = this;
        this.cs.postWithParams('/api/Agent/GetBankName?PolicyType=1', '').then(function (res) {
            _this.bankNames = res.BankDetail;
            console.log(res);
        }).catch(function (err) {
            console.log(err.error.text);
        });
    };
    PaymentModePage.prototype.selectBank = function (ev) {
        console.log(ev.target.value, this.bankNames);
        var abc = ev.target.value;
        this.filterData = this.bankNames.filter(function (x) { return (x.Value).toUpperCase().includes(abc.toUpperCase()); });
        console.log(this.filterData);
    };
    PaymentModePage.prototype.getBranchName = function () {
        var _this = this;
        this.cs.postWithParams('/api/Agent/GetBranchLocations', '').then(function (res) {
            _this.branchNames = res.BranchDetail;
            console.log(res);
        }).catch(function (err) {
            console.log(err.error.text);
        });
    };
    PaymentModePage.prototype.selectBranch = function (ev) {
        console.log(ev.target.value, this.branchNames);
        var abc = ev.target.value;
        this.filterBranchData = this.branchNames.filter(function (x) { return (x.Value).toUpperCase().includes(abc.toUpperCase()); });
        console.log("Filetr Data", this.filterBranchData);
    };
    PaymentModePage.prototype.selectedBankList = function (data) {
        console.log(data);
        this.bankId = data.Key;
        this.chequeForm.patchValue({ 'bankName': data.Value });
        this.dualCheckForm.patchValue({ 'bankName': data.Value });
        localStorage.setItem('chequeBank', JSON.stringify(data.Value));
        this.filterData = [];
    };
    PaymentModePage.prototype.selectedBranchList = function (data) {
        console.log(data);
        this.branchId = data.Key;
        console.log(this.branchId);
        this.chequeForm.patchValue({ 'branchName': data.Value });
        this.dualCheckForm.patchValue({ 'branchName': data.Value });
        this.filterBranchData = [];
    };
    PaymentModePage.prototype.selectPID = function (ev) {
        var data = this.PIDData.find(function (x) { return x.PaymentID == ev.target.value; });
        this.filterPID = this.PIDData.filter(function (x) { return x.PaymentID !== ev.target.value; });
        this.paymentModePID = data.PaymentMode;
        if (this.paymentMode == 'dualCheque') {
            this.totalAmount = this.totalAmount;
        }
        else {
            this.totalAmount = 0;
        }
        if (this.paymentModePID !== 'ChequeDemandDraft') {
            if (this.paymentMode == 'dualCheque') {
                this.totalAmount = this.totalAmount;
            }
            else {
                this.totalAmount = 0;
            }
            console.log("Netbanking", this.totalAmount);
            this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
            console.log("Netbanking 2", this.totalAmount);
            this.PIDForm.patchValue({ 'PID2': '' });
            this.PIDForm.patchValue({ 'PID3': '' });
            this.PIDForm.patchValue({ 'PID4': '' });
            this.PIDForm.patchValue({ 'PID5': '' });
            if (this.travelProp.SavePolicy[0].TotalPremium < this.totalAmount) {
                this.paymentDone = true;
                this.PIDArray.push(data.PaymentID);
                this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
            }
            else {
                this.paymentDone = true;
                this.showToast('Kindly select amount greater then premium amount');
                this.PIDArray.push(data.PaymentID);
            }
        }
        else {
            if (this.paymentMode == 'dualCheque') {
                this.totalAmount = this.totalAmount;
            }
            else {
                this.totalAmount = 0;
            }
            console.log("Cheque", this.totalAmount);
            this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
            console.log("Final Cheque", this.totalAmount);
            this.PIDForm.patchValue({ 'PID2': '' });
            this.PIDForm.patchValue({ 'PID3': '' });
            this.PIDForm.patchValue({ 'PID4': '' });
            this.PIDForm.patchValue({ 'PID5': '' });
            if (this.travelProp.SavePolicy[0].TotalPremium < this.totalAmount) {
                this.paymentDone = true;
                this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
                this.PIDArray.push(data.PaymentID);
            }
            else {
                this.paymentDone = false;
                this.showToast('Kindly select amount greater then premium amount');
                this.PIDArray.push(data.PaymentID);
            }
        }
    };
    PaymentModePage.prototype.selectPID1 = function (ev) {
        console.log(ev.target.value);
        var data = this.filterPID.find(function (x) { return x.PaymentID == ev.target.value; });
        console.log(data);
        this.filterPID1 = this.filterPID.filter(function (x) { return x.PaymentID !== ev.target.value; });
        console.log("Filter PID", this.filterPID);
        this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
        console.log(this.totalAmount);
        if (this.travelProp.SavePolicy[0].TotalPremium < this.totalAmount && this.paymentModePID === 'ChequeDemandDraft') {
            console.log("Payment Done");
            this.paymentDone = true;
            this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
            this.PIDArray.push(data.PaymentID);
        }
        else {
            console.log("Payment Not Done");
            this.paymentDone = false;
            this.showToast('Kindly select amount greater then premium amount');
            this.PIDArray.push(data.PaymentID);
        }
    };
    PaymentModePage.prototype.selectPID2 = function (ev) {
        console.log(ev.target.value);
        var data = this.filterPID1.find(function (x) { return x.PaymentID == ev.target.value; });
        console.log(data);
        this.filterPID2 = this.filterPID1.filter(function (x) { return x.PaymentID !== ev.target.value; });
        console.log("Filter PID", this.filterPID1);
        this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
        console.log(this.totalAmount);
        if (this.travelProp.SavePolicy[0].TotalPremium < this.totalAmount && this.paymentModePID === 'ChequeDemandDraft') {
            console.log("Payment Done");
            this.paymentDone = true;
            this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
            this.PIDArray.push(data.PaymentID);
        }
        else {
            console.log("Payment Not Done");
            this.paymentDone = false;
            this.showToast('Kindly select amount greater then premium amount');
            this.PIDArray.push(data.PaymentID);
        }
    };
    PaymentModePage.prototype.selectPID3 = function (ev) {
        console.log(ev.target.value);
        var data = this.filterPID2.find(function (x) { return x.PaymentID == ev.target.value; });
        console.log(data);
        this.filterPID3 = this.filterPID2.filter(function (x) { return x.PaymentID !== ev.target.value; });
        console.log("Filter PID", this.filterPID3);
        this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
        console.log(this.totalAmount);
        if (this.travelProp.SavePolicy[0].TotalPremium < this.totalAmount && this.paymentModePID === 'ChequeDemandDraft') {
            console.log("Payment Done");
            this.paymentDone = true;
            this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
            this.PIDArray.push(data.PaymentID);
        }
        else {
            console.log("Payment Not Done");
            this.paymentDone = false;
            this.showToast('Kindly select amount greater then premium amount');
            this.PIDArray.push(data.PaymentID);
        }
    };
    PaymentModePage.prototype.selectPID4 = function (ev) {
        console.log(ev.target.value);
        var data = this.filterPID3.find(function (x) { return x.PaymentID == ev.target.value; });
        console.log(data);
        this.filterPID4 = this.filterPID3.filter(function (x) { return x.PaymentID !== ev.target.value; });
        console.log("Filter PID", this.filterPID4);
        this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
        console.log(this.totalAmount);
        if (this.travelProp.SavePolicy[0].TotalPremium < this.totalAmount && this.paymentModePID === 'ChequeDemandDraft') {
            console.log("Payment Done");
            this.paymentDone = true;
            this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
            this.PIDArray.push(data.PaymentID);
        }
        else {
            console.log("Payment Not Done");
            this.paymentDone = false;
            this.showToast('Kindly select amount greater then premium amount');
            this.PIDArray.push(data.PaymentID);
        }
    };
    PaymentModePage.prototype.makePay = function (form) {
        var _this = this;
        console.log(this.paymentMode);
        if (this.isNewPID) {
            this.transType = 'PID';
            this.makePayment(form).then(function () {
                _this.paymentConfirmation();
            });
        }
        else {
            if (this.paymentMode == 'PID') {
                var PFPaymentID = this.PIDArray.join(':');
                this.PFPaymentID = PFPaymentID;
                this.paymentId = '0';
                this.paymentConfirmation();
            }
            else if (this.paymentMode == 'dualCheque') {
                console.log("DUal Cheque");
                var PFPaymentID = this.PIDArray.join(':');
                this.PFPaymentID = PFPaymentID;
                this.paymentId = this.paymentRes.PaymentID;
                this.paymentConfirmation();
            }
            else if (this.paymentMode == 'paymentLink') {
                console.log("Payment Link");
                this.makePayment(form).then(function () {
                    _this.payByLink();
                });
            }
            else {
                this.makePayment(form).then(function () {
                    _this.PFPaymentID = _this.paymentRes.PFPaymentID;
                    _this.paymentId = _this.paymentRes.PaymentID;
                    _this.paymentConfirmation();
                });
            }
        }
    };
    PaymentModePage.prototype.validateCheque = function (form) {
        if (form.value.chequeNo.length == 6) {
            if (form.value.chequeDate !== null && form.value.chequeAmount !== null && form.value.branchName !== null && form.value.bankName !== null) {
                console.log("Valid");
                this.transType = "PID";
                this.makePayment(form);
            }
            else {
                console.log("inValid");
            }
        }
        else {
            console.log("inValid cheque");
        }
    };
    PaymentModePage.prototype.showPaymentLink = function (data) {
        if (data == 'email') {
            this.showMobile = false;
            this.showEmail = true;
        }
        else {
            this.showEmail = false;
            this.showMobile = true;
        }
    };
    PaymentModePage.prototype.payByLink = function () {
        var _this = this;
        if (this.showEmail) {
            console.log(this.paymentLinkEmail);
            this.payLinkBody = {
                "EmailID": this.paymentLinkEmail,
                "PolicyID": this.travelProp.SavePolicy[0].PolicyID,
                "isRazorPay": true
            };
            var str = JSON.stringify(this.payLinkBody);
            this.cs.showLoader = true;
            this.cs.postWithParams('/api/payment/SendCustomerPaymentLink', str).then(function (res) {
                console.log(res);
                _this.linkPayment = true;
                _this.cs.showLoader = false;
                var msg = "Payment link successfully sent to " + _this.paymentLinkEmail + "\nNote: Payment will be completed on the customer portal";
                _this.presentToast(msg);
                // document.getElementById("myNav6").style.height = "100%";  
            }).catch(function (err) {
                console.log(err);
                _this.cs.showLoader = false;
                // document.getElementById("myNav6").style.height = "0%";  
            });
        }
        else {
            this.payLinkBody = {
                "PolicyID": this.travelProp.SavePolicy[0].PolicyID,
                "MobileNo": this.paymentLinkMobile,
                "IsRazorPay": true
            };
            var str = JSON.stringify(this.payLinkBody);
            this.cs.showLoader = true;
            this.cs.postWithParams('/api/payment/SendCustomerPaymentLinkSMS', str).then(function (res) {
                console.log(res);
                _this.linkPayment = true;
                _this.cs.showLoader = false;
                var msg = "Payment link successfully sent to " + _this.paymentLinkMobile + "\nNote: Payment will be completed on the customer portal";
                _this.presentToast(msg);
                // document.getElementById("myNav6").style.height = "100%";  
            }).catch(function (err) {
                console.log(err);
                _this.cs.showLoader = false;
                // document.getElementById("myNav6").style.height = "0%";  
            });
            console.log(this.paymentLinkMobile);
        }
    };
    PaymentModePage.prototype.presentToast = function (msg) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        this.cs.goToHome();
                        return [2 /*return*/];
                }
            });
        });
    };
    PaymentModePage.prototype.makePayment = function (form) {
        var _this = this;
        return new Promise(function (resolve) {
            if (_this.isNewPID) {
                _this.commonPayRequest = {
                    "TransType": _this.transType,
                    "PaymentAmount": _this.newPID.pidAmount,
                    "PidDetails": {
                        "PolicyType": _this.newPID.policyType,
                        "PF_CustomerId": "",
                        "iPartnerCustomerId": _this.newPID.CustomerId
                    },
                    "GatewayReturnURL": config.baseURL + "/PaymentGateway/PGIPaymentProcess",
                    "PolicyIDs": "",
                    "PayerType": _this.newPID.PayerType,
                    "ModeID": 0,
                    "UserRole": "AGENT",
                    "IPAddress": config.ipAddress,
                    "PaymentMode": "ChequeDemandDraft",
                    "ChequeDetails": {
                        "MICR": "MICR",
                        "InstrumentNo": form.value.chequeNo,
                        "InstrumentDate": moment(form.value.chequeDate).format('DD-MMM-YYYY'),
                        "BankID": _this.bankId,
                        "BankBranch": form.value.branchName,
                        "PaymentMode": 1,
                        "BranchDepositedLocation": _this.branchId,
                        "ChequeDDType": "3",
                        "IsDepositAtBankBranch": "N"
                    }
                };
            }
            else {
                if (_this.paymentMode == 'dualCheque') {
                    _this.transType = "PID";
                    _this.commonPayRequest = {
                        "PaymentMode": "ChequeDemandDraft",
                        "ChequeDetails": {
                            "MICR": "MICR",
                            "InstrumentNo": form.value.chequeNo,
                            "InstrumentDate": moment(form.value.chequeDate).format('DD-MMM-YYYY'),
                            "BankID": _this.bankId,
                            "BankBranch": form.value.branchName,
                            "PaymentMode": 1,
                            "BranchDepositedLocation": _this.branchId,
                            "ChequeDDType": "3",
                            "IsDepositAtBankBranch": "N"
                        },
                        "TransType": _this.transType,
                        "PaymentAmount": form.value.chequeAmount,
                        "PidDetails": {
                            "PolicyType": _this.planDetails.Product,
                            "PF_CustomerId": _this.travelProp.SavePolicy[0].PF_CustomerID,
                            "iPartnerCustomerId": ""
                        },
                        "GatewayReturnURL": config.baseURL + "/PaymentGateway/PGIPaymentProcess",
                        "PolicyIDs": _this.travelProp.SavePolicy[0].PolicyID,
                        "PayerType": _this.custType,
                        "ModeID": 0,
                        "UserRole": "AGENT",
                        "IPAddress": config.ipAddress
                    };
                }
                if (_this.paymentMode == 'paymentLink') {
                    _this.commonPayRequest = {
                        "TransType": "POLICY_PAYMENT",
                        "GatewayReturnURL": "",
                        "PolicyIDs": _this.travelProp.SavePolicy[0].PolicyID,
                        "PayerType": _this.custType,
                        "ModeID": 0,
                        "UserRole": "AGENT",
                        "IPAddress": config.ipAddress,
                        "PaymentMode": "CUSTOMERPAYMENTLINK",
                        "PaymentAmount": ""
                    };
                }
                else {
                    _this.commonPayRequest = {
                        "TransType": _this.transType,
                        "GatewayReturnURL": config.baseURL + "/PaymentGateway/PGIPaymentProcess",
                        "PolicyIDs": _this.travelProp.SavePolicy[0].PolicyID,
                        "PayerType": _this.custType,
                        "ModeID": 0,
                        "UserRole": "AGENT",
                        "IPAddress": config.ipAddress,
                        "PaymentMode": "ChequeDemandDraft",
                        "ChequeDetails": {
                            "MICR": "MICR",
                            "InstrumentNo": form.value.chequeNo,
                            "InstrumentDate": moment(form.value.chequeDate).format('DD-MMM-YYYY'),
                            "BankID": _this.bankId,
                            "BankBranch": form.value.branchName,
                            "PaymentMode": 1,
                            "BranchDepositedLocation": _this.branchId,
                            "ChequeDDType": "3",
                            "IsDepositAtBankBranch": "N"
                        }
                    };
                }
            }
            var str = JSON.stringify(_this.commonPayRequest);
            localStorage.setItem('paymentModeReq', str);
            console.log(str);
            _this.cs.showLoader = true;
            _this.cs.postWithParams('/api/Payment/CommonPayment', str).then(function (res) {
                console.log(res);
                _this.paymentRes = res;
                if (_this.paymentMode == 'dualCheque') {
                    _this.totalAmount = form.value.chequeAmount;
                    _this.PIDArray.push(_this.paymentRes.PFPaymentID);
                }
                localStorage.setItem('commonPayRes', JSON.stringify(_this.paymentRes));
                _this.cs.showLoader = false;
                resolve();
            }).catch(function (err) {
                console.log("Error", err);
                _this.cs.showLoader = false;
                _this.totalAmount = form.value.chequeAmount;
                resolve();
            });
        });
    };
    PaymentModePage.prototype.paymentConfirmation = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var body = {
                "MultiPFPaymentID": _this.PFPaymentID,
                "PolicyIds": _this.travelProp.SavePolicy[0].PolicyID,
                "PaymentID": _this.paymentId,
                "TransType": "POLICY_PAYMENT"
            };
            var str = JSON.stringify(body);
            console.log(str, _this.paymentMode);
            _this.cs.showLoader = true;
            _this.cs.postWithParams('/api/Payment/PolicyPaymentTagging', str).then(function (res) {
                console.log(res);
                localStorage.setItem('paymentConfRes', JSON.stringify(res));
                _this.paymentRes = res;
                if (_this.paymentMode == 'Cheque') {
                    _this.router.navigateByUrl('cheque-confirmation');
                }
                else {
                    _this.router.navigateByUrl('payment-confirmation');
                }
                _this.cs.showLoader = false;
                resolve();
            }).catch(function (err) {
                console.log("Error", err);
                _this.cs.showLoader = false;
                resolve();
            });
            resolve();
        });
    };
    PaymentModePage.prototype.cancelPay = function (ev) {
        this.router.navigateByUrl('payment');
    };
    PaymentModePage.prototype.showToast = function (msg) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: 5000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PaymentModePage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    PaymentModePage = tslib_1.__decorate([
        Component({
            selector: 'app-payment-mode',
            templateUrl: './payment-mode.page.html',
            styleUrls: ['./payment-mode.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, ToastController, Router])
    ], PaymentModePage);
    return PaymentModePage;
}());
export { PaymentModePage };
//# sourceMappingURL=payment-mode.page.js.map