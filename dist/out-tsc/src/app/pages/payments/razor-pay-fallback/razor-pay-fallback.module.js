import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RazorPayFallbackPage } from './razor-pay-fallback.page';
var routes = [
    {
        path: '',
        component: RazorPayFallbackPage
    }
];
var RazorPayFallbackPageModule = /** @class */ (function () {
    function RazorPayFallbackPageModule() {
    }
    RazorPayFallbackPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [RazorPayFallbackPage]
        })
    ], RazorPayFallbackPageModule);
    return RazorPayFallbackPageModule;
}());
export { RazorPayFallbackPageModule };
//# sourceMappingURL=razor-pay-fallback.module.js.map