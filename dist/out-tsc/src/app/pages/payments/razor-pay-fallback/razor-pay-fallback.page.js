import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var RazorPayFallbackPage = /** @class */ (function () {
    function RazorPayFallbackPage() {
    }
    RazorPayFallbackPage.prototype.ngOnInit = function () {
        //   window.addEventListener('message', function(event) {
        //     if (typeof event.data == 'object' && event.data.razorPayDetails) {
        //       console.log(event);
        //       // this.post1(event.data.url, event.data.razorPayDetails);
        //       let method = "post"; // Set method to post by default if not specified.
        //       var form = document.createElement("form");
        //       form.setAttribute("method", method);
        //       form.setAttribute("action", event.data.url);
        //       //form.setAttribute("target", "iFrame123");
        //       for(var key in event.data.razorPayDetails) {
        //           if(event.data.razorPayDetails.hasOwnProperty(key)) {
        //               var hiddenField = document.createElement("input");
        //               hiddenField.setAttribute("type", "hidden");
        //               hiddenField.setAttribute("name", key);
        //               hiddenField.setAttribute("value", event.data.razorPayDetails[key]);
        //               form.appendChild(hiddenField);
        //           }
        //       }
        //       document.body.appendChild(form);
        //       form.submit();
        //     }
        // }, false);
        window.addEventListener('message', function (a) {
            console.log("Fallback", a);
        }, false);
    };
    RazorPayFallbackPage = tslib_1.__decorate([
        Component({
            selector: 'app-razor-pay-fallback',
            templateUrl: './razor-pay-fallback.page.html',
            styleUrls: ['./razor-pay-fallback.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], RazorPayFallbackPage);
    return RazorPayFallbackPage;
}());
export { RazorPayFallbackPage };
//# sourceMappingURL=razor-pay-fallback.page.js.map