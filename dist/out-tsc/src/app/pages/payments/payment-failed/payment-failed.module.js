import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PaymentFailedPage } from './payment-failed.page';
var routes = [
    {
        path: '',
        component: PaymentFailedPage
    }
];
var PaymentFailedPageModule = /** @class */ (function () {
    function PaymentFailedPageModule() {
    }
    PaymentFailedPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PaymentFailedPage]
        })
    ], PaymentFailedPageModule);
    return PaymentFailedPageModule;
}());
export { PaymentFailedPageModule };
//# sourceMappingURL=payment-failed.module.js.map