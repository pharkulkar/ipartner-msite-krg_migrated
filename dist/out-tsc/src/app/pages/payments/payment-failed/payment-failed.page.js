import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var PaymentFailedPage = /** @class */ (function () {
    function PaymentFailedPage() {
    }
    PaymentFailedPage.prototype.ngOnInit = function () {
    };
    PaymentFailedPage = tslib_1.__decorate([
        Component({
            selector: 'app-payment-failed',
            templateUrl: './payment-failed.page.html',
            styleUrls: ['./payment-failed.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], PaymentFailedPage);
    return PaymentFailedPage;
}());
export { PaymentFailedPage };
//# sourceMappingURL=payment-failed.page.js.map