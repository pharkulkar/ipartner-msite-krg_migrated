import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
import { ToastController } from '@ionic/angular';
var ChequeConfirmationPage = /** @class */ (function () {
    function ChequeConfirmationPage(cs, toastCtrl) {
        this.cs = cs;
        this.toastCtrl = toastCtrl;
        this.chequeDetails = false;
    }
    ChequeConfirmationPage.prototype.ngOnInit = function () {
        var _this = this;
        this.getData().then(function () {
            _this.createChequeForm();
        });
    };
    ChequeConfirmationPage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
            _this.quoteResponse = JSON.parse(localStorage.getItem('quoteResponse'));
            _this.travelPlan = JSON.parse(localStorage.getItem('travelPlan'));
            _this.proposalResponse = JSON.parse(localStorage.getItem('proposalResponse'));
            _this.proposalRequest = JSON.parse(localStorage.getItem('proposalRequest'));
            _this.customerRequest = JSON.parse(localStorage.getItem('customerRequest'));
            _this.paymentModeReq = JSON.parse(localStorage.getItem('paymentModeReq'));
            _this.commonPayRes = JSON.parse(localStorage.getItem('commonPayRes'));
            _this.paymentConfRes = JSON.parse(localStorage.getItem('paymentConfRes'));
            _this.chequeBankName = JSON.parse(localStorage.getItem('chequeBank'));
            console.log("IMP", _this.quoteRequest);
            console.log(_this.quoteResponse);
            console.log(_this.proposalResponse);
            console.log(_this.travelPlan);
            console.log(_this.proposalRequest);
            console.log(_this.customerRequest);
            console.log(_this.paymentModeReq);
            console.log(_this.commonPayRes);
            console.log(_this.paymentConfRes);
            resolve();
        });
    };
    ChequeConfirmationPage.prototype.createChequeForm = function () {
        this.chequeConfForm = new FormGroup({
            proposalNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].ProposalNumber),
            pfProposalNumber: new FormControl(this.proposalResponse.SavePolicy[0].PF_ProsalNumber),
            chequeNumber: new FormControl(this.paymentModeReq.ChequeDetails.InstrumentNo),
            bank: new FormControl(this.chequeBankName),
            chequeDate: new FormControl(moment(this.paymentModeReq.ChequeDetails.InstrumentDate).format('YYYY-MM-DD')),
            chequeAmount: new FormControl(this.proposalResponse.SavePolicy[0].TotalPremium)
        });
    };
    ChequeConfirmationPage.prototype.showChequeDetails = function (ev) {
        this.chequeDetails = !this.chequeDetails;
        if (this.chequeDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("details").style.background = "url(" + imgUrl + ")";
            document.getElementById("details").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("details").style.background = "url(" + imgUrl + ")";
            document.getElementById("details").style.backgroundRepeat = 'no-repeat';
        }
    };
    ChequeConfirmationPage.prototype.downloadPdf = function (ev) {
        // let downloadURL = this.cs.pdfDownload('PAYINSLIP',this.paymentConfRes.EPFPaymentID);
        var downloadURL = 'http://cldiliptrapp03.cloudapp.net:9006/mobileapi/api/file/DownloadPDF?type=PAYINSLIP&value=JY9RJLS%2fY6S7UeFJOWxaNQ%3d%3d';
        this.cs.save(downloadURL, this.paymentConfRes.EPFPaymentID + ".pdf");
        // window.open(downloadURL, "_blank");
        this.presentToast('Your pdf will be downloaded');
    };
    ChequeConfirmationPage.prototype.presentToast = function (msg) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: 3000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ChequeConfirmationPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    ChequeConfirmationPage = tslib_1.__decorate([
        Component({
            selector: 'app-cheque-confirmation',
            templateUrl: './cheque-confirmation.page.html',
            styleUrls: ['./cheque-confirmation.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, ToastController])
    ], ChequeConfirmationPage);
    return ChequeConfirmationPage;
}());
export { ChequeConfirmationPage };
//# sourceMappingURL=cheque-confirmation.page.js.map