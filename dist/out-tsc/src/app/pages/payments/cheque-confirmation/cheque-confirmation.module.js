import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ChequeConfirmationPage } from './cheque-confirmation.page';
var routes = [
    {
        path: '',
        component: ChequeConfirmationPage
    }
];
var ChequeConfirmationPageModule = /** @class */ (function () {
    function ChequeConfirmationPageModule() {
    }
    ChequeConfirmationPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ChequeConfirmationPage]
        })
    ], ChequeConfirmationPageModule);
    return ChequeConfirmationPageModule;
}());
export { ChequeConfirmationPageModule };
//# sourceMappingURL=cheque-confirmation.module.js.map