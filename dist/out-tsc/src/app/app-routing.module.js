import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';
var routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
    { path: 'health', loadChildren: './pages/health/cal-health-premium/cal-health-premium.module#CalHealthPremiumPageModule' },
    { path: 'travel', loadChildren: './pages/travel/cal-travel-premium/cal-travel-premium.module#CalTravelPremiumPageModule' },
    { path: 'dash-board', loadChildren: './pages/dash-board/dash-board.module#DashBoardPageModule' },
    { path: 'health-proposal', loadChildren: './pages/health/health-proposal/health-proposal.module#HealthProposalPageModule' },
    { path: 'premium', loadChildren: './pages/health/premium/premium.module#PremiumPageModule' },
    { path: 'health-summary', loadChildren: './pages/health/health-summary/health-summary.module#HealthSummaryPageModule' },
    { path: 'swap', loadChildren: './pages/swap/swap.module#SwapPageModule' },
    { path: 'travel-quote', loadChildren: './pages/travel/travel-quote/travel-quote.module#TravelQuotePageModule' },
    { path: 'travel-premium', loadChildren: './pages/travel/travel-premium/travel-premium.module#TravelPremiumPageModule' },
    { path: 'travel-summary', loadChildren: './pages/travel/travel-summary/travel-summary.module#TravelSummaryPageModule' },
    { path: 'travel-proposal', loadChildren: './pages/travel/travel-proposal/travel-proposal.module#TravelProposalPageModule' },
    { path: 'payment', loadChildren: './pages/payments/payment/payment.module#PaymentPageModule' },
    // { path: 'payment-health', loadChildren: './pages/payments/payment-health/payment-health.module#PaymentHealthPageModule' },
    // { path: 'payment-motor', loadChildren: './pages/payments/payment-motor/payment-motor.module#PaymentMotorPageModule' },
    { path: 'payment-mode', loadChildren: './pages/payments/payment-mode/payment-mode.module#PaymentModePageModule' },
    // { path: 'travel-pay-confirmation', loadChildren: './pages/payments/travel-pay-confirmation/travel-pay-confirmation.module#TravelPayConfirmationPageModule' },
    // { path: 'health-pay-confirmation', loadChildren: './pages/payments/health-pay-confirmation/health-pay-confirmation.module#HealthPayConfirmationPageModule' },
    { path: 'payment-failed', loadChildren: './pages/payments/payment-failed/payment-failed.module#PaymentFailedPageModule' },
    { path: 'cheque-confirmation', loadChildren: './pages/payments/cheque-confirmation/cheque-confirmation.module#ChequeConfirmationPageModule' },
    { path: 'payment-confirmation', loadChildren: './pages/payments/payment-confirmation/payment-confirmation.module#PaymentConfirmationPageModule' },
    { path: 'health-renewal', loadChildren: './pages/renew/health/health-renewal/health-renewal.module#HealthRenewalPageModule' },
    { path: 'health-renewal-summary', loadChildren: './pages/renew/health/health-renewal-summary/health-renewal-summary.module#HealthRenewalSummaryPageModule' },
    { path: 'health-renewal-proposal', loadChildren: './pages/renew/health/health-renewal-proposal/health-renewal-proposal.module#HealthRenewalProposalPageModule' },
    { path: 'pid', loadChildren: './pages/pid/pid.module#PIDPageModule' },
    { path: 'razor-pop-up', loadChildren: './pages/payments/razor-pop-up/razor-pop-up.module#RazorPopUpPageModule' },
    { path: 'razor-pay-fallback', loadChildren: './pages/payments/razor-pay-fallback/razor-pay-fallback.module#RazorPayFallbackPageModule' },
    { path: 'pay-in-slips', loadChildren: './pages/pay-in-slips/pay-in-slips.module#PayInSlipsPageModule' },
    { path: 'health-renewal-premium', loadChildren: './pages/renew/health/health-renewal-premium/health-renewal-premium.module#HealthRenewalPremiumPageModule' },
    { path: 'savequotes', loadChildren: './pages/savequotes/savequotes.module#SavequotesPageModule' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forRoot(routes, { useHash: true, preloadingStrategy: PreloadAllModules })
            ],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map